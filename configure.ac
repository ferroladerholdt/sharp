#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.
#
# Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
#

AC_PREREQ([2.63])
AC_INIT([SharP], [0.1], [aderholdtwf1@ornl.gov])
AC_CONFIG_HEADERS([include/config.h include/sharp-config.h])
AC_CONFIG_MACRO_DIR([m4])

AC_GNU_SOURCE

m4_include([m4/compiler.m4])
m4_include([m4/ucs.m4])

AM_INIT_AUTOMAKE([1.9 foreign subdir-objects])

### force hwloc to be embedded as we don't need docs or utils
ac_configure_args="$ac_configure_args --enable-embedded-mode --disable-netloc"

# Checks for programs.
AC_PROG_CC
AC_PROG_LIBTOOL

# Checks for UCS/UCM
AC_PROG_CXX
AC_PROG_SED
AM_PROG_AS
AC_PROG_INSTALL
AC_HEADER_STDC
LT_LIB_M

AC_CONFIG_SUBDIRS([external/hwloc])
AC_CONFIG_SUBDIRS([external/memkind])

# Check for a checked out version of hwloc (NOTE: This also covers uthash)
AC_CHECK_FILES(
    [./external/hwloc/configure.ac], 
    [AC_SUBST([has_hwloc],["yes"])], 
    [AC_SUBST([has_hwloc],["no"])]
)

# Check for memkind 
AC_CHECK_FILES(
    [./external/memkind/configure.ac], 
    [AC_SUBST([has_memkind],["yes"])], 
    [AC_SUBST([has_memkind],["no"])]
)
# Check for pmem
AC_CHECK_FILES(
    [./external/nvml/Makefile], 
    [AC_SUBST(has_nvml,["yes"])], 
    [AC_SUBST(has_nvml, ["no"])]
)

if test "$has_hwloc" = "no"; then
AC_CONFIG_COMMANDS(
    [hwloc-init],
    [cd external/hwloc; git checkout 8ab198775d83393c49cd20de2ddff396c7df98a4; ./autogen.sh; cd ../../], 
    [AC_MSG_NOTICE(["Continuing..."])]
)
fi

if test "$has_memkind" = "no"; then
AC_CONFIG_COMMANDS(
    [memkind-init],
    [cd external/memkind; git checkout a7f6ed76bb8cab133ddf4deed44874bd43828b6a; ./autogen.sh; cd ../../], 
    [AC_MSG_NOTICE(["done"])]
)
fi

if test "$has_nvml" = "no"; then
AC_CONFIG_COMMANDS(
    [nvml-init], 
    [cd external/nvml; git checkout 69a2d496989926058e670f2efd45ca01d97dae59; cd ../../], 
    [AC_MSG_NOTICE(["Checked out pmem commit 69a2d496989926058e670f2efd45ca01d97dae59"])]
)
fi

# Check for built jemalloc
AC_CHECK_FILES(
    [./external/memkind/jemalloc/obj/lib/libjemalloc.a], 
    [AC_SUBST(has_jemk,["yes"])], 
    [AC_SUBST(has_jemk,["no"])]
)

if test "$has_jemk" = "no"; then
dnl The next line will force the jemalloc used by memkind to be 
dnl configured without inheriting the configure options given to 
dnl SharP (i.e., CFLAGS are changed). This allows jemalloc to be 
dnl correctly configured and built
AC_CONFIG_COMMANDS(
    [jemk-init], 
    [cd external/memkind; sed -i "30s/\(.*\)/\1 --cache-file=config.cache/g" build_jemalloc.sh; CFLAGS="-std=gnu99 -Wall -pipe -g3 -fvisibility=hidden -O3 -funroll-loops" ./build_jemalloc.sh; cd ../../], 
    [AC_MSG_NOTICE(["done"])]
)
fi

AC_ARG_ENABLE(cuda,
     [AS_HELP_STRING(
        [--enable-cuda],
        [enable cuda usage (Default: disabled)]
    )],
    [with_cuda=yes],
    [with_cuda=no])    

AC_ARG_ENABLE(comm,
    [AS_HELP_STRING(
        [--disable-comm],
        [disable communication layer (Default: enabled)]
    )],
    [with_comm=no],
    [with_comm=yes])

AC_ARG_WITH(rte,
    [AS_HELP_STRING(
        [--with-rte],
        [enable Comm layer usage of librte (Default: disabled)]
    )],
    [],
    [with_rte=no])    

AC_ARG_WITH([comm_mpi],
    [AS_HELP_STRING(
        [--with-comm-mpi], 
        [enable the MPI comm layer (Default: disabled)]
    )],
    [],
    [with_comm_mpi=no])

AC_ARG_WITH([comm_shmem],
    [AS_HELP_STRING(
        [--with-comm-shmem], 
        [enable the SHMEM comm layer (Default: disabled)]
    )],
    [],
    [with_comm_shmem=no])

AC_ARG_WITH([comm_ucx],
    [AS_HELP_STRING(
        [--with-comm-ucx], 
        [enable the UCX comm layer (Default: disabled)]
    )],
    [],
    [with_comm_ucx=no])

AC_ARG_WITH([external_ucx],
    [AS_HELP_STRING(
        [--with-external-ucx], 
        [make use of a different version of UCX (Default: disabled)]
    )],
    [],
    [with_external_ucx=no])

AM_CONDITIONAL(
    [INTERNAL_UCS], 
    [test x$with_comm_ucx = xno && test x$with_external_ucx = xno])

comm_count=""
ucx_present=""
mpi_present=""
shmem_present=""

AS_IF(
    [test "x$with_comm" != xno && test "x$with_comm_mpi" != xno],
    [AS_IF(
        [test -d $with_comm_mpi],
        [P_CFLAGS=$CFLAGS
         CFLAGS="$CFLAGS -I$with_comm_mpi/include"
         AC_CHECK_HEADER(
            [$with_comm_mpi/include/mpi.h],
            [AC_SUBST(MPI_CFLAGS, "-I$with_comm_mpi/include")
             AC_SUBST(MPI_LDFLAGS, "-L$with_comm_mpi/lib -lmpi")
             AC_DEFINE(COMM_MPI, 1, [MPI Comm layer])
             comm_count="x$comm_count"
             mpi_present="yes"],
            [mpi_present="no"]
        )
         CFLAGS=$P_CFLAGS
        ],
        [AC_SEARCH_LIBS(
            [MPI_Init], 
            [mpi], 
            [AC_DEFINE(COMM_MPI, 1, [MPI Comm layer])
             mpi_present="yes"
             comm_count="x$comm_count"], 
            [AC_MSG_ERROR(
                [unable to find the MPI library]
            )]
        )]
    )],
    []
)


AS_IF(
    [test "x$with_comm" != xno && test "x$with_comm_shmem" != xno],
    [AS_IF(
        [test -d $with_comm_shmem],
        [P_CFLAGS=$CFLAGS
         CFLAGS="$CFLAGS -I$with_comm_shmem/include"
         AC_CHECK_HEADER(
            [$with_comm_shmem/include/shmem.h],
            [AC_SUBST(SHMEM_CFLAGS, "-I$with_comm_shmem/include")
             AC_SUBST(SHMEM_LDFLAGS, "-L$with_comm_shmem/lib -loshmem")
             AC_DEFINE(COMM_SHMEM, 1, [SHMEM comm layer])
             comm_count="x$comm_count"
             shmem_present="yes"],
            [shmem_present="no"]
        )
         CFLAGS=$P_CFLAGS
        ],
        [AC_SEARCH_LIBS(
            [shmem_init], 
            [openshmem], 
            [AC_DEFINE(COMM_SHMEM, 1, [SHMEM Comm layer])
             shmem_present="yes"
             comm_count="x$comm_count"], 
            [AC_MSG_ERROR(
                [unable to find -lopenshmem]
            )]
        )]
    )],
    []
)

AS_IF(
    [test "x$with_comm" != xno && test "x$with_comm_ucx" != xno],
    [AS_IF(
        [test -d $with_comm_ucx],
        [P_CFLAGS=$CFLAGS
         CFLAGS="$CFLAGS -I$with_comm_ucx/include"
         AC_CHECK_HEADER([$with_comm_ucx/include/ucp/api/ucp.h],
            [AC_SUBST(UCX_CFLAGS, "-I$with_comm_ucx/include")
             AC_SUBST(UCX_LDFLAGS, "-L$with_comm_ucx/lib -lucp -luct -lucs -lucm")
             AC_DEFINE(COMM_UCX, 1, [UCX Comm layer])
             comm_count="x$comm_count"
             ucx_present="yes"],
            [ucx_present="no"]
        )
         CFLAGS=$P_CFLAGS
        ],              
        [AC_SEARCH_LIBS(
            [ucp_init_version],
            [ucp],
            [AC_DEFINE(COMM_UCX, 1, [UCX Comm layer])
             comm_count="x$comm_count"
             ucx_present="yes"],
            [AC_MSG_ERROR(
                [unable to find the UCX library]
            )]
        )]
    )],
    []
)

AS_IF(
    [test "x$with_external_ucx" != xno], 
    [AS_IF(
        [test "x$ucx_present" = x], 
        [AS_IF(
            [test -d $with_external_ucx],
            [P_CFLAGS=$CFLAGS
             CFLAGS="$CFLAGS -I$with_external_ucx/include"
             AC_CHECK_HEADER(
                [$with_external_ucx/include/ucp/api/ucp.h],
                [AC_SUBST(UCX_CFLAGS, "-I$with_external_ucx/include")
                 AC_SUBST(UCX_LDFLAGS, "-L$with_external_ucx/lib -lucs -lucm")
                 ucx_present="yes"],
                [ucx_present="no"]
            )
             CFLAGS=$P_CFLAGS
            ], 
            [AC_SEARCH_LIBS(
                [ucp_init_version],
                [ucp],
                [ucx_present="yes"],
                [AC_MSG_ERROR(
                    [unable to find UCX library]
                )]
            )]
        )],
        []
    )],
    []
)

AS_IF(
    [test "x$with_comm" != xno && test "x$with_rte" != xno],
    [AS_IF(
        [test -d $with_rte],
        [AC_CHECK_HEADER(
            [$with_rte/include/rte.h],
            [AC_SUBST(RTE_CFLAGS, "-I$with_rte/include")
             AC_SUBST(RTE_LDFLAGS, "-L$with_rte/lib -lrte")
             AC_DEFINE(WITH_RTE, 1, [RTE])
             rte_present="yes"],
            [rte_present="no"]
        )],
        [AC_SEARCH_LIBS(
            [rte_init], 
            [rte], 
            [AC_DEFINE(WITH_RTE, 1, [RTE])],
            [AC_MSG_ERROR(
                [unable to find librte]
            )]
        )]
    )],
    []
)

if test "$with_cuda" != no; then
     AC_SEARCH_LIBS(
        [cudaMalloc], 
        [cudart], 
        [AC_DEFINE(WITH_CUDA, 1, [CUDA])],
        [AC_MSG_ERROR([unable to find -lcuda])])   
fi

AS_IF([test "x$with_comm" != "xno" && test "$comm_count" != "x"],
      AC_MSG_ERROR(["One and only one communication layer must be selected"]),
      [])

AM_CONDITIONAL([HAVE_SHMEM], [test x$shmem_present = xyes])
AM_CONDITIONAL([HAVE_RTE], [test x$rte_present = xyes])
AM_CONDITIONAL([HAVE_UCX], [test x$ucx_present = xyes])
AM_CONDITIONAL([HAVE_MPI], [test x$mpi_present = xyes])
AM_CONDITIONAL([HAVE_COMM], [test x$with_comm = xyes])
AM_CONDITIONAL([USE_CUDA], [test x$with_cuda = xyes])

# Check for doxygen
AC_PATH_PROG(DOXYGEN, doxygen, NO_DOXYGEN)
if test "$DOXYGEN" = NO_DOXYGEN; then
	AC_MSG_NOTICE([Couldn't find Doxygen -- Make doc will fail])
fi

DOXYGEN_OUTPUT_DIRECTORY="docs"

# Checks for header files.
AC_CHECK_HEADERS([stdlib.h string.h malloc.h stdio.h time.h])


# Checks for typedefs, structures, and compiler characteristics.
AC_C_INLINE
AC_TYPE_SIZE_T

# Checks for library functions.
AC_CHECK_FUNCS([strtoul malloc])

# check if submodules are there
if test "$has_hwloc" = "no" || test "$has_memkind" = "no" || test "$has_nvml" = "no"; then
    git submodule update --init --recursive
fi

# check if the prefix is set
if test "$prefix" != "NONE"; then 
    sed -i "93s,^\(export prefix =\) .*,\1 $prefix," external/nvml/src/common.inc
else
    sed -i "93s,^\(export prefix =\) .*,\1 /usr/local," external/nvml/src/common.inc
fi

LT_INIT

AC_CONFIG_FILES([
        Makefile 
        src/Makefile 
        src/utils/ucm/Makefile
        src/utils/ucs/Makefile
        tests/Makefile
        ])
AC_OUTPUT

