/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*
*/

/**
 * @brief  It is a collective operation called by all PEs in the group. It 
 * allocates the memory using the memory allocator, and returns a pointer is 
 * returned to a local object for each participating PE. 

 * @param [in]     data_tier       Data tier which provides the usage model and the
 *                                 constraints 
 * @param [in]     map             Map provides a distribution of data elements on data tier
 * @param [in]     group           The group of PEs associated with the data structure creation
 * @param [in]     callback        sharp_datastructure_cb 
 * @param [out]    object_id       The pointer to an opaque object encapsulating the distributed object.  
 */

sharp_create_datastructure(sharp_data_tier *data_tier,  size_t size,
                           sharp_data_map *map, sharp_group *group,
                           sharp_datastructure_cb *create_cb, sharp_object
                           *object_id);



/*
 * @brief  Creates a array structure for the allocated object id

 * @param [in]  object_id       The pointer to an opaque object encapsulating 
                                the distributed object.
 * @param [in]  size_element    The size of each element 
 * @param [in]  num_elems       The number of elements
 * @param [in]  type            The datatype of each of the element 
 * @param [out] low_index       The low index of the local array where data is stored
 * @param [out] high_index      The high index of the local array where data is stored
 */

sharp_create_array(sharp_object *object_id, size_t size_element, int num_elems,
                   int *low_index, int *high_index, sharp_datatype *dtype);

/*
 * @brief  Stores data in the array at the “index”

 * @param [in] object_id       The pointer to an opaque object encapsulating the distributed object.
 * @param [in] num_elems       The number of elements to store
 * @param [in] type            The datatype of each of the element 
 * @param [in] index           The index of the array to store the data
 */

sharp_array_put(sharp_object *object_id, int index, int type, int num_elems);


/*
 * @brief  Stores data in the array at the index “index”

 * @param [in] object_id    The pointer to an opaque object encapsulating the 
                            distributed object.
 * @param [in] num_elems    The number of elements to store
 * @param [in] num_elems    The number of elements to store
 */

sharp_query_array_index(sharp_object *object_id, sharp_usage *usage_hints,
                        sharp_constraints *usage_constraints, int *low_index,
                        int *high_index);
