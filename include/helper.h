/**
 * Copyright (c) 2016 UT-Battelle, LLC. ALL RIGHTS RESERVED.
 */

#ifndef __SHARP_HELPER_H
#define __SHARP_HELPER_H

extern sharp_node_info_t sharp_node;

static inline sharp_dev_item_t * get_device_from_list_by_id(int id)
{
    sharp_dev_item_t * device;

    ucs_list_for_each(device, sharp_node.dev_l, next_p) {
        if(device->device.device_id == id) {
            return device;
        }
    }

    return NULL;
}

static inline sharp_md_list_item_t * get_mdomain_from_list_by_id(int id)
{
    sharp_md_list_item_t * md_item;

    ucs_list_for_each(md_item, sharp_node.md_l, next_p) {
        if(sharp_md_get_md_id(md_item->mem_attrib) == id) {
            return md_item;
        }
    }

    return NULL;
}

#endif
