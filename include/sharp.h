#ifndef __SHARP_H
#define __SHARP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <sharp/utils/sharp/sharp_errors.h>

#include <sharp/sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp/sharp_dtiers/api/sharp_dtiers.h>
#include <sharp/allocator/api/sharp_allocator_api.h>

#include <sharp/sharp_groups/api/sharp_groups.h>
#include <sharp/sobjects/api/sharp_objects.h>
#include <sharp/sharp_maps/api/sharp_maps.h>
#include <sharp/comms/comms-include.h>

#ifdef __cplusplus
}
#endif



#endif
