# SharP : Shared Memory data-centric Programming Model for Extreme and Heterogeneous Systems

SharP is a data structure-centric Programming Abstraction for Extreme 
and Heterogeneous Systems. SharP provides a simple, usable, and 
portable abstractions over hierarchical-heterogeneous memory allowing
for simple local/distributed allocation of memory on various 
memories (e.g., DRAM, High-Bandwidth Memory (HBM), and Non-Volatile
Random Access Memory (NVRAM)). In addition, SharP provides users 
with simple interfaces to create distributed data structures on the 
allocated memory. 

Compilation instructions:
------------------------

Compilation dependencies: 
    Libraries: libnuma, librte, orte, one of (MPI, UCX, or OpenSHMEM)
    Autotools: autoconf >= 2.63 and automake >= 1.11.1 and libtool == 2.4.2

Steps to build with UCX support:
    ORTE:

    git clone https://github.com/open-mpi/ompi.git
    cd ompi
    git checkout 0ab76750193575477c6bc629d537b8da8125a393
    ./autogen.pl --no-ompi
    ./configure --prefix=$PREFIX \
                --with-devel-headers \
                --enable-mpirun-prefix-by-default
    make && make install

    librte:

    git clone https://github.com/uccs/librte
    cd librte
    ./autogen.pl
    ./configure --prefix=$PREFIX \
                --with-orte \
                --with-orte-lib=$OMPIDIR/lib \
                --with-orte-include=$OMPIDIR/include
    make && make install

    xpmem: (if necessary) 

    git clone https://gitlab.com/hjelmn/xpmem
    cd xpmem
    ./autogen.sh
    ./configure --prefix=$PREFIX 
    make && make install
    sudo insmod kernel/xpmem.ko
    sudo chmod 666 /dev/xpmem

    ucx:

    git clone https://github.com/openucx/ucx.git
    cd ucx
    git checkout v1.5.x
    ./autogen.sh
    ./configure --prefix=$PREFIX --enable-cma --with-xpmem=$PREFIX
    make && make install

SharP Build instructions (allocator only, no CUDA support):
$ ./autogen.sh
$ ./configure --prefix=$PREFIX --disable-comm --with-external-ucx=$PREFIX
$ make && make install

SharP Build instructions (allocator only, CUDA support):
$ ./autogen.sh
$ ./configure --prefix=$PREFIX --disable-comm --enable-cuda --with-external-ucx=$PREFIX
$ make && make install

SharP Build instructions (UCX-based Communication layer, CUDA support):
$ ./autogen.sh
$ ./configure --prefix=$PREFIX --with-comm-ucx=$PREFIX --with-rte=$PREFIX --enable-cuda --with-external-ucx=$PREFIX
$ make && make install

Unit tests:
-----------

To check the installation, you can make use of the bundled unit tests:

$ make test


