#!/bin/bash

PWD=`pwd`
PREFIX=${PWD}/install.sharp
WITH_PATH=0
WITH_XPMEM=0
WITH_SHMEM=0
WITH_UCX=1
WITH_BUILTIN_MPI=0

HELP=`cat <<EOF
usage: ./build_sharp.sh [OPTIONS]
    -p or --prefix          Provide install prefix (--prefix=DIR)
    -x or --with-xpmem      Provide path for XPMEM install (--with-xpmem=DIR)
    -s or --with-shmem      Provide path for SHMEM install (--with-shmem=DIR)
    -b                      Include MPI with ORTE build
    -h or --help            Print this message
EOF
`

for i in "$@"
do
    case $i in
        -p=*|--prefix=*)
        WITH_PATH=1
        PREFIX="${i#*=}"
        shift
        ;;
        -x=*|--with-xpmem=*)
        WITH_XPMEM=1
        XPMEM_PATH="${i#*=}"
        shift
        ;;
        -s=*|--with-shmem=*)
        WITH_SHMEM=1
        WITH_UCX=0
        SHMEM_PATH="${i#*=}"
        shift
        ;;
        -b)
        WITH_BUILTIN_MPI=1
        shift
        ;;
        -h|--help)
        echo "${HELP}"
        exit 0
        shift
        ;;
        *)
    
        ;;
    esac
done

if [ ${WITH_XPMEM} -eq 0 ]; then
    echo "NOTE: XPMEM path not set. Not using XPMEM"
fi

echo "Building SharP with the following options:"
if [ ${WITH_UCX} -eq 1 ]; then
    echo "Comms: ucx"
fi

if [ ${WITH_SHMEM} -eq 1 ]; then
    echo "Comms: openshmem"
fi

if [ ${WITH_BUILTIN_MPI} -eq 1 ]; then
    echo "Bundled MPI: yes"
else
    echo "Bundled MPI: no"
fi

# TODO: add mpi

echo ""
for i in {5,4,3,2,1}; do
    echo -n -e "Compiling in ${i} seconds\r"
    sleep 1
done

BREAK=0
RETFLEX=0
RETNUMA=0
RETPKGCONFIG=0

# this is for ubuntu/debian systems
if [ -f /etc/debian_version ]; then
    TEST_FLEX=`dpkg -l flex | grep ii`
    RETFLEX=$?
    TEST_NUMA=`dpkg -l libnuma-dev | grep ii`
    RETNUMA=$?
    TEST_PKGCONFIG=`dpkg -l pkg-config | grep ii`
    RETPKGCONFIG=$?
elif [ -f /usr/bin/yum ] && [ -f /etc/redhat-release ]; then
    TEST_FLEX=`yum list installed flex`
    RETFLEX=$?
    TEST_NUMA=`yum list installed numactl`
    RETNUMA=$?
    TEST_PKGCONFIG=`yum list installed pkgconfig`
    RETPKGCONFIG=$?
fi

if ! [ ${RETFLEX} -eq 0 ]; then
    echo "Flex is not installed (apt-get install flex)"
    BREAK=1
fi

if ! [ ${RETNUMA} -eq 0 ]; then
    echo "libnuma-dev is not installed (apt-get install libnuma-dev)"
    BREAK=`expr ${BREAK} + 1`
fi

if ! [ ${RETPKGCONFIG} -eq 0 ]; then
    echo "pkg-config is not installed (apt-get install pkg-config)"
    BREAK=`expr ${BREAK} + 1`
fi

if [ ${BREAK} -gt 0 ]; then
    echo "ERROR: ${BREAK} packages need installation"
    exit -1
fi


mkdir build-sharp; cd build-sharp

echo "export PATH=${PREFIX}/bin:$PATH" >> ./source-sharp.sh
echo "export LD_LIBRARY_PATH=${PREFIX}/lib:$LD_LIBRARY_PATH" >> ./source-sharp.sh

. source-sharp.sh

# build dependencies
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.xz
wget http://ftp.gnu.org/gnu/automake/automake-1.15.tar.xz
wget http://ftp.gnu.org/gnu/libtool/libtool-2.4.2.tar.xz

tar xvf autoconf-2.69.tar.xz
tar xvf automake-1.15.tar.xz
tar xvf libtool-2.4.2.tar.xz

cd autoconf-2.69/; ./configure --prefix=${PREFIX}; make -j3; make install; cd ..
cd automake-1.15/; ./configure --prefix=${PREFIX}; make -j3; make install; cd ..
cd libtool-2.4.2/; ./configure --prefix=${PREFIX}; make -j3; make install; cd ..

# build ucx
git clone https://github.com/openucx/ucx
cd ucx
git checkout v1.5.x
./autogen.sh

if [ ${WITH_XPMEM} -eq 0 ]; then
    ./configure --prefix=${PREFIX}
else
    ./configure --prefix=${PREFIX} --with-xpmem=${XPMEM_PATH}
fi

make -j3 && make install
cd ..


# build ompi
git clone https://github.com/open-mpi/ompi
cd ompi
if [ $WITH_SHMEM -eq 1 ]; then 
    git checkout v4.0.x
    ./autogen.pl 
    ./configure --prefix=${PREFIX} --enable-mpirun-prefix-by-default --with-ucx=${PREFIX}
else
    git checkout 0ab76750193575477c6bc629d537b8da8125a393
    if [ ${WITH_BUILTIN_MPI} -eq 1 ]; then
        ./autogen.pl 
    else 
        ./autogen.pl --no-ompi
    fi
    ./configure --prefix=${PREFIX} --with-ucx=${PREFIX} --with-devel-headers --enable-mpirun-prefix-by-default
fi
make -j3
make install
cd ..

# build librte
if [ $WITH_SHMEM -eq 0 ]; then
    git clone https://github.com/uccs/librte
    cd librte
    ./autogen.pl
    ./configure --prefix=${PREFIX} --with-orte --with-orte-lib=${PREFIX}/lib --with-orte-include=${PREFIX}/include
    make -j3 && make install
    cd ..
fi

git clone https://gitlab.com/ferroladerholdt/sharp.git
cd sharp
./autogen.sh
if [ $WITH_SHMEM -eq 0 ]; then
    ./configure --prefix=${PREFIX} --with-comm-ucx=${PREFIX} --with-rte=${PREFIX} --with-external-ucx
else
    ./configure --prefix=${PREFIX} --with-comm-shmem=${PREFIX} --with-external-ucx=${PREFIX}
fi
make -j3
make install
cd ..
