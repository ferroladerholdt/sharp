set smartindent                                                                 
set tabstop=4                                                                   
set shiftwidth=4                                                                
set expandtab                                                                   
set colorcolumn=72,80                                                           
set cino=:0,(0                                                                  
set tw=80   
