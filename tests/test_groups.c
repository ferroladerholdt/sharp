/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <sharp.h>

#include <common.h>

int main(void) {
    int ret = 0;
    int error = 0;
    double starttime, endtime;
    int nr_domains_reported = 0;
    ucs_list_link_t *list;
    sharp_constraint_t constraint;
    sharp_hint_t hint;
    sharp_data_tier data_tier;
    sharp_group_network_t nw_group;
    sharp_group_allocated_t ag1, ag2;
    sharp_group_data_t dg1, dg2;
    void * buffer = NULL;
    void * buffer2 = NULL;
    int rank, size;
    sharp_object_t  sobj1, sobj2;
    double * p1, *p2;
    int low1, low2;
    int high1, high2;
    int i = 0;

    MPI_Init(NULL, NULL);
    starttime = MPI_Wtime();
    error = sharp_init();
    endtime = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /* *** MEMORY ALLOCATION *** */
    buffer = malloc(100*sizeof(double));
    buffer2 = malloc(100*sizeof(double));
    p1 = (double *)malloc(100*sizeof(double));
    p2 = (double *)malloc(100*sizeof(double));

    printf("[%s] error %d on init() (time taken: %f seconds)\n", 
                                                   __FILE__, 
                                                   error,
                                                   endtime-starttime);

    starttime = MPI_Wtime();
    list = sharp_query_mds(&nr_domains_reported);
    endtime = MPI_Wtime();

    printf("[%s] number of mdomains: %d (time taken: %f seconds)\n",
                                                               __FILE__,
                                                    nr_domains_reported,
                                                      endtime-starttime);

    if(list == NULL) {
        ret = -1;
        goto out;
    }

    hint = SHARP_HINT_CPU;
    sharp_create_constraint(100*sizeof(double),
                            SHARP_ACCESS_INTERP,
                            0,
                            0,
                            0,
                            0,
                            &constraint);
    starttime = MPI_Wtime();
    error = sharp_create_data_tier(&hint, &constraint, &data_tier);
    endtime = MPI_Wtime();
    if(error < 1) {
        printf("*** WARNING could not find a memory domain ***\n");
        
        printf("with:\n");
        printf("hint: %s\n", hint_to_string(hint));
        printf("constraint: 0x%lx 0x%lx\n", 
            (unsigned long) ((data_tier.constraints)>>64),
            (unsigned long) (data_tier.constraints));
        ret = -1;
        goto out;
    } else {
        printf("data-tier created in %f seconds with %d mds\n", 
                                                    endtime-starttime,
                                                    error);
        print_data_tier(&data_tier);
    }
    
    create_network_group_mpi(MPI_COMM_WORLD,
                             size,
                             SHARP_MPI,
                             &nw_group);   
    starttime = MPI_Wtime();
    error = sharp_create_group(SHARP_MPI, &nw_group, size, &data_tier,
                               100*sizeof(double), buffer, &ag1);
    endtime = MPI_Wtime();
    printf("created group in %f seconds\n", endtime-starttime);
   
    /* simple barrier test to ensure the network layer is setup */ 
    barrier_test_mpi(&ag1);

    starttime = MPI_Wtime();
    error = sharp_create_group(SHARP_MPI, &nw_group, size, &data_tier,
                               100*sizeof(double), buffer2, &ag2);     
    error = sharp_create_group_allocate(SHARP_MPI, &nw_group, size, 
                                        &data_tier, 100*sizeof(double), &ag2);
    endtime = MPI_Wtime();
    printf("created group in %f seconds\n", endtime-starttime);

    starttime = MPI_Wtime();
    error = sharp_create_data_group(0, 1, 1, &ag1, 1, &dg1);
    endtime = MPI_Wtime();
    printf("created data group in %f seconds\n", endtime-starttime);

    starttime = MPI_Wtime();
    error = sharp_create_data_group(0, 1, 1, &ag2, 1, &dg2);
    endtime = MPI_Wtime();
    printf("created data group in %f seconds\n", endtime-starttime);


    sharp_attach_array(&dg1, 1, NULL, SHARP_DOUBLE, 100, &low1, &high1, &sobj1);

    starttime = MPI_Wtime();
    sharp_attach_array(&dg2, 1, NULL, SHARP_DOUBLE, 100, &low2, &high2, &sobj2);
    endtime = MPI_Wtime();
    printf("created data group in %f seconds\n", endtime-starttime);

    starttime = MPI_Wtime();
    sharp_get_darray(0, SHARP_DOUBLE, 100, &sobj1, p1);
    endtime = MPI_Wtime();
    printf("Got data from array in %f seconds\n", endtime-starttime);

    sharp_get_darray(0, SHARP_DOUBLE, 100, &sobj2, p2);

    for(i=low1;i<high1;i++) {
        p1[i] = 1.0*i;
    }

    for(i=low2; i<high2; i++) {
        p2[i] = 1.0*i;
    }

    sharp_put_darray(p1, SHARP_DOUBLE, 100, 0, &sobj1);
    sharp_put_darray(p2, SHARP_DOUBLE, 100, 0, &sobj2);

    sharp_detach_array(&sobj1, &dg1);
    sharp_detach_array(&sobj2, &dg2);

    sharp_attach_array(&dg1, 1, NULL, SHARP_DOUBLE, 100, &low1, &high1, &sobj1);
    sharp_attach_array(&dg2, 1, NULL, SHARP_DOUBLE, 100, &low2, &high2, &sobj2);

    /* perform a get here */
    memset(p1, 0, 100*sizeof(double));
    memset(p2, 0, 100*sizeof(double));
    
    sharp_get_darray(0, SHARP_DOUBLE, 100, &sobj1, p1);
    sharp_get_darray(0, SHARP_DOUBLE, 100, &sobj2, p2);

    for(i=low1;i<high1;i++) {
        if(p1[i] != p2[i]) {
            printf("** ERROR in get_darray() (%f, %f) **\n", p1[i], p2[i]);
            ret = -1;
            goto out;
        }
    }

    printf("success: array 1 is equal to array 2\n");
    
out:
    free(buffer);
    free(buffer2);
    free(p1);
    free(p2);
    sharp_finalize();
    MPI_Finalize();
    return ret;
}
