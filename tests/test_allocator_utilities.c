#include <allocator/sharp_allocator.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <string.h>
#include "test_allocator_utilities.h"
#include <ucs/datastruct/list.h>
#include <ucs/debug/memtrack.h>
#include <utils/ucs/datastruct/list.h>
#include <unistd.h>
#include <signal.h>

extern sharp_node_info_t sharp_node;
const clockid_t clock_id = CLOCK_THREAD_CPUTIME_ID;


/* *********************************************** */
sharp_dev_t get_device_number(int x)
{
    sharp_dev_item_t item;
    sharp_dev_t device;
    int i = 0;


    sharp_dev_item_t * first = ucs_list_head(sharp_node.dev_l,
                                             sharp_dev_item_t,
                                             next_p);

    item = *ucs_container_of(first,
                             sharp_dev_item_t,
                             device);


    for(; i < x; i++) {
      first = ucs_list_next(&first->next_p, sharp_dev_item_t, next_p);
      item = *ucs_container_of(first,
                              sharp_dev_item_t,
                              device);
    }


    item = * ucs_container_of(ucs_list_head( sharp_node.dev_l,
                                              sharp_dev_item_t,
                                              next_p),
                              sharp_dev_item_t,
                              device);

    device = item.device;
    return device;
}

/* *********************************************** */
int random_allocs(sharp_allocator_t *a,
                           ucs_list_link_t * node_list,
                           size_t base_size,
                           size_t max_chunk_multiple,
                           size_t early_cutoff)
{
    size_t allocated = 0;

    while (1) {
        if (allocated == early_cutoff && early_cutoff > 0 ) {
            break;
        }
        pointer_node_t *node = malloc(sizeof(pointer_node_t));
        memset(node, 0, sizeof(*node));


        if (!node) {
            printf("normal malloc failed, aborting test\n");
            /*  this leaks memory, but in the test it isn't important */
            goto fail;
        }

        size_t chunks_in_allocation = 1 + (rand() % (max_chunk_multiple - 1));
        int err = a->ops->chunk_alloc(a, 
                                      base_size * chunks_in_allocation,
                                      &node->data);

        if (err == SHARP_ERR_NO_MEMORY) {
            break; // this is expected eventually
        } else if (err == SHARP_OK) {
            z_assert(node->data);
            node->bytes=chunks_in_allocation*base_size;
            ucs_list_insert_after(node_list, &node->list);
        } else if (err == SHARP_CUDA_ERROR) {
            fprintf(stderr,
                    "%s::%i - CUDA error on size %lu\n",
                    __FILE__,
                    __LINE__,
                    chunks_in_allocation * base_size);
            goto fail;
        } else {
            printf("%s::%i - Unexpected error code %i '%s' \n",
                   __FILE__,
                   __LINE__,
                   err,
                   sharp_error_string(err));
            goto fail;
        }
        allocated++;
    }

    return allocated;

fail:
    return -1;
}

/* ***************************************************** */
int allocate_until_failure(sharp_allocator_t *a,
                           ucs_list_link_t * node_list,
                           size_t base_size,
                           size_t early_cutoff)
{
    size_t current_size=1;
    while (1) {
        if (current_size - 1 == early_cutoff && early_cutoff > 0) {
            break;
        }
        pointer_node_t *node = malloc(sizeof(pointer_node_t));
        memset(node, 0, sizeof(*node));


        if (!node) {
            printf("normal malloc failed, aborting test\n");
            /*  this leaks memory, but in the test it isn't important */
            goto fail;
        }
        int err = a->ops->chunk_alloc( a, base_size*current_size, &node->data);

        if (err == SHARP_ERR_NO_MEMORY) {
            break; // this is expected eventually
        } else if (err == SHARP_OK) {
            z_assert(node->data);
            node->bytes=current_size*base_size;
            ucs_list_insert_after(node_list, &node->list);
        } else if (err == SHARP_CUDA_ERROR) {
            fprintf(stderr,
                    "%s::%i - CUDA error on size %lu\n",
                    __FILE__,
                    __LINE__,
                    current_size);
            goto fail;
        } else {
            printf("%s::%i - Unexpected error code %i '%s' \n",
                   __FILE__,
                   __LINE__,
                   err,
                   sharp_error_string(err));
            goto fail;
        }

        current_size++;
    }

    return current_size - 1;

fail:
    return -1;
}

/* ***************************************************** */
int free_all_memory(sharp_allocator_t *a, ucs_list_link_t *node_list)
{
    int freed=0;
    //free all of the chunks
    pointer_node_t * node=NULL;
    ucs_list_for_each( node, node_list, list) {
        int err = a->ops->chunk_release(a, &node->data);
        if (err != SHARP_OK) {
            fprintf(stderr,
                    "%s::%i - Unexpected error code %i\n",
                    __FILE__,
                    __LINE__,
                    err);
        } else {
            ++freed;
        }
        ucs_list_del(&node->list);
    }
    return freed;
}

/* *********************************************** */
int test_list_allocations(ucs_list_link_t *node_list, int(*fun)(void*, size_t))
{
    pointer_node_t *node;

    int success_count=0;

    ucs_list_for_each(node, node_list, list) {
        if (!fun(node->data, node->bytes)) {
            ++success_count;
        }
    }
    return success_count;
}



/* *********************************************** */
void now(struct timespec * t)
{
    int err = clock_gettime(clock_id, t);
    if (err) {
        perror("Aborting program");
        exit(1);
    }
}

/* *********************************************** */
void resolution(struct timespec *res)
{
    int err = clock_getres(clock_id, res);
    if (err){
        perror("Aborting program");
        exit(1);
    }
}

/* *********************************************** */
char * time_diff_to_string(struct timespec start,
                           struct timespec end,
                           char * str,
                           size_t len)
{
    struct timespec answer;
    answer.tv_sec = end.tv_sec - start.tv_sec;
    answer.tv_nsec = end.tv_nsec - start.tv_nsec;

    if (answer.tv_nsec < 0) {
        answer.tv_nsec += 1000000000;
        --answer.tv_sec;
    }

    snprintf(str, len, "%li.%.9li", answer.tv_sec, answer.tv_nsec);

    return str;
}

/* *********************************************** */
int safe_long_parsing(char * str, long * output)
{
    char * last = str + strlen(str);
    char * end = NULL;
    *output = strtol(str, &end, 10);
    if (last != end) {
        return 1;
    }
    return 0;
}

/* ***************************************************** */
sharp_errors_t test_allocator(sharp_allocator_t *a, 
                              size_t block_size,
                              size_t max_random_blocks,
                              size_t early_cutoff,
                              int(*test_buffer_function)(void*,size_t))
{
    //allocate successively larger chunks until a failure occurs
    UCS_LIST_HEAD(node_list);

    const int buffSize = 1024;
    char buff[buffSize];

    struct timespec start, end;

    now(&start);
    int allocated = allocate_until_failure(a, &node_list, block_size,early_cutoff);
    if (0 > allocated) {
        eprintf0("There was an unexpected error allocating all of the memory"
                 " on the gpu\n");
        goto fail;
    }
    now(&end);
    printf("The first batch of %i allocations took %s seconds\n",
           allocated, time_diff_to_string(start, end, buff, buffSize));

    int tested = test_list_allocations(&node_list, test_buffer_function);
    if (allocated != tested) {
        eprintf("%i allocations failed to be usable, only %i were\n",
                allocated,
                tested);
        goto fail;
    }

    now(&start);
    int freed = free_all_memory(a, &node_list);
    if (allocated != freed) {
        eprintf("There was an error freeing all of the memory. "
                "Expected to free %i but only freed %i\n",
                allocated, freed);
        goto fail;
    }
    now(&end);
    printf("The first batch of %i deallocations took %s seconds\n",
           freed, time_diff_to_string(start, end, buff, buffSize));

    //attept the same allocation again
    now(&start);
    int allocated_second_time;
    allocated_second_time = allocate_until_failure(a, &node_list, block_size, early_cutoff);
    if (allocated != allocated_second_time) {
        eprintf("Expected %i allocations, but %i were performed\n"
                 , allocated,
                 allocated_second_time);
        goto fail;
    }
    now(&end);
    printf("The second batch of %i allocations took %s seconds\n",
           allocated, time_diff_to_string(start, end, buff, buffSize));


    tested = test_list_allocations(&node_list, test_buffer_function);
    if (allocated != tested) {
        eprintf("%i allocations failed to be usable, only %i were\n",
                allocated,
                tested);
        goto fail;
    }


    now(&start);
    if (allocated != free_all_memory(a, &node_list)) {
        eprintf0("There was an error freeing all of the memory");
        goto fail;
    }
    now(&end);
    printf("The second batch of %i deallocations took %s seconds\n",
           freed, time_diff_to_string(start, end, buff, buffSize));



    /*attempt to allocate a larger chunk to force the allocator to
     *combine chunks to reduce fragmentation*/

    /* this can be thought of as the sum of all integers from 0 to allocated
     * since each allocation is one block bigger than the last one and their are
     * allocated of them.
     * */
    int blocks_allocated = (allocated * (allocated + 1))  / 2 ;
    size_t total_size = blocks_allocated * block_size;

    now(&start);
    void * ptr;
    int err = a->ops->chunk_alloc(a, total_size, &ptr);

    if (err != SHARP_OK) {
        eprintf("Could not allocate a buffer of %lu MB\n",
                (total_size / 1024 / 1024)+1);
        goto fail;
    } else {
        z_assert(ptr);
    }
    now(&end);
    printf("A large allocation of %lu bytes took %s seconds\n",
           total_size * block_size,
           time_diff_to_string(start, end, buff, buffSize));

    if (test_buffer_function( ptr, total_size)) {
        eprintf0("There was an error using the buffer\n");
        goto fail;
    }

    /* after this function all gpu memory has been returned to the allocator */
    now(&start);
    a->ops->chunk_release(a, &ptr);
    now(&end);
    printf("dealocating the large chunk took %s seconds\n",
           time_diff_to_string( start, end, buff, buffSize));

    now(&start);
    int third_mass_allocation = allocate_until_failure(a,
                                                       &node_list,
                                                       block_size,
                                                       early_cutoff);


    /* the constant factor is a heuristic */
    double factor = 0.99;
    if (third_mass_allocation < allocated * factor) {
        eprintf("third_mass_allocation: %i, "
                "expected to be at least %i, optimally %i\n",
                third_mass_allocation,
                (int)(allocated*factor),
                allocated);
        goto fail;
    }
    now(&end);
    printf("Allocating %i blocks took %s seconds\n",
           third_mass_allocation,
           time_diff_to_string(start, end, buff, buffSize));



    tested = test_list_allocations(&node_list,test_buffer_function);
    if (allocated != tested) {
        eprintf("%i allocations failed to be usable, only %i were\n",
                allocated,
                tested);
        goto fail;
    }

    now(&start);

    int third_free = free_all_memory(a, &node_list);
    if (third_mass_allocation != third_free) {
        eprintf("only %i of %i were freed in thrid wave\n",
                third_free,
                third_mass_allocation);
        goto fail;
    }

    now(&end);
    printf("Dealocating %i blocks took %s seconds\n",
           third_free, time_diff_to_string(start, end, buff, buffSize));

    now(&start);
    int random_chunks = random_allocs(a,
                                      &node_list,
                                      block_size,
                                      max_random_blocks,
                                      early_cutoff);
    now(&end);
    printf("Random allocation 1 allocated %i in %s seconds\n",
           random_chunks,
           time_diff_to_string(start,end,buff,buffSize));

    now(&start);
    int random_frees = free_all_memory(a, &node_list);
    if (random_chunks != random_frees) {
        eprintf("only freed %i of %i cunks of random size\n",
                random_frees, random_chunks);
        goto fail;
    }
    now(&end);


    printf("Random deallocation 1 freed %i in  %s seconds\n",
           random_frees,
           time_diff_to_string(start,end,buff,buffSize));


    now(&start);
    random_chunks = random_allocs(a,
                                  &node_list,
                                  block_size,
                                  max_random_blocks,
                                  early_cutoff);

    now(&end);
    printf("Random allocation 2 allocated %i in  %s seconds\n",
               random_chunks,
               time_diff_to_string(start,end,buff,buffSize));

    now(&start);
    random_frees = free_all_memory(a, &node_list);
    if (random_chunks != random_frees) {
        eprintf("only freed %i of %i cunks of random size\n",
                random_frees, random_chunks);
        goto fail;
    }
    now(&end);
    printf("Random deallocation 2 freed %i in %s seconds\n",
           random_frees,
           time_diff_to_string(start,end,buff,buffSize));


    return SHARP_OK;
fail:
    return SHARP_ERR_NO_MEMORY;
}


/* *********************************************** */
int sharp_init(void) 
{
    return sharp_create_node_info();
}

/* *********************************************** */
void sharp_finalize(void) 
{
    sharp_destroy_node_info();
}


/* ************************************************************** */
/*  print the PID and call abort to generate a core dump */
void signal_abort(int sig)
{
    eprintf("Pid %i Aborting due to signal %i: %s\n",
            getpid(),
            sig,
            strsignal(sig));
    abort();
}

/* ************************************************************** */
/* replace signal handlers with one that is more useful than ucs's
 * see the signal_abort function for a description
 */
void install_signal_handlers()
{

#define install(x) \
        if( signal(x, signal_abort ) == SIG_ERR ){ \
            perror("Failed to set signal handler:" #x ); \
        }

    install(SIGBUS);
    install(SIGTERM);
    install(SIGINT);
#undef install
}



