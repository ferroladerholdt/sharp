/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <ucs/debug/debug.h>
#include <allocator/sharp_allocator.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <ucs/datastruct/list.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "test_allocator_utilities.h"


extern ucs_list_link_t allocator_list;

int sharp_init(void) {
    return sharp_create_node_info();
}

void sharp_finalize(void) {
    sharp_destroy_node_info();
}

static inline const char * md_type_to_string(sharp_md_type_t type) {
    if(type == SHARP_MD_DDR) {
        return "DDR";
    } else if(type == SHARP_MD_HBM) {
        return "HBM";
    } else if(type == SHARP_MD_NVRAM) {
        return "NVRAM";
    } else if(type == SHARP_MD_L1i) {
        return "L1 instruction cache";
    } else if(type == SHARP_MD_L1d) {
        return "L1 data cache";
    } else if(type == SHARP_MD_L2) {
        return "L2 cache";
    } else if(type == SHARP_MD_L3) {
        return "L3 cache";
    } else {
        return "UNKNOWN";
    }
}

static inline const char * md_access_to_string(sharp_md_mem_access_t access) {
    if(access == LOCAL) {
        return "LOCAL";
    } else if(access == NODE_R_ONLY) {
        return "NODE R ONLY";
    } else if(access == NODE_RW) {
        return "NODE RW";
    } else if(access == REMOTE_R_ONLY) {
        return "REMOTE R ONLY";
    } else if(access == REMOTE_RW) {
        return "REMOTE RW";
    } else {
        return "UNKNOWN";
    }
}

int main(void) {
    int ret = 0;
    int error = 0;
    ucs_list_link_t * list;
    sharp_md_list_item_t * mdomains;
    int nr_domains_reported = 0;
    int nr_domains_counted = 0;

    error = sharp_init();
    if (error != SHARP_OK) {
        return error;
    }

    list = sharp_query_mds(&nr_domains_reported);
    if(list == NULL) {
        ret = -1;
        goto out;
    }

    printf("\n*** Check your mdomains domains ***\n");
    ucs_list_for_each(mdomains, list, next_p) {
        printf("md id: %lu device id: %lu type: %s access: %s pointer:%p ", 
           sharp_md_get_md_id(mdomains->mem_attrib),
           sharp_md_get_device_id(mdomains->mem_attrib),
           md_type_to_string(sharp_md_get_type(mdomains->mem_attrib)),
           md_access_to_string(sharp_md_get_access(mdomains->mem_attrib)),
           mdomains
           );
        if(sharp_md_get_persistent(mdomains->mem_attrib)) {
            printf("persistent ");
        } 
        if(sharp_md_get_near_nic(mdomains->mem_attrib)) {
            printf("near nic ");
        }
        printf("\n");
        nr_domains_counted++;
    }

    /* get an allocator list */
    ucs_list_link_t * lst = &allocator_list;
    sharp_allocator_list_item_t * alloc;
    ucs_list_for_each(alloc, lst, link){
    }

out:
    sharp_finalize();
    return ret;
}
