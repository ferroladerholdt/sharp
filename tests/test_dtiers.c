/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <ucs/debug/debug.h>

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <utils/sharp/sharp_errors.h>

#include <common.h>
#include <limits.h>

#define DTIER_TEST_MAX_MDS          32
#define DTIER_TESTS                  7 

/* type information: 0 -> DDR, 1 -> GPU, 2 -> NVRAM */
typedef enum {
    DDR,
    GPU,
    NVRAM
} mem_t;

typedef struct {
    int nr_memories;
    size_t size[DTIER_TEST_MAX_MDS]; /* in KB */
    sharp_md_mem_access_t access[DTIER_TEST_MAX_MDS];
    int md_id[DTIER_TEST_MAX_MDS];
} mem_info_t;

mem_info_t memories[3];

static inline const char * type_to_string(mem_t type) {
    if(type == DDR) {
        return "DDR";
    } else if(type == GPU) {
        return "GPU";
    } else {
        return "NVRAM";
    }
}

static inline int init_memories(void) {
    int i = 0;
    for(;i<3;i++) {
        memories[i].nr_memories = 0;
        memset(memories[i].size,0, DTIER_TEST_MAX_MDS*sizeof(size_t));
        memset(memories[i].access,
               0, 
               DTIER_TEST_MAX_MDS*sizeof(sharp_md_mem_access_t));
        memset(memories[i].md_id,0, DTIER_TEST_MAX_MDS*sizeof(int));
    }
    return 0;
}

static inline int convert_to_mem_info(ucs_list_link_t * list,
                                      int nr_domains)
{
    sharp_md_list_item_t * mdomain;

    ucs_list_for_each(mdomain, list, next_p) {  
        /* type is my index into the array */
        unsigned char type = sharp_md_get_type(mdomain->mem_attrib);
        int nr_memories = memories[type].nr_memories;

        memories[type].size[nr_memories] = 
            sharp_md_get_size(mdomain->mem_attrib) << 10;
        memories[type].access[nr_memories] =
            sharp_md_get_access(mdomain->mem_attrib);
        memories[type].nr_memories++;
    }
    return 0;
}

static inline int analyze_test1(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;
    for(i=0;i<memories[type].nr_memories;i++) {
        if(memories[type].size[i] >= INT_MAX) {
            if(nr_found == 0) {
                return -1; /*there was a mdomain with the size not found*/
            }
        }
    }
    return 0;
}

static inline int analyze_test2(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;
    for(i=0;i<memories[type].nr_memories;i++) {
        if(memories[type].size[i] >= CHAR_MAX) {
            if(nr_found == 0) {
                return -1; /*there was a mdomain with the size not found*/
            }
        }
    }
    return 0;
}

static inline int analyze_test3(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;
    for(i=0;i<memories[type].nr_memories;i++) {
        if(memories[type].access[i] == REMOTE_RW) {
            if(nr_found == 0) {
                return -1; /*there was a mdomain with the size not found*/
            }
        }
    }
    return 0;
}

static inline int analyze_test4(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;
    for(i=0;i<memories[type].nr_memories;i++) {
        if(memories[type].access[i] >= NODE_RW) {
            if(nr_found == 0) {
                return -1; /*there was a mdomain with the size not found*/
            }
        }
    }
    return 0;
}

static inline int analyze_test5(mem_t type, sharp_data_tier * data_tier) {
    int nr_found = data_tier->nr_mds;

    if(type == NVRAM && memories[type].nr_memories && nr_found == 0) {
        return -1;
    }
    return 0;
}

static inline int analyze_test6(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;

    if(type == NVRAM 
       && memories[type].nr_memories 
       && nr_found == 0) {
        for(i=0;i<memories[type].nr_memories;i++) {
            if(memories[type].access[i] == REMOTE_RW) {
                return -1;
            }
        }
    }
    return 0;
}

static inline int analyze_test7(mem_t type, sharp_data_tier * data_tier) {
    int i = 0;
    int nr_found = data_tier->nr_mds;

    for(i=0;i<memories[type].nr_memories;i++) {
        if(memories[type].access[i] == LOCAL && nr_found == 0) {
            return -1;
        }
    }
    
    return 0;
}

static inline int analyze_results(mem_t type, sharp_data_tier * data_tier_list)
{
    int ret = 0;

    ret = analyze_test1(type, &data_tier_list[0]);
    if(ret != 0) {
        printf("*** Warning test 1 failed ***\n");
        return ret;
    }

    ret = analyze_test2(type, &data_tier_list[1]);
    if(ret != 0) {
        printf("*** Warning test 2 failed ***\n");
        return ret;
    }
   
    ret = analyze_test3(type, &data_tier_list[2]);
    if(ret != 0) {
        printf("*** Warning test 3 failed ***\n");
        return ret;
    }

    ret = analyze_test4(type, &data_tier_list[3]);
    if(ret != 0) {
        printf("*** Warning test 4 failed ***\n");
        return ret;
    }

    ret = analyze_test5(type, &data_tier_list[4]);
    if(ret != 0) {
        printf("*** Warning test 5 failed ***\n");
        return ret;
    }

    ret = analyze_test6(type, &data_tier_list[5]);
    if(ret != 0) {
        printf("*** Warning test 6 failed ***\n");
        return ret;
    }

    ret = analyze_test7(type, &data_tier_list[6]);
    if(ret != 0) {
        printf("*** Warning test 7 failed ***\n");
        return ret;
    }

    return ret;
}

/* For this there will be 7 tests:
 *  1. a memory with a INT_MAX size requirement (probable fail)
 *  2. a memory with a CHAR_MAX size requirement (probable pass)
 *  3. a memory with interprocess access
 *  4. a memory with intraprocess access
 *  5. presistent memory
 *  6. persistent with interprocess access
 *  7. local access
 */
static inline int generate_test(mem_t type) {
    sharp_data_tier data_tier_list[DTIER_TESTS];
    int ret = 0;
    int hint_type;
    int i = 0;

    if(type == GPU) {
        hint_type = SHARP_HINT_GPU;
    } else {
        hint_type = SHARP_HINT_CPU;
    }

    sharp_create_data_tier(
        data_tier_list, 7,
        hint_type, 0, INT_MAX,
        hint_type, 0, CHAR_MAX,
        hint_type, SHARP_ACCESS_INTERP, CHAR_MAX,
        hint_type, SHARP_ACCESS_INTRAP, CHAR_MAX,
        hint_type, SHARP_CONSTRAINT_PERSISTENT, CHAR_MAX,
        hint_type, SHARP_ACCESS_INTERP | SHARP_CONSTRAINT_PERSISTENT, CHAR_MAX,
        hint_type, SHARP_ACCESS_LOCAL, CHAR_MAX);

    analyze_results(type, data_tier_list);

    /* leaving this in for visual confirmations */
    for(;i<7;i++) {
        print_data_tier(&data_tier_list[i]);
    }

    return ret;
}

int main(void) {
    int ret = 0;
    int error = 0;
    int nr_domains_reported = 0;
    ucs_list_link_t *list;
    int i = 0;

    error = sharp_init();
    if (error != SHARP_OK) {
        return error;
    }

    list = sharp_query_mds(&nr_domains_reported);
    if (list == NULL) {
        ret = -1;
        goto out;
    }

    /* 
     * here's how we will test:
     *  1. traverse the mdomain list gathering mdomain information
     *  2. create a list of available memories and attributes
     *  3. generate tests based on these memories and attributes
     *  4. predetermine data tiers that should be created and those that 
     *     should not be
     *  5. create the data tiers
     *  6. do the predetermined tiers match the actual tiers?
     */
    for(i=0;i<3;i++) {
        error = generate_test(i);
        if(error != 0) {
            printf("*** WARNING data tier test failed on %s\n", 
                type_to_string(i));
        }
    }

    
out:
    sharp_finalize();
    return ret;
}
