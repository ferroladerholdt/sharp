/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*
* Groups test: This is a simple test. The idea is to create two groups,
* one with user allocated memory and one with library allocated memory. 
* Two objects will be created, split among the groups. The same 
* operations will be performed on the objects and then the objects 
* compared. 
*
* Tests:
*   memory domains
*   data tiers
*   groups
*   sharp_hash
*/

#include <unistd.h>


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ucs/debug/debug.h>

#include <mpi.h>

MPI_Datatype sharp_ucx_mpi_buffer_exchange;
MPI_Datatype sharp_ucx_mpi_worker_exchange;



#include <comms/comms-include.h>
#include <sharp/sharp_errors.h>
#include <sharp_maps/api/sharp_maps.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sobjects/api/sharp_objects.h>

#include <utils/sharp/sharp_errors.h>
#include <assert.h>
#include <limits.h>
#include <time.h>

#include "common.h"
#include <signal.h>

#define ERROR_OUT(x,...) err = (x); \
    if (err < 0) {\
        printf("%s::%i ",__FILE__,__LINE__);\
        printf(__VA_ARGS__); goto finish;\
    } else (void) 0

#define mprint(...) \
    do{ \
        if (rank == 0) { \
            err = fflush(stdout); \
            if (err == -1) perror("fflush failed"); \
            err = fprintf(stdout, "%s:%i ",__FILE__, __LINE__); \
            if (err == -1) perror("fprintf failed"); \
            err = fprintf(stdout, __VA_ARGS__); \
            if (err == -1) perror("fprintf failed"); \
            err = fflush(stdout); \
            if (err == -1) perror("fflush failed"); \
        } \
    } while(0)

#define XSLEEP do{ \
    static volatile int x=1;  \
    void * last = (void*)signal(SIGCONT, signal_exit_XSLEEP); \
    if (SIG_ERR == last) { \
         perror("Failed to set signal hander for xsleep"); \
     } \
     printf("pid: %i waiting on debugger\n", getpid()); \
     while(x && XSLEEP_gloabl_x) { sleep (1); } \
     if (SIG_ERR == signal(SIGCONT, last)) { \
         perror("Failed restoring signal handler after XSLEEP");\
     } \
} while(0)

typedef struct value_struct {
    int value;
    int rank_saved;
    int marker;
} value_struct_t;

typedef struct local_add_data {
    int added;
    int key;
    value_struct_t *value;
} local_add_data_t;

typedef struct local_find_data {
    int found;
    int key;
    char const * error_string;
} local_find_data_t;

typedef struct local_update_data {
    int key;
    int success;
    int to_marker;
    char const * err_string;
} local_update_data_t;

typedef struct local_delete_data {
    int key;
    int success;
    char const * err_string;
} local_delete_data_t;


//The rank and size of the entire job. Read only after initilization
int rank, size;
const char * cyan_text = "\033[;36m";
const char * reset_graphics = "\033[;0m";
int XSLEEP_gloabl_x = 1;
const clockid_t clock_id = CLOCK_THREAD_CPUTIME_ID;

void signal_exit_XSLEEP(int sig)
{
    if (1 || sig) {}
    XSLEEP_gloabl_x = 0;
}

uint64_t hash_fun(void *xx) 
{
    uint64_t x = *(int*)xx;
    x = (x ^ (x >> 30)) * UINT64_C(0xbf58476d1ce4e5b9);
    x = (x ^ (x >> 27)) * UINT64_C(0x94d049bb133111eb);
    x = x ^ (x >> 31);
    return x;
}

int compare_fun(void * a, void *b)
{
    return *(int*)a == *(int*)b;
}

int get_key(int rank, int size, int count)
{
    return rank + size*count;
}

int get_rank_for_key(int key, int size)
{
    return (key) % size;
}

int barrier()
{
    return MPI_Barrier(MPI_COMM_WORLD);
}

/* *********************************************** */
void now(struct timespec * t)
{
    int err = clock_gettime(clock_id, t);
    if (err) {
        perror("Aborting program");
        exit(1);
    }
}

void resolution(struct timespec *res)
{
    int err = clock_getres(clock_id, res);
    if (err) {
        perror("Aborting program");
        exit(1);
    }
}

char * time_diff_to_string(struct timespec start,
                           struct timespec end,
                           char * str,
                           size_t len) 
{
    struct timespec answer;
    answer.tv_sec = end.tv_sec - start.tv_sec;
    answer.tv_nsec = end.tv_nsec - start.tv_nsec;

    if (answer.tv_nsec < 0) {
        answer.tv_nsec += 1000000000;
        --answer.tv_sec;
    }

    snprintf(str, len, "%li.%.9li", answer.tv_sec, answer.tv_nsec);

    return str;
}

void output_time_info(struct timespec begin, struct timespec end, char * msg)
{
    const int time_str_len = 1024;
    char time_str[time_str_len];

    if (rank != 0) {
        return;
    }

    printf("%s: %s seconds\n",
           msg,
           time_diff_to_string(begin,end,time_str, time_str_len));
}

int error_agree(int out)
{
    int found = out;
    MPI_Allreduce(&found, &out, 1, MPI_INT, MPI_LOR, MPI_COMM_WORLD);
    return out;
}



int local_add_callback2(void * data,
                        sharp_hash_slot_data_t *slot_data,
                        sharp_hash_action_t *actions,
                        unsigned count) 
{
    local_add_data_t *dat = data;
    for (int i = 0; i<count; i++) {
        if (slot_data[i].used != SHARP_HASH_SLOT_UNUSED) {
            continue;
        }

        /* we have an empty slot, insert the key */
        memcpy(slot_data[i].key_buf, &dat->key, sizeof(dat->key));
        memcpy(slot_data[i].value_buf, dat->value, sizeof(*dat->value));
        dat->added = 1;
        actions[i]=SHARP_HASH_ADD;
        return 0;
    }
    return 1;
}

int local_add2(sharp_object_t* hash, int key, void * value)
{
    local_add_data_t data={};

    data.key = key;
    data.value = value;
    data.added=0;
    int err = sharp_hash_iterate_neighborhood(hash,
                                              &key,
                                              0,
                                              local_add_callback2,
                                              &data);
    if (err) { 
        return err; 
    }
    return (data.added != 1);
}


int local_find2_callback(void * ptr,
                         sharp_hash_slot_data_t * slot_data,
                         sharp_hash_action_t * actions,
                         unsigned count) 
{
    local_find_data_t * dat = ptr;
    unsigned i;
    for (i = 0; i<count; i++) {
        if (slot_data[i].used == SHARP_HASH_SLOT_UNUSED) {
            dat->error_string="Found empty slot while doing local_find2_callback";
            return 1;
        }

        if (compare_fun(slot_data[i].key_buf, &dat->key)) {
            break;
        }
    }
    if (i == count) {
        dat->error_string="Key not found by local_find2_callback";
        return 1;
    }

    /* ensure that the value is what was expected */
    dat->found=0;
    value_struct_t * val = slot_data[i].value_buf;
    if (val->rank_saved != dat->key) {
        dat->error_string="rank stored inconsistent";
        return 1;
    }

    if (val->marker != 0) {
        dat->error_string="Marker changed";
        return 1;
    }

    dat->found=1;
    return 0;
}

int local_find2(sharp_object_t *hash, int key, char const ** error_string)
{
    local_find_data_t data={};

    data.key = key;
    data.error_string = "";
    data.key=key;


    int err = sharp_hash_iterate_neighborhood(hash,
                                              &key,
                                              1,
                                              local_find2_callback,
                                              &data);
    *error_string = data.error_string;
    if (err) { 
        return err; 
    }
    return data.found != 1;
}

int local_update_callback2(void * data,
                           sharp_hash_slot_data_t *slot_data,
                           sharp_hash_action_t *actions,
                           unsigned count) 
{
    local_update_data_t * dat = data;
    for (int i=0; i<count; i++) {
        if (! compare_fun(&dat->key, slot_data[i].key_buf)) {
            continue;
        }

        void * marker_ptr = slot_data[i].value_buf + offsetof(value_struct_t, marker);

        memcpy(marker_ptr, &dat->to_marker, sizeof(int));
        dat->success=1;
        actions[i]=SHARP_HASH_UPDATE;
        return 0;
    }
    return 1;
}

int local_update2(sharp_object_t *hash, 
                  int key, 
                  int to_marker, 
                  char const ** error_string)
{
    local_update_data_t data = {};
    data.key=key;
    data.to_marker=to_marker;
    data.err_string="";

    int err = sharp_hash_iterate_neighborhood(hash,
                                              &key,
                                              0,
                                              local_update_callback2,
                                              &data);
    *error_string = data.err_string;

    err = err || data.success != 1;
    return err;
}

int local_delete_callback2(void * data,
                           sharp_hash_slot_data_t *slot_data,
                           sharp_hash_action_t *actions,
                           unsigned count) 
{
    local_delete_data_t * dat = data;
    for (int i=0; i<count; i++) {
        if (slot_data[i].used == SHARP_HASH_SLOT_UNUSED) {
            /* this might happen if the tests higher up in the call stack change */
            dat->err_string = "Unexpected empty slot";
            return 1;
        }

        if (! compare_fun(&dat->key, slot_data[i].key_buf)) {
            continue;
        }
        dat->success=1;
        actions[i]=SHARP_HASH_DELETE;
        return 0;
    }
    return 1;
}

int local_delete2(sharp_object_t *hash, int key, char const ** error_string)
{
    local_delete_data_t data = {};
    data.key=key;
    data.err_string="";

    int err = sharp_hash_iterate_neighborhood(hash,
                                              &key,
                                              0,
                                              local_delete_callback2,
                                              &data);
    *error_string = data.err_string;
    err = err || data.success != 1;
    return err;
}

int test_neighborhood_iteration2(size_t array_count)
{
    int err;

    sharp_data_tier dtier;
    sharp_group_allocated_t ag;
    sharp_object_t hash;

    size_t buffer_size = sharp_hash_get_required_size(sizeof(int),
                                                      sizeof(value_struct_t),
                                                      array_count,
                                                      size,
                                                      64,
                                                      64,
                                                      1);
    ERROR_OUT(sharp_create_data_tier(&dtier, 1,
                              SHARP_HINT_CPU, SHARP_ACCESS_INTERP, buffer_size),
              "Could not create data tier\n");

    ERROR_OUT(sharp_create_group_allocate(buffer_size,
                                          SHARP_MPI,
                                          (void*)(uintptr_t)MPI_COMM_WORLD,
                                          size,
                                          &dtier,
                                          &ag),
              "Could create  allocation group\n");

    barrier();

    ERROR_OUT(sharp_attach_hash(&ag,
                                sizeof(int),
                                sizeof(value_struct_t),
                                SHARP_HASH_CLEAR,
                                /* smaller than array_count since our value is 
                                 * larger */
                                array_count/10,
                                hash_fun,
                                compare_fun,
                                32,
                                64,
                                1,
                                &hash),
              "Faild to attach hash\n");
    barrier();


    /* add some keys with iteration */
    value_struct_t value = {.value = rank, .rank_saved=rank, .marker=0};
    ERROR_OUT(local_add2(&hash, rank, &value),
              "Rank %i failed to local_add2 key %i, error_code %i\n", 
              rank, rank, err);

    char const * err_string;
    /* make sure a normal find can find the key */
    value_struct_t val_out;
    ERROR_OUT(sharp_hash_find(&hash, &rank, NULL, &val_out),
              "sharp_hash_find failed to find key %i\n", rank);

    ERROR_OUT(value.value != val_out.value,
              "value.value != val_out.value on rank %i\n", rank);

    ERROR_OUT(value.rank_saved != val_out.rank_saved,
              "value.rank_saved != val_out.rank_saved on rank %i\n", rank);

    ERROR_OUT(value.marker != val_out.marker,
              "value.marker != val_out.marker on rank %i\n", rank);

    ERROR_OUT(local_find2(&hash, rank, &err_string),
              "local_find2 error: %s code %i\n", err_string, err);

    ERROR_OUT(local_update2(&hash, rank, 1, &err_string),
              "local_update2 error: '%s' code: %i\n", err_string, err);

    value.marker=1;
    ERROR_OUT(sharp_hash_find(&hash, &rank, NULL, &val_out),
              "sharp_hash_find failed to find key %i\n", rank);

    ERROR_OUT((value.value != val_out.value || \
               value.rank_saved != val_out.rank_saved || \
               value.marker != val_out.marker) , 
               "Value found != val_out for key %i\n", rank);

    ERROR_OUT(local_update2(&hash, rank, 2, &err_string),
              "local_update2 error: '%s' code: %i\n", err_string, err);

    value.marker=2;
    ERROR_OUT(sharp_hash_find(&hash, &rank, NULL, &val_out),
              "sharp_hash_find failed to find key %i\n", rank);

    ERROR_OUT(value.value != val_out.value,
              "Rank: %i Value %i does not match expected value %i \n",
              rank, value.value, val_out.value);

    ERROR_OUT(value.rank_saved != val_out.rank_saved,
              "Rank: %i rank_saved %i does not match expected value %i \n",
              rank, value.rank_saved, val_out.rank_saved);

    ERROR_OUT(value.marker != val_out.marker,
              "Rank: %i marker %i does not match expected value %i \n",
              rank, val_out.marker, value.marker);

    /* now delete the thing */
    ERROR_OUT(local_delete2(&hash, rank, &err_string),
              "Failed local2 delete: '%s' key: %i\n",err_string,rank);

    /* expect a failure in lookup */
    ERROR_OUT(SHARP_KEY_NOT_FOUND != sharp_hash_find(&hash, 
                                                     &rank, 
                                                     NULL, 
                                                     &val_out),
              "Found key unexpectidly after local2 delete %i\n", rank);

    err = error_agree(err);
    if (err) { 
        goto failure;
    }

    mprint("Passed test_neighborhood_iteration test2\n");
    return err;

finish:
    err = error_agree(err);

failure:
    mprint("Failed test_neighborhood_iteration test2, sharp error: %s\n", sharp_error_string(err));
    return err;
}

static inline int check_local_keys(sharp_object_t *hash, int  expect) 
{
    int err,out=0;
    int found = 0;
    size_t local_blocks = sharp_hash_get_local_block_count(hash);

    int keys[sharp_hash_get_items_per_block(hash)];
    int values[sharp_hash_get_items_per_block(hash)];
 
    for (size_t i = 0; i < local_blocks; i++) {
        int count;
        err = sharp_hash_get_local_key_value_pairs(hash, 
                                                   i, 
                                                   keys, 
                                                   values, 
                                                   &count);
        if (err) {
            fflush(stdout);
            fprintf(stderr,
                    "Local iteration failed on rank %i block %lu str: %s\n",
                    rank, i, "sharp_error_string(err)");
            out = 1;
            count = 0;
        }
        found += count;

        for (int j = 0; j < count; j++) {
            if (get_rank_for_key(keys[j],size) != values[j]-1) {
                fflush(stdout);
                fprintf(stderr,
                        "Rank %i: Key %i expected to go with value %i not %i "
                        "as found\n",
                        rank,
                        keys[j],
                        get_rank_for_key(keys[j],size),
                        keys[j]);
                out = 1;
            }
        }
    }

    out = error_agree(out);

    int total_in_hash;
    MPI_Allreduce(&found,
                  &total_in_hash, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    if (total_in_hash != expect) {
        mprint("expected %i (global) keys, found %i\n",
                expect,
                total_in_hash);
        out = 1;
    }
        
    return out;
}


int basic_test_given_hash(sharp_object_t *hash)
{
    int err, out, ret;
    struct timespec begin = {0}, end = {0};
    const int time_str_len = 1024;
    char time_str[time_str_len];

    resolution(&end);
    mprint("Blocks per pe : %lu\n", hash->sobject.shash.blocks_per_pe);
    mprint("%zu %i %zu\n", sharp_hash_get_local_block_count(hash),
           size,
           sharp_hash_get_items_per_block(hash));

    mprint("Total slots(global): %lu\n",
           sharp_hash_get_local_block_count(hash)*
           (size_t)size*
           sharp_hash_get_items_per_block(hash));
    mprint("Clock resolution: %s\n", time_diff_to_string(begin,end,time_str, time_str_len));

    now(&begin);
    barrier();
    int keys_in;

    /* since keys_in starts at 0 and ends when an error occurs inserting it
     * contains the actual number of keys in the array, not one more than */
    for (keys_in = 0; keys_in < INT_MAX; keys_in++) {
        int value = rank + 1;
        int bob = get_key(rank,size,keys_in);
        if (sharp_hash_add_unique(hash, &bob, &value)) {
            fflush(stdout);
            fprintf(stderr, "Rank: %i Inserted %i keys\n",rank,  keys_in);
            break;
        } 
        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, &value)) {
            fflush(stdout);
            fprintf(stderr,
                    "rank %i Could not find key %i/%i/%lu\n",
                    rank,
                    bob,
                    keys_in,
                    hash_fun(&bob)%256);
        }

    }

    now(&end);
    barrier();
    
    /* output some useful statistics */
    int global_insert_count;
    MPI_Allreduce(&keys_in,
                  &global_insert_count, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    if (rank == 0) {
        printf("Total keys inserted (global) %i\n", global_insert_count);
        printf("Load factor %f\n",
               global_insert_count /
               (double) (hash->sobject.shash.blocks_per_pe *
                         sharp_hash_get_items_per_block(hash)*
                         size));
    }


    output_time_info(begin, end, "Insert keys");

    out = check_local_keys(hash, global_insert_count);
    ret= out;
       
    if (rank == 0) {
        printf("post insert local iteration: %i\n", !out);
    }
    if (out) {
        goto finish;
    }

    barrier();
    now(&begin);

    int found = 0;
    //each rank finds its own keys
    for (int i = 0; i < keys_in; i++) {
        int key = get_key(rank, size, i);
        int value;
        err = sharp_hash_find(hash, &key, NULL, &value);
        if (err != SHARP_OK) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i could not find key %i index %i/%lu\n",
                    rank, key, i,hash_fun(&key)%256);
            continue;
        }
        if (value - 1 != rank) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i found bad value %i at index %i\n",
                    rank, value, i);
            continue;
        }
        found++;
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Look up keys");

    found = keys_in - found;

    ret = error_agree(out);
    out = ret;
    
    if (rank == 0) {
        printf("Insert and lookup success: %i\n", !out);
    }
    if (out) {
        goto finish;
    }

    out = check_local_keys(hash, global_insert_count);
    ret= out;
       
    if (rank == 0) {
        printf("post lookup local iteration: %i\n", !out);
    }
    if (out) {
        goto finish;
    }

    now(&begin);
    barrier();
    out=0;
    int keep_index = keys_in - keys_in / 2;
    for (int i = 0; i < keep_index; i++) {
        int bob = get_key(rank,size,i);

        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, NULL)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i next key to delete is already missing. key:%i\n",
                    rank, bob);
            out = 1;
            continue;
        }

        if (SHARP_OK != sharp_hash_delete(hash, &bob)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i failed to delete key %i index %i \n",
                    rank, bob, i);
            out = 1;
        }

        if (SHARP_OK == sharp_hash_find(hash, &bob, NULL, NULL)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i key %i was deleted but still reachable\n",
                    rank, bob);
            out = 1;
        }
    }
    barrier();
    now(&end);
    output_time_info(begin,end, "Delete half the keys");

    now(&begin);
    barrier();

    /* do lookups expect failure until <= keep_index */
    for (int i = 0; i < keep_index; i++) {
        int bob = get_key(rank,size,i);
        if (SHARP_OK == sharp_hash_find(hash, &bob, NULL, NULL)) {
            fflush(stdout);
            fprintf(stderr,
                    "line: %i Rank %i key %i was deleted but still reachable\n",
                    __LINE__, rank, bob);
            out = 1;
        }
    }

    /* do lookups expect failure until <= keep_index */
    for (int i = 0; i < keep_index; i++) {
        int bob = get_key(rank,size,i);
        if (SHARP_OK == sharp_hash_find(hash, &bob, NULL, NULL)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i key %i index %i was unexpectedly reachable\n",
                    rank, i, bob);
            out = 1;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Search the deleted keys");


    barrier();
    for (int i = keep_index; i < keys_in; i++) {
        int bob = get_key(rank,size,i);
        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, NULL)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i key %i index %i was not found after other keys "
                    "were deleted\n",
                    rank, i, bob);
            out = 1;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Search the remaining keys");

    out = error_agree(out);
    if (rank == 0) {
        printf("delete and lookup success: %i\n", !out);
    }
    if (out) {
        goto finish;
    }

    now(&begin);
    barrier();

    /* test updating pieces of the hash */
    for (int i = keep_index; i < keys_in; i++) {
        int val;
        int bob = get_key(rank,size,i);
        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, &val)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i failed to find key %i index %i for update test\n",
                    rank, bob, i);
            out = 1;
            continue;
        }
        if (val != rank + 1) {
            fflush(stdout);
            fprintf(stderr,
                    "%i -- Rank %i key index %i was %i when %i was expected\n",
                    __LINE__,rank, i, val, rank + 1);
            continue;
        }

        val = -val;
        if (SHARP_OK != sharp_hash_add_or_update(hash, &bob, &val)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i failed to update key %i index %i\n",
                    rank, bob, i);
            out = 1;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Update the remaining keys");
    now(&begin);
    barrier();


    for (int i = keep_index; i < keys_in; i++) {
        int val;
        int bob = get_key(rank,size,i);
        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, &val)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i failed to find key %i index %i for update test\n",
                    rank, bob, i);
            out = 1;
            continue;
        }
        if (val != -rank-1) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i expected value %i found instead of %i at index "
                    "%i\n",
                    rank, val, -rank, i);
            out = 1;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Find the updated keys");
    barrier();
    now(&begin);

    /* test inserting with update function */
    for (int i = 0; i < keys_in; i++) {
        int val = rank + 1;
        int bob = get_key(rank,size,i);
        if (SHARP_OK != sharp_hash_add_or_update(hash, &bob, &val)) {
            /* this is likely due to unpredictable filling  */
            keys_in = i;
            continue;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin,
                     end,
                     "Insert half the keys with the update function");
    barrier();
    now(&begin);

    /* do lookups expecting original values */
    for (int i = 0; i < keys_in; i++) {
        int val;
        int bob = get_key(rank,size,i);
        if (SHARP_OK != sharp_hash_find(hash, &bob, NULL, &val)) {
            fflush(stdout);
            fprintf(stderr,
                    "Rank %i key %i index %i was unexpectedly missing\n",
                    rank, i, bob);
            out = 1;
            continue;
        }
        if (val != rank + 1) {
            fflush(stdout);
            fprintf(stderr,
                    "%i -- Rank %i failed to find value %i index %i for update "
                    "test. Found %i\n",
                    __LINE__, rank, rank + 1, i, val);
            out = 1;
        }
    }

    barrier();
    now(&end);
    output_time_info(begin, end, "Search the updated keys after reinsertion");
    out = error_agree(out);
    if (rank == 0) {
        printf("update and lookup success: %i\n", !out);
    }
    if (out) {
        goto finish;
    }
    
    barrier();
    now(&begin);

    out = check_local_keys(hash, global_insert_count);
    
    barrier();
    now(&end);
    output_time_info(begin, end, "Local iteration");

finish:
    return out;
}

int test_alignment(size_t array_count, int items_per_block)
{
    int err, out;
    sharp_data_tier dtier[2];
    sharp_group_allocated_t ag1;
    sharp_object_t hash;

    int alignment = 12;
    array_count /= 20;


    size_t buffer_size =  sharp_hash_get_required_size(sizeof(int),
                                                       sizeof(int),
                                                       array_count,
                                                       size,
                                                       64,
                                                       items_per_block,
                                                       1 << alignment);
    mprint("Array size: %lu\n", array_count);
    mprint("buffer_size: %lu\n", buffer_size);

    /*** create data tiers ***/
    err = sharp_create_data_tier(dtier, 
                                 1,
                                 SHARP_HINT_CPU, 
                                 SHARP_ACCESS_INTERP, 
                                 buffer_size);
    assert(err >= 0);

    /*** create groups ***/
    err = sharp_create_group_allocate(buffer_size,
                                      SHARP_MPI,
                                      (void*)MPI_COMM_WORLD,
                                      size, &dtier[0], &ag1);
    if (err != SHARP_OK) {
        fflush(stdout);
        fprintf(stderr,
                "error(%d) on group1 %s:%d\n", err, __FILE__, __LINE__);
        return -1;
    }

    do {
        /*** attach hash ***/
        err = sharp_attach_hash(&ag1,
                                sizeof(int),
                                sizeof(int),
                                SHARP_HASH_CLEAR,
                                array_count,
                                hash_fun,
                                compare_fun,
                                64,
                                items_per_block,
                                1<<alignment,
                                &hash);
        mprint("%salignment=%i%s\n",cyan_text, 1<<alignment, reset_graphics);
        barrier();

        if (err != SHARP_OK) {
            mprint("Error attaching hash: %s\n", sharp_error_string(err));
            exit(1);
        }

        out = basic_test_given_hash(&hash);
        out = error_agree(out);
        mprint("Local iteration success %i\n",!out);
        if (out) {
            goto finish;
        }

    } while(alignment--);

finish:
    return out;
}

int basic_test(size_t array_count)
{
    int err, out;
    sharp_data_tier dtier;
    sharp_group_allocated_t ag1;
    sharp_object_t hash;


    size_t buffer_size = sharp_hash_get_required_size(sizeof(int),
                                                      sizeof(int),
                                                      array_count,
                                                      size,
                                                      64,
                                                      1,
                                                      1);
    mprint("Array size: %lu\n", array_count);
    mprint("buffer_size: %lu\n", buffer_size);

    /*** create data tiers ***/
    err = sharp_create_data_tier(&dtier, 
                                 1,
                                 SHARP_HINT_CPU, 
                                 SHARP_ACCESS_INTERP, 
                                 buffer_size);

    /*** create groups ***/
    err = sharp_create_group_allocate(buffer_size,
                                      SHARP_MPI,
                                      (void*)MPI_COMM_WORLD,
                                      size, &dtier, &ag1);
    if (err != SHARP_OK) {
        fflush(stdout);
        fprintf(stderr,
                "error(%d) on group1 %s:%d\n", err, __FILE__, __LINE__);
        return -1;
    }

    /*** attach hash ***/
    err = sharp_attach_hash(&ag1,
                            sizeof(int),
                            sizeof(int),
                            SHARP_HASH_CLEAR,
                            array_count,
                            hash_fun,
                            compare_fun,
                            64,
                            64,
                            1,
                            &hash);
    if (err != SHARP_OK) {
        fflush(stdout);
        fprintf(stderr, "Error attaching hash: %s\n", sharp_error_string(err));
        exit(1);
    }

    barrier();
    out = basic_test_given_hash(&hash);
    if (rank == 0) {
        printf("Local iteration success %i\n",!out);
    }
    if (out) {
        goto finish;
    }

finish:
    return out;

}

int main(int argc, char * argv[])
{
    MPI_Init(&argc, &argv);
    sharp_comms_init(argc, argv);
    int ret = 0;

    size_t array_count = 1ul * 1ul * 1000ul;

    if (argc == 2) {
        array_count = atoi(argv[1]);
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    printf("rank: %d, size: %d\n", rank, size);

    //MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

    if (sharp_init()) {
        fflush(stdout);
        fprintf(stderr, "Sharp Init failed\n");
        exit(1);
    }

    if (size == 1) {
        //XSLEEP;
    }


    barrier();
    ret = basic_test(array_count);
    if (ret) { 
        goto finish;
    }

#if 0
    barrier();
    ret = test_neighborhood_iteration(array_count);
    if (ret) goto finish;
#endif

    barrier();
    ret = test_neighborhood_iteration2(array_count);
    if (ret) { 
        goto finish;
    }

    for (int items_per_block = 64; items_per_block; items_per_block--) {
        barrier();
        int err;
        
        mprint("%sTesting with items_per_block = %s%i\n", cyan_text, reset_graphics, items_per_block);
        ret = test_alignment(array_count, items_per_block);
        ret = error_agree(ret);
        if (ret) {
            err = ret;
            mprint("Error with items_per_block == %i\n", items_per_block);
            goto finish;
        }
    }

finish:
    barrier();
    if (rank == 0) {
        printf("Test complete, success: %i\n", !ret);
    }
    sleep(1);
    sharp_comms_finalize();
    sharp_finalize();
    MPI_Finalize();
    return ret;
}
