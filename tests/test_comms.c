/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
/*
#include <ucs/debug/debug.h>

#include <comms/comms-include.h>
#include <sharp/sharp_errors.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <ucs/datastruct/list.h>
#include <ucs/sys/compiler_def.h>
*/

#include <sharp.h>

#include <unistd.h> 

#if COMM_MPI == 1
#include <mpi.h>
#else 
#define MPI_COMM_WORLD NULL
#endif

#define SIZE 1024

enum {
    COMMS_PUT,
    COMMS_GET,
    COMMS_PUT_NBI,
    COMMS_GET_NBI,
    COMMS_STOP
};

char *comms_values[5] = {"put", "get", "put_nbi", "get_nbi", "\0"};

int sharp_init(int argc, char ** argv) {
    int error;
    error = sharp_create_node_info();
    if (error != SHARP_OK) {
        return error;
    }
    error = sharp_comms_init(argc, argv);
    if (error != SHARP_OK) {
        printf("error on comm init\n");
        return error;
    }

    return 0;
}

void sharp_finalize(void) {
    sharp_comms_finalize();
    sharp_destroy_node_info();
}

static inline int test_range(long high,
                             size_t pe,
                             char flag,
                             sharp_group_allocated_t * ag)
{
    int i = 0;
    int buff;
    int * buff_array=NULL;
    int ret = 0;
    size_t offset = 0; 
  
    if (flag == COMMS_GET || flag == COMMS_GET_NBI || flag == COMMS_PUT_NBI) {
        buff_array = (int *)malloc(sizeof(int)*high);
        memset(buff_array, 0, sizeof(int)*high);
    }

    for (;i<high;i++, offset += sizeof(int)) {
        if (flag == COMMS_PUT) {
            buff = i;
            ret = sharp_comms_put(&buff, sizeof(int), offset, pe, ag->id);
        } else if (flag == COMMS_PUT_NBI) {
            buff_array[i] = i;
            ret = sharp_comms_put_nbi(&buff_array[i], sizeof(int), offset, pe, ag->id);
        } else if (flag == COMMS_GET) {
            ret = sharp_comms_get(&buff_array[i], sizeof(int), offset, pe, ag->id);
        } else if (flag == COMMS_GET_NBI) {
            ret = sharp_comms_get_nbi(&buff_array[i], sizeof(int), offset, pe, ag->id);
        }
        if (ret < SHARP_OK) {
            return -1;
        }            
    }
    
    if (flag == COMMS_PUT_NBI || flag == COMMS_GET_NBI) {
        sharp_comms_flush(ag->id);
        for (i = 0; i < high; i++) {
            if (buff_array[i] != i) {
                free(buff_array);
                return -1;
            }
        }
        ret = 0;
    }
    free(buff_array);
    return ret;      
}

static inline int test_wrapper(long local_low, 
                               long local_high,
                               size_t pe,
                               size_t size,
                               char flag,
                               sharp_group_allocated_t * ag) 
{
    int i = 0;
    int ret = 0;
    
    ret = test_range((local_high-local_low), pe, flag, ag);
    if (ret < 0) {
        return ret;
    }

    for (i = 0; i < size; i++) {
        if (i == pe)
            continue;
        ret = test_range((local_high-local_low), i, flag, ag);
        if (ret < 0) {
            break;
        }
    }
    if (ret < 0) {
        return ret;
    }

    return SHARP_OK;
}


int main(int argc, char ** argv) {
    int ret = 0;
    int rank = 0;
    int size = 0;
    sharp_data_tier dtier;
    sharp_group_allocated_t ag, reduce_ag;
    int error = 0;
    long local_low, local_high;
    long nr_per_pe;
    int * reduce_buffer;
    int i = 0;

    ret = sharp_init(argc, argv);
    if (ret != SHARP_OK) {
        return ret;
    }

    sharp_comms_get_pe(MPI_COMM_WORLD, &rank);
    sharp_comms_get_size(MPI_COMM_WORLD, &size);


    sharp_create_data_tier(&dtier, 1, SHARP_HINT_CPU, SHARP_ACCESS_INTERP, 1024);
    error = sharp_create_group_allocate(SIZE*sizeof(int), SHARP_MPI, MPI_COMM_WORLD, size, &dtier, &ag);
    if (error != SHARP_OK) {
        return error;
    }
    
    /* there's no typo on size */
    error = sharp_create_group_allocate(size*sizeof(int), SHARP_MPI, MPI_COMM_WORLD, size, &dtier, &reduce_ag);
    if (error != SHARP_OK) {
        return error;
    }

    nr_per_pe = SIZE / size;
    local_low = nr_per_pe * rank;
    local_high = (nr_per_pe * (rank+1));
    reduce_buffer = reduce_ag.buffer_ptr;

    for (i = 0;i<COMMS_STOP; i++) {
        if (rank == 0) {
            printf("testing %s locally and remotely...", comms_values[i]);
        }
        error = test_wrapper(local_low, local_high, rank, size, i, &ag);
        *reduce_buffer = error;
        sharp_comms_barrier(&ag);
        if (rank == 0) {
            int buff = 0;
            int j;
            for (j = 1; j < size; j++) {
                int loop_error;
                loop_error = sharp_comms_get(&buff, sizeof(int), 0, j, reduce_ag.id);
                if (loop_error != SHARP_OK) {
                    return loop_error;
                }
                error += buff;
            }
            if (error == 0) {
                printf("passed.\n");
            } else {
                printf("failed.\n");
                return -1;
            }
        }
        sharp_comms_barrier(&ag);
    }

    sharp_finalize();
    return ret;
}
