
#ifndef  test_allocator_utilities_INC
#define  test_allocator_utilities_INC

#include <allocator/sharp_allocator.h>
#include <time.h>
#include <utils/sharp/sharp_errors.h>



/* print a message to stderr including the file name and line number at the
 * beginning of the string. This should otherise be a drop in replacement for
 * fprintf to stderr assuming you have at least one extra argument to print */
#define eprintf(format, ...) fprintf (stderr, "%s:%i - " format, \
                              __FILE__, \
                              __LINE__, \
                              __VA_ARGS__ )

/* like the above macro, but it sill work for messages without arguments to
 * print */
#define eprintf0(message) fprintf(stderr, \
                                  "%s: %i - %s", \
                                  __FILE__, \
                                  __LINE__, \
                                  message);


/* *********************************************** */
#define z_assert( expr ) \
    do { \
        if( ! expr ){ \
            XSLEEP; \
            abort();\
        }\
    } \
    while(0)


int XSLEEP_gloabl_x = 1;
void signal_exit_XSLEEP(int sig){
    if(1 || sig){}
    XSLEEP_gloabl_x = 0;
}

#define XSLEEP do{ \
    static volatile int x=1;  \
    void * last = signal(SIGCONT, signal_exit_XSLEEP); \
    if( SIG_ERR == last ){ \
        perror("Failed to set signal hander for xsleep"); \
    } \
    printf("pid: %i waiting on debugger\n", getpid()); \
    while( x && XSLEEP_gloabl_x){ sleep ( 1 ); } \
    if( SIG_ERR == signal( SIGCONT, last)){ \
        perror("Failed restoring signal handler after XSLEEP");\
    } \
} while(0)






/* *********************************************** */
sharp_dev_t get_device_number();


/* *********************************************** */
typedef struct pointer_node{
    void * data;
    size_t bytes;
    ucs_list_link_t list;
} pointer_node_t;

/* *********************************************** */
int random_allocs(sharp_allocator_t *a,
                           ucs_list_link_t * node_list,
                           size_t base_size,
                           size_t max_chunk_multiple,
                           size_t early_cutoff);


/* ***************************************************** */
int allocate_until_failure(sharp_allocator_t *a,
                           ucs_list_link_t * node_list,
                           size_t base_size,
                           size_t early_cutoff);

/* ***************************************************** */
int free_all_memory( sharp_allocator_t *a, ucs_list_link_t *node_list);

/* ***************************************************** */
int test_list_allocations(ucs_list_link_t *node_list, int(*fun)(void*, size_t));

/* *********************************************** */
extern const clockid_t clock_id ;

/* *********************************************** */
int safe_long_parsing(char * str, long * output);

/* *********************************************** */
char * time_diff_to_string( struct timespec start,
                            struct timespec end,
                            char * str,
                            size_t len);

/* *********************************************** */
void resolution( struct timespec *res);

/* *********************************************** */
void now( struct timespec * t );


/* *********************************************** */
sharp_errors_t test_allocator( sharp_allocator_t *a, 
                              size_t block_size,
                              size_t max_random_blocks,
                              size_t early_cutoff,
                              int(*test_buffer_function)(void*,size_t));

/* *********************************************** */
int sharp_init(void);

/* *********************************************** */
void sharp_finalize(void);


/* ************************************************************** */
/*  print the PID and call abort to generate a core dump */
void signal_abort(int sig);

/* ************************************************************** */
/* replace signal handlers with one that is more useful than ucs's
 * see the signal_abort function for a description
 */
void install_signal_handlers();




#endif   /* ----- #ifndef test_allocator_utilities_INC  ----- */
