/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/




#include <allocator/sharp_allocator.h>
#include <assert.h>
#include <hwloc/cuda.h>
#include <hwloc/cudart.h>
#include <stdio.h>
#include <string.h>
#include "test_allocator_utilities.h"
#include "test_allocator_utilities.c"
#include <unistd.h>
#include <utils/sharp/sharp_errors.h>


/* print a more detailed listing of where the error occured */
static inline cudaError_t print_cuda_error_detailed( cudaError_t err,
                                                     char * file,
                                                     int line,
                                                     char * str){
    if( err != cudaSuccess ){
        if( !str  ){
            str = "";
        }
        const char * name = cudaGetErrorName(err);
        const char * string = cudaGetErrorString(err);
        fprintf(stderr,"%s::%i - %s: %s: %s\n",file, line, str, name,string);
    }
    return err;
}
#define PRINT_CUDA_ERROR_DETAILED(err, str) \
    print_cuda_error_detailed( err, __FILE__, __LINE__, str)





/* *********************************************** */
int test_buffer_usage(void * deviceptr, size_t len){
    int err = PRINT_CUDA_ERROR_DETAILED(cudaMemset( deviceptr, 1, len),"");
    if( err != cudaSuccess ){
        return 1;
    }

    uint8_t * cpu_ptr = malloc( len );

    err = PRINT_CUDA_ERROR_DETAILED( cudaMemcpy(cpu_ptr,
                                          deviceptr,
                                          len,
                                          cudaMemcpyDeviceToHost),
                               "");

    if( err != cudaSuccess ){
        free(cpu_ptr);
        return 1;
    }

    size_t sum=0;
    for( size_t i=0; i<len; i++){
        sum+= cpu_ptr[i];
    }
    free(cpu_ptr);

    return sum != len; //return 0 if they are the same
}



/* ***************************************************** */
int main(void) {
    int ret = 0;
    int error = 0;

    char buff[1024];
    struct timespec start, end;

    error = sharp_init();
    if( error !=  SHARP_OK){
        goto fail;
    }

    sharp_dev_t device;

    device = get_device_number(0);

    memset( &start, 0, sizeof(start));
    resolution( &end );

    printf("Clock resolution in seconds: %s\n",
           time_diff_to_string(start, end, buff, 1024));


    sharp_allocator_t *sharp_alloc = malloc(sizeof(sharp_allocator_t));

    sharp_allocator_type_t type = SHARP_ALLOCATOR_GMEM;

    if( SHARP_OK != sharp_allocator_init(&sharp_alloc, type, "", device) ){
        eprintf0("Failed to initilize the allocator\n");
        goto fail;
    }

    if( cudaSuccess != PRINT_CUDA_ERROR_DETAILED(cudaSetDevice(1),
        "Could not set cuda device in main, aborting")){
        goto fail;
    }

    now(&start);
    if( SHARP_OK != test_allocator( sharp_alloc,
                                    4096,
                                    1024,
                                    0,
                                    test_buffer_usage)){
        eprintf0("Failed the mass gpu malloc and free function\n");
        goto fail;
    }
    now(&end);


    printf("test successfull Total test time: %s\n",
           time_diff_to_string(start,end,buff,1024));
out:
    sharp_finalize();
    return ret;

fail:
    eprintf0("A failure has occured, aborting from main\n");
    ret = -1;
    goto out;
}

void usage(char * prog_name){
  FILE* where = stdout;
  fprintf(where, "usage: %s [numa [early_cutoff]]\n", prog_name);
  fprintf(where, "\n");
  fprintf(where, "numa is an integer that represents the numa node that memory is allocated on. The default is 0\n");
  fprintf(where, "early_cutoff is an integer in base 10 that represents how many blocks to allocate before quiting. The default is 0 which means that there will be no early cut off before there is a memory allocation failure\n");
}
