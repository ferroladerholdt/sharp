/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

/* print a message to stderr including the file name and line number at the
 * beginning of the string. This should otherise be a drop in replacement for
 * fprintf to stderr assuming you have at least one extra argument to print */
#define eprintf(format, ...) fprintf (stderr, "%s:%i - " format, \
                              __FILE__, \
                              __LINE__, \
                              __VA_ARGS__ )

/* like the above macro, but it sill work for messages without arguments to
 * print */
#define eprintf0(message) fprintf(stderr, \
                                  "%s: %i - %s", \
                                  __FILE__, \
                                  __LINE__, \
                                  message);

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <wait.h>

#include <ucs/debug/debug.h>

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <ucs/datastruct/list.h>
#include <allocator/sharp_allocator.h>
#include <allocator/sharp_allocator_pmem.h>
#include <ucs/debug/memtrack.h>
#include <string.h>
#include <utils/sharp/sharp_errors.h>
#include <libpmem.h>

#include "test_allocator_utilities.h"
#include "test_allocator_utilities.c"



/* configuration variables */
const int persistant_file_size = 256 * 1024 * 1024;
const int trash_size = 100;
const unsigned char set_trash_to=2;
char const * backing_file = "/tmp/bob";



/* non configuration variable */
const int buffSize=1024;

static uint32_t byte_sum( uint8_t *c, size_t len){
    uint32_t ret=0;
    size_t i;
#pragma omp parallel for reduction( +: ret)
    for( i=0; i<len; i++){
        ret += c[i];
    }
    return ret;
}

int test_buffer_usage(void * _ptr, size_t len){
    memset(_ptr, 1, len);
    uint32_t sum= byte_sum(_ptr, len);
    return sum != (uint32_t)len; //return 0 if they are the same
}

static void safe_sync(void *ptr, size_t len){
    if( pmem_is_pmem(ptr, len) ){
        pmem_persist(ptr, len);
    } else {
        if( pmem_msync(ptr,len) ){
            perror("Safe sync failed");
        }
    }
}


int test_reset(){
    int err;


    err = sharp_init();
    if( err !=  SHARP_OK){
        goto fail;
    }

    sharp_dev_t device = get_device_number(0);
        
    /* make sure a new allocator does not change the range from the other one */
    sharp_allocator_t *a2;
    a2 = malloc(sizeof(sharp_allocator_t));
    if(! a2){
        eprintf0("Failed to allocate the sharp_allocator_t\n");
        goto fail;
    }
    err = sharp_allocator_init(&a2, SHARP_ALLOCATOR_PMEM, "", device);
    install_signal_handlers(); /* hopefully override ucs's signal handlers */
    err = sharp_allocator_pmem_init2(a2,
                                     backing_file,
                                     persistant_file_size);
    if( err != SHARP_OK ){
        eprintf0("Could not reinitilize another allocator");
        goto fail;
    }

    void *trash2;
    err = a2->ops->chunk_alloc(a2, trash_size, &trash2);
    if( err != SHARP_OK){
        eprintf0("Error allocating trash array\n");
        goto fail;
    }

    unsigned int sum = byte_sum(trash2, trash_size);
    if( trash_size * set_trash_to != sum){
        eprintf("Expected byte_sum of %i, found %i\n", 
                trash_size*set_trash_to,
                sum);
        goto fail;
    }


    err = a2 -> ops -> chunk_alloc(a2, trash_size, &trash2);
    if( err != SHARP_ERR_UNSUPPORTED ){
        eprintf("SHARP_ERR_UNSUPPORTED expected, recieved: %i-%s\n",
                err,
                sharp_error_string(err));
        goto fail;
    }
    err = a2 -> ops -> chunk_memalign(a2, trash_size, &trash2, 256);

    if( err != SHARP_ERR_UNSUPPORTED ){
        eprintf("SHARP_ERR_UNSUPPORTED expected, recieved: %i-%s\n",
                err,
                sharp_error_string(err));
        goto fail;
    }

    return 0;
fail:
    return 1;
}

int test_first(){
    int ret = 0;
    int error = 0;

    char buff[buffSize];
    struct timespec start, end;
    int err;

    error = sharp_init();
    if( error !=  SHARP_OK){
        goto fail;
    }

    sharp_dev_t device = get_device_number(0);
    
    memset( &start, 0, sizeof(start));
    resolution( &end );

    printf("Clock resolution in seconds: %s\n",
           time_diff_to_string(start, end, buff, 1024));


    sharp_allocator_t *sharp_alloc = malloc(sizeof(sharp_allocator_t));

    sharp_allocator_type_t type = SHARP_ALLOCATOR_PMEM;

    /* remove the backing file */
    err = unlink(backing_file);
    if( err != 0 && errno != ENOENT ){
        eprintf("Failed to unlink %s with system error: %s\n",
                backing_file,
                strerror(errno));
    }


    err = sharp_allocator_init(&sharp_alloc, type, "", device);
    install_signal_handlers(); /* hopefully override ucs's signal handlers */
    err = sharp_allocator_pmem_init2(sharp_alloc,
                                     backing_file,
                                     persistant_file_size);

    /* the allocator is not allowed to free the first chunk so go ahead and get
     * it so it won't be used lost */

    void * trash;
    sharp_alloc -> ops -> chunk_alloc(sharp_alloc, trash_size, &trash);
    assert(trash);
    memset(trash, set_trash_to, trash_size);
    safe_sync(trash, trash_size);

    if( SHARP_OK !=  err ){
        eprintf0("Failed to initilize the allocator\n");
        goto fail;
    }

    now(&start);
    if( SHARP_OK != test_allocator( sharp_alloc ,
                                    1024,
                                    1024,
                                    0,
                                    test_buffer_usage)){
        eprintf0("Failed the mass gpu malloc and free function\n");
        goto fail;
    }
    now(&end);

    
    printf("First access test successfull Total test time: %s\n",
           time_diff_to_string(start,end,buff,buffSize));
out:
    sharp_finalize();
    return ret;

fail:
    eprintf0("A failure has occured, aborting from main\n");
    ret = -1;
    goto out;
}

int main() {
    const int child = fork();
    if(child == -1){
        eprintf0("Could not fork, test aborted");
        return 2;
    }
    if (! child){
        int ret = test_first();
        return ret;
    } else {
        int status;
        int options=0;
        while( 1 ){
            if( child != waitpid(-1, &status, options) ){
                perror("");
                errno=0;
            }
            if(WIFEXITED(status) || WIFSIGNALED(status)){
                break;
            }
        }
        int ret = test_reset();
        if( ret ){
            eprintf0("Parrent testing failed\n");
        }

        if( WIFEXITED(status)  ){
            int temp;
            temp = WEXITSTATUS(status);
            if (temp){
                eprintf("Child exited with status %i. Test failed\n", temp);
                ret = 2;
            }
        }

        if( ! ret ){
            printf("Testing sucessfull\n");
        }
        return ret;
    }
}
