/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

/* print a message to stderr including the file name and line number at the
 * beginning of the string. This should otherise be a drop in replacement for
 * fprintf to stderr assuming you have at least one extra argument to print */
#define eprintf(format, ...) fprintf (stderr, "%s:%i - " format, \
                              __FILE__, \
                              __LINE__, \
                              __VA_ARGS__ )

/* like the above macro, but it sill work for messages without arguments to
 * print */
#define eprintf0(message) fprintf(stderr, \
                                  "%s: %i - %s", \
                                  __FILE__, \
                                  __LINE__, \
                                  message);

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <wait.h>

#include <sharp.h>
/*
#include <ucs/debug/debug.h>

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <ucs/datastruct/list.h>
#include <allocator/sharp_allocator.h>
#include <allocator/sharp_allocator_pmem.h>
#include <ucs/debug/memtrack.h>
#include <string.h>
#include <utils/sharp/sharp_errors.h>
*/

#include "test_allocator_utilities.h"
#include "test_allocator_utilities.c"



/* non configuration variable */
const int buffSize=1024;


extern sharp_node_info_t sharp_node;
extern ucs_list_link_t allocator_list;

static uint32_t byte_sum( uint8_t *c, size_t len){
    uint32_t ret=0;
    size_t i;
#pragma omp parallel for reduction( +: ret)
    for (i = 0; i < len; i++) {
        ret += c[i];
    }
    return ret;
}

int test_buffer_usage(void * _ptr, size_t len){
    memset(_ptr, 1, len);
    uint32_t sum= byte_sum(_ptr, len);
    return sum != (uint32_t)len; //return 0 if they are the same
}

/* ************************************************************** */
static int test_first(int numa_choice, int early_cutoff){
    int ret = 0;

    char buff[buffSize];
    struct timespec start, end;
    int err;

    err = sharp_init();
    if( err !=  SHARP_OK){
        goto fail;
    }

    memset( &start, 0, sizeof(start));
    resolution( &end );

    printf("Clock resolution in seconds: %s\n",
           time_diff_to_string(start, end, buff, 1024));


    sharp_allocator_list_item_t * allocator;
    int count = 0;
    ucs_list_for_each(allocator, &allocator_list, link) {
        if(count == numa_choice)
            break;
    }
    sharp_allocator_t * sharp_alloc = allocator->allocator;
    install_signal_handlers(); /* hopefully override ucs's signal handlers */
    
    now(&start);
    if( SHARP_OK != test_allocator( sharp_alloc, 
                                    1024,
                                    1024,
                                    early_cutoff,
                                    test_buffer_usage)){
        eprintf0("Failed the mass malloc and free function\n");
        goto fail;
    }
    now(&end);

    
    printf("First access test successfull Total test time: %s\n",
           time_diff_to_string(start,end,buff,buffSize));
out:
    sharp_finalize();
    return ret;

fail:
    eprintf0("A failure has occured, aborting from main\n");
    ret = -1;
    goto out;
}

const long early_cutoff_default=1000;

void usage(char * prog_name){
  FILE* where = stdout;
  fprintf(where, "usage: %s [numa [early_cutoff]]\n", prog_name);
  fprintf(where, "\n");
  fprintf(where, "numa is an integer that represents the numa node that memory is allocated on. The default is 0\n");
  fprintf(where, "early_cutoff is an integer in base 10 that represents how many blocks to allocate before quiting. The default is %li. A value of <= 0 will cause the allocator to continue until there has been an allocation failure.\n", early_cutoff_default);
}

int main(int argc, char * argv[]) {
    //fprintf(stderr, "PID()=%i\n", getpid());
    long choice=0;
    long early_cutoff=early_cutoff_default;

    switch(argc){
      case 3:
        if( safe_long_parsing(argv[2], &early_cutoff)){
            fprintf(stderr, "early_cutoff must be an integer in base 10\n");
            usage(argv[0]);
            return 1;
        }

      case 2:
        if( safe_long_parsing(argv[1], &choice)){
            fprintf(stderr, "Numa choice must be an integer in base 10\n");
            usage(argv[0]);
            return 1;
        }
        break;
      default:
        break;
    }

    int ret = test_first(choice, early_cutoff);
    
    if( ! ret ){
        printf("Testing sucessfull\n");
    }
    return ret;
}
