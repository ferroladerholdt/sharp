/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <ucs/debug/debug.h>
#include <comms/comms-include.h>
#include <sharp/sharp_errors.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sobjects/api/sharp_objects.h>
#include <sharp_maps/api/sharp_maps.h>
#include <ucs/datastruct/list.h>
#include <sharp-config.h>

#if COMM_MPI == 1
#include <mpi.h>
#else 
#define MPI_COMM_WORLD NULL
#endif

#include <unistd.h> 
#include <time.h>

#define SIZE    1024


int sharp_init(int argc, char ** argv) {
    int error;
    error = sharp_create_node_info();
    if(error != SHARP_OK) {
        return error;
    }
    error = sharp_comms_init(argc, argv);
    if(error != SHARP_OK) {
        printf("error on comm init\n");
        return error;
    }

    return 0;
}

void sharp_finalize(void) {
    sharp_comms_finalize();
    sharp_destroy_node_info();
}


/*
 * The idea behind this function is to recreate the same functionality as 
 * attach array and confirm the object is built correctly
 */
static inline int confirm_object_attributes(sharp_map_type_t map_type, 
                                            sharp_map_callback_t cb,
                                            sharp_array_elem_type_t datatype,
                                            size_t nr_elems,
                                            size_t low, 
                                            size_t high,
                                            sharp_group_allocated_t * ag,
                                            sharp_object_t * sobj) 
{
    size_t indices_per_pe = nr_elems / ag->nw_group->num_pes;
    int error = 0;

    error += (sobj->alloc_group == ag) - 1;
    error += (sobj->sobject.sarray.elem_type == datatype) - 1;
    error += (sobj->obj_id == (ag->object_id_counter - 1)) - 1;
    error += (sobj->sobject.sarray.nr_elements_total == nr_elems) - 1;
    error += (sobj->map_type == map_type) - 1 ;
    
    if (map_type == SHARP_UNIFORM_MAP) {
        error += (sobj->map.nr_indices_per_pe == indices_per_pe) - 1;
    } else {
        error += (sobj->map.cb == cb) - 1;
    }

    return error; 
}

static inline int put_elems(void *in_buf, sharp_array_elem_type_t elem_type,
                            size_t num_elems, size_t start_index,
                            sharp_object_t *sobj)
{
    int comms_error = 0;

    comms_error = sharp_darray_put_elems(in_buf, 
                                         elem_type, 
                                         num_elems, 
                                         start_index, 
                                         sobj);
    if(comms_error != SHARP_OK) {
        fprintf(stderr, "ERROR: failure in darray_put_elems related to the communication layer (%d:%s)\n", comms_error, sharp_error_string(comms_error));
        return comms_error;
    }
    return SHARP_OK;
}

static inline int get_elems(size_t start_index,
                            sharp_array_elem_type_t elem_type,
                            size_t num_elems, sharp_object_t *sobj,
                            void *outbuf)
{
    int comms_error = 0;

    comms_error = sharp_darray_get_elems(start_index, 
                                         elem_type, 
                                         num_elems, 
                                         sobj, 
                                         outbuf);
    if(comms_error != SHARP_OK) {
        fprintf(stderr, "ERROR: failure in darray_get_elems related to the communication layer (%d:%s)\n", comms_error, sharp_error_string(comms_error));
        return comms_error;
    }

    return SHARP_OK;
}

/* basically the following:
 *  0 => 1,3,5,7,9,...
 *  1 => 0,2,4,6,8,...
 */
int custom_map_single_stride(size_t index, size_t * low, size_t * high, size_t * index_offset)
{
    *low = *high = index;
    *index_offset = index / 2;
    if ((index & 0x1) == 0x1) { //an odd index
        return 0;
    } else {
        return 1;
    }
} 

int main(int argc, char ** argv) {
    int ret = 0;
    int my_pe = 0;
    int size = 0;
    sharp_data_tier dtier;
    sharp_group_allocated_t ag;
    int error = 0;
    int i;
    size_t low, high; 
    sharp_object_t sobj;
    int *put_array, *get_array;
    int comms_error = 0;


    ret = sharp_init(argc, argv);
    if(ret != SHARP_OK) {
        return ret;
    }

    sharp_comms_get_pe(MPI_COMM_WORLD, &my_pe);
    sharp_comms_get_size(MPI_COMM_WORLD, &size);
    sharp_create_data_tier(&dtier, 1, SHARP_HINT_CPU, SHARP_ACCESS_INTERP, 1024);
    error = sharp_create_group_allocate(SIZE*sizeof(int), SHARP_MPI, MPI_COMM_WORLD, size, &dtier, &ag);
    if(error != SHARP_OK) {
        return error;
    }

    error = sharp_attach_array(&ag, SHARP_CUSTOM_MAP, custom_map_single_stride, 
                               SHARP_INT, SIZE, &low, &high, &sobj);
    if(error != SHARP_OK) {
        return error;
    } 

    error = confirm_object_attributes(SHARP_CUSTOM_MAP,
                                      custom_map_single_stride,
                                      SHARP_INT,
                                      SIZE,
                                      (size_t) low, (size_t) high,
                                      &ag, &sobj); 
    if(error != 0) {
        return error;
    }

    /* array is attached successfully */
    /* ok, so tests should be in the following order:
     *  1) each PE writes to the full range of the array
     *  1.5) each PE gets the full range of array and tests
     *  2) each PE writes to only the first and last part of their local array
     *  3) each PE will incrementally write a buffer to the shared 
     *     data structure
     */
    if(my_pe < 2) {
  
        put_array = (int *)malloc(SIZE*sizeof(int));
        get_array = (int *)malloc(SIZE*sizeof(int));
        for(i=0;i<=SIZE; i++) {
            put_array[i] = my_pe;
        } 

        /* (1) and (1.5) */
        for(i=0;i<size;i++) {
            if(my_pe == i) {
                int j = 0;

                comms_error = put_elems(put_array, SHARP_INT, SIZE, 0, &sobj);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }

                comms_error = get_elems(0, SHARP_INT, SIZE, &sobj, get_array);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }

                for(j=0;j<SIZE;j++) {
                    if(put_array[j] != get_array[j]) {
                        fprintf(stderr,"put_array[%d] != get_array[%d] (%d != %d)\n", j, j, put_array[j], get_array[j]);
                        return SHARP_INTERNAL_ERROR;
                    }
                }
            }
            sharp_comms_barrier(&ag);
        }
    
        /* (2) */
        for(i=0;i<size;i++) {
            if(my_pe == i) {
                int put_val_front, put_val_back;
                int get_val_front, get_val_back;
            
                put_val_front = my_pe -1;
                put_val_back = my_pe + 1;
                if (my_pe&0x1) { 
                    low = 0;
                    high = SIZE-2;
                } else {
                    low = 1;
                    high = SIZE-1;
                }
                comms_error = put_elems(&put_val_front, SHARP_INT, 1, low, &sobj);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }
            
                comms_error = put_elems(&put_val_back, SHARP_INT, 1, high, &sobj);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }

                comms_error = get_elems(low, SHARP_INT, 1, &sobj, &get_val_front);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }

                comms_error = get_elems(high, SHARP_INT, 1, &sobj, &get_val_back);
                if(comms_error != SHARP_OK) {
                    return comms_error;
                }

                if(get_val_front != put_val_front ||
                   get_val_back != put_val_back) {
                    fprintf(stderr, "failed to retreive the same value placed\n");
                    return SHARP_INTERNAL_ERROR;
                }
            }
        }
        sharp_comms_barrier(&ag); /* we need to sync, but not in the for loop */

        /* (3) */
        for(i=0;i<size;i++) {
            if(my_pe == i) {
                int j = 0;
                int k = 0;
            
                for(j=0;j<SIZE;j++) {
                    put_array[j] = j; 
                }

                for(j=1;j<SIZE;j++) {
                    comms_error = put_elems(put_array, SHARP_INT, j, 0, &sobj);
                    if(comms_error != SHARP_OK) {
                        return comms_error;
                    }

                    comms_error = get_elems(0, SHARP_INT, j, &sobj, get_array);
                    if(comms_error != SHARP_OK) {
                        return comms_error;
                    }
                
                    for(k=0;k<j;k++) {
                        if(put_array[k] != get_array[k]) {
                            return SHARP_INTERNAL_ERROR;
                        }
                    }
                }
            }
        }
        sharp_comms_barrier(&ag); /* we need to sync, but not in the for loop */
        free(put_array);
        free(get_array); 
    } else {
        sharp_comms_barrier(&ag);
        sharp_comms_barrier(&ag);
        sharp_comms_barrier(&ag);
    }
    printf("test completed @ PE %lu\n", comms_my_pe);
    sharp_finalize();
    return ret;
}
