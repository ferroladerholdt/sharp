#ifndef __TESTS_COMMON_H
#define __TESTS_COMMON_H

#include <ucs/datastruct/list.h>
#include <string.h>

extern sharp_node_info_t sharp_node;

int sharp_init(void) {
    return sharp_create_node_info();
}

void sharp_finalize(void) {
    sharp_destroy_node_info();
}

static inline const char * md_type_to_string(sharp_md_type_t type) {
    if(type == SHARP_MD_DDR) {
        return "DDR";
    } else if(type == SHARP_MD_HBM) {
        return "HBM";
    } else if(type == SHARP_MD_NVRAM) {
        return "NVRAM";
    } else if(type == SHARP_MD_L1i) {
        return "L1 instruction cache";
    } else if(type == SHARP_MD_L1d) {
        return "L1 data cache";
    } else if(type == SHARP_MD_L2) {
        return "L2 cache";
    } else if(type == SHARP_MD_L3) {
        return "L3 cache";
    } else {
        return "UNKNOWN";
    }
}

static inline const char * md_access_to_string(sharp_md_mem_access_t access) {
    if(access == LOCAL) {
        return "LOCAL";
    } else if(access == NODE_R_ONLY) {
        return "NODE R ONLY";
    } else if(access == NODE_RW) {
        return "NODE RW";
    } else if(access == REMOTE_R_ONLY) {
        return "REMOTE R ONLY";
    } else if(access == REMOTE_RW) {
        return "REMOTE RW";
    } else {
        return "UNKNOWN";
    }
}

static inline const char * hint_to_string(sharp_hint_t hint) {
    if(hint == SHARP_HINT_CPU) {
        return "CPU Usage\n";
    } else if(hint == SHARP_HINT_GPU) {
        return "GPU Usage\n";
    } else if(hint == SHARP_HINT_LATENCY_OPT 
              || hint == SHARP_HINT_BANDWIDTH_OPT) {
        return "Network optimal\n";
    } else {
        return "Unknown usage\n";
    }
}

static inline void print_data_tier(sharp_data_tier * data_tier)
{
    int i = 0;
    printf("\n*** Check your data tier with %d mdomains ***\n",
            data_tier->nr_mds);

    printf("mds_ids: ");
    for(;i<data_tier->nr_mds;i++) {
        printf("%d ", data_tier->md_ids[i]);
    }
    printf("\n");

    printf("constraints (high)0x%lx (low)0x%lx\n", 
        (unsigned long) ((data_tier->constraints)>>64),
        (unsigned long) (data_tier->constraints));
    printf("hint: %s\n", hint_to_string(data_tier->hints));
}

static inline void clear_data_tier(sharp_data_tier * data_tier) {
    memset(data_tier, 0, sizeof(sharp_data_tier));
}

static inline int machine_has_gpu(void) {
    sharp_dev_item_t * device;
    
    ucs_list_for_each(device, sharp_node.dev_l, next_p) {
        if(device->device.type == SHARP_DEV_CUDA
           || device->device.type == SHARP_DEV_PHI
           || device->device.type == SHARP_DEV_OPENCL) {
            return 1;
        }
    }
    return 0;
}

#endif
