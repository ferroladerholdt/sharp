#!/bin/sh
###
# Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
###

###
# Test if memkind's autogen.sh is there, if it is add VERSION 
# in external/memkind
###
if [ ! -f external/memkind/VERSION ] && [ -f external/memkind/configure.ac ]; then
    echo "0.0.0" > external/memkind/VERSION
fi

autoreconf -ifv

# other commands below
