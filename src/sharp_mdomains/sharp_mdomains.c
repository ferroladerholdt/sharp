/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#include <hwloc.h>
#include <hwloc/helper.h>

/* for debugging */
#include <ucs/debug/debug.h>
#include <ucs/debug/memtrack.h>

/* for the list */
#include <ucs/datastruct/list.h>

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <allocator/sharp_allocator.h>


/**
 * @ingroup SHARP_INTERNAL
 * @brief The distance matrix between all memory domains within the system 
 */
float * sharp_distance_matrix = NULL;

/**
 * @ingroup SHARP_INTERNAL
 * @brief The global node information structure
 */
sharp_node_info_t sharp_node;


UCS_LIST_HEAD(device_list);
UCS_LIST_HEAD(memory_list);
UCS_LIST_HEAD(allocator_list);

/* we need to keep this for the memory allocator at a later point */
hwloc_topology_t sharp_topology;

/* 
    from hwloc/src/bitmap.c 
    * We need this to obtain a snapshot of a NUMA's cpuset *
*/
struct hwloc_bitmap_s {
    unsigned ulongs_count;
    unsigned ulongs_allocated;
    unsigned long * ulongs;
    int infinite;
#ifdef HWLOC_DEBUG
    int magic;
#endif
};

/* 
    some function prototypes for ease of reference (in order of appearance)
    these are functions not included sharp_mdomains_md.h
*/
static inline void set_device_locality(sharp_dev_t *, 
                                       hwloc_obj_t);
static inline sharp_md_list_item_t * add_memory_domain(size_t, 
                                             sharp_md_type_t, 
                                             sharp_md_mem_access_t, 
                                             int, 
                                             int);
static inline void gather_device_info(int *, 
                                      int *, 
                                      hwloc_obj_t);
static inline void build_numa(int, 
                              int *, 
                              hwloc_obj_t);
static inline int discover_topology(void);

/**
 * @ingroup SHARP_INTERNAL
 * @brief
 * Determine and set the locality of a particular device with respect to a 
 * CPU. This is necessary in order to determine memory domain to memory 
 * domain locality. More specifically, the distance between a particular 
 * memory and the NIC or GPU.
 *
 * @param [in]  dev         The device
 * @param [in]  obj_parent  The great grandparent of the device 
 * returns nothing
*/
static inline void set_device_locality(sharp_dev_t * dev,                
                                       hwloc_obj_t obj_parent) 
{
    /* 
        lineage of an OS device if attached to a numa node:
        os device -> pci device -> pci bridge -> numa -> machine
		
        if it's not attached, then the pci bridge will connect to the 
        machine
    */
    hwloc_obj_t obj = obj_parent;
    for (;obj != NULL;obj = obj->parent) {
        if(obj->type == HWLOC_OBJ_NODE) {
            dev->numa_locality.device_locality = obj->os_index;
            return;
         }
    }
    dev->numa_locality.device_locality = -1;
}

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Adds a memory domain to the global memory domain list 
 *
 * @param [in] size                 The size of the memory domain in kB
 * @param [in] type                 The type of memory domain
 * @param [in] access               The accessibility of the memory domain
 * @param [in] md_id                The memory domain's ID
 * @param [in] device_id            The device connected to this memory 
 * @returns The added memory domain
 */
static inline sharp_md_list_item_t * add_memory_domain(size_t size, 
                                             sharp_md_type_t type, 
                                             sharp_md_mem_access_t access, 
                                             int md_id, 
                                             int device_id)
{
    sharp_md_list_item_t *memory = 
        (sharp_md_list_item_t *) ucs_malloc(sizeof(sharp_md_list_item_t));
    memory->mem_attrib = 0;

    sharp_md_set_size(memory->mem_attrib, size);
    sharp_md_set_type(memory->mem_attrib, type);
    sharp_md_set_access(memory->mem_attrib, access);
    sharp_md_set_md_id(memory->mem_attrib, md_id);
    sharp_md_set_device_id(memory->mem_attrib, device_id);

    if (type == SHARP_MD_NVRAM) {
        sharp_md_set_persistent(memory->mem_attrib);
    }
	
    /* persistent and near nic should be set separatly */
    ucs_list_add_tail(&memory_list, &(memory->next_p));

    return memory;
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Add a device to the global device list
 *
 * @param [in] md_id        The ID of the memory domain associated with the 
 *                          device 
 * @param [in] num_caches   The number of caches for this device
 * @param [in] device_id    The ID of this device
 * @param [in] type         The type of device
 * @param [in] obj          The hwloc object representing this device
 * returns the added device 
 */
static inline sharp_dev_item_t * add_device_item(int md_id,
                                                 int num_caches,
                                                 int device_id,
                                                 sharp_dev_type_t type,
                                                 hwloc_obj_t obj)
{
    sharp_dev_item_t * my_device = 
        (sharp_dev_item_t *) ucs_malloc(sizeof(sharp_dev_item_t));
    my_device->device.md_id = md_id;
    my_device->device.num_caches = num_caches;
    my_device->device.device_id = device_id;
    my_device->device.type = type;

    if(type > SHARP_DEV_CPU) {
        set_device_locality(&my_device->device, obj);
    } else {
        hwloc_bitmap_t cpuset = obj->cpuset;
        int i = 0;
        for(;i<cpuset->ulongs_count;i++) {
            unsigned long test = cpuset->ulongs[i];
            if(test) {
                my_device->device.numa_locality.locality.cpuset = 
                                                         cpuset->ulongs[i];
                my_device->device.numa_locality.locality.count_index = i;
            }
        }
    }
    ucs_list_add_tail(&device_list, &(my_device->next_p));
    return my_device;
}

static inline void add_device_item_nic(int * device_count,
                                       int * mem_count,
                                       hwloc_obj_t obj,
                                       hwloc_obj_t obj_parent)
{
    sharp_md_list_item_t *ptr;
    sharp_dev_item_t * my_device = NULL;

    if(obj->attr->osdev.type == HWLOC_OBJ_OSDEV_NETWORK) {
        my_device = add_device_item(0, 
                                    0, 
                                    *device_count, 
                                    SHARP_DEV_NIC,
                                    obj_parent);
    } else {
        my_device = add_device_item(0,
                                    0,
                                    *device_count,
                                    SHARP_DEV_FABRIC,
                                    obj_parent);
    }

    if(obj->attr->osdev.type == HWLOC_OBJ_OSDEV_OPENFABRICS && 
        my_device->device.numa_locality.device_locality >= 0) {
        /* find near memory and set near nic bit */
        ucs_list_for_each(ptr, &memory_list, next_p) {
            if(sharp_md_get_device_id(ptr->mem_attrib) 
                == my_device->device.numa_locality.device_locality) {
                sharp_md_set_near_nic(ptr->mem_attrib);
                break;
            } 
        }
    } 
	
    *device_count = *device_count + 1;

}

static inline void add_device_item_cuda(int * device_count,
                                        int * mem_count,
                                        hwloc_obj_t obj,
                                        hwloc_obj_t obj_parent)
{
    int z = 0;
    size_t size;

    add_device_item(*mem_count, 
                    1, 
                    *device_count, 
                    SHARP_DEV_CUDA, 
                    obj_parent);
    for(z=0;z<obj->infos_count;z++) {
        if(strncmp(obj->infos[z].name, "CUDAGlobalMemorySize", 20) == 0) { 
            /* main memory */
            size = strtoul(obj->infos[z].value, NULL, 10);

            add_memory_domain(size, 
                              SHARP_MD_HBM, NODE_RW, 
                              *mem_count, *device_count);
            *mem_count = *mem_count + 1;
            break;
        } else if(strncmp(obj->infos[z].name, "CUDAL2CacheSize", 15) == 0) {
            /* L2 Cache */
            size = strtoul(obj->infos[z].value, NULL, 10);
            add_memory_domain(size, 
                              SHARP_MD_L2, LOCAL, 
                              *mem_count, *device_count);
            *mem_count = *mem_count + 1;
            /* set shared cache here  */
            break;
        }
    }
    *device_count = *device_count + 1;
}

static inline void add_device_item_phi(int * device_count,
                                       int * mem_count,
                                       hwloc_obj_t obj,
                                       hwloc_obj_t obj_parent)
{
    int z = 0;
    size_t size;
    add_device_item(*mem_count,0,*device_count,SHARP_DEV_PHI,obj_parent);

    for(;z<obj->infos_count;z++) {
        /* no cache info from hwloc? */
        if(strncmp(obj->infos[z].name, "MICMemorySize", 13) == 0) {
            size = strtoul(obj->infos[z].value, NULL, 10);
            add_memory_domain(size, SHARP_MD_HBM,
                              NODE_RW, *mem_count,*device_count);
            *mem_count = *mem_count+1;
            break;
        } 
    }
    *device_count = *device_count + 1;
}

static inline void add_device_item_opencl(int * device_count,
                                          int * mem_count,
                                          hwloc_obj_t obj,
                                          hwloc_obj_t obj_parent)
{
    int z;
    size_t size;
    
    add_device_item(*mem_count,1,*device_count,SHARP_DEV_OPENCL,obj_parent);
    for(z=0;z<obj->infos_count;z++) {
        /* no cache info from hwloc? */
        if(strncmp(obj->infos[z].name, "OpenCLGlobalMemorySize", 22) == 0) {
            size = strtoul(obj->infos[z].value, NULL, 10);
            add_memory_domain(size, SHARP_MD_HBM,
                              NODE_RW,*mem_count,
                              *device_count); 
            *mem_count = *mem_count+1;
            break;
        } 
    }
    *device_count = *device_count + 1;
}

static inline void add_device_item_pmem(int * device_count,
                                        int * mem_count,
                                        hwloc_obj_t obj,
                                        hwloc_obj_t obj_parent)
{
    /* The rest of this function is handled by the caller */
    add_device_item(*mem_count, 0, *device_count, SHARP_DEV_PMEM, obj_parent);
    *device_count = *device_count + 1;
}

/* meant to gather information on OS devices (cuda0, mic0, etc.) */
static inline void gather_device_info(int *device_count,  
                                      int * mem_count, 
                                      hwloc_obj_t obj)
{
    /* NUMA or MACHINE */
    hwloc_obj_t obj_parent;

    /* FIXME: explain the logic here. it's true in general (other than NVDIMM)
       but it needs to be documented */
    if(obj->parent->type != HWLOC_OBJ_MACHINE) {
        obj_parent = obj->parent->parent->parent; 
    } else {
        obj_parent = obj->parent;
    }

    /* eth0 or mlx4_0 */
    if(obj->attr->osdev.type == HWLOC_OBJ_OSDEV_NETWORK
       || obj->attr->osdev.type == HWLOC_OBJ_OSDEV_OPENFABRICS) { 
        add_device_item_nic(device_count, 
                            mem_count,
                            obj,
                            obj_parent);
    } else if(obj->attr->osdev.type == HWLOC_OBJ_OSDEV_COPROC) {
        if(obj->name[0] == 'c') { /* cuda0 */
            add_device_item_cuda(device_count,
                                 mem_count,
                                 obj,
                                 obj_parent);
        } else if(obj->name[0] == 'm') { /* mic0 */
            add_device_item_phi(device_count,
                                mem_count,
                                obj,
                                obj_parent);
        } else if(obj->name[0] == 'o') { /* opencl0d0 */
            add_device_item_opencl(device_count,
                                   mem_count,
                                   obj,
                                   obj_parent);
        }
    } else if(obj->attr->osdev.type == HWLOC_OBJ_OSDEV_BLOCK) {
        if(obj->name[0] == 'p') { /* pmem0 */
            unsigned long size = 0;
            /* NVDIMMS are not connected anyone in particular nor are they
               connected to a bridge */
            obj_parent = obj->parent->parent;
            
            /* convert to KB */
            size = 
                strtoul(obj->infos[0].value, NULL, 10) >> 10; 
            add_memory_domain(size, 
                              SHARP_MD_NVRAM, 
                              REMOTE_RW, 
                              *mem_count, 
                              *device_count);
            add_device_item_pmem(device_count,
                                 mem_count,
                                 obj,
                                 obj_parent);
            *mem_count = *mem_count+1;
        }
    }
}

/* @ingroup SHARP_INTERNAL
 * @brief This function gathers information related to a NUMA node (or CPU) 
 * including the NUMA node's logical cores and caches.
 
 * @param [in,out]	dev		The list of devices
 * @param [in,out]	mem		The list of memories
 * @param [in,out]	mem_count	The number of memories in the list
 * @param [in] 		obj		hwloc object related to the NUMA node
 * returns nothing
 */
static inline void build_numa(int device_id, int * mem_count, hwloc_obj_t obj) 
{
    int i = 0;
	
    if(obj->type >= HWLOC_OBJ_L1CACHE && obj->type <= HWLOC_OBJ_L3ICACHE) {
        sharp_md_list_item_t * memory;
        /* 
            this will assign cache level based on 
            SHARP_MD_L1i + number. This is a direct map
        */
        memory = add_memory_domain(obj->attr->cache.size, SHARP_MD_L1i + 
                                   obj->attr->cache.depth, LOCAL, 
                                   *mem_count, device_id);
		
        if(obj->arity > 1) {
            sharp_md_set_shared_cache(memory->mem_attrib);
        } /* it's already 0, no else needed */
        *mem_count = *mem_count + 1;
    }

    for(i=0;i<obj->arity;i++) {
        /* if 1 child is not a cache, none of the children are caches */
        build_numa(device_id, mem_count, obj->children[i]);
    }
}

static inline sharp_dev_item_t * get_device_from_list(int device_id) {
    sharp_dev_item_t * ptr;

    ucs_list_for_each(ptr, &device_list, next_p) {
        if(ptr->device.device_id == device_id) {
            break;
        }
    }

    return ptr;
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Populate the distance matrix of memory domains on the node, where 
 * distances represent the relative distance between memory domains with 
 * respect to latency
 *
 * @param [in] mem_count            The number of memory domains
 * @param [in] dev_count            The number of devices
 * @param [in] distances            hwloc's representation of distances
 * returns nothing
 */
static inline void populate_distance_matrix(int mem_count, 
                            int dev_count, 
                            const struct hwloc_distances_s * distances) 
{
    sharp_dev_item_t * dev;
    sharp_md_list_item_t * mem, * mem_inner;
    sharp_md_type_t type, inner_type;
	
    int i = -1;
    int j = 0;
    int row_device = 0;
    int col_device = 0;

    ucs_list_for_each(mem, &memory_list, next_p) {
        i++; j = 0;
        type = sharp_md_get_type(mem->mem_attrib);
        if(type == SHARP_MD_DDR || type >= SHARP_MD_L1i) {
            row_device = sharp_md_get_device_id(mem->mem_attrib);
        } else {
            int device_id = sharp_md_get_device_id(mem->mem_attrib);
            dev = get_device_from_list(device_id);
            if(dev->device.numa_locality.device_locality != -1) {
                row_device = dev->device.numa_locality.device_locality;
            } else {
                for(j = 0; j < mem_count; j++) {
                    sharp_distance_matrix[i*mem_count+j] = 1.0;
                }
                continue;
            }
        }
        ucs_list_for_each(mem_inner, &memory_list, next_p) {
            inner_type = sharp_md_get_type(mem_inner->mem_attrib);
            if(inner_type == SHARP_MD_DDR || inner_type >= SHARP_MD_L1i) {
                col_device = sharp_md_get_device_id(mem_inner->mem_attrib);
            } else {
                int device_id = sharp_md_get_device_id(mem_inner->mem_attrib);
                dev = get_device_from_list(device_id);
                if(dev->device.numa_locality.device_locality != -1) {
                    col_device = dev->device.numa_locality.device_locality;
                } else {
                    sharp_distance_matrix[i*mem_count+j] = 1.0;
                    j++;
                    continue;
                }
            }
            sharp_distance_matrix[i*mem_count+j] = 
                distances->latency[row_device * distances->nbobjs+col_device];
            j++;
        }	 
    }
}

/** 
 * @ingroup SHARP_INTERNAL
 * @brief This function discovers the topology of the system and places this 
 * infromation in the sharp_node global variable. 

 * @return The success of the function (0 for success)
 */
static inline int discover_topology(void) {
    hwloc_obj_t obj;
    hwloc_obj_type_t object_type;
    unsigned long flags = 0;
    int device_count, mem_count;
    const struct hwloc_distances_s * distances;
    int nr_numas = 0;

    /* 
    flags for use:
        HWLOC_TOPOLOGY_FLAG_WHOLE_SYSTEM;
    */
    flags = HWLOC_TOPOLOGY_FLAG_WHOLE_SYSTEM;

    hwloc_topology_init(&sharp_topology);
    hwloc_topology_set_flags(sharp_topology, flags);
    hwloc_topology_set_all_types_filter(sharp_topology, HWLOC_TYPE_FILTER_KEEP_ALL);
    hwloc_topology_load(sharp_topology);

    device_count = mem_count = 0;

    nr_numas = hwloc_get_nbobjs_by_type(sharp_topology, HWLOC_OBJ_NODE);

    if(nr_numas) {
        object_type = HWLOC_OBJ_NODE;
    } else {
        object_type = HWLOC_OBJ_MACHINE;
    }
        
    /* CPU/NUMA related information */
    for(
        obj = hwloc_get_next_obj_by_type(sharp_topology, 
                                         object_type, 
                                         NULL); 
        obj; 
        obj = hwloc_get_next_obj_by_type(sharp_topology, 
                                         object_type, 
                                         obj)) {
        /* count the number of new memories */
        unsigned int orig_mem_count = mem_count; 		
        size_t size = obj->memory.local_memory;
        sharp_dev_item_t * device = NULL;

        device = add_device_item(mem_count,
                                 0,
                                 device_count,
                                 SHARP_DEV_CPU,
                                 obj);

        /* convert size to kilobytes to be uniform across different 
           types of memory domains */
        add_memory_domain((size>>10), 
                          SHARP_MD_DDR, 
                          REMOTE_RW, 
                          mem_count, 
                          device_count);
        mem_count++;

        /* now find all the processing units and caches 
           associated to this CPU */
        build_numa(device_count, &mem_count, obj);
        device->device.num_caches = mem_count - orig_mem_count-1;
        device_count++;
    }
	 
    /* NIC/GPU/PHI related information */
    for(obj = hwloc_get_next_osdev(sharp_topology, NULL); 
        obj; 
        obj = hwloc_get_next_osdev(sharp_topology, obj)) {
        gather_device_info(&device_count, &mem_count, obj);
    }

    /* distance matrix */
    distances = hwloc_get_whole_distance_matrix_by_type(sharp_topology, 
                                                        HWLOC_OBJ_NODE);
    if(distances && distances->latency) { 
        /* create a mem_count x mem_count matrix */
        sharp_distance_matrix = 
            (float *)ucs_malloc(sizeof(float)*mem_count*mem_count);
        populate_distance_matrix(mem_count, 
                                 device_count, 
                                 distances);
    }

    return 0;
}

/* now for the stuff from mdomains.h */

/*
 * @brief Return a list of memory domains
 
 * @param [out] nr_domains	The number of memory domains in the list
 * @return 			The list of memory domains
 */
ucs_list_link_t * sharp_query_mds(int * nr_domains) {
	/* i'm skipping null pointer checks because only we call this */
    *nr_domains = ucs_list_length(&memory_list);
    return sharp_node.md_l;
}

/* 
 * @brief Simple external wrapper to the discover_topology function
 
 * @return Success of the call (0 is success, anything else is failure)
 */
int sharp_create_node_info(void) {
    int ret = 0;

    /* FIXME: this should really be in sharp_init not here */
    ucs_debug_init();

    /* TODO: add function to get this PE's cpuset for numa location */

    ret = discover_topology();

    sharp_node.dev_l = &device_list;
    sharp_node.md_l = &memory_list;

    /* init all of the allocators */
    sharp_allocator_init_all(&allocator_list);
    sharp_md_list_item_t * mem;
    sharp_allocator_list_item_t * item = ucs_list_next(&allocator_list, 
                                                     sharp_allocator_list_item_t, 
                                                     link);
    ucs_list_for_each(mem, sharp_node.md_l, next_p) {
        if(sharp_md_get_type(mem->mem_attrib) > SHARP_MD_NVRAM) {
            continue;
        } 
        mem->allocator = item->allocator;
        item = ucs_list_next(&item->link, sharp_allocator_list_item_t, link);
    }

    return ret;
}

/*
 * @brief Free the stored device and memory domain information

 * @return void
 */
void sharp_destroy_node_info(void) {
    sharp_dev_item_t * dev_ptr;
    sharp_md_list_item_t * mem_ptr;

    ucs_list_for_each(dev_ptr, &device_list, next_p) {
        ucs_free(dev_ptr);
    }

    ucs_list_for_each(mem_ptr, &memory_list, next_p) {
        ucs_free(mem_ptr);
    }	

    /* FIXME: this should be in sharp_finalize or equivalent */
    ucs_debug_cleanup();
}
