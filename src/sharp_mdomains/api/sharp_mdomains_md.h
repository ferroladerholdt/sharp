/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef __SHARP_MD_H
#define __SHARP_MD_H


/* 
	to remove implicit typeof
reasoning:
	the addition of list.h caused some versions of gcc to complain
	that typeof was being implicitly called. This removes that 
	complaint
*/
#define typeof __typeof__

#include <ucs/datastruct/list.h>

#include <stdlib.h>

/**
 * @defgroup SHARP_INTERNAL
 * All APIs should be placed in here. 
 */

/**
 * @defgroup SHARP_API
 * All functions/etc that are apart of the user facing API 
 * should be placed in this group.
 */

/**
 * @defgroup SHARP_DATATYPE 
 * All data structures should be placed in this group.
 */

/**
 * @ingroup SHARP_DATATYPE 
 * \brief Memory Domain type defining the type of memory a memory domain is. 
 */
typedef enum {
    SHARP_MD_DDR,           /**< \brief DDR memory */
    SHARP_MD_HBM,           /**< \brief HBM memory */
    SHARP_MD_NVRAM,         /**< \brief NVRAM memory */
    SHARP_MD_L1i,           /**< \brief L1 instruction cache memory */
    SHARP_MD_L1d,           /**< \brief L1 data cache memory */
    SHARP_MD_L2,            /**< \brief L2 cache memory */
    SHARP_MD_L3             /**< \brief L3 cache memory */
} sharp_md_type_t;

/** 
 * @ingroup SHARP_DATATYPE
 * \brief Device type defining the type of device a device is. 
 */
typedef enum {
    SHARP_DEV_CPU,          /**< \brief CPU device */
    SHARP_DEV_CUDA,         /**< \brief CUDA device */
    SHARP_DEV_OPENCL,       /**< \brief OpenCL device */
    SHARP_DEV_NIC,          /**< \brief NIC device */
    SHARP_DEV_FABRIC,       /**< \brief OpenFabric device */
    SHARP_DEV_PHI,          /**< \brief Intel Xeon Phi device */
    SHARP_DEV_PMEM          /**< \brief A Persistent Memory device */
} sharp_dev_type_t;

/**
 * @ingroup SHARP_DATATYPE
 * \brief Accessibility for memory domain
 */
typedef enum {
    LOCAL,              /**< Memory Domain is only accessible locally */
    NODE_R_ONLY,        /**< Node only. Zero copy read support for ON node */
    NODE_RW,            /**< Node only. Zero copy read write support */
    REMOTE_R_ONLY,      /**< Read only remotely accessible memory */
    REMOTE_RW           /**< Remotely accessible memory */
} sharp_md_mem_access_t;

/**
 * @ingroup SHARP_DATATYPE 
 * \brief Memory domain object, which is a 1-to-1 mapping to physical memories
 * on the node. 
 */
typedef __uint128_t	sharp_md_t;

/*                                                                              
layout of memory domain bits for sharp_md_t:                                                   
(lower bits)                                                                         
63                    31                   7    6     0                         
+----------------------+-------------------+----+-----+                         
|     Size             | shared cache info | NN | ATP |                         
+----------------------+-------------------+----+-----+                         
                                                                                
(1 bit)		NN = Near nic                                                                   
(7 bits)	ATP = Access, Type, Persistent (in that order)
(1 bit)		shared cache info
(23 bits)	reserved for future use
(32 bits)	size in KB
                                                                                
(upper bits)                                                                         
63             31      0                                                        
+--------------+-------+                                                        
|   Device ID  | MD ID |                                                        
+--------------+-------+                                                        

(32 bits)	memory domain ID
(32 bits)	device ID                                                      
*/

/* forward declaration */
struct sharp_numadomain_locality_t;
typedef struct sharp_numadomain_locality_t sharp_nd_locality_t;
struct sharp_allocator;

/**
 * @ingroup SHARP_DATATYPE
 * \brief Memory domain list, which is used for a linked list of memory 
 * domains.
 */
typedef struct sharp_md_list_item {
    sharp_md_t              mem_attrib; /**< The memory domain attributes */
    struct sharp_nd_locality_t *(*get_locality)(struct sharp_md_list_item *md); /**< Function returning the locality information for the domain */
    struct sharp_allocator  *allocator;
    ucs_list_link_t         next_p; /**< The next item in the linked list */
} sharp_md_list_item_t;

/** 
 * @ingroup SHARP_DATATYPE 
 * \brief This structure represents a NUMA node, which is a 
 * single CPU with multiple physical and logical cores. The 
 * cores represented by this data structure are logical 
 * cores, which are located on the physical cores. The caches 
 * are respective for each core and may be unified across 
 * multiple cores.
 */
struct sharp_numadomain_locality_t {
    unsigned long cpuset; /**< A snapshot of the HWLOC cpuset 
                               associated with this numa node */
    unsigned int count_index; /**< The index in HWLOC's cpuset */
};

/** 
 * @ingroup SHARP_DATATYPE
 * \brief This represents a physical device (i.e., CPU, GPU, or NIC) located
 * in the system. This structure contains the necessary information about the 
 * device to understand the device's attributes.  
 */
typedef struct sharp_dev {
    int md_id; /**< The memory domain associated with this device. */
    union {
        sharp_nd_locality_t locality; /**< The numa node information 
                                           associated with this device. */
        int device_locality; /**< The locality of this device to 
                                  another device */
    } numa_locality;
    int device_id; /**< The unique identifier associated with this device */
    int num_caches; /**< The number of caches associated with this device */
    sharp_dev_type_t type; /**< The type of device */
} sharp_dev_t;

/**
 * @ingroup SHARP_DATATYPE
 * \brief A linked list of all of the devices on this particular node
 */
typedef struct sharp_dev_item {
    sharp_dev_t device;     /**< A device */
    ucs_list_link_t next_p; /**< Linked list */
} sharp_dev_item_t;

/** 
 * @ingroup SHARP_DATATYPE 
 * \brief Each node will be represented as a list of devices and memories. 
 */
typedef struct sharp_node_info {
    ucs_list_link_t * dev_l;/**< The list of devices on the node 
                                 indexed by device id */
    ucs_list_link_t * md_l; /**< The list of memories on the node 
                                 indexed by memory domain id */
} sharp_node_info_t;


/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the size of the memory domain in kilobytes 

 * \param [inout]   _x      The memory domain being modified
 * \param [in]      _y      The size
 */
#define sharp_md_set_size(_x, _y)\
{\
    __uint128_t _tmp = (_y & 0xffffffff);\
    _tmp = (_tmp << 32);\
    _x |= _tmp;\
}

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the ID of the memory domain 

 * \param [inout]   _x      The memory domain being modified
 * \param [in]      _y      The ID
 */
#define sharp_md_set_md_id(_x, _y)\
{\
    __uint128_t _tmp = (_y & 0xffffffff);\
    _tmp = (_tmp << 64);\
    _x |= _tmp;\
}

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the access of the memory domain

 * \param [inout]   _x      The memory domain being modified
 * \param [in]      _y      The access enumeration
 */
#define sharp_md_set_access(_x, _y)\
{\
    uint8_t _tmp = (uint8_t)(_y & 0x7);\
    _tmp = _tmp << 4;\
    _x |= _tmp;\
}

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the type of the memory domain 

 * \param [inout]   _x      The memory domain being modified
 * \param [in]      _y      The type enumeration
 */
#define sharp_md_set_type(_x, _y)\
{\
    uint8_t _tmp = (uint8_t)(_y & 0x7);\
    _tmp = _tmp << 1;\
    _x |= _tmp;\
}

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the persistence indicator of the memory domain

 * \param [inout]   _x      The memory domain being modified
 */
#define sharp_md_set_persistent(_x)	(_x |= 1)

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the device ID of the memory domain 

 * \param [inout]   _x      The memory domain being modified
 * \param [in]      _y      The device id
 */
#define sharp_md_set_device_id(_x, _y)\
{\
    __uint128_t _tmp = (_y & 0xffffffff);\
    _tmp = (_tmp << 96);\
    _x |= _tmp;\
}

/**
 * @ingroup SHARP_INTERNAL
 * \brief Set the locality to the NIC in the memory domain

 * \param [inout]   _x      The memory domain being modified
 */
#define sharp_md_set_near_nic(_x)       (_x |= 0x80)

/**
 * @ingroup SHARP_INTERNAL 
 * @brief Set the shared cache information bit indicating this memory domain 
 * shares a cache with another memory domain
 *
 * @param [inout]   _x      The memory domain being modified 
 */
#define sharp_md_set_shared_cache(_x)   (_x |= (1<<8))

/** 
 * @ingroup SHARP_INTERNAL
 * \brief Retrieve the size from the memory domain

 * \param [in]  _x      The memory domain being used
 * \returns The size in kilobytes
 */
#define sharp_md_get_size(_x)       ((uint64_t) (_x >> 32) & 0xffffffff)

/** 
 * @ingroup SHARP_INTERNAL
 * \brief Retrieve the memory domain's ID 
 *
 * \param [in]  _x      The memory domain being used
 * \returns The memory domain's ID
 */
#define sharp_md_get_md_id(_x)      ((uint64_t) ((_x >> 64) & 0xffffffff))

/** 
 * @ingroup SHARP_INTERNAL
 * \brief Retrieve the memory domain's associated device ID

 * \param [in]  _x      The memory domain being used
 * \returns The device ID associated with the memory domain
 */
#define sharp_md_get_device_id(_x)  ((uint64_t) ((_x >> 96) & 0xffffffff))

/**
 * @ingroup SHARP_INTERNAL
 * @brief Retrieve the "near nic" bit from a memory domain
 *
 * @param [in] _x       The memory domain being used
 * @returns The "near nic" bit
 */
#define sharp_md_get_near_nic(_x)   ((uint32_t) ((_x & 0x80) >> 7))

/**
 * @ingroup SHARP_INTERNAL
 * @brief Retrieve the access attributes related to a memory domain
 *
 * @param [in] _x       The memory domain being used
 * @returns The access attributes of the memory domain
 */
#define sharp_md_get_access(_x)     ((uint8_t) (_x >> 4) & 0x7)

/**
 * @ingroup SHARP_INTERNAL
 * @brief Retrieve the type of memory domain
 *
 * @param [in] _x       The memory domain being used 
 * @returns The type of the memory domain
 */
#define sharp_md_get_type(_x)       ((uint8_t) (_x >> 1) & 0x7)

/**
 * @ingroup SHARP_INTERNAL
 * @brief Retrieve the persistent bit of the memory domain
 * 
 * @param [in] _x       The memory domain being used
 * @returns The persistent bit of the memory domain
 */
#define sharp_md_get_persistent(_x) ((uint8_t) _x & 0x1) 

/**
 * @ingroup SHARP_INTERNAL
 * @brief Retrieve the shared cache information from the memory domain
 *
 * @param [in] _x       The memory domain being used 
 * @returns The shared cache information 
 */
#define sharp_md_get_shared_cache(_x)   ((uint16_t) ((_x >> 8) & 0x1))

/**
 * @ingroup SHARP_INTERNAL 
 * \brief Query the memory domains found on the local node. 
 * \param [out] nr_domains		the number of memory domains being returned.	
 * \return A list of memory domains on success, NULL on error.
*/ 
ucs_list_link_t * sharp_query_mds(int * nr_domains);

/** 
 * @ingroup SHARP_INTERNAL
 * \brief Probe the topology of the local node and build the node information structure.
 * \return error code
*/
int sharp_create_node_info(void);

/** 
 * @ingroup SHARP_INTERNAL
 * \brief Free the memory used to store the local node information structure.
*/
void sharp_destroy_node_info(void);
#endif
