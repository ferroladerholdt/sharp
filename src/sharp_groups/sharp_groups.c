/**                                                                             
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.                    
*                                          
*
*/

#include <string.h>
#include <errno.h>

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sobjects/api/sharp_objects.h>
#include <allocator/sharp_allocator.h>
#include <helper.h>
#include <config.h>

#include <sharp/sharp_errors.h>

#include <ucs/debug/memtrack.h>

#if COMM_UCX == 1 || COMM_MPI == 1 || COMM_SHMEM == 1
#include <comms/comms-include.h>
#endif

/* external data */
extern sharp_node_info_t sharp_node;

unsigned int global_alloc_group_id_counter = 0;

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Creates a network group. The network group can be a 
 * MPI Communicator, OpenSHMEM active set, or Task groups.
 *
 * @param [in]  *group_info     A pointer to the network group element.
 * @param [in]  size            The size of the network group.
 * @param [in]  nw_type         The type of network group 
 * @param [out] *nw_group       The created network group
 *
 * @return error code.
 */
static inline int create_network_group(void * group_info, 
                                       size_t size,
                                       sharp_group_network_type_t nw_type,
                                       sharp_group_network_t * nw_group)
{
    nw_group->group_info = group_info;
    nw_group->num_pes = size;
    nw_group->group_type = nw_type;

    return SHARP_OK;
}

/**
 * @ingroup SHARP_INTERNAL 
 * @brief This function should call the local allocators for the 
 * respective memory domains and allocate the appropriate amount of bytes 
 * for the group.
 *
 * @param [in] size             The total size in bytes to allocate 
 * @param [in] group_size       The number of PEs in the allocation group
 * @param [in] data_tier_list   The data tiers on which to allocate the data
 * @returns The pointer to the allocated buffer. Returns NULL if memory is not
 * available.
 */
static inline void * alloc_object_memory(size_t size, 
                                         size_t group_size,
                                         sharp_data_tier * data_tier_list)
{
    void * ret = NULL;
    size_t size_per_pe = (size / group_size);
#if defined(COMM_SHMEM)
    ret = shmem_malloc(size_per_pe);
#else
    int i = 0;
    sharp_md_list_item_t * mem = NULL;
    int error = 0;

    for (i=0;i<data_tier_list->nr_mds;i++) {
        mem = get_mdomain_from_list_by_id(data_tier_list->md_ids[i]);

        error = mem->allocator->ops->chunk_alloc(mem->allocator, 
                                                 size_per_pe,
                                                 &ret);
        if (error == 0 && ret > 0) {
            return ret;
        }
    }
#endif
    return ret;
}



static inline int set_alloc_group_network(sharp_group_network_type_t nw_type,
                                          sharp_group_network_t * nw_group,
                                          sharp_group_allocated_t *alloc_group)
{
    int pe = 0;
#if COMM_UCX == 1 || COMM_MPI == 1 || COMM_SHMEM == 1
    sharp_comms_get_pe(nw_group->group_info, &pe);
#endif
    alloc_group->my_pe = pe;
    return SHARP_OK;
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Part of the allocation group creation process. This function performs
 * all the necessary work other than allocating memory for the object buffer
 *
 * @param [in] size                     The size in bytes for the object buffer
 * @param [in] network_group_type       The type of network group (MPI/SHMEM/etc.)
 * @param [in] * network_group_info     Reference to network group information
 * @param [in] network_group_size       The size of the network group
 * @param [in] group_size               The size of the allocation group
 * @param [in] * data_tier_list         A list of data tiers on which the 
                                        memory is allocated
 * @param [out] *alloc_group            The created allocation group
 * @returns An error code
 */
static inline int create_group(size_t size,
                               sharp_group_network_type_t network_group_type,
                               void * network_group_info,
                               size_t network_group_size,
                               sharp_data_tier * data_tier_list,
                               sharp_group_allocated_t *alloc_group)
{
    sharp_group_network_t * nw_group;

    nw_group = 
        (sharp_group_network_t *) ucs_malloc(sizeof(sharp_group_network_t));
    if (NULL == nw_group) {
        return SHARP_ERR_NO_MEMORY;
    }

    create_network_group(network_group_info, 
                         network_group_size,
                         network_group_type,
                         nw_group);

    alloc_group->nw_group = nw_group;
    alloc_group->data_tier_list = data_tier_list;
    alloc_group->alloc_size = size / network_group_size;
    alloc_group->map = NULL;
    alloc_group->object_id_counter = 0;

    set_alloc_group_network(network_group_type,
                            nw_group,
                            alloc_group);

    /* network related information */
    alloc_group->id = global_alloc_group_id_counter;
    global_alloc_group_id_counter++;

#if COMM_UCX == 1 || COMM_MPI == 1 || COMM_SHMEM == 1
    sharp_comms_register_buffer(alloc_group->buffer_ptr,
                                alloc_group->alloc_size,
                                (sharp_group_network_t *)nw_group,
                                alloc_group->id);
#endif
    return SHARP_OK;
}

/* simplified API */
int sharp_create_group(size_t size,
                       void * buffer,
                       sharp_group_network_type_t network_group_type,
                       void * network_group_info,
                       size_t network_group_size,
                       sharp_data_tier * data_tier_list,
                       sharp_group_allocated_t *alloc_group)
{
    int ret;
    alloc_group->buffer_ptr = buffer;
    ret = create_group(size,
                       network_group_type,
                       network_group_info,
                       network_group_size,
                       data_tier_list,
                       alloc_group);
    return ret;
}

/* simplified API */
int sharp_create_group_allocate(size_t size,
                                sharp_group_network_type_t network_group_type,
                                void * network_group_info,
                                size_t network_group_size,
                                sharp_data_tier * data_tier_list,
                                sharp_group_allocated_t *alloc_group)
{
    int ret;
  
    alloc_group->buffer_ptr = 
        alloc_object_memory(size, network_group_size, data_tier_list);

    if (alloc_group->buffer_ptr == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    ret = create_group(size,
                       network_group_type,
                       network_group_info,
                       network_group_size,
                       data_tier_list,
                       alloc_group);
    return ret;
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Initializes the hash used for holding the object to PE mappings
 *
 * @param [in] *alloc_group             The allocation group to hold the mapping
 * @returns A reference to the created hash
 */
static inline khash_t(object_maps) * init_map_hash(sharp_group_allocated_t * 
                                                    alloc_group)
{
    khash_t(object_maps) * hash = kh_init(object_maps);
    alloc_group->map = hash;   
    return hash;
}

static inline khash_t(obj_to_pe) * add_objectid_to_map(
                                                     khash_t(object_maps) * map, 
                                                     unsigned int id)
{
    int put_ret = 0;
    khiter_t iter;
    khash_t(obj_to_pe) * pe_map = NULL;

    iter = kh_put(object_maps, map, id, &put_ret);
    if (put_ret != -1) {
        pe_map = kh_init(obj_to_pe);
        kh_value(map, iter) = pe_map;
    }
    return pe_map;
}

static inline int add_pe_to_map(khash_t(obj_to_pe) * map,
                                unsigned int pe,
                                sharp_map_item_t * map_item)
{
    int put_ret = 0;
    khiter_t iter; 

    iter = kh_put(obj_to_pe, map, pe, &put_ret);
    if (put_ret != -1) {
        kh_value(map, iter) = map_item;
    }
    return 0; 
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Updates an allocation group's object to PE mapping information. 
 * This will allocate the allocation group's map if it does not exist and then
 * add the object to PE hash to the map based on an object ID key
 * 
 * @param [in] *alloc_group         The allocation group whose map is being 
 *                                  modified
 * @param [in] group_size           The size of the allocation group
 * @param [in] map_type             The type of data map being 
 *                                  used (i.e., Uniform or custom) 
 * @param [in] *map                 The data map for the object being attached
 *                                  to the allocation group
 * @param [in] nr_elems             The number of elements in the object
 * @param [out] *low                The local PE's low index
 * @param [out] *high               The local PE's high index
 * @param [in] *s_obj               The SharP object
 * @returns An error code 
 */
static inline int update_group_map(sharp_group_allocated_t * alloc_group,
                                   sharp_map_type_t map_type,
                                   sharp_map_callback_t cb,
                                   size_t nr_elems, 
                                   size_t * low, size_t * high,
                                   sharp_object_t * s_obj)
{
    int i = 0;
    int my_pe = alloc_group->my_pe;
    khash_t(object_maps) * map_table = NULL;
    khash_t(obj_to_pe) * pe_map = NULL; 
    unsigned int obj_id = s_obj->obj_id;
    size_t group_size = alloc_group->nw_group->num_pes;
    int indices_per_pe = s_obj->map.nr_indices_per_pe;
    //int next_low = 0; /* the current PE's high + 1 */

    /* we should never encounter an attachment of an object over top
     * of another object by id.. so i'm skipping this check */
    if (alloc_group->map == NULL) {
        /* this is an initial allocation only */
        map_table = init_map_hash(alloc_group);
    }
    /* add object id key to hash table */
    pe_map = add_objectid_to_map(map_table, obj_id);

    for (i = 0; i<group_size; i++) {
        sharp_map_item_t * map_item =
            (sharp_map_item_t *)ucs_malloc(sizeof(sharp_map_item_t));

        if (map_type == SHARP_UNIFORM_MAP) {
            if (i == my_pe) {
                *low = s_obj->sobject.sarray.low_index = i*indices_per_pe;
                *high = s_obj->sobject.sarray.high_index = (i+1)*indices_per_pe-1;
            }
            map_item->low = i*indices_per_pe;
            map_item->high = (i+1)*indices_per_pe-1; 
        } 
        add_pe_to_map(pe_map, i, map_item);
    }

    return SHARP_OK;
}

/* all PEs within alloc group need to call this */
int sharp_attach_array(sharp_group_allocated_t * alloc_group,
                       sharp_map_type_t map_type,
                       sharp_map_callback_t cb, 
                       sharp_array_elem_type_t elem_type, size_t nr_elems,
                       size_t *low_index, size_t *high_index, 
                       sharp_object_t * s_obj)
{
    /* 
     * check if local allocation is enough to hold this 
     * FIXME: make this check for custom map sizes and already allocated 
     * objects
     */

#if COMM_UCX == 1 || COMM_MPI == 1 || COMM_SHMEM == 1
    if (((nr_elems * elem_type) / alloc_group->nw_group->num_pes) 
        > alloc_group->alloc_size) {
        return SHARP_ERR_NO_MEMORY;
    } 
#endif
    
    s_obj->alloc_group = alloc_group;
    s_obj->sobject.sarray.elem_type = elem_type;
    s_obj->obj_id = alloc_group->object_id_counter;
    alloc_group->object_id_counter++;
    s_obj->sobject.sarray.nr_elements_total = nr_elems;
    s_obj->map_type = map_type;
    
    if (map_type != SHARP_UNIFORM_MAP) {
        s_obj->map.cb = cb;
    } else {
        unsigned int indices_per_pe = 
            nr_elems / alloc_group->nw_group->num_pes; 
        s_obj->map.nr_indices_per_pe = indices_per_pe;
    }

    update_group_map(alloc_group,
                     map_type,
                     cb,
                     nr_elems,
                     low_index, high_index,
                     s_obj);

    return SHARP_OK;
}

/* FIXME: complete... */
int sharp_attach_remote_array(unsigned int object_id,
                              sharp_group_allocated_t * alloc_group,
                              sharp_object_t * s_obj)
{


    return SHARP_ERR_UNSUPPORTED;
}

/* This should detach the array from the data object. However, we can 
simply remove the array from the data object */
int sharp_detach_array(sharp_object_t *s_obj, sharp_group_allocated_t * group)
{
    sharp_data_tier * dtier = group->data_tier_list;
    sharp_constraint_t constraints = dtier->constraints;
    khash_t(obj_to_pe) * pe_map;
    unsigned int obj_id = s_obj->obj_id;
    khiter_t iter, iter_tmp;

    if (sharp_constraint_get_interp_decomp(constraints) == 0 
       && sharp_constraint_get_interj_decomp(constraints) == 0) {
        /* remove local references and remove from map hash */
        s_obj->sobject.sarray.low_index = 0;
        s_obj->sobject.sarray.high_index = 0;
        s_obj->sobject.sarray.nr_elements_total = 0;
        s_obj->alloc_group = NULL;
        s_obj->obj_id = -1;
        s_obj->map.cb = NULL;

        iter_tmp = iter = kh_get(object_maps, group->map, obj_id);
        if (iter != kh_end(group->map)) {
            /* this means it's here */
            pe_map = kh_value(group->map, iter);
            for (iter = kh_begin(pe_map); iter != kh_end(pe_map); ++iter) {
                if (kh_exist(pe_map, iter)) {
                    sharp_map_item_t * map_item = kh_value(pe_map, iter);
                    kh_value(pe_map, iter) = NULL;
                    free(map_item);
                }
            } 
            /* destroy the mapping */
            kh_destroy(obj_to_pe, pe_map);
            /* now delete the mapping in the main map */
            kh_value(group->map, iter_tmp) = NULL;
        } else {
            /* we are trying to remove a mapping from an empty list */
            return SHARP_ERR_LAST; /* FIXME: make me meaningful */
        }
    } 

    /* 
     * We should not necessary do anything if decomposition is enabled. 
     * The reasoning for this is that the object may be reused and possibly
     * only have minor changes (i.e., map is custom rather than uniform)
     */
    
    return SHARP_OK;
}

/* have the hash zero the appropriate data. */
static inline void sharp_hash_clear_backing_data(struct sharp_object *s_obj) 
{
    sharp_group_allocated_t * alloc_group = s_obj->alloc_group;
    /* TODO make this smaller since only the block headers NEED initilization */
    memset(alloc_group->buffer_ptr, 0, alloc_group->alloc_size);
}

static inline int sharp_hash_do_allocation(sharp_group_allocated_t *alloc_group,
                                           size_t slots,
                                           sharp_object_t *hash)
{
    size_t key_size = hash->sobject.shash.key_size;
    size_t value_size = hash->sobject.shash.value_size;
    size_t group_size = alloc_group->nw_group->num_pes;
    sharp_hash_t *sh = &hash->sobject.shash;
    uint64_t bytes = sharp_hash_get_required_size(key_size,
                                                  value_size,
                                                  slots,
                                                  group_size,
                                                  sh->neighborhood_size,
                                                  sh->items_per_block,
                                                  sh->alignment);
    if (bytes > alloc_group->alloc_size * group_size) {
        return SHARP_ERR_NO_MEMORY;
    }

    hash->alloc_group = alloc_group;
    hash->obj_id = alloc_group->object_id_counter;
    alloc_group->object_id_counter++;
    return SHARP_OK;
}

/* PEs within alloc group need to call this */
int sharp_attach_hash(sharp_group_allocated_t * alloc_group,
                      size_t key_size,
                      size_t value_size,
                      sharp_hash_attach_options_t attach_option,
                      size_t slots,
                      uint64_t (*hash_fun)(void * key),
                      int (*compare_fun)(void *a, void *b),
                      int neighborhood_size ,
                      int items_per_block,
                      int alignment,
                      sharp_object_t * s_obj)
{
    sharp_hash_t * sh = &s_obj->sobject.shash;

    sh->key_size = key_size;
    sh->value_size = value_size;
    sh->hash_fun = hash_fun;
    sh->compare_keys = compare_fun;
    sh->neighborhood_size = neighborhood_size;
    sh->items_per_block = items_per_block;
    sh->alignment = alignment;

    if (sh->neighborhood_size < 1) return SHARP_REQUEST_UNSATISFIABLE;
    if (sh->key_size < 1) return SHARP_REQUEST_UNSATISFIABLE;
    if (sh->value_size < 0) return SHARP_REQUEST_UNSATISFIABLE;
    if (! sh->hash_fun) return SHARP_REQUEST_UNSATISFIABLE;
    if (! sh->compare_keys) return SHARP_REQUEST_UNSATISFIABLE;
    if (sh->items_per_block < 1) return SHARP_REQUEST_UNSATISFIABLE;
    if (sh->items_per_block > 64) return SHARP_REQUEST_UNSATISFIABLE;
    /* alignment must be a power of 2 or 0 */
    if (sh->alignment < 0) return SHARP_REQUEST_UNSATISFIABLE;
    if (! sh->alignment) return SHARP_REQUEST_UNSATISFIABLE;
    if ((sh->alignment - 1) & sh->alignment ) return SHARP_REQUEST_UNSATISFIABLE;
    size_t group_size = alloc_group->nw_group->num_pes;
    size_t base = sharp_hash_get_required_blocks(slots,
                                                 group_size,
                                                 items_per_block,
                                                 alignment,
                                                 neighborhood_size);
    s_obj->sobject.shash.blocks_per_pe = base;
    int err = sharp_hash_do_allocation(alloc_group,
                                       slots,
                                       s_obj);
    if(err) return err;

    if(SHARP_HASH_CLEAR == attach_option) {
        sharp_hash_clear_backing_data(s_obj);
    }

    s_obj->sobject.shash.attached = 1;
    return SHARP_OK;
}

/* Removing a hash from an object is mostly a nop, but the code below should
 * cause crashes insead of unpredictable behavior if the object is accessed
 * after detaching */
int sharp_detach_hash(sharp_object_t *s_obj, sharp_group_allocated_t * group)
{
    struct sharp_hash * sh = &s_obj->sobject.shash;

    sh->hash_fun = NULL;
    sh->compare_keys = NULL;
    sh->blocks_per_pe = 0;
    sh->neighborhood_size = 0;
    sh->attached = 0;
    sh->alignment = 0;
    sh->items_per_block = 0;

    return SHARP_OK;
}

/* user can call free on the allocation group afterwards */
void sharp_destroy_allocation_group(sharp_group_allocated_t * alloc_group)
{
    free(alloc_group->nw_group);
    free(alloc_group->buffer_ptr);
    kh_destroy(object_maps, alloc_group->map);
}
