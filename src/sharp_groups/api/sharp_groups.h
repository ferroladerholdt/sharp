/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */

#ifndef __SHARP_GROUPS_
#define __SHARP_GROUPS_

#include <ucs/sys/compiler_def.h>
#include <sharp_maps/api/sharp_maps.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <khash.h>

#include <stdint.h>

/* forward declaration */
struct sharp_object;
typedef enum sharp_hash_attach_options sharp_hash_attach_options_t;

static inline khint_t simple_hash(khint_t key) {
    return key;
}

/* f: moving to khash */
KHASH_INIT(obj_to_pe,
           uint32_t,
           sharp_map_item_t *,
           1,
           simple_hash,
           kh_int_hash_equal);

KHASH_INIT(object_maps,
           uint32_t,
           khash_t(obj_to_pe) *,
           1,
           simple_hash,
           kh_int_hash_equal);

/**
 * @ingroup SHARP_DATATYPE
 * @brief Enumeration of underlying network types 
 */
typedef enum {
    SHARP_MPI       = UCS_BIT(0), /**< MPI network */
    SHARP_SHMEM     = UCS_BIT(1), /**< OpenSHMEM network */
    SHARP_TSKGRP    = UCS_BIT(2)  /**< Task based network */ 
} sharp_group_network_type_t;

/** 
 * @ingroup SHARP_DATATYPE
 * @brief Enumeration of the possible data types for SharP objects 
 */
typedef enum {
    SHARP_INT = sizeof(int),
    SHARP_BYTE = sizeof(char),
    SHARP_CHAR = sizeof(char),
    SHARP_SHORT= sizeof(short),
    SHARP_LONG = sizeof(long),
    SHARP_FLOAT = sizeof(float),
    SHARP_DOUBLE = sizeof(double),
    SHARP_LONG_DOUBLE = sizeof(long double),
    SHARP_UNSIGNED_CHAR = sizeof(char),
    SHARP_SIGNED_CHAR = sizeof(char),
    SHARP_UNSIGNED_SHORT = sizeof(short),
    SHARP_UNSIGNED_LONG = sizeof(long),
    SHARP_UNSIGNED = sizeof(int)
} sharp_array_elem_type_t;

/**
 * @internal
 * @ingroup SHARP_INTERNAL_DATATYPE
 * @brief Network group of Underlying programming model 
 *
 */
typedef struct sharp_group_network {
    void                         *group_info; /**< Pointer to underlying 
                                                   network's group information */
    int                          num_pes;     /**< The number of PEs in the group */
    sharp_group_network_type_t   group_type;  /**< The type of the group */
} sharp_group_network_t;

/**
 * @ingroup SHARP_DATATYPE
 * @brief Sharp group with buffers allocated for storing datastructures 
 *
 */
typedef struct sharp_group_allocated {
    void                            *buffer_ptr;       /**< Pointer to the local allocate buffer */
    size_t                          alloc_size;        /**< Size of local allocation */
    int                             my_pe;             /**< OpenSHMEM rank/pe/index
                                                       similiar to network group,
                                                       this is only a cached **/
    sharp_group_network_t           *nw_group;         /**< Network group type **/
    sharp_data_tier                 *data_tier_list;   /**< List of data tiers **/
    khash_t(object_maps)            *map;              /**< SharP object map */
    unsigned int                    object_id_counter; /**< ID assigned to newly created objects */
    unsigned int                    id; /**< ID assigned to this allocation group */
} sharp_group_allocated_t;

/**
 * @ingroup SHARP_API
 * @brief Creates an allocation group with user
 * allocated memory. The allocation group uses the allocated memory for 
 * the data structure as well as maps onto the network group. 
 * The network group can be either a MPI communicator, OpenSHMEM active set, 
 * or Task groups.
 *
 * @param [in] size                 The size of the allocated memory
 * @param [in] *buffer              The allocated memory
 * @param [in] network_group_type   The programming model group type
 *                                  (MPI/OpenSHMEM/other)
 * @param [in] *network_group_info  Reference to the programming model's group
 * @param [in] network_group_size   The size of the group
 * @param [in] *data_tier_list      The list of data tiers to create data 
 *                                  structures
 * @param [out] *alloc_group        The group mapped onto the network group
 * @returns Error code.
 */
int sharp_create_group(size_t size,
                       void * buffer,
                       sharp_group_network_type_t network_group_type,
                       void * network_group_info,
                       size_t network_group_size,
                       sharp_data_tier * data_tier_list,
                       sharp_group_allocated_t *alloc_group);


/**
 * @ingroup SHARP_API
 * @brief Creates an allocation group with library
 * allocated memory. The allocation group creates space (memory, which is 
 * allocated by the library) for data structure as well as maps onto the 
 * network group. The network group can be either a MPI communicator,
 * OpenSHMEM active set, or Task groups.
 *
 * @param [in] size                 The size of the memory to be allocated
 * @param [in] network_group_type   The programming model group type
 *                                  (MPI/OpenSHMEM/other)
 * @param [in] *network_group_info  Reference to the programming model's group
 * @param [in] network_group_size   The size of the network group
 * @param [in] *data_tier_list      The list of data tiers to create data 
 *                                  structures
 * @param [out] *alloc_group        The group mapped onto the network group
 * @returns Error code.
 */
int sharp_create_group_allocate(size_t size,
                                sharp_group_network_type_t network_group_type,
                                void * network_group_info,
                                size_t network_group_size,
                                sharp_data_tier * data_tier_list,
                                sharp_group_allocated_t *alloc_group);

/**
 * @ingroup SHARP_API
 * @brief Attaches the array structure to already created data group. 
 * 
 *
 * @param [in]  alloc_group     The alloc group where the array has to be mapped. 
 * @param [in]  map_type        The type of map passed in
 * @param [in]  *cb             The custom mapping function (SHARP_MAP_NULL if 
                                uniform mapping is used) 
 * @param [in]  elem_type       Array element type 
 * @param [in]  nr_elems        The total number of elements in the array
 * @param [out] low_index       The lower index of the array local to the caller
 * @param [out] high_index      The higher index of the array local to the caller
 * @param [out] *s_obj          The sharp object created with the array
 *                              attached. 
 * @return Error code.
 */
int sharp_attach_array(sharp_group_allocated_t *alloc_group, 
                    sharp_map_type_t map_type, sharp_map_callback_t cb, 
                    sharp_array_elem_type_t elem_type, 
                    size_t nr_elems,
                    size_t *low_index, size_t *high_index, 
                    struct sharp_object *s_obj);

/**
 * @ingroup SHARP_API
 * @brief Attach data group to already created sharp object.
 *
 * @param [in] object_id        The object id of the remote object
 * @param [in] *data_group      The data group to attach to the remote object
 * @param [out] *s_obj          A local representation of the sharp object
 *
 * @return Error code.
 */
int sharp_attach_remote_array(unsigned int object_id, 
                              sharp_group_allocated_t * alloc_group,
                              struct sharp_object *s_obj);

/**
 * @ingroup SHARP_API
 * @brief Detaches the array structure from already created data group. 
 * 
 *
 * @param [in]  data_group      The data group where the array has to be mapped.
 * @param [in]  *s_obj          The sharp object to detached.
 * @return Error code.
 */ 

int sharp_detach_array(struct sharp_object *s_obj, 
                       sharp_group_allocated_t *group);

/**
 * @ingroup SHARP_API
 * @brief Attach an hash to the allocation group
 *
 * @param [in] alloc_group The group that will
 * @param [in] key_size    The size of 1 key in bytes
 * @param [in] value_size  The size of 1 value in bytes
 * @param [in] clear_hash  If set to SHARP_CLEAR_HASH the hash will clear any
 *                         data necissary for correct usage as an empty hash.
 *                         If set to SHARP_KEEP_HASH the current data from
 *                         the allocation group will be intrepreted as a
 *                         valid hash.
 * @param [in] slots       The minimum number of slots for the distributed hash
 * @param [in] hash_fun    The function used to hash keys
 * @param [in] compare_fun The function used to compare keys
 * @param neighborhood_size  The distance a key/value pair is allowed to be
 *                           placed from the location determined by the hash.
 *                           This can be increased safely if reattaching to
 *                           an already initilized hash, but it can not be
 *                           decreased. Increasing the value will often allow
 *                           the hash to achieve a higher load factor at the
 *                           cost of access time.
 */
int sharp_attach_hash(sharp_group_allocated_t * alloc_group,
                      size_t key_size,
                      size_t value_size,
                      sharp_hash_attach_options_t attach_option,
                      size_t slots,
                      uint64_t (*hash_fun)(void * key),
                      int (*compare_fun)(void *a, void *b),
                      int neighborhood_size ,
                      int items_per_block,
                      int alignment,
                      struct sharp_object * s_obj);



/**
 * @ingroup SHARP_API
 * @brief Destroys the allocation group freeing its associated memories.
 *
 *
 * @param [in]  alloc_group     The allocation group to be freed.
 * @return Void
 */

void sharp_destroy_allocation_group(sharp_group_allocated_t * alloc_group);

#endif 
