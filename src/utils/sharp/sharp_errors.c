/*
 * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
 */

#include "sharp_errors.h"

const char *sharp_error_string(sharp_errors_t error)
{
    switch (error) {
    case SHARP_OK:
        return "Success";
    case SHARP_ERR_NO_MEMORY:
        return "Out of memory";
    case SHARP_ERR_BIND_MEM_ERROR:
        return "Cannot bind the memory";
    case SHARP_ERR_NOT_IMPLEMENTED:
        return "The feature is not implemented";
    case SHARP_ERR_UNSUPPORTED:
        return "Unsupported operation";
    case SHARP_ERR_INVALID_FREE_ATTEMPT:
        return "The free operation has failed";
    case SHARP_CUDA_ERROR:
        return "Cuda returned an error in the process of compleating a request";
    case SHARP_ERR_FAILED_OPENING_BACKING_FILE:
        return "Could not open the backing file";
    case SHARP_ERR_BACKING_FILE_TOO_LARGE:
        return "The backing file was too large";
    case SHARP_ERR_UNKNOWN_ERROR:
        return "An error occured that sharp was not prepared to handel";
    case SHARP_INVALID_MEMORY_RANGE:
        return "A memory range was specified that is invalid";
    case SHARP_ERR_BAD_EXPECTED_VALUE:
        return "The expected value was not the value that was found";
    case SHARP_COMM_FAILURE:
        return "The underlying communication layer has had a failure";
    default:
        return "Unknown error";
    };
}
