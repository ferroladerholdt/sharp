/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */

#ifndef SHARP_TYPES_ERRORS_H_
#define SHARP_TYPES_ERRORS_H_

typedef enum {
    SHARP_OK                         =   0,

    /* Failure codes */
    SHARP_ERR_NO_MEMORY              =  -1,
    SHARP_ERR_BIND_MEM_ERROR         =  -2,
    SHARP_ERR_NOT_IMPLEMENTED        =  -3,
    SHARP_ERR_UNSUPPORTED            =  -4,
    SHARP_ERR_INVALID_FREE_ATTEMPT   =  -5,
    SHARP_CUDA_ERROR                 =  -6,
    SHARP_ERR_FAILED_OPENING_BACKING_FILE = -7,
    SHARP_ERR_BACKING_FILE_TOO_LARGE =  -8,
    SHARP_INVALID_MEMORY_RANGE       =  -9,
    SHARP_ERR_UNKNOWN_ERROR          = -10,
    SHARP_INTERNAL_ERROR             = -11,
    SHARP_ERR_BAD_EXPECTED_VALUE     = -12,
    SHARP_COMM_FAILURE               = -13,
    SHARP_ERR_COMM_MISSING           = -14,
    SHARP_EXCHANGE_FAILURE           = -15,
    SHARP_KEY_NOT_FOUND              = -16,
    SHARP_COULD_NOT_FIT_KEY          = -17,
    SHARP_REQUEST_UNSATISFIABLE      = -18,
    SHARP_ERR_NO_MDS                 = -19,

    SHARP_ERR_LAST                   = -100
} sharp_errors_t ;

/**
 * @param  status SHARP Error code.
 *
 * @return Verbose Error message.
 */
const char *sharp_error_string(sharp_errors_t error);

#endif

