/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */

#ifndef __SHARP_OBJECTS_
#define __SHARP_OBJECTS_

#include <sharp_groups/api/sharp_groups.h>

/**
 * @ingroup SHARP_DATATYPE
 * @brief Enumeration of SHARP datatypes 
 *
 */
typedef enum sharp_datatype {
    SHARP_ARRAY,                /**< SharP Arrays */
    SHARP_HASHTABLE,            /**< SharP Hashes */
    SHARP_GRAPH                 /**< SharP Graphs */
} sharp_datatype_t;


/**
 * @ingroup SHARP_DATATYPE
 * @brief SHARP array type 
 *
 */
typedef struct sharp_array {
    sharp_array_elem_type_t      elem_type; /**< The types of elements stored 
                                                in this array */
    unsigned long                low_index; /**< This PE's low index of the 
                                                array */
    unsigned long                high_index; /**< This PE's high index of the
                                                 array */
    unsigned long                nr_elements_total; /**< The total number of 
                                                        elements in the 
                                                        array */
} sharp_array_t;

/**
 * @ingroup SHARP_DATATYPE
 * @brief Attaching options when attaching a SharP hash to an allocation group.
 */
typedef enum sharp_hash_attach_options{
    SHARP_HASH_NOP,  /**< Indicates that the hash data should remain unchanged
                          on attachment. This is used reattaching a hash data 
                          structure to an allocation group. */
    SHARP_HASH_CLEAR /**< Indicates that the hash data should be cleared upon 
                          attachment. This should be used when first attaching
                          a SharP hash. */
} sharp_hash_attach_options_t;

/** 
 * @ingroup SHARP_DATATYPE
 * @brief Function pointer to user provided hash function to be used by the 
 * SharP hash. 
 */
typedef uint64_t (*sharp_hash_hash_function)(void*);

/**
 * @ingroup SHARP_DATATYPE
 * @brief Function pointer to user provided compare function to be used by
 * the SharP hash.
 */
typedef int (*sharp_hash_compare_function)(void*, void*);

/**
 * @ingroup SHARP_DATATYPE
 * @brief The used status of a sharp_hash_slot_data_t structure */
typedef enum sharp_hash_slot_used_state {
    /** The slot is not used */
    SHARP_HASH_SLOT_UNUSED = 0,
    /** The slot is used */
    SHARP_HASH_SLOT_USED = 1
} sharp_hash_slot_used_state_t;

/** 
 * @ingroup SHARP_DATATYPE
 * @brief This holds the access data stored in a hash slot.
 * These are not directly stored in the hash table. The pointers should never 
 * be overwritten and should always point to a valid key/value pair even if 
 * the slot is not currently used.
 * */
typedef struct sharp_hash_slot_data {
    void * key_buf;                    /**< A pointer to the key for the 
                                            slot */
    void * value_buf;                  /**< A pointer to the value for 
                                            the slot */
    sharp_hash_slot_used_state_t used; /**< Whether or not the slot 
                                            actually contains any data. 
                                            Note that the above pointers 
                                            will not be NULL in either case */
} sharp_hash_slot_data_t;

typedef enum sharp_hash_action {
    SHARP_HASH_UPDATE, /**< update the item in the hash */
    SHARP_HASH_DELETE, /**< remove the item from the hash */
    SHARP_HASH_NEXT_WRAP, /**< increment the counter wrapping back to 0 if 
                            neighborhood size is exceeded. 'next' should not be 
                            changed when using this functionality */
    SHARP_HASH_ADD, /**< Add to the hash. Please note that if 2+ keys compare 
                      equal in the hash table results in undefined behavior */
    SHARP_HASH_NEXT_FINISH, /**< increment the counter and finish if it now 
                              exceeds the neighborhood size. 'next' should 
                              not be changed when using this functionality */
    SHARP_HASH_ABORT, /**< Quit without committing changes to the hash table */
    SHARP_HASH_FINISH /**< Quit and save any changes to the hash table. If 
                        updates were made without returning UPDATE or DELETE 
                        the result is undefined */
} sharp_hash_action_t;


/**
 * @ingroup SHARP_DATATYPE
 * @brief Sharp hash is a distributed synchronized key value store for 
 * homogeneous keys and homogeneous values.
 *
 * The keys and values are stored in blocks distributed across  the allocation 
 * group. Blocks contain a 64 bit lock, 64 bit used_mask and a number of keys 
 * followed 
 * by a number of values. This 'structure' isn't a struct since it is variable 
 * size and there is no way to represent a variable size struct.
 */
typedef struct sharp_hash {
    /**what kind of keys this hash holds */
    size_t key_size;
    /**what kind of values this hash holds */
    size_t value_size;

    /**The function that will be used to hash keys */
    uint64_t (*hash_fun)(void * key);
    /**The function that will be used to compare keys */
    int (*compare_keys)(void * key1, void *key2);

    /**The  number of blocks per pe. */ 
    size_t blocks_per_pe;

    /**The  size of the neighborhood that key/value pairs must be within. The
     * key value pair may be between [hash, hash+neighborhood_size) where hash
     * is the location the key/value pair would go if the hash table was empty.
     * This does mean that the element can end up in another block if there is
     * not a valid free slot in the desired block. */
    size_t neighborhood_size;

    /** The number of key/value pairs in  each block 
     * This must be in the range [1,64] */
    int items_per_block;

    /** The required alignment for blocks
     * For example 4096 will page align the blocks on most machines; the space 
     * between blocks is wasted. 
     * This must be a positive power of 2. 0 is not a power of 2. */
    int alignment;

    /** If the hash is attached or not.
     * 1 indicates attached. 0 indicates not attached. */
    int attached;

} sharp_hash_t;

/**
 * @ingroup SHARP_DATATYPE
 * @brief SHARP graph type 
 *
 */
typedef struct sharp_graph {

} sharp_graph_t;

/**
 * @ingroup SHARP_DATATYPE
 * @brief SHARP object that encapsulates the datastructure, group data, and
 * access information.  
 * 
 *
 */
typedef struct sharp_object {
    sharp_group_allocated_t     *alloc_group; /**< The allocation group this 
                                                   object is allocated on */
    sharp_map_type_t            map_type;     /**< The type of map associated
                                                   with this object */
    union {
        unsigned long            nr_indices_per_pe; /**< Number of indices per 
                                                        PE assuming a uniform 
                                                        mapping */
        sharp_map_callback_t    cb;           /**< A callback function for 
                                                   use with custom maps */
    } map;                                    /**< The map information 
                                                   associated with this 
                                                   object */
    union {
        sharp_array_t           sarray;       /**< Array specific information */
        sharp_hash_t            shash;        /**< Hash specific information */
        sharp_graph_t           sgraph;       /**< Graph specific information */
    } sobject;                                /**< Object specific information */
    unsigned int                obj_id;       /**< An ID for this particular 
                                                   SharP object */
} sharp_object_t;


/**
 * @ingroup SHARP_API
 * @brief Transfer the array elements from input buffer to the object 
 *
 * @param [in]  *in_buf        Input buffer
 * @param [in]  elem_type      Array element type to be transferred 
 * @param [in]  num_elems      The number of elements to be transferred
 * @param [in]  start_index    Array index of the sharp object to be transferred
 * @param [in]  sobj           Sharp Object to be transferred into 
 *
 * @return Error code.
 * 
 */

int sharp_darray_put_elems(void *in_buf, sharp_array_elem_type_t elem_type, 
                           size_t num_elems, size_t start_index, 
                           sharp_object_t *sobj);

/**
 * @ingroup SHARP_API
 * @brief Transfer the array elements from the SharP object into an out buffer 
 *
 *
 * @param [in]  start_index    Array index of the sharp object to be transferred
 * @param [in]  elem_type      Array element type to be transferred 
 * @param [in]  num_elems      The number of elements to be transferred
 * @param [in]  *sobj          Sharp Object to be transferred into 
 * @param [in]  *outbuf        The output buffer to be transferred from 
 *
 * @return Error code.
 * 
 */
int sharp_darray_get_elems(size_t start_index, 
                           sharp_array_elem_type_t elem_type, 
                           size_t num_elems, sharp_object_t *sobj,
                           void *outbuf);

/**
 * @ingroup SHARP_API
 * @brief Atomically swap a 64 bit integer in a SharP array of 64 bit integers.
 * NOTE: This assumes all elements of the array are 64 bit integers and has 
 * undefined behavior if this assumption is not met. 
 *
 * @param [in]  in           The value to be swapped with the value pointed to 
 *                           by index
 * @param [out] out          The value that was in the array at index
 * @param [in]  index        The index to be modified
 * @param [in] *sobj         Pointer to the SharP object defining the array
 *
 * @returns SHARP_OK if the atomic operation completed successfully; otherwise,
 * SHARP_COMM_FAILURE.
 */
int sharp_darray_atomic_swap64(uint64_t in,
                               uint64_t *out,
                               size_t index,
                               sharp_object_t *sobj);

/**
 * @ingroup SHARP_API
 * @brief Atomically compare and swap a 64 bit integer in a SharP array of 
 * 64 bit integers.
 * NOTE: This assumes all elements of the array are 64 bit integers and has 
 * undefined behavior if this assumption is not met. 
 *
 * @param [in]  in              The value to be swapped with the value 
 *                              pointed to by index
 * @param [in]  expected_value  The value expected to be in the array at index
 * @param [out] out             The value that was at index
 * @param [in]  index           The index to be modified
 * @param [in]  *sobj           Pointer to the SharP object defining the array
 *
 * @returns SHARP_OK if the atomic operation completed successfully; otherwise,
 * SHARP_COMM_FAILURE.
 */
int sharp_darray_atomic_cswap64(uint64_t in,
                                uint64_t expected_value,
                                uint64_t * out,
                                size_t index,
                                sharp_object_t * sobj);

/**
 * @ingroup SHARP_API
 * @brief Atomically add a 64 bit integer in a SharP array of 64 bit integers 
 * with another 64 bit integer.
 * NOTE: This assumes all elements of the array are 64 bit integers and has 
 * undefined behavior if this assumption is not met. 
 *
 * @param [in] in           The value to be added with the value pointed to 
 *                          by index
 * @param [in] index        The index to be modified
 * @param [in] *sobj        Pointer to the SharP object defining the array
 *
 * @returns SHARP_OK if the atomic operation completed successfully; otherwise,
 * SHARP_COMM_FAILURE.
 */
int sharp_darray_atomic_add64(uint64_t in,
                              size_t index,
                              sharp_object_t * sobj);

/**
 * @ingroup SHARP_API
 * @brief Atomically fetch and add a 64 bit integer in a SharP array of 64 
 * bit integers with another 64 bit integer.
 * NOTE: This assumes all elements of the array are 64 bit integers and has 
 * undefined behavior if this assumption is not met. 
 *
 * @param [in]  in           The value to be added with the value pointed to 
 *                           by index
 * @param [out] out          The value that was in the array at index
 * @param [in]  index        The index to be modified
 * @param [in]  *sobj        Pointer to the SharP object defining the array
 *
 * @returns SHARP_OK if the atomic operation completed successfully; otherwise,
 * SHARP_COMM_FAILURE.
 */
int sharp_darray_atomic_fadd64(uint64_t in,
                               uint64_t *out,
                               size_t index,
                               sharp_object_t * sobj);

/**
 * @ingroup SHARP_API
 * @brief Add an item to the hash. It is erroneous to add an already existing
 * key
 *
 * @param [in]  hash         The hash's Sharp Object
 * @param [in]  key_buf      The key to be added
 * @param [in]  value_buf    The value to be added
 *
 * @return Error code.
 * 
 */
int sharp_hash_add_unique(sharp_object_t * hash, 
                          void * key_buf,
                          void * value_buf);

/**
 * @ingroup SHARP_API
 * @brief Add an item to the hash, or update it if it already exists.
 *
 * @param [in]  hash         The hash's Sharp Object
 * @param [in]  key          The key to be added
 * @param [in]  value        The value to be added
 *
 * @return Error code.
 * 
 */
int sharp_hash_add_or_update(sharp_object_t * hash, 
                             void * key,
                             void * value);

/**
 * @ingroup SHARP_API
 * @brief Retrieve an item from the hash.
 *
 * @param [in]  hash         The hash's Sharp Object
 * @param [in]  key_buf      The key to be retrieved
 * @param [out] key_out      If not null the hash's key will be stored here
 * @param [out] value_buf    If not null the key's value will be stored here
 *
 * @return Error code.
 */
int sharp_hash_find(sharp_object_t * hash, 
                    void * key_buf,
                    void * key_out,
                    void * value_buf);

/**
 * @ingroup SHARP_API
 * @brief Delete an item from the hash.
 *
 * @param [in]  hash         The hash's Sharp Object
 * @param [in]  key_buf      The key to be deleted
 *
 * @return Error code.
 */
int sharp_hash_delete(sharp_object_t * hash, 
                      void * key_buf);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Get the size of a block.
 *
 * @param [in] hash The object that will have it's block size calculated
 *
 * @returns The size in bytes of the blocks the hash object uses
 **/
size_t sharp_hash_block_size(sharp_object_t * hash);

/**
 * @ingroup SHARP_API
 * @brief Get the number of local blocks in the hash
 *
 * @param [in] hash   The hash to get the number of local blocks from. The hash 
 *                    must currently be attached to an allocation group.
 *
 * @returns The number of local blocks for this hash is returned. This will 
 * be at least 1.
 */
size_t sharp_hash_get_local_block_count(sharp_object_t *hash);

/**
 * @ingroup SHARP_API
 * @brief Get the number of items in a hash block
 *
 * @param [in] hash   The hash to get the number of items per block from
 *
 * @returns The number of key value pairs in a block for this hash
 */
size_t sharp_hash_get_items_per_block(sharp_object_t *hash);

/**
 * @ingroup SHARP_API
 * @brief Get the key/value pairs out of a local block
 * To find out how many key/value pairs may be stored call 
 * sharp_hash_get_items_per_block
 *
 * @param [in] hash     The hash to retrieve the key value pairs from
 * @param [in] block    The local block number.
 * @param [in,out] keys    Where to store the keys. If NULL the keys will not 
 *                         be stored.
 * @param [in,out] values  Where to store the values. If NULL the values will 
 *                         not be stored.
 * @param [out] count      The number of key value pairs that were stored. 
 *                         This may not be NULL.
 *
 * @returns An error code.
 */
size_t sharp_hash_get_local_key_value_pairs(sharp_object_t * hash,
                                            size_t block,
                                            void * keys,
                                            void * values,
                                            int * count);

/** 
 * @ingroup SHARP_API
 * @brief Get the number of bytes that must be avaliable to create the hash.
 * 
 * @param [in] key_size       The size of the key
 * @param [in] value_size     The size of the value
 * @param [in] requested_slots    The number of requested hash slots
 * @param [in] number_of_pes  The number of pe's using the hash
 * @param [in] items_per_block The number of items in each block
 * @param [in] alignment The required alignment for each block
 */ 
size_t sharp_hash_get_required_size(size_t key_size,
                                    size_t value_size,
                                    uint64_t requested_slots,
                                    uint64_t number_of_pes,
                                    int items_per_block,
                                    int alignment,
                                    int neighborhood_size);

/**
 * @ingroup SHARP_API
 * @brief Get the number of hash slots the hash will use
 *
 * @param [in] requested_slots The number of slots the user requests
 * @param [in] number_of_pes The number of pes that will be used for the hash
 * @returns The number of slots the hash will use
 */
size_t sharp_hash_get_required_slots(uint64_t requested_slots,
                                     uint64_t number_of_pes,
                                     int items_per_block,
                                     int alignment,
                                     int neighborhood_size);

/**
 * @ingroup SHARP_INTERNAL
 * @brief Get the number of blocks the hash will use
 *
 * @param [in] requested_slots The number of slots the user requests
 * @param [in] number_of_pes The number of pes that will be used for the hash
 * @returns The number of blocks the hash will use
 */
size_t sharp_hash_get_required_blocks(uint64_t requested_slots,
                                      uint64_t number_of_pes,
                                      int items_per_block,
                                      int alignment,
                                      int neighborhood_size);

/** 
 * @ingroup SHARP_DATATYPE
 *
 * @brief A callback used for flexible user control over the hash contents.
 *
 * @param [in,out] data An opaque type that sharp does not know anything about.
 * @param [in] slot_data An array of structures that contains pointers into
 *                       the network buffer. 
 *                       Copy changes into these when updating an item.
 *                       Changing the contents of the buffers without setting 
 *                       the coresponding action has unspecified behavior.
 * @param [out] actions  A list of sharp_actions. These corespond one to one
 *                       with the slot_data items. Changes these to signal 
 *                       requests to the item.
 * @param [in] count     The number of items in both the slot_data and 
 *                       actions array.
 * @returns sharp_hash_action_t This is the requested action for the 
 *                              sharp_object to take.
 *
 * The function is used with the iterate_neighborhood function to allow 
 * arbitrary manipulation of a section of the hash table defined by a 
 * particular key's neighborhood. The function performs changes to the 
 * sharp_hash by manipulating provided memory in the slot_data array and 
 * setting an appropriate value in the actions array.
 * For example to change a value one would overwrite the slot_data[i].value_buf 
 * contents (not pointer), and set the SHARP_HASH_UPDATE action in actions[i].
 * The function must NOT call any operations (including the safe restricted 
 * ones) on the current sharp_hash that is called the callback. 
 *
 * Returning non zero will prevent any changes from being written. 
 * Returning zero will allow the changes to be written if writing is currently
 * permitted.
 * */

typedef int (*sharp_hash_callback_function)(void * data,
                                            sharp_hash_slot_data_t * slot_data,
                                            sharp_hash_action_t * actions,
                                            unsigned count);

/**
 * @ingroup SHARP_API
 * @brief This function provides the ability to arbitrarily change the elements
 * of a neighborhood of a key in a sharp_hash object.
 *
 * This function is dangerous and if the safer more restricted functions 
 * accomplish your goal then prefer them.
 *
 * The callback 'fun' that is provided will be called with the provided context 
 * once.
 *
 * @param [in] hash The has that will be used
 * @param [in] key The key's neighborhood that will be used. This key does not 
 *                 need to be in the hash already.
 * @param [in] read_only If 0 the neighborhood will be modifiable. 
 *                       If 1 the neighborhood will not be modifiable.
 *                       All other values result in undefined behavior.
 *                       Prefer 1 since that will result in more concurrency.
 * @param [in] fun The function that will be called.
 * @param [in] context The callbacks context. Sharp has no requirements on 
 *                     this value.
 */
int sharp_hash_iterate_neighborhood(sharp_object_t *hash,
                                    void * key,
                                    int read_only,
                                    sharp_hash_callback_function fun,
                                    void * context);


#endif
