/**
 * Copyright (c) 2016 Oak Ridge National Laboratory. ALL RIGHTS RESERVED.
 */

#include <config.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sharp_maps/api/sharp_maps.h>
#include <sobjects/api/sharp_objects.h>

#include <sharp/sharp_errors.h>
#include <comms/comms-include.h>
#include <assert.h>
#include <stdint.h>

typedef enum {
    DARRAY_GET,
    DARRAY_PUT
} darray_operation_type_t;

/*
 * This is the uniform mapping equivalent of the user provided callback. 
 * Using this unifies the code paths for more simplistic maintenance.
 */
static inline int uniform_map_callback(int index, 
                                       size_t * low,  
                                       size_t * high,
                                       size_t * index_offset,
                                       sharp_object_t * sobj)
{
    int ret = 0; 

    /* 
     * This is meant to reduce the number of divides and multiplies on the 
     * uniform map path 
     */    
    if (*low != 0) { //this has been called before
        ret = 1;
        *low += sobj->map.nr_indices_per_pe;
        *high += sobj->map.nr_indices_per_pe;
        *index_offset = 0; 
    } else { // this is the first call 
        ret = index / sobj->map.nr_indices_per_pe;
        *low = ret * sobj->map.nr_indices_per_pe;
        *high = *low + sobj->map.nr_indices_per_pe - 1;
        *index_offset = index - *low;
    }

    return ret;    
}

static inline int darray_get_and_put(darray_operation_type_t op_type,
                                     sharp_map_type_t map_type,
                                     size_t low,
                                     size_t nr_elems,
                                     sharp_array_elem_type_t elem_type,
                                     sharp_object_t * sobj,
                                     void * inbuf)
{
    size_t offset = 0;
    size_t transfer_low_index, transfer_high_index;
    size_t transfered_indexes;
    size_t remote_offset, transfer_size;
    int pe = 0;
    size_t pe_low_index=0, pe_high_index=0;
    int error = 0;
    size_t high = low + nr_elems-1;
    size_t pe_offset;

    for (;low <= high;) {
        if (map_type == SHARP_UNIFORM_MAP) {
            pe += uniform_map_callback(low, 
                                       &pe_low_index, 
                                       &pe_high_index,
                                       &pe_offset, 
                                       sobj);
        } else { 
            pe = sobj->map.cb(low, 
                              &pe_low_index, 
                              &pe_high_index,
                              &pe_offset);
        }

        transfer_high_index = (high < pe_high_index) ? high : pe_high_index;
        transfer_low_index = (low > pe_low_index) ? low : pe_low_index;
        remote_offset = (pe_offset) * elem_type;
        transfered_indexes = (transfer_high_index - transfer_low_index + 1);
        transfer_size = (transfered_indexes) * elem_type;
        
        if (op_type == DARRAY_GET) {
            if (pe == comms_my_pe) { 
                memcpy(inbuf+offset, 
                       sobj->alloc_group->buffer_ptr+remote_offset,
                       transfer_size);
            } else {
                error = sharp_comms_get_nbi(inbuf+offset,
                                            transfer_size,
                                            remote_offset,
                                            pe,
                                            sobj->alloc_group->id);
                if (error < 0 && error != -13) {
                    return SHARP_COMM_FAILURE;
                }
            }
        } else {
            if (pe == comms_my_pe) {
                memcpy(sobj->alloc_group->buffer_ptr+remote_offset,
                       inbuf+offset,
                       transfer_size);
            } else {
                error = sharp_comms_put_nbi(inbuf+offset,
                                            transfer_size,
                                            remote_offset,
                                            pe,
                                            sobj->alloc_group->id);
                if (error < 0 && error != -13) {
                    return SHARP_COMM_FAILURE;
                }
            }
        }
        offset += transfer_size;
        low += transfered_indexes;
    }

    error = sharp_comms_flush(0);
    if (error < 0) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

int sharp_darray_put_elems(void * in_buf,
                           sharp_array_elem_type_t elem_type,
                           size_t nr_elems, 
                           size_t start_index,
                           sharp_object_t * sobj)
{
    int ret = 0;
    size_t low = start_index; /* beginning of range */
    sharp_map_type_t map_type = sobj->map_type;

    /* bounds checking */
    if ((low < 0) || (low+nr_elems-1) > 
        sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST; /* FIXME: meaningful? */
    }

    if (nr_elems <= 0) {
        return SHARP_ERR_LAST;
    }

    ret = darray_get_and_put(DARRAY_PUT,
                             map_type,
                             low,
                             nr_elems,
                             elem_type,
                             sobj,
                             in_buf);
    return ret;
}

int sharp_darray_get_elems(size_t start_index,
                           sharp_array_elem_type_t elem_type,
                           size_t nr_elems, 
                           sharp_object_t *sobj,
                           void * outbuf)
{
    int ret = 0;
    size_t low = start_index; /* beginning of range */
    sharp_map_type_t map_type = sobj->map_type;

    /* bounds checking */
    if ((low < 0) || (low+nr_elems-1) > 
        sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST; /* FIXME: meaningful? */
    }

    ret = darray_get_and_put(DARRAY_GET,
                             map_type,
                             low,
                             nr_elems,
                             elem_type,
                             sobj,
                             outbuf);

    return ret;
   
}

int sharp_darray_atomic_swap64(uint64_t in,
                               uint64_t *out,
                               size_t index,
                               sharp_object_t *sobj)
{
    sharp_map_type_t map_type = sobj->map_type;
    size_t pe = 0;
    size_t pe_low_index = 0, pe_high_index = 0, pe_offset;
    size_t offset;

    if (index < 0 || index > sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST;
    }

    if (map_type == SHARP_UNIFORM_MAP) {
        pe = uniform_map_callback(index,
                                  &pe_low_index,
                                  &pe_high_index,
                                  &pe_offset,
                                  sobj);
    } else {
        pe = sobj->map.cb(index,
                          &pe_low_index,
                          &pe_high_index,
                          &pe_offset);
        if (pe < 0 || pe > comms_size) {
            return SHARP_ERR_LAST; /* FIXME: meaningful */
        }
    }

    offset = (pe_offset) * sizeof(uint64_t);
    return sharp_comms_atomic_swap64(in,
                                     out,
                                     offset,
                                     pe,
                                     sobj->alloc_group->id);
}
   

int sharp_darray_atomic_cswap64(uint64_t in,
                                uint64_t expected_value,
                                uint64_t * out,
                                size_t index,
                                sharp_object_t * sobj)
{
    sharp_map_type_t map_type = sobj->map_type;
    size_t pe = 0;
    size_t pe_low_index = 0, pe_high_index = 0, pe_offset = 0;
    size_t offset;

    if (index < 0 || index > sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST;
    }

    if (map_type == SHARP_UNIFORM_MAP) {
        pe = uniform_map_callback(index,
                                  &pe_low_index,
                                  &pe_high_index,
                                  &pe_offset,
                                  sobj);
    } else {
        pe = sobj->map.cb(index,
                          &pe_low_index,
                          &pe_high_index,
                          &pe_offset);
    }

    offset = (pe_offset) * sizeof(uint64_t);
    *out = in;
    return sharp_comms_atomic_cswap64(out,
                                      expected_value,
                                      offset,
                                      pe,
                                      sobj->alloc_group->id);
}

int sharp_darray_atomic_add64(uint64_t in,
                              size_t index,
                              sharp_object_t * sobj)
{
    sharp_map_type_t map_type = sobj->map_type;
    size_t pe = 0;
    size_t pe_low_index = 0, pe_high_index = 0, pe_offset = 0;
    size_t offset;

    if (index < 0 || index > sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST;
    }

    if (map_type == SHARP_UNIFORM_MAP) {
        pe = uniform_map_callback(index,
                                  &pe_low_index,
                                  &pe_high_index,
                                  &pe_offset,
                                  sobj);
    } else {
        pe = sobj->map.cb(index,
                          &pe_low_index,
                          &pe_high_index,
                          &pe_offset);
        if (pe < 0 || pe > comms_size) {
            return SHARP_ERR_LAST; /* FIXME: meaningful */
        }
    }

    offset = (pe_offset) * sizeof(uint64_t);
    return sharp_comms_atomic_add64(in,
                                    offset,
                                    pe,
                                    sobj->alloc_group->id);   
}

int sharp_darray_atomic_fadd64(uint64_t in,
                               uint64_t *out,
                               size_t index,
                               sharp_object_t * sobj)
{
    sharp_map_type_t map_type = sobj->map_type;
    size_t pe = 0;
    size_t pe_low_index = 0, pe_high_index = 0, pe_offset = 0;
    size_t offset;

    if (index < 0 || index > sobj->sobject.sarray.nr_elements_total) {
        return SHARP_ERR_LAST;
    }

    if (map_type == SHARP_UNIFORM_MAP) {
        pe = uniform_map_callback(index,
                                  &pe_low_index,
                                  &pe_high_index,
                                  &pe_offset,
                                  sobj);
    } else {
        pe = sobj->map.cb(index,
                          &pe_low_index,
                          &pe_high_index,
                          &pe_offset);
        if (pe < 0 || pe > comms_size) {
            return SHARP_ERR_LAST; /* FIXME: meaningful */
        }
    }

    offset = (pe_offset) * sizeof(uint64_t);
    return sharp_comms_atomic_fadd64(in,
                                     offset,
                                     pe,
                                     out,
                                     sobj->alloc_group->id);
}
