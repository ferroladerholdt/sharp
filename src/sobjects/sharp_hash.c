/**
 * Copyright (c) 2016 Oak Ridge National Laboratory. ALL RIGHTS RESERVED.
 */

#include <config.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sharp_maps/api/sharp_maps.h>
#include <sobjects/api/sharp_objects.h>

#include <sharp/sharp_errors.h>
#include <comms/comms-include.h>
#include <assert.h>
#include <stdint.h>


/**the following block section abstracts away the type of locking that will be
 * done over a block range */
typedef int (*sharp_hash_lock_unlock_range_function)(sharp_object_t * hash, 
                                                     size_t begin, 
                                                     size_t end);


typedef struct sharp_hash_block_range_locker {
    sharp_hash_lock_unlock_range_function const lock;
    sharp_hash_lock_unlock_range_function const unlock;
} sharp_hash_block_range_locker_t;


static inline int read_unlock(sharp_object_t *hash, 
                              size_t block);

static inline int write_unlock(sharp_object_t *hash, 
                               size_t block);

static inline int read_lock(sharp_object_t *hash, 
                            size_t block, 
                            int * have_lock);

static inline int write_lock(sharp_object_t *hash, 
                             size_t block, 
                             int * have_lock);

static inline int write_lock_range(sharp_object_t *hash, 
                                   size_t begin, 
                                   size_t end); 

static inline int write_unlock_range(sharp_object_t *hash, 
                                     size_t begin, 
                                     size_t end);

static inline int read_lock_range(sharp_object_t *hash, 
                                  size_t begin, 
                                  size_t end);

static inline int read_unlock_range(sharp_object_t *hash, 
                                    size_t begin, 
                                    size_t end); 


/* do not reorder the following array */
sharp_hash_block_range_locker_t sharp_hash_block_range_lockers [] = {
    {write_lock_range, write_unlock_range},
    {read_lock_range, read_unlock_range}
};

static const uint64_t bit64 = ((uint64_t)1) << 63;

static inline size_t get_group_size_for_object(sharp_object_t *obj) 
{
    return obj->alloc_group->nw_group->num_pes;
}

static inline size_t integer_div_ceil(uint64_t a, uint64_t b) 
{
    return (a+b-1)/b;
}

static inline size_t get_items_per_block(sharp_object_t *hash) 
{
    return hash->sobject.shash.items_per_block;
}

size_t sharp_hash_get_items_per_block(sharp_object_t *hash) 
{
    return get_items_per_block(hash);
}

/* how far past the neighborhood size we are allowed to reach when moving 
 * values around to make room.
 * This currently has an effect on stack usage */
static const int reorganization_reach=1;

static inline size_t hash_block_size(sharp_object_t *hash) 
{
    sharp_hash_t * sh = &hash->sobject.shash;
    int lock_size = 8; /* this is defined as uin64_t, thus 8 = 8 bytes */
    int used_mask_size = 8; /* this is defined as uin64_t (8 = 8 bytes) */
    size_t header_size = lock_size + used_mask_size;

    size_t per_block = get_items_per_block(hash);
    size_t key_space = per_block * hash->sobject.shash.key_size;
    size_t value_space = per_block * hash->sobject.shash.value_size;
    size_t required_space = key_space + value_space + header_size;

    size_t times = required_space / sh->alignment;
    size_t mod = required_space % sh->alignment;
    if (mod) {
        return  (times + 1) * sh->alignment;
    }

    return required_space;
}

size_t sharp_hash_block_size(sharp_object_t *hash) 
{
    return hash_block_size(hash);
}

/*  returns the lowest offset into the block that slot belongs to */
static inline size_t block_lower_offset(sharp_object_t * hash, size_t slot) 
{
    return slot - slot % get_items_per_block(hash);
}

static inline uint64_t get_used_mask(uint8_t *block) 
{
    uint64_t * raw = ((uint64_t*)block);
    /* requires that lock is an uint64_t */
    return *(raw+1);
}

static inline void * first_key(sharp_object_t *hash, uint8_t *block) 
{
    return block + 16 ; /* assumes both lock and used_mask are uin64_t */
}

static inline void * first_value(sharp_object_t *hash, uint8_t *block) 
{
    uint8_t * keys = (uint8_t*)first_key(hash, block);
    return keys + 
        hash->sobject.shash.key_size*
        get_items_per_block(hash);
}

static inline void set_buffer_used_bit(sharp_object_t *hash,
                                       uint8_t *block_dat,
                                       int slot,
                                       sharp_hash_slot_used_state_t used) 
{
    size_t slots_per_block = get_items_per_block(hash);
    int in_block = slot / slots_per_block; 
    int in_block_offset = slot % slots_per_block;
    uint8_t * actual_block = block_dat + in_block * hash_block_size(hash);

    /* copy in the key, value, and used bit */
    uint64_t * bit_mask = (uint64_t*)(actual_block + 8);
    uint64_t mask = 1ull << (63 - in_block_offset);

    if (used == SHARP_HASH_SLOT_USED) {
        *bit_mask |= mask;
    } else {
        *bit_mask &= ~mask;
    }
}

/** sets up the invariants for a sharp_hash_slot_data */
static inline sharp_hash_slot_data_t get_slot_data(sharp_object_t *hash,
                                                   uint8_t * block_data,
                                                   int offset) 
{
    int per_block = get_items_per_block(hash);
    int block = offset / per_block;
    int actual_offset = offset % per_block;
    size_t block_size = hash_block_size(hash);
    uint8_t * actual = block_data + block * block_size;
    int used = ((get_used_mask(actual) << actual_offset) & bit64) >> 63;
    void * key = first_key(hash,actual);
    void * value = first_value(hash,actual);

    key +=hash->sobject.shash.key_size*actual_offset;
    value += hash->sobject.shash.value_size*actual_offset;

    sharp_hash_slot_data_t ret = {
        .key_buf = key,
        .value_buf = value,
        .used = used
    };

    return ret;
}

/* set the slot in the provided block_dat array
 * used must be 0 or 1, use the enum in the header should be used
 * key_buf and value_buf must must not be null unless used == 0.
 */
static inline void set_slot(sharp_object_t *hash,
                            uint8_t *block_dat,
                            int slot,
                            sharp_hash_slot_used_state_t used,
                            void * key_buf,
                            void * value_buf) 
{
    sharp_hash_t * sh = &hash->sobject.shash;
    size_t key_size = sh->key_size;
    size_t value_size = sh->value_size;

    set_buffer_used_bit(hash, block_dat, slot, used);

    if (used == SHARP_HASH_SLOT_USED) {
        sharp_hash_slot_data_t ss = get_slot_data(hash, block_dat, slot);
        memcpy(ss.key_buf, key_buf, key_size);
        memcpy(ss.value_buf, value_buf, value_size);
    }
}

/** convenience function for set_slot when you already have a slot data 
 * a memcpy isn't needed since currently a slot's internal pointers should be 
 * pointing at the network buffer
 * */
static inline void set_slot_from_slot_data(sharp_object_t *hash,
                                           uint8_t *block_dat,
                                           uint64_t slot,
                                           sharp_hash_slot_data_t slot_data) 
{
    set_buffer_used_bit(hash, block_dat, slot, slot_data.used);
}
                                            
/* find how many bytes may be needed by an insertion operation for blocks */
static inline int block_size_for_insert(sharp_object_t *hash) 
{
    size_t block_size = hash_block_size(hash);
    size_t min_blocks = integer_div_ceil(hash->sobject.shash.neighborhood_size*
                                         reorganization_reach,
                                         get_items_per_block(hash));
    min_blocks++;
    return (block_size * min_blocks);
}

/* find how many bytes may be needed by an insertion operation for blocks */
static inline int block_size_for_read(sharp_object_t *hash) 
{
    size_t block_size = hash_block_size(hash);
    size_t min_blocks = integer_div_ceil(hash->sobject.shash.neighborhood_size,
                                         get_items_per_block(hash));
    min_blocks++;

    return (block_size * min_blocks);
}

/*  returns the number of blocks the slots are spread across  */
static inline int slots_spread_accross(sharp_object_t *hash,
                                       size_t begin,
                                       size_t end) 
{
    size_t low = block_lower_offset(hash, begin);
    size_t high = block_lower_offset(hash, end-0);

    return ((high - low)/ get_items_per_block(hash))+1;
}

/* returns total number of (global) slots in a hash */
static inline size_t total_slots(sharp_object_t *hash) 
{
    sharp_hash_t *sh = &hash->sobject.shash;
    int per_block = get_items_per_block(hash);
    return get_group_size_for_object(hash) * sh->blocks_per_pe * per_block;
}



/* ************************************************************************* */
static inline size_t block_number_by_slot(sharp_object_t *hash,uint64_t slot) 
{
    slot = slot % total_slots(hash);
    int per_block = get_items_per_block(hash);
    return slot / per_block;
}

/** returns the number of things that were unlocked successfully
 * if this number isn't end-begin there is likely a bug somewhere since this can
 * unlock another pe's locks.
 * */
static inline int unlock_range(sharp_object_t *hash,
                               int (*unlock_fun)(sharp_object_t *, size_t),
                               size_t begin,
                               size_t end) 
{
    int spread = slots_spread_accross(hash,begin,end);
    int per_block = get_items_per_block(hash);
    size_t total_blocks = total_slots(hash) / per_block;

    size_t starting_block = block_number_by_slot(hash,begin);
    for (int i=0; i<spread; i++) {
        int err = unlock_fun(hash,(starting_block+i)%total_blocks) ;
        if (err) {
            return err;
        }
    }
    return SHARP_OK;
}

/** returns the number of things that were locked successfully. 
 *  begin and end are specified as slot numbers
 **/
static inline int lock_range(sharp_object_t *hash,
                             int (*lock_fun)(sharp_object_t *, size_t, int *),
                             int (*unlock_fun)(sharp_object_t *, size_t),
                             size_t begin,
                             size_t end) 
{
    int spread = slots_spread_accross(hash,begin,end);
    size_t total_blocks = total_slots(hash) / get_items_per_block(hash);
    size_t starting_block = block_number_by_slot(hash,begin);

    while (1) {
        int i;
        for (i=0; i<spread; i++) {
            int have_lock=0;
            int err = lock_fun(hash,
                               (starting_block+i)%total_blocks,
                               &have_lock);

            if (err != 0) {
                return err;
            }

            if (have_lock == 0) {
                /* if a lock fails back off and try again */
                size_t top=(starting_block + (i-1));
                if (i > 0) {
                    err = unlock_range(hash, 
                                       unlock_fun,
                                       begin,
                                       get_items_per_block(hash)
                                       *top);
                    if (err != 0) {
                        return err;
                    }
                }
                break;
            }
        }

        if (i == spread) {
            return SHARP_OK;
        }
    }
}

static inline uint64_t pe_with_block(sharp_object_t *hash, size_t block) 
{
    uint64_t ret = block / hash->sobject.shash.blocks_per_pe;
    return ret;
}

/* return the offset of the lock in the remote node */
static inline uint64_t block_offset(sharp_object_t *hash, size_t block) 
{
    uint64_t block_size = hash_block_size(hash);
    uint64_t remote_block_number = block % hash->sobject.shash.blocks_per_pe;
    uint64_t offset = remote_block_number * block_size;

    return offset;
}

static inline int add_lock(sharp_object_t *hash,
                           size_t block,
                           uint64_t val,
                           uint64_t *out) 
{
    uint64_t pe = pe_with_block(hash, block);
    uint64_t lock_offset = block_offset(hash, block);
    int err = 0;
    int id = hash->alloc_group->id;

    err = sharp_comms_lock_acquire(pe, id);
    if (err != SHARP_OK) { 
        return err;
    }

    err = sharp_comms_atomic_fadd64(val, lock_offset, pe, out, id);
    if (err != SHARP_OK) { 
        return err;
    }

    err = sharp_comms_lock_release(pe, id);
    if (err != SHARP_OK) { 
        return err;
    }

    return SHARP_OK;
}

static inline int read_lock(sharp_object_t *hash, 
                            size_t block, 
                            int *have_lock) 
{
    uint64_t old_value = 0;
    int err;

    *have_lock = 0;
    err = add_lock(hash, block, 1, &old_value);
    if (err != 0) {
        return err;
    }

    if (old_value > (uint64_t)1<<32) {
        /* the lock acquisition failed due to a writer holding a write lock*/
        return read_unlock(hash,block);
    }

    *have_lock = 1;
    return SHARP_OK;
}

static inline int read_unlock(sharp_object_t *hash, size_t block) 
{
    uint64_t trash = 0;
    int ret;
    
    ret = add_lock(hash, block, -1, &trash);
    /* assert that at least one read lock was held when we unlocked */
    assert(trash<<32);
    return ret;
}

static inline int write_lock(sharp_object_t *hash, 
                             size_t block, 
                             int* have_lock) 
{
    uint64_t old_value = 0;
    int err;

    *have_lock = 0;
    err = add_lock(hash, block, 1ul<<32, &old_value);
    if (err != 0) {
        return err;
    }


    if (old_value > 0) {
        /* the lock was already locked by a writer, but this is not an error */
        return write_unlock(hash, block);
    }

    *have_lock = 1;

    /* wait for all readers to release the read lock */
   while (old_value << 32) {
        err = add_lock(hash, block, 0, &old_value);
        if (err != 0) {
            return err;
        }
    }

    return SHARP_OK;
}

static inline int write_unlock(sharp_object_t *hash, size_t block) 
{
    uint64_t trash=0;
    int ret;

    ret  = add_lock(hash, block, ((uint64_t)-1)<<32, &trash);
    return ret;
}

static inline int write_lock_range(sharp_object_t *hash, 
                                   size_t begin, 
                                   size_t end) 
{
    return lock_range(hash, write_lock, write_unlock, begin, end);
}

static inline int write_unlock_range(sharp_object_t *hash, 
                                     size_t begin, 
                                     size_t end) 
{
    return unlock_range(hash, write_unlock, begin,end);
}

static inline int read_lock_range(sharp_object_t *hash, 
                                  size_t begin, 
                                  size_t end) 
{
    return lock_range(hash, read_lock, read_unlock, begin, end);
}

static inline int read_unlock_range(sharp_object_t *hash, 
                                    size_t begin, 
                                    size_t end) 
{
    return unlock_range(hash, read_unlock, begin, end);
}

/* convert a hash into a slot */
static inline size_t hash_to_slot(sharp_object_t * hash, 
                                  uint64_t val) 
{
    return val % (total_slots(hash));
}

/* wrapper that turns local puts into memcpy */
static inline int put_wrapper(void *ptr,
                              size_t bytes,
                              size_t offset,
                              int pe,
                              sharp_object_t *obj) 
{
    int id = obj->alloc_group->id;

    if (comms_my_pe == pe) {
        memcpy(obj->alloc_group->buffer_ptr+offset, ptr,  bytes);
        return SHARP_OK;
    }

    return sharp_comms_put(ptr,bytes,offset,pe,id);
}

/* wrapper that turns local gets into memcpy */
static inline int get_wrapper(void *ptr,
                              size_t bytes,
                              size_t offset,
                              int pe,
                              sharp_object_t *obj) 
{
    int id = obj->alloc_group->id;

    if (comms_my_pe == pe) {
        memcpy(ptr, obj->alloc_group->buffer_ptr+offset, bytes);
        return SHARP_OK;
    }

    return sharp_comms_get(ptr,bytes,offset,pe,id);
}

static inline int read_block(sharp_object_t *hash, 
                             uint8_t *block_dat, 
                             uint64_t block_number) 
{
    uint64_t offset = block_offset(hash, block_number);
    uint64_t pe = pe_with_block(hash, block_number);
    int id = hash->alloc_group->id;
    int err;

    err = sharp_comms_lock_acquire(pe, id);
    if (err != 0) { 
        return err;
    }

    err = get_wrapper(block_dat,
                     hash_block_size(hash),
                     offset,
                     pe,
                     hash);
    if (err != 0) {
        return err;
    }

    err = sharp_comms_lock_release(pe, id);
    if (err != 0) {
        return err;
    }

    return SHARP_OK;
}

static inline int write_block(sharp_object_t *hash, 
                              uint8_t *block_dat, 
                              uint64_t block_number) 
{
    uint64_t offset = block_offset(hash, block_number);
    uint64_t pe = pe_with_block(hash, block_number);
    int id = hash->alloc_group->id;
    int err;

    err = sharp_comms_lock_acquire(pe, id);
    if (err != 0) {
        return err;
    }

    /* assumes 64bit lock and that we don't want to overwrite the lock since 
     * that may cause a race condition */
    err = put_wrapper(block_dat + 8,
                     sharp_hash_block_size(hash) - 8 , 
                     offset + 8,
                     pe,
                     hash);

    if (err != 0) {
        return err;
    }

    err = sharp_comms_lock_release(pe, id);
    if (err != 0) {
        return err;
    }

    return SHARP_OK;
}

static inline int iow_block(sharp_object_t *hash,
                            uint8_t * block_dat,
                            size_t slot,
                            size_t slot_count) 
{
    size_t block_size = hash_block_size(hash);
    uint64_t total_blocks = total_slots(hash) / get_items_per_block(hash);
    unsigned int block_count = slots_spread_accross(hash,slot, slot+slot_count);
    size_t begin_block = block_number_by_slot(hash,slot);
    size_t end_block = begin_block + block_count;
    int err;

    /* operate on the blocks in reverse order since write updates may contain 
     * multiple copies of the same block and the one at the lowest address is 
     * expected to be the newest */
    block_dat = block_dat + (end_block - begin_block - 1)*block_size;
    for (uint64_t block=end_block - 1; 
         block != begin_block -1 ;
         block--, block_dat -= block_size) {
        uint64_t modded_block = block % total_blocks;
        err = write_block(hash, block_dat, modded_block);
        if (err != 0) {
            return err;
        }
    }
    sharp_comms_flush(0);
    return SHARP_OK;
}


static inline int ior_block(sharp_object_t *hash,
                            uint8_t * block_dat,
                            size_t slot,
                            size_t slot_count) 
{
    size_t block_size = hash_block_size(hash);
    uint64_t total_blocks = total_slots(hash) / get_items_per_block(hash);
    unsigned int block_count = slots_spread_accross(hash,slot, slot+slot_count);
    size_t begin_block = block_number_by_slot(hash,slot);
    size_t end_block = begin_block + block_count;
    int err;

    /* operate on the blocks in reverse order since write updates may contain 
     * multiple copies of the same block and the one at the lowest address is 
     * expected to be the newest */
    block_dat = block_dat + (end_block - begin_block - 1)*block_size;
    for (uint64_t block=end_block - 1; 
         block != begin_block -1 ;
         block--, block_dat -= block_size) {
        uint64_t modded_block = block % total_blocks;
        err = read_block(hash, block_dat, modded_block);
        if (err != 0) {
            return err;
        }
    }
    sharp_comms_flush(0);
    return SHARP_OK;
}

static inline int write_out_blocks(sharp_object_t *hash,
                                   uint8_t * block_dat,
                                   size_t slot,
                                   size_t slot_count) 
{
    return iow_block(hash, block_dat, slot, slot_count);
}

static inline int read_in_blocks(sharp_object_t *hash,
                                 uint8_t *block_dat,
                                 uint64_t slot,
                                 uint64_t slot_count) 
{
    return ior_block(hash, block_dat, slot, slot_count);
}

static inline int find_empty_in_block(uint8_t *dat,
                                      size_t low_offset,
                                      size_t high_offset) 
{
    uint64_t used_mask = get_used_mask(dat);
    size_t i;

    if (used_mask == 0) {
        return low_offset;
    }

    used_mask <<= low_offset;
    i = low_offset;
    for (i = low_offset; i < high_offset; i++, used_mask <<= 1) {
        //if and only if key is used 
        if ((used_mask & bit64) == 0) {
            return i;
        }
    }

    return -1;
}

/*  find a free slot , if one is not in neighborhood size try moving things 
 *  from slots up to upper_limit distance away
 *
 *  return the slot number (relative to block_dat's low) of the free slot or -1 
 *  if no slot could be made */
static inline int find_free_slot(sharp_object_t *hash,
                                 uint8_t *block_dat,
                                 uint64_t low_offset,
                                 uint64_t upper_limit) 
{
    for (int iterations = 0; iterations < 2; iterations ++) {
        uint8_t * block = block_dat;
        int block_size = hash_block_size(hash);
        int to_search = upper_limit - low_offset;
        int block_number = 0;
        int per_block = get_items_per_block(hash);
        int empty;

        /* first block */
        if (upper_limit > per_block) {
            empty = find_empty_in_block(block, low_offset, per_block);
            to_search -= per_block - low_offset;
        } else {
            empty = find_empty_in_block(block,
                                        low_offset,
                                        upper_limit);
            return empty;
        }

        if (empty >= 0 && (uint64_t)empty <= upper_limit) {
            return empty;
        }

        block_number++;
        block += block_size;

        /* middle blocks */
        for (int i = 1;
             i < slots_spread_accross(hash,
                                      low_offset,
                                      upper_limit) - 1;
             i++, block += block_size, block_number++) {
            empty = find_empty_in_block(block, 0, per_block);
            to_search -= per_block;
            if (empty >= 0 && empty <= (int)upper_limit) {
                return per_block * block_number + empty;
            }
        }
        /* end block */
        empty = find_empty_in_block(block, 0, to_search);
        if (empty >= 0 && empty <= (int)upper_limit) {
            return per_block * block_number + empty;
        }
    }

    return -1;
}

/* try to move elements to make a free slot in the half open range 
 * [low_offset, goal_offset)
 * there is assumed to not already be a free slot in [low_offset,goal_offset)
 *
 * @param [in] hash                    the sharp_object we are working on
 * @param [in] block_dat_true_low_slot the low slot for the block compared to 
 *                                     the entire distributed backing array
 * @param [in] low_offset              the optimal location for the free slot
 *                                     this is also the lowest slot that should
 *                                     be read from or written to.
 * @param [in] goal_offset             where we want the free slot to be behind
 * @param [in] hard_limit              nothing at or past this slot should be 
 *                                     read or written to
 *
 * FIXME: should this be here? it currently does nothing ... 
 *
 * @returns -1 is returned if the space could not be made otherwise the new 
 * free slot is returned*/
static inline int make_room(sharp_object_t *hash,
                            uint8_t * block_dat,
                            uint64_t block_dat_true_low_slot,
                            uint64_t low_offset,
                            uint64_t goal_offset,
                            uint64_t hard_limit) 
{
    /* This function is currently disabled.
     * It causes intermitent bugs when the data is split over 3+ blocks
     */
    return -1;
    uint64_t (*hash_fun)(void * key) = hash->sobject.shash.hash_fun;
    int neighborhood_size = hash->sobject.shash.neighborhood_size;
    int empty = find_free_slot(hash, block_dat, goal_offset, hard_limit);

    if (empty == -1) {
        return -1;
    }

    while (1) {
        /* find previous key that belongs here and is in front of low_offset */
        int low_search = empty - neighborhood_size;
        assert(low_offset < INT_MAX);
        if (low_search < (int)low_offset) {
            low_search = low_offset;
        }

        sharp_hash_slot_data_t dat;
        for ( ; low_search != empty; low_search++) {
            dat = get_slot_data(hash, block_dat, low_search);
            /* TODO Investigate if this branch is unpredictable with a real 
             * work load. If so we could use bitwise operations to remove it */
            if (!dat.used) {
                continue;
            }
            uint64_t desired = hash_to_slot(hash, hash_fun(dat.key_buf));
            uint64_t absolue_empty = empty + block_dat_true_low_slot;
            /* desired can not be < block_dat_true_low_slot or we would have 
             * already returned */
            if (desired == absolue_empty) {
                fprintf(stderr,"desired <= absolue_empty\n");
            }
            if (desired + neighborhood_size > absolue_empty && desired > absolue_empty) {
                break;
            }
        }
        if (low_search == empty) {
            return -1;
        }

        /* assert that we are making progress */
        assert(low_search < empty);
        /* swap low_search and empty. Low search is the new empty */
        set_slot(hash, block_dat, empty, SHARP_HASH_SLOT_USED, 
                 dat.key_buf, dat.value_buf);
        set_slot(hash, block_dat, low_search, SHARP_HASH_SLOT_UNUSED,
                 NULL, NULL);
        empty = low_search;
        assert((uint64_t)empty >= low_offset);

        /* if the slot is suitable for return */
        if (goal_offset > (uint64_t)empty) {
            return empty;
        }
    }
    return -1;
}

/* returns true iff all of the blocks between low_slot and high_slot belong to 
 * the provided pe. This function does not work if you "wrap" all the way back
 * around so that low_slot > high_slot, but the provided pe owns both */
static inline int blocks_belong_to_pe(sharp_object_t *hash,
                                      int pe,
                                      int low_slot,
                                      int high_slot) 
{
    /* FIXME: this is broken */
    return 0;
    /* wrapping around means that the internal buffer pointer can't be used as 
     * a contigious buffer to access the low and high blocks, so pretend they 
     * are remote, and let read/write functions deal with any local vs 
     * nonlocal optimizations. */
    if (high_slot <= low_slot) { 
        return 0;
    }

    size_t low_block = block_number_by_slot(hash, low_slot);
    size_t high_block = block_number_by_slot(hash, high_slot);

    if (high_block <= low_block) {
        return 0;
    }

    int same_pe = pe_with_block(hash, low_block) ==
                  pe_with_block(hash, high_block);
    
    return same_pe;
}

/* ************************************************************************* */
/* buffer -  where to write the data if it is non local
 * out - points to where the data can now be found after the function is 
 * finished (if no errors have occurred)
 * This points to either the local data (if all of the slots are local)
 * or buffer after the data has been read in. There should be no assumptions 
 *
 * made about what buffer now contains 
 * low - the lowest slot number to read
 * high - one past the last slot number to read
 *
 * returns a sharp_error
 */
static inline int setup_block_ptr(sharp_object_t *hash,
                                  uint8_t * buffer,
                                  uint8_t ** out,
                                  int low,
                                  int count) 
{
    int high = (low + count) % total_slots(hash);
    char same_pe = blocks_belong_to_pe(hash, comms_my_pe, low, high);
    int err = SHARP_OK;
    uint8_t * block_ptr;

    if (same_pe == 1) {
        block_ptr = hash->alloc_group->buffer_ptr +
            block_offset(hash, block_number_by_slot(hash,low));
    } else {
        block_ptr = buffer;
        err = read_in_blocks(hash, block_ptr, low, count);
        if (err != SHARP_OK) {
            fprintf(stderr, "error on read blocks\n");
            return err;
        }
    }
#ifndef NDEBUG
    size_t blocks = slots_spread_accross(hash, low, low+count);
    size_t block_size = hash_block_size(hash);
    void * buffer_ptr = hash->alloc_group->buffer_ptr;
    size_t alloc_size = hash->alloc_group->alloc_size;
    assert(block_ptr == buffer || 
           (void*)(buffer_ptr + alloc_size) >= 
           (void*)(block_ptr + blocks * block_size));

#endif
    *out = block_ptr;
    return err;
}

int sharp_hash_add_unique(sharp_object_t * hash, 
                          void * key_buf,
                          void * value_buf) 
{
    int err = SHARP_OK;
    size_t block_data_size = block_size_for_insert(hash);
    uint8_t * block_ptr = NULL;
    uint8_t block_dat[block_data_size];
    sharp_hash_t *sh = &hash->sobject.shash;
    uint64_t key_hash = hash_to_slot(hash,sh->hash_fun(key_buf));
    size_t slot = hash_to_slot(hash, key_hash);
    char same_pe;
    int free_slot;

    /* lock what we may write */
    err = write_lock_range(hash,
                           slot,
                           slot + sh->neighborhood_size * reorganization_reach);
    if (err != 0) { 
        goto cleanup;
    }

    same_pe = blocks_belong_to_pe(hash,
                                  comms_my_pe,
                                  slot,
                                  slot+sh->neighborhood_size * 
                                    reorganization_reach);
    err = setup_block_ptr(hash,
                          block_dat,
                          &block_ptr,
                          slot,
                          sh->neighborhood_size * reorganization_reach);
    if (err != 0) {
        goto cleanup;
    }

    const size_t low_block_offset = block_lower_offset(hash,slot);
    const size_t local_slot_offset = slot - low_block_offset;
    free_slot = find_free_slot(hash,
                               block_ptr,
                               local_slot_offset,
                               local_slot_offset+sh->neighborhood_size);
    if (free_slot == -1) {
        free_slot = make_room(hash,
                              block_ptr,
                              low_block_offset,
                              local_slot_offset,
                              local_slot_offset+sh->neighborhood_size,
                              local_slot_offset+sh->neighborhood_size *
                                reorganization_reach);
        if (free_slot == -1) {
            err = SHARP_COULD_NOT_FIT_KEY;
            goto cleanup;
        }
    }

    assert(free_slot - local_slot_offset <= sh->neighborhood_size);
    set_slot(hash, block_ptr, free_slot, SHARP_HASH_SLOT_USED,
             key_buf, value_buf);

    /* write out the updated blocks */
    if (same_pe == 0) {
        assert(block_ptr == block_dat);
        err = write_out_blocks(hash,
                               block_ptr,
                               slot,
                               sh->neighborhood_size * reorganization_reach);
        if (err != 0) {
            goto cleanup;
        }
    }

cleanup:
    /* unlock what we locked */
    write_unlock_range(hash,
                       slot,
                       slot + sh->neighborhood_size * reorganization_reach);
    return err;
}

static inline int find_key_in_block(sharp_object_t *hash,
                                    uint8_t *block,
                                    size_t low_bound,
                                    size_t high_bound,
                                    void *key_buf) 
{
    sharp_hash_t * sh = &hash->sobject.shash;
    int (*compare_keys)(void * key1, void *key2) = sh->compare_keys;
    uint64_t used_mask = get_used_mask(block);
    used_mask <<= low_bound;
    size_t key_size = sh->key_size;
    uint8_t * keys = first_key(hash,block);

    /* assumes 64 key/value pairs per block */
    for (size_t i = low_bound; 
         i < high_bound && used_mask != 0; 
         i++, used_mask <<= 1) {

        //iff key is used 
        if (used_mask & bit64) {
            if (compare_keys(key_buf, keys + i*key_size)) {
                return i;
            }
        }
    }

    return -1;
}

/* find a key in the blocks array provided and return its offset or -1 if it 
 * was not found*/
static inline int find_key_in_blocks(sharp_object_t *hash,
                                     uint8_t * block_dat,
                                     void * key_buf,
                                     size_t low_bound,
                                     size_t high_bound) 
{
    int ret=-1;    
    size_t blocks = slots_spread_accross(hash,low_bound, high_bound);
    size_t block_size = hash_block_size(hash);
    int to_search;

    if (blocks == 1) {
        return find_key_in_block(hash, 
                                 block_dat,
                                 low_bound,
                                 high_bound,
                                 key_buf);
    }

    int per_block = get_items_per_block(hash);
    int where = find_key_in_block(hash,
                                  block_dat,
                                  low_bound,
                                  per_block,
                                  key_buf);

    if (where != -1) {
        return where;
    }

    low_bound = block_lower_offset(hash,low_bound)+per_block;
    to_search = high_bound - low_bound;
    block_dat += block_size;

    for (size_t i = 1; i < blocks - 1; i++, block_dat += block_size) {
        ret = find_key_in_block(hash,
                                block_dat,
                                0,
                                per_block,
                                key_buf);
        to_search -= per_block;
        if (ret != -1) {
            return ret + per_block * i;
        }
    }

    ret = find_key_in_block(hash,
                            block_dat,
                            0,
                            to_search,
                            key_buf);

    if (ret != -1) {
        return ret + per_block * (blocks - 1);
    }

    return ret;
}

int sharp_hash_add_or_update(sharp_object_t * hash, 
                             void * key_buf,
                             void * value_buf) 
{
    int err=SHARP_OK;
    size_t block_data_size = block_size_for_insert(hash);
    uint8_t block_dat[block_data_size];
    sharp_hash_t *sh = &hash->sobject.shash;
    uint64_t key_hash = sh->hash_fun(key_buf); 
    const size_t slot = hash_to_slot(hash,key_hash);
    const size_t relative_slot = slot - block_lower_offset(hash,slot);
    uint8_t * block_ptr;
    char same_pe;
    int found_at;

    /* lock what we may write */
    err = write_lock_range(hash,
                           slot,
                           slot+sh->neighborhood_size * reorganization_reach);

    if (err != SHARP_OK) {
        goto cleanup;
    }
    
    same_pe = blocks_belong_to_pe(hash,
                                  comms_my_pe,
                                  slot,
                                  slot+sh->neighborhood_size * 
                                    reorganization_reach);
    err = setup_block_ptr(hash, 
                          block_dat, 
                          &block_ptr, 
                          slot, 
                          sh->neighborhood_size);
    if (err != SHARP_OK) { 
        goto cleanup;
    }

    found_at = find_key_in_blocks(hash,
                                  block_ptr,
                                  key_buf,
                                  relative_slot,
                                  relative_slot + sh->neighborhood_size);
    if (found_at == - 1) {
        found_at = find_free_slot(hash,
                                  block_ptr,
                                  relative_slot,
                                  relative_slot + sh->neighborhood_size);

        if (found_at == -1) {
            found_at = make_room(hash,
                                 block_ptr,
                                 block_lower_offset(hash,slot),
                                 relative_slot,
                                 relative_slot + sh->neighborhood_size,
                                 sh->neighborhood_size * reorganization_reach);
        }
            
        if (found_at == -1) {
            err = SHARP_COULD_NOT_FIT_KEY;
            goto cleanup;
        }
    }

    set_slot(hash, 
             block_ptr, 
             found_at, 
             SHARP_HASH_SLOT_USED,
             key_buf, 
             value_buf);

    if (same_pe == 0) {
        assert(block_ptr == block_dat);
        err = write_out_blocks(hash,
                               block_ptr,
                               slot,
                               sh->neighborhood_size * reorganization_reach);
        if (err != SHARP_OK) {
            goto cleanup;
        }
    }

cleanup:
    write_unlock_range(hash,
                       slot,
                       slot+sh->neighborhood_size * reorganization_reach);
    return err;
}

static inline int retrieve_from_blocks(sharp_object_t *hash,
                                       uint8_t * block_dat,
                                       size_t low_bound,
                                       size_t high_bound,
                                       void * key_buf,
                                       void * key_out,
                                       void * value_buf) 
{
    int where = find_key_in_blocks(hash,
                                   block_dat,
                                   key_buf,
                                   low_bound,
                                   high_bound);
    if (where == -1) {
        return SHARP_KEY_NOT_FOUND;
    }

    sharp_hash_slot_data_t dat = get_slot_data(hash, block_dat, where);

    if (key_out != 0) {
        memcpy(key_out, dat.key_buf, hash->sobject.shash.key_size);
    }

    if (value_buf != 0) {
        memcpy(value_buf, dat.value_buf, hash->sobject.shash.value_size);
    }

    return SHARP_OK;
}

int sharp_hash_find(sharp_object_t * hash, 
                    void * key_buf,
                    void * key_out,
                    void * value_buf) 
{
    sharp_hash_t * sh = &hash->sobject.shash;
    size_t block_dat_size = block_size_for_read(hash);
    uint8_t * block_ptr; 
    uint8_t block_dat[block_dat_size];
    size_t slot = hash_to_slot(hash, sh->hash_fun(key_buf));
    int neighborhood_size = sh->neighborhood_size;
    int err;

    err = read_lock_range(hash, slot, slot+neighborhood_size);
    if (err != 0) {
        goto cleanup;
    }

    err = setup_block_ptr(hash,
                          block_dat,
                          &block_ptr,
                          slot,
                          neighborhood_size);
    if (err != 0) { 
        goto cleanup;
    }
    
    const size_t low_block_offset = slot - block_lower_offset(hash,slot);
    const size_t high_offset = low_block_offset + neighborhood_size;
    err = retrieve_from_blocks(hash,
                               block_ptr,
                               low_block_offset,
                               high_offset,
                               key_buf,
                               key_out,
                               value_buf);
        
cleanup:
    read_unlock_range(hash,slot, slot+neighborhood_size);
    return err;
}

int sharp_hash_delete(sharp_object_t * hash, 
                      void * key_buf) 
{
    int err = SHARP_OK;
    size_t block_data_size = block_size_for_read(hash);
    uint8_t *block_ptr = NULL;
    uint8_t block_dat[block_data_size];
    sharp_hash_t *sh = &hash->sobject.shash;
    uint64_t slot = hash_to_slot(hash, sh->hash_fun(key_buf));
    const size_t local_offset = slot - block_lower_offset(hash, slot);
    int where;

    /* lock what we may write */
    err = write_lock_range(hash,
                           slot,
                           slot + sh->neighborhood_size);
    if (err != SHARP_OK) { 
        goto cleanup;
    }

    err = setup_block_ptr(hash,
                          block_dat,
                          &block_ptr,
                          slot,
                          sh->neighborhood_size);
    if (err != SHARP_OK) { 
        goto cleanup;
    }

    where = find_key_in_blocks(hash,
                               block_ptr,
                               key_buf,
                               local_offset,
                               local_offset + sh->neighborhood_size);
    if (where == -1) {
        err =  SHARP_KEY_NOT_FOUND;
        goto cleanup;
    }
    
    /* unset slot's used bit */
    set_slot(hash, block_ptr, where, SHARP_HASH_SLOT_UNUSED, NULL,NULL);
    err = blocks_belong_to_pe(hash,
                              comms_my_pe,
                              slot,
                              sh->neighborhood_size);
    if (err == 0) {
        assert(block_ptr == block_dat);
        err = write_out_blocks(hash,
                               block_ptr,
                               slot,
                               sh->neighborhood_size);
    }
    if (err != 0) { 
        goto cleanup;
    }

cleanup:
    /* unlock array */
    err = write_unlock_range(hash,
                             slot,
                             slot+sh->neighborhood_size);
    return err;
}


size_t sharp_hash_get_local_block_count(sharp_object_t *hash) 
{
    return hash->sobject.shash.blocks_per_pe;
}

size_t sharp_hash_get_local_key_value_pairs(sharp_object_t * hash,
                                            size_t block,
                                            void * keys,
                                            void * values,
                                            int * count) 
{
    int err;
    int local_count = 0;
    size_t my_low = hash->alloc_group->my_pe *
        hash->sobject.shash.blocks_per_pe *
        get_items_per_block(hash);
    size_t global_offset = my_low + get_items_per_block(hash)*block;
    size_t block_size = hash_block_size(hash);
    uint8_t block_dat[block_size];

    err = read_lock_range(hash,
                          global_offset,
                          global_offset + get_items_per_block(hash));
    if (err != SHARP_OK) {
        goto cleanup;
    }

    err = read_in_blocks(hash,
                         block_dat,
                         global_offset,
                         get_items_per_block(hash) - 1);
    if (err != SHARP_OK) {
        goto cleanup;
    }

    err = read_unlock_range(hash,
                            global_offset,
                            global_offset + get_items_per_block(hash));
    if (err != SHARP_OK) {
        /* while this isn't normal we have already attempted to unlock the
         * range */
        goto normal_exit;
    }

    size_t key_size = hash->sobject.shash.key_size;
    size_t value_size = hash->sobject.shash.key_size;
    void * key_first = first_key(hash,block_dat);
    void * value_first = first_value(hash,block_dat);
    uint64_t bit_mask = get_used_mask(block_dat);

    for (int i = 0; 
         i < (int)get_items_per_block(hash) && bit_mask != 0; 
         i++, bit_mask <<= 1) {
        if ((bit_mask & bit64) == 0) {
            continue;
        }

        if (keys) memcpy(keys + local_count * key_size,
                         key_first + i * key_size,
                         key_size);

        if (values) memcpy(values + local_count * value_size,
                          value_first + i * value_size,
                          value_size);
        local_count++;
    }

    goto normal_exit;
cleanup:
    err = read_unlock_range(hash,
                            global_offset,
                            global_offset + get_items_per_block(hash));
    return err;

normal_exit:
    *count = local_count;
    return err;
}

size_t sharp_hash_get_required_blocks(uint64_t requested_slots,
                                      uint64_t number_of_pes,
                                      int items_per_block,
                                      int alignment,
                                      int neighborhood_size) 
{
    size_t  base = integer_div_ceil(requested_slots, number_of_pes);
    //TODO replace 64 with something
    size_t low = integer_div_ceil(base, items_per_block);
    /* FIXME minimum  arbitrarly choosen;
     * needs to be based on neighborhood size  to prevent a single block from
     * being used twice in operations in small hashes */

    /* how far out an opration can 'reach' for data */
    size_t blocks_reach = (neighborhood_size / items_per_block + 1) +
                          (reorganization_reach + 1);

    /* This ensures that no block is used twice in the same operation */
    size_t minimum = integer_div_ceil(blocks_reach, number_of_pes) + 1;

    if (low < minimum) { 
        low = minimum;
    }

    return low;
}

size_t sharp_hash_get_required_size(size_t key_size,
                                    size_t value_size,
                                    uint64_t requested_slots,
                                    uint64_t number_of_pes,
                                    int neighborhood_size,
                                    int items_per_block,
                                    int alignment) 
{
    sharp_hash_t dummy_hash={
        .items_per_block = items_per_block,
        .alignment = alignment,
        .neighborhood_size = neighborhood_size,
        .value_size = value_size,
        .key_size = key_size,
    };

    sharp_object_t dummy_object = {
        .sobject.shash = dummy_hash,
    };

    size_t per_block = sharp_hash_block_size(&dummy_object);
    size_t blocks = sharp_hash_get_required_blocks(requested_slots,
                                                   number_of_pes,
                                                   items_per_block,
                                                   alignment,
                                                   neighborhood_size);

    return blocks * per_block * number_of_pes;
}

size_t sharp_hash_get_required_slots(uint64_t requested_slots,
                                     uint64_t number_of_pes,
                                     int items_per_block,
                                     int alignment,
                                     int neighborhood_size) 
{
    size_t slots = get_items_per_block(NULL) *
        sharp_hash_get_required_blocks(requested_slots,
                                       number_of_pes,
                                       items_per_block,
                                       alignment,
                                       neighborhood_size);
    return slots;
}

int sharp_hash_iterate_neighborhood(sharp_object_t *hash,
                                    void * key_buf,
                                    int read_only,
                                    sharp_hash_callback_function fun,
                                    void * context) 
{
    int err;
    int should_write=0;
    sharp_hash_t *sh = &hash->sobject.shash;
    uint64_t const slot = hash_to_slot(hash, sh->hash_fun(key_buf));

    sharp_hash_block_range_locker_t locker = 
        sharp_hash_block_range_lockers[read_only];
    size_t const block_data_size = block_size_for_read(hash);
    uint8_t block_dat[block_data_size];
    sharp_hash_slot_data_t slot_datas[sh->neighborhood_size];
    sharp_hash_action_t actions[sh->neighborhood_size];

    /* lock the distributed hash blocks we will access */
    err = locker.lock(hash,
                      slot,
                      slot + sh->neighborhood_size);
    if (err != 0) {
        goto sharp_hash_cleanup;
    }

    err = read_in_blocks(hash,
                         block_dat,
                         slot,
                         sh->neighborhood_size);
    if (err != SHARP_OK) {
        goto sharp_hash_cleanup;
    }

    const size_t local_offset = slot - block_lower_offset(hash,slot);
    
    for (int i = 0; i < sh->neighborhood_size; i++) {
        slot_datas[i] = get_slot_data(hash, block_dat, local_offset + i);
        actions[i] = SHARP_HASH_NOP;
    }

    err = fun(context, slot_datas, actions, sh->neighborhood_size);
    if (err != 0) { 
        goto sharp_hash_cleanup;
    }

    for (int i = 0; i < sh->neighborhood_size; i++) {
        switch(actions[i]) {
            case SHARP_HASH_DELETE:
                slot_datas[i].used = SHARP_HASH_SLOT_UNUSED;
                set_slot_from_slot_data(hash,
                                        block_dat,
                                        local_offset + i,
                                        slot_datas[i]);
                break;
            case SHARP_HASH_ADD:
                slot_datas[i].used = SHARP_HASH_SLOT_USED;
                set_slot_from_slot_data(hash,
                                        block_dat,
                                        local_offset + i,
                                        slot_datas[i]);
                should_write = 1;
                break;
            case SHARP_HASH_UPDATE:
                should_write = 1;
                break;
            default:
                break;
        }
    }

    if ((read_only && should_write) == 0) {
        err = write_out_blocks(hash,
                               block_dat,
                               slot,
                               sh->neighborhood_size);
        if (err != SHARP_OK) {
            goto sharp_hash_cleanup;
        }
    }

sharp_hash_cleanup:
    /* unlock array */
    locker.unlock(hash,
                  slot,
                  slot+sh->neighborhood_size);
    return err;

}
