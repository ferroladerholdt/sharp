/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef SHARP_COMMS_NULL_H
#define SHARP_COMMS_NULL_H

#include <sharp/sharp_errors.h>

/*
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief The network data associated with the MPI communication layer. This 
 * is used for communication within an allocation group.
 */
typedef struct network_data {
} network_data_t;

extern network_data_t * nw_data; 

extern size_t comms_my_pe;
extern size_t comms_size;
extern int sharp_nr_groups;   
extern int sharp_nw_data_threshold;



static inline int sharp_comms_get_pe(void *nw_info, int *pe) 
{
    *pe = -1;
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_get_size(void *nw_info, int *size)
{
    *size = -1;
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_init(int argc, char ** argv) 
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_finalize(void) 
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_flush_pe(int pe, unsigned int group_id) 
{
    return SHARP_COMM_FAILURE;
} 

static inline int sharp_comms_flush(unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_barrier(sharp_group_allocated_t * ag)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_register_buffer(void * buffer, 
                                           size_t length,
                                           sharp_group_network_t *nw_group,
                                           unsigned int group_id) 
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_lock_acquire(unsigned int pe, 
                                           unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_lock_release(unsigned int pe, 
                                           unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_put(void * inbuf,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_get(void * outbuf,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_put_nbi(void * inbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_get_nbi(void * outbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_atomic_swap64(uint64_t val,
                                            uint64_t * buf,
                                            size_t remote_offset,
                                            unsigned int pe,
                                            unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_atomic_cswap64(uint64_t * buf,
                                    uint64_t expected,
                                    size_t remote_offset,
                                    unsigned int pe,
                                    unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_atomic_fadd64(uint64_t local,
                                     size_t remote_offset,
                                     uint64_t pe,
                                     uint64_t *out,
                                     unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

static inline int sharp_comms_atomic_add64(uint64_t local,
                                     size_t remote_offset,
                                     uint64_t pe,
                                     unsigned int group_id)
{
    return SHARP_COMM_FAILURE;
}

#endif
