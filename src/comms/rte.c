/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <config.h>
#include <sharp/sharp_errors.h>

#include <ucs/debug/memtrack.h>

#include <stdio.h>

/* externs */
extern size_t comms_my_pe;
extern size_t comms_size; 
extern size_t comms_jobid; 

#ifdef COMM_UCX
#include <ucp/api/ucp.h>
extern ucp_context_h sharp_ucp_context;
extern ucp_worker_h sharp_ucp_worker;
#endif

#ifdef WITH_RTE
#include <rte.h>


/*
 * A global rte_group. Contains all the PEs within the job. 
 */ 
static rte_group_t rte_group;
rte_srs_session_t session;

/**
 * @ingroup SHARP_INTERNAL
 * @brief Wrapper for RTE's initialization function.
 *
 * @param [in] argc                     The number of command line arguments
 * @param [in] **argv                   The command line arguments
 * @returns an error code 
 */
static inline int init_rte(int argc, char ** argv) 
{
    return rte_init(&argc, &argv, &rte_group);
}

static inline int set_pe_nr(void) 
{
    int pe = rte_get_ec_index(rte_group, rte_get_my_ec());
    comms_my_pe = (size_t) pe;
    return pe;
}

/**
 * @ingroup SHARP_INTERNAL
 * @internal
 *
 * Set the group_size in the global variable comms_size
 *
 * This function returns void since there is no defined error code from rte 
 * for getting the size
 */
static inline int set_group_size(void) 
{
    int size = rte_group_size(rte_group);
    comms_size = (size_t) size; 
    return size;
}

/**
 * @ingroup SHARP_INTERNAL
 * @brief Adds an item to an RTE iovec 
 *
 * @param [in] *iovec                   RTE iovec to be modified
 * @parma [in] *iovec_count             Index number of iovec to be modified
 * @param [in] *iov_base                The base_address to be used by the 
 *                                      iovec
 * @param [in] count                    The number of bytes for the data 
 *                                      pointed to by iov_base
 * @returns none
 */
static inline void add_iovec(rte_iovec_t *iovec, 
                             int *iovec_count, 
                             void *iov_base, 
                             uint32_t count)
{
    iovec[*iovec_count].iov_base = iov_base;
    iovec[*iovec_count].type = rte_datatype_uint1;
    iovec[*iovec_count].count = count;
    *iovec_count += 1;
} 

static inline int unpack_iovec(rte_iovec_t *iovec, int index, void *message,
                                int *offset) 
{
    return rte_unpack(&iovec[index], message, (uint32_t *)offset);
}

int comms_rte_exchange_address(void * buffer,
                               void *** worker_param_addresses,
                               void *** pack_param,
                               uint64_t * remotes,
                               void * register_buffer,
                               void * nw_group_info) 
{
    rte_iovec_t iovec[5];
    int i;
    int iovec_count = 0;
    char * SRS_KEY = "endpoints key";
    int error = 0;
    int ret = 0;
    int offset;  
    void ** worker_addresses=NULL;
    void ** pack=NULL;
    ucs_status_t status;
    size_t pack_size;
    void * worker_address=NULL;
    ucp_mem_h * mem = register_buffer;
    size_t worker_len;
    unsigned char * worker_buf;
    unsigned char * pack_buf;
    *worker_param_addresses=NULL;
    *pack_param=NULL;

    worker_addresses = (void **) ucs_malloc(comms_size* sizeof(void *));
    if(NULL == worker_addresses) {
        ret = SHARP_ERR_NO_MEMORY;
        goto fail;
    }

    pack = (void **) ucs_malloc(comms_size* sizeof(void *));
    if(NULL == pack) {
        ret = SHARP_ERR_NO_MEMORY;
        goto fail;
    }
    

    status = ucp_rkey_pack(sharp_ucp_context, *mem, &pack[comms_my_pe], 
                           &pack_size);
    if(status != UCS_OK) {
        ret = error;
        goto fail;
    }


    status = ucp_worker_get_address(sharp_ucp_worker, 
                                    (ucp_address_t **) &worker_address, 
                                    &worker_len);
    if(status != UCS_OK) {
        ret = SHARP_COMM_FAILURE;
        goto fail;
    }

    worker_buf = ucs_malloc(worker_len);
    pack_buf = ucs_malloc(pack_size);
    remotes[comms_my_pe] = (uint64_t)buffer;
    memcpy(worker_buf, worker_address, worker_len);
    memcpy(pack_buf, pack[comms_my_pe], pack_size);

    add_iovec(iovec, &iovec_count, &worker_len, sizeof(size_t));
    add_iovec(iovec, &iovec_count, worker_buf, worker_len);
    add_iovec(iovec, &iovec_count, &pack_size, sizeof(size_t));
    add_iovec(iovec, &iovec_count, pack_buf, pack_size);
    add_iovec(iovec, &iovec_count, &remotes[comms_my_pe], sizeof(uint64_t));

    error = rte_srs_session_create(rte_group, 3286, &session);
    if(error != RTE_SUCCESS) {
        ret = error;
        goto fail;
    }
    error = rte_srs_set_data(session, SRS_KEY, iovec, iovec_count);
    if(error != RTE_SUCCESS) {
        ret = error;
        goto fail_destroy_session;
    }

    error = rte_srs_exchange_data(session);
    if(error != RTE_SUCCESS) {
        ret = error;
        goto fail_destroy_session;
    }

    /* obtain the network information here... */
    for(i=0;i<comms_size;i++) {
        int msg_size;
        void *message;
        offset = 0;
        size_t r_worker_len;
        size_t r_pack_size;

        if(i == comms_my_pe) {
            worker_addresses[i] = ucs_malloc(worker_len);        
            if(NULL == worker_addresses[i]){
                ret = SHARP_ERR_NO_MEMORY;
                goto fail_purge_arrays;
            }
            memcpy(worker_addresses[i], worker_address, worker_len); 
            continue;
        }

        error = rte_srs_get_data(session, rte_group_index_to_ec(rte_group, i),
                                 SRS_KEY, &message, &msg_size);
        if(error != RTE_SUCCESS) {
            ret = error;
            goto fail_purge_arrays;
        }

        iovec[0].iov_base = &r_worker_len;
        iovec[0].count = sizeof(size_t);
        error = unpack_iovec(iovec, 0, message, &offset);
        if(error != RTE_SUCCESS) {
            ret = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }
        
        worker_addresses[i] = ucs_malloc(r_worker_len);
        if(NULL == worker_addresses[i]) {
            ret = SHARP_ERR_NO_MEMORY;
            goto fail_purge_arrays;
        }

        iovec[1].iov_base = worker_buf;
        iovec[1].count = r_worker_len;
        
        error = unpack_iovec(iovec, 1, message, &offset);
        if (error != RTE_SUCCESS) {
            ret = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }

        iovec[2].iov_base = &r_pack_size;
        iovec[2].count = sizeof(size_t);

        error = unpack_iovec(iovec, 2, message, &offset);
        if (error != RTE_SUCCESS) {
            ret = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }

        pack[i] = ucs_malloc(r_pack_size);
        if(NULL == pack[i]) {
            ret = SHARP_ERR_NO_MEMORY;
            goto fail_purge_arrays;
        }
 
        iovec[3].iov_base = pack_buf;
        iovec[3].count = r_pack_size;

        error = unpack_iovec(iovec, 3, message, &offset);
        if (error != RTE_SUCCESS) {
            ret = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }

        iovec[4].iov_base = &remotes[i];
        iovec[4].count = sizeof(size_t);

        error = unpack_iovec(iovec, 4, message, &offset);
        if (error != 0) {
            ret = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }
        memcpy(worker_addresses[i], worker_buf, r_worker_len);
        memcpy(pack[i], pack_buf, r_pack_size);
    }

    *worker_param_addresses = worker_addresses;
    *pack_param = pack; 

    free(worker_buf);
    free(pack_buf);

    return ret;

fail_purge_arrays:
    for(i=0; i<comms_size; i++){
        ucs_free(worker_addresses[i]);
        ucs_free(pack[i]);
    }
fail_destroy_session:
    rte_srs_session_destroy(&session);
fail:
    ucs_free(worker_address); 
    ucs_free(pack);
    return ret;
}

int comms_rte_init(int argc, char ** argv) 
{
    int error;

    error = init_rte(argc, argv);
    if(error) {
        return error;
    }

    error = set_pe_nr();
    if(error == -1) {
        return error;
    }

    error = set_group_size();
    if(error == -1) {
        return error;
    }

    return SHARP_OK;
}

int comms_rte_barrier(void) {
    return rte_barrier(rte_group);
}

#elif WITH_PMIX

#include <pmix.h>

static pmix_proc_t myproc;

static inline int internal_pmi_init(int argc, char ** argv)
{
    pmix_proc_t myproc;
    pmix_value_t value;
    pmix_value_t * val = &value;
    int error;

    error = PMIx_Init(&myproc);
    if (error != PMIX_SUCCESS) {
        fprintf(stderr, "PMIX_INIT failed ns %s rank %d (%d)\n", myproc.nspace, myproc.rank, error);
        return SHARP_COMM_FAILURE;
    }

    comms_my_pe = myproc.rank;
    printf("my comm pe: %d\n", myproc.rank);

    error = PMIx_Get(&myproc, PMIX_UNIV_SIZE, NULL, 0, &val);
    if (error != PMIX_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    comms_size = val->data.uint32;
    PMIX_VALUE_RELEASE(val);

    printf("my comm size: %lu\n", comm_size);

    return SHARP_OK;
}

int comms_rte_init(int argc, char ** argv)
{
    return internal_pmi_init(argc, argv);
}

static inline int _pmix_exchange_address(void * worker_address,
                                         size_t worker_len,
                                         void *** worker_address)
{
    pmix_info_t *info;
    pmix_pdata_t * pdata;
    pmix_byte_object_t buffer_object;
    void * group_addresses; 
    char key[32]; 
    size_t i;
    int error = 0;
    int ret = 0;
    
    /* Package the data */
    sprintf(key, "%lu", comms_my_pe);
    buffer_object.bytes = malloc(worker_len);
    buffer_object.size = worker_len;
    memcpy(buffer_object.bytes, worker_address, buffer_object.size);
    PMIX_INFO_CREATE(info, 1);
    info[0].value.type = PMIX_BYTE_OBJECT;
    info[0].value.data.bo = buffer_object;
    strcpy(info[0].key, key);
     /* Ship the data */
    error = PMIx_Publish(info, 1);
    if(error != PMIX_SUCCESS) {
        PMIX_INFO_FREE(info, 1);
        return SHARP_COMM_FAILURE;
    }
    PMIX_INFO_FREE(info, 1);
     /* Fence (Global Barrier) to ensure the data is there */
    error = PMIx_Fence(NULL, 0, NULL, 0);
    if(error != PMIX_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    
    /* allocate ucp_endpoints */
    sharp_endpoints = (ucp_ep_h *)malloc(sizeof(ucp_ep_h) * comms_size);
    group_addresses = (void *)malloc(worker_len * comms_size);
     /* Perform a lookup on all other addresses */ 
    PMIX_PDATA_CREATE(pdata, comms_size-1);
    for(i=0;i<comms_size;i++) {
        //unsigned long rank;
        
        /* we do not need an endpoint to ourself */
        if(i == comms_my_pe) {
            continue;
        }
        sprintf(key, "%lu", i);
        error = PMIx_Lookup(&pdata[i], 1, NULL, 0);
        if(error != PMIX_SUCCESS) { /* Comm error */
            ret = SHARP_COMM_FAILURE;
            PMIX_PDATA_RELEASE(pdata);
            goto failure;
        }
         if(pdata[0].proc.rank != i) { /* Error in PMIX */
            ret = SHARP_COMM_FAILURE;
            PMIX_PDATA_RELEASE(pdata);
            goto failure;
        }
         /* store the remote worker's address */
        memcpy(group_addresses+i, (void *) &(pdata[0].value.data.bo), worker_len);    
    }
    /* free the data */
    PMIX_PDATA_RELEASE(pdata);
    
    /* create endpoints */       
    for(i=0; i<comms_size; i++) {
        ucs_status_t status;
        if(i == comms_my_pe) {
            continue;
        }
        status = ucp_ep_create(sharp_ucp_worker, 
                               group_addresses+i, 
                               &sharp_endpoints[i]);
        if(status != UCS_OK) {
            ret = SHARP_COMM_FAILURE;
        }
    }
     /* ... and done */
failure:
    free(group_addresses);
    return ret;
}

int comms_rte_exchange_address(

int pmix_barrier_all(void)
{
    int rc;
    pmix_proc_t proc;

    proc = myproc;
    proc.rank = PMIX_RANK_WILDCARD;
    
    rc = PMIx_Fence(&proc, 1, NULL, 0);
    if (rc != PMIX_SUCCESS) {
        fprintf(stderr, "Barrier Failed\n");
        return -1;
    }

    return 0;
}

int internal_pmix_finalize(void)
{
    int rc;
    pmix_info_t barrier_info;
    pmix_value_t barrier_flag;

    barrier_flag.type = PMIX_BOOL;
    barrier_flag.data.flag = 1;

    strcpy(barrier_info.key, "PMIX_EMBED_BARRIER");
    barrier_info.value = barrier_flag;

    /*
     * with a barrier
     */
    rc = PMIx_Finalize(&barrier_info, 1);
    return 0;
}


#else 
int comms_rte_exchange_address(void * buffer,
                               size_t length,
                               void * worker_address, 
                               size_t worker_len,
                               void *** worker_param_addresses,
                               void *** pack_param)
{
    return SHARP_COMM_FAILURE;
}

int comms_rte_barrier(void) {
    return SHARP_COMM_FAILURE;
}
    
int comms_rte_init(int argc, char ** argv) 
{
    return SHARP_COMM_FAILURE;
}

#endif /* WITH_RTE */

