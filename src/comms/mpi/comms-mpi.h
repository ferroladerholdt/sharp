/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef SHARP_COMMS_MPI_H
#define SHARP_COMMS_MPI_H

#include <mpi.h>
#include <ucs/debug/memtrack.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sharp/sharp_errors.h>

/*
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief The network data associated with the MPI communication layer. This 
 * is used for communication within an allocation group.
 */
typedef struct network_data {
    MPI_Win * win; /*< The MPI Window information for a particular group */
} network_data_t;

extern network_data_t * nw_data; 

extern size_t comms_my_pe;
extern size_t comms_size;
extern int sharp_nr_groups;   
extern int sharp_nw_data_threshold;



static inline int sharp_comms_get_pe(void *nw_info, int *pe) 
{
    *pe = (int)comms_my_pe;   
    return SHARP_OK;
}

static inline int sharp_comms_get_size(void *nw_info, int *size)
{
    *size = (int)comms_size;
    return SHARP_OK;
}

static inline int sharp_comms_init(int argc, char ** argv) 
{
    int flag = 0;
    MPI_Initialized(&flag);
    if(flag == 0) {
        int error = 0;
        error = MPI_Init(&argc, &argv);
        if(error != MPI_SUCCESS) {
            return error;
        }
    }     
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    comms_my_pe = (size_t) rank;
    comms_size = (size_t) size;

    sharp_nw_data_threshold = 32;
    sharp_nr_groups = 0;
    nw_data = (network_data_t *)ucs_malloc(sizeof(network_data_t)*sharp_nw_data_threshold);
    memset(nw_data, 0, sizeof(network_data_t) * sharp_nw_data_threshold);

    return SHARP_OK;
}

static inline int sharp_comms_finalize(void) 
{
    int flag = 0;
    MPI_Finalized(&flag);
    if(flag == 0) {
        return MPI_Finalize();
    } else {
        return SHARP_OK;
    }
}

static inline int sharp_comms_flush_pe(int pe, unsigned int group_id) 
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;

    return MPI_Win_flush(pe, *win);
} 

static inline int sharp_comms_flush(unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    int error;

    error = MPI_Win_flush_all(*win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;
}

static inline int sharp_comms_barrier(sharp_group_allocated_t * ag)
{
    MPI_Comm comm = ag->nw_group->group_info;
    int error;
    error = MPI_Barrier(comm);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;

}

static inline int sharp_comms_register_buffer(void * buffer, 
                                           size_t length,
                                           sharp_group_network_t *nw_group,
                                           unsigned int group_id) 
{
    network_data_t * net_data = &nw_data[group_id];
    net_data->win = (MPI_Win *)ucs_malloc(sizeof(MPI_Win));
    MPI_Comm comm; 
    MPI_Info info;
    int error;

    comm = (MPI_Comm) nw_group->group_info;

    MPI_Info_create(&info);
    MPI_Info_set(info, "same_size", "true");
    MPI_Info_set(info, "same_disp", "true");

    /* 
     * FIXME: if this fails, MPI will likely fail. However, that will not 
     * always be true. It will be useful to grab the error code returned
     * by this and pass back a failure to the user. 
     */
    error = MPI_Win_create(buffer, 
                           length, 
                           1, 
                           info, 
                           comm,
                           net_data->win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }

    error = MPI_Win_lock_all(MPI_MODE_NOCHECK, *(net_data->win));
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_lock_acquire(unsigned int pe, 
                                           unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    int error;

    error = MPI_Win_lock(MPI_LOCK_SHARED,
                         pe,
                         0,
                         *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE; /* FIXME: make me more meaningful */
    }
    return SHARP_OK;
}

static inline int sharp_comms_lock_release(unsigned int pe, 
                                           unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    int error;

    error = MPI_Win_unlock(pe, *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE; /* FIXME: make me more meaningful */
    }
    return SHARP_OK;
}

static inline int sharp_comms_put(void * inbuf,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    MPI_Datatype datatype = MPI_UNSIGNED_CHAR;
    int error;

    error = MPI_Put(inbuf,
                    size,
                    datatype,
                    pe,
                    remote_offset,
                    size,
                    datatype,
                    *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }

    error = MPI_Win_flush(pe, *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;
}

static inline int sharp_comms_get(void * outbuf,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    MPI_Datatype datatype = MPI_UNSIGNED_CHAR;
    int error;

    error = MPI_Get(outbuf,
                    size,
                    datatype,
                    pe,
                    remote_offset,
                    size,
                    datatype,
                    *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }

    error = MPI_Win_flush(pe, *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;
}

static inline int sharp_comms_put_nbi(void * inbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    MPI_Datatype datatype = MPI_UNSIGNED_CHAR;
    int error;

    error = MPI_Put(inbuf_base,
                    size,
                    datatype,
                    pe,
                    remote_offset,
                    size,
                    datatype,
                    *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }


    return SHARP_OK;
}

static inline int sharp_comms_get_nbi(void * outbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    network_data_t * nd = &nw_data[group_id];
    MPI_Win * win = (MPI_Win *)nd->win;
    MPI_Datatype datatype = MPI_UNSIGNED_CHAR;
    int error;

    error = MPI_Get(outbuf_base,
                    size,
                    datatype,
                    pe,
                    remote_offset,
                    size,
                    datatype,
                    *win);
    if(error != MPI_SUCCESS) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_atomic_swap64(uint64_t val,
                                            uint64_t * buf,
                                            size_t remote_offset,
                                            unsigned int pe,
                                            unsigned int group_id)
{
    int err = MPI_Fetch_and_op(&val,
                               buf,
                               MPI_UINT64_T,
                               pe,
                               remote_offset,
                               MPI_REPLACE,
                               *nw_data[group_id].win);
    if(err) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;
}

static inline int sharp_comms_atomic_cswap64(uint64_t * buf,
                                    uint64_t expected,
                                    size_t remote_offset,
                                    unsigned int pe,
                                    unsigned int group_id)
{
    uint64_t out;
    int err;
    err = MPI_Compare_and_swap(buf,
                               &expected,
                               &out,
                               MPI_UINT64_T,
                               pe,
                               remote_offset,
                               *nw_data[group_id].win);

    if( err ){
        return SHARP_COMM_FAILURE;
    }

    *buf = out;
    if( expected != out ){
        return SHARP_ERR_BAD_EXPECTED_VALUE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_atomic_fadd64(uint64_t local,
                                     size_t remote_offset,
                                     uint64_t pe,
                                     uint64_t *out,
                                     unsigned int group_id)
{
    int err = MPI_Fetch_and_op(&local,
                               out,
                               MPI_UINT64_T,
                               pe,
                               remote_offset,
                               MPI_SUM,
                               *nw_data[group_id].win);

    if( err ){
        return SHARP_COMM_FAILURE;
    } 
    return SHARP_OK;

}

static inline int sharp_comms_atomic_add64(uint64_t local,
                                     size_t remote_offset,
                                     uint64_t pe,
                                     unsigned int group_id)
{
    uint64_t out;
    int err = MPI_Fetch_and_op(&local,
                             &out,
                             MPI_UINT64_T,
                             pe,
                             remote_offset,
                             MPI_REPLACE,
                             *nw_data[group_id].win);

    if( err ){
        return SHARP_COMM_FAILURE;
    } 
    return SHARP_OK;
}

#endif
