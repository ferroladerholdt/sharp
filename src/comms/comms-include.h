/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef SHARP_COMMS_H
#define SHARP_COMMS_H

#include <sharp-config.h>

#if COMM_UCX == 1
#include <comms/ucx/comms-ucx.h>
#endif /* COMM_UCX == 1 */

#if COMM_MPI == 1
#include <comms/mpi/comms-mpi.h>
#endif /* COMM_MPI == 1 */

#if COMM_SHMEM == 1
#include <comms/shmem/comms-shmem.h>
#endif

/* cannot have two communication substrates */
#if (COMM_SHMEM == COMM_MPI) && (COMM_MPI == COMM_UCX)
#include <comms/null/comms-null.h>
#endif

extern size_t comms_my_pe;
extern size_t comms_size; 

extern int sharp_nr_groups;   
extern int sharp_nw_data_threshold;
 
/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the PE index/rank/number for a calling PE based on the 
 * network group's information. 
 *
 * @param [in]  *nw_info                The network group for the caller (i.e., MPI communicator, OpenSHMEM active set, etc.)
 * @param [out] *pe                     The caller's PE index in the network
 *                                      group
 * @returns an error code                                     
 */
static inline int sharp_comms_get_pe(void *nw_info, int *pe);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the size of a network group or the world depending on the 
 * the network information passed in
 *
 * @param [in]  *nw_info                The network group information for the caller (i.e., MPI communicator, OpenSHMEM active set, etc.)
 * @param [out] *size                   The caller's network's size
 *
 * @returns an error code
 */ 
static inline int sharp_comms_get_size(void *nw_info, int *size);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Initializes the underlying communication layer
 *
 * @param [in] argc                     The command line argument count for this program
 * @param [in] **argv                   The command line arguments for this program
 *
 * @returns an error code
 */
static inline int sharp_comms_init(int argc, char **argv);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Registration of SharP or user allocated data with the network. 
 * This will give a reference from the allocated memory to the 
 * communication layer.
 *
 * @param [in] *buffer          The buffer to be used with the network
 * @param [in] length           The length of the buffer in bytes
 * @param [in] *nw_group        A reference to the network group
 * @param [in] group_id         The associated allocation group's group id
 * @returns An error code
 */
static inline int sharp_comms_register_buffer(void * buffer,
                                              size_t length,
                                              sharp_group_network_t *nw_group,
                                              unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Calls the communication layer's collective barrier operation for the 
 * network group associated with an allocation group.
 *
 * @param [in] *ag              The allocation group to call the barrier
 *
 * @returns An error code
 */
static inline int sharp_comms_barrier(sharp_group_allocated_t * ag);

/* TODO: need to better define the semantics of this lock... */
/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Acquire a lock on the communication buffer. Attempt to leverage 
 * the network layer's locking mechanisms if available. 
 *
 * @param [in] pe               The PE whose buffer is to be locked
 * @param [in] group_id         The ID of the allocation group containing the 
 *                              PE
 * @returns SHARP_OK on success and SHARP_COMM_FAILURE on failure
 */
static inline int sharp_comms_lock_acquire(unsigned int pe, unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Acquire a lock on the communication buffer. Attempt to leverage 
 * the network layer's locking mechanisms if available. 
 *
 * @param [in] pe               The PE whose buffer is to be locked
 * @param [in] group_id         The ID of the allocation group containing the 
 *                              PE
 * @returns SHARP_OK on success and SHARP_COMM_FAILURE on failure.
 */
static inline int sharp_comms_lock_release(unsigned int pe, unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Complete all outstanding RMA operations between the calling 
 * and target PE within an allocation group. 
 *
 * @param [in] pe               The PE to complete RMA operations with
 * @param [in] group_id         The ID of the allocation containing the PE
 * 
 * @returns SHARP_OK on success and SHARP_COMM_FAILURE on failure. 
 */
static inline int sharp_comms_flush_pe(int pe, unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Complete all outstanding RMA operations by the calling PE to all 
 * other PEs within an allocation group. Calling PE must be in the allocation
 * group. 
 *
 * @param [in] group_id         The ID of the allocation group to be flushed
 * 
 * @returns SHARP_OK on success and SHARP_COMM_FAILURE on failure. 
 *
 */
static inline int sharp_comms_flush(unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Puts bytes from a local buffer to another PE's data structure's 
 * buffer. This assumes that both PEs are within the same allocation group.
 * Call will block until completion.
 *
 * @param [in] *inbuf              The address of the buffer with the data 
 *                                 being placed on the network
 * @param [in] size                The size of the data being placed on the 
 *                                 network
 * @param [in] remote_offset       The offset in bytes for the remote buffer
 * @param [in] pe                  The PE index receiving the data
 * @param [in] group_id            The allocation group's group id.
 * @returns Error code
 */
static inline int sharp_comms_put(void * inbuf,
                                  size_t size,
                                  size_t remote_offset,
                                  unsigned int pe,
                                  unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Puts bytes from a local buffer to another PE's buffer. Assumes both
 * PEs are within the same allocation group. Call does not block until 
 * completion. A flush is necessary to guarantee completion. 
 *
 * @param [in] *inbuf              The address of the buffer with the data 
 *                                 being placed on the network
 * @param [in] size                The size of the data being placed on the 
 *                                 network
 * @param [in] remote_offset       The offset in bytes for the remote buffer
 * @param [in] pe                  The PE index receiving the data
 * @param [in] group_id            The allocation group's group id.
 * @returns Error code

 */
static inline int sharp_comms_put_nbi(void * inbuf_base,
                                      size_t size,
                                      size_t remote_offset,
                                      unsigned int pe,
                                      unsigned int group_id);


/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Retrieves bytes of data from the buffer of another PE and places the
 * data into a local buffer. This assumes that both PEs are within the same 
 * allocation group.
 *
 * @param [in] *outbuf             The address of the buffer to store the 
 *                                 data from the network
 * @param [in] size                The size in bytes to pull from a remote PE's
 *                                 buffer
 * @param [in] remote_offset       The offset in bytes for the remote buffer
 * @param [in] pe                  The PE index which has the data
 * @param [in] group_id            The group id of the allocation group
 * @returns Error code
 */
static inline int sharp_comms_get(void * outbuf,
                                  size_t size,
                                  size_t remote_offset,
                                  unsigned int pe,
                                  unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Retrieves bytes of data from the buffer of another PE and places the
 * data into a local buffer. This assumes that both PEs are within the same 
 * allocation group. This is a non-blocking call. A flush will be necessary to 
 * guarantee completion of the operation.
 *
 * @param [in] *outbuf             The address of the buffer to store the 
 *                                 data from the network
 * @param [in] size                The size in bytes to pull from a remote PE's
 *                                 buffer
 * @param [in] remote_offset       The offset in bytes for the remote buffer
 * @param [in] pe                  The PE index which has the data
 * @param [in] group_id            The group id of the allocation group
 * @returns Error code
 */
static inline int sharp_comms_get_nbi(void * outbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id);


/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Swaps a 64 bit value 
 * @param [in]  val             The value to be swapped into memory
 * @param [out] buf             The retrieved value will be stored here
 * @param [in]  remote_offset   The offset in bytes to the value on the 
 *                              remote pe
 * @param [in]  pe              The PE index being operated on
 * @param [in]  group_id        The group ID of the allocation group
 *
 * @returns SHARP_OK if the swap was successful and SHARP_COMM_FAILURE if
 * there is an error with the underlying communication layer during the 
 * swap. 
 */
static inline int sharp_comms_atomic_swap64(uint64_t val,
                                            uint64_t * buf,
                                            size_t remote_offset,
                                            unsigned int pe,
                                            unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Swaps a 64 bit value if and only if the expected value is equal to
 * the current value.
 *
 * @param [in,out] buf             The retrieved value will be stored here and
 *                                 the data to be swapped in is read from here.
 * @param [in] expected            The value we expect the remote value to be
 * @param [in] remote_offset       The offset in bytes where the compared value 
 *                                 can be found on the pe
 * @param [in] pe                  The PE index which has the compared value
 * @param [in] group_id            The group id of the allocation group
 *
 * @returns SHARP_OK if the swap was successful and SHARP_ERR_BAD_EXPECTED_VALUE
 * if the swap failed because the expected value differed from the value found. 
 * SHARP_COMM_FAILURE is returned if there was a failure on the underlying 
 * communication layer
 */
static inline int sharp_comms_atomic_cswap64(uint64_t * buf,
                               uint64_t expected,
                               size_t remote_offset,
                               unsigned int pe,
                               unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Atomically add a local and remote 64bit number.
 *
 * @param [in] local               The value to be added to the remote value
 * @param [in] offset              The offset in bytes where the second value 
 *                                 can be found on the pe given below
 * @param [in] pe                  The PE index which has the compared value
 * @param [in] group_id            The group id of the allocation group
 *
 * @returns SHARP_OK if the operation was successful 
 * SHARP_COMM_FAILURE is returned if there was a failure on the underlying 
 * communication layer
 */
static inline int sharp_comms_atomic_add64(uint64_t local,
                                     size_t offset,
                                     uint64_t pe,
                                     unsigned int group_id);

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Atomically fetch and add a local and remote 64bit number.
 *
 * @param [in] local               The value to be added to the remote value
 * @param [in] offset              The offset in bytes where the second value 
 *                                 can be found on the pe given below
 * @param [in] pe                  The PE index which has the compared value
 * @param [out] out                The remote value before the operation
 * @param [in] group_id            The group id of the allocation group
 *
 * @returns SHARP_OK if the operation was successful 
 * SHARP_COMM_FAILURE is returned if there was a failure on the underlying 
 * communication layer
 */
static inline int sharp_comms_atomic_fadd64(uint64_t local,
                                     size_t offset,
                                     uint64_t pe,
                                     uint64_t *out,
                                     unsigned int group_id);


#endif
