/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef SHARP_COMMS_UCX_H
#define SHARP_COMMS_UCX_H

#include <sharp-config.h>

#include <ucs/sys/compiler_def.h>
#include <ucp/api/ucp.h>
#include <ucp/api/ucp_def.h>

#include <utils/sharp/sharp_errors.h>
#include <sharp_groups/api/sharp_groups.h>
#include <comms/sharp-rte-interface.h>

#include <unistd.h>
#include <sched.h>

#include <rte.h>

/* I'm running with something also interfacing RTE; so don't use RTE */
#define RTE_INTERFACE_WITH_MPI    1
#define RTE_INTERFACE_WITH_SHMEM  2

extern size_t comms_my_pe;
extern size_t comms_size; 
extern unsigned int global_alloc_group_id_counter;

extern int sharp_nr_groups;   
extern int sharp_nw_data_threshold;
extern void * internal_buffer;
extern int sharp_barrier_buffer;
extern char sharp_rte_interface_flag;

extern int (*comms_data_exchange)(void * buffer,
                           void *** pack_param,
                           uint64_t * raddr,
                           void * register_buffer,
                           void * nw_group_info);

static inline int sharp_comms_register_buffer(void * buffer,
                                              size_t length,
                                              sharp_group_network_t *nw_group,
                                              unsigned int groupid);
static inline int __sharp_barrier(void); 

/*
 * @internal
 * @ingroup SHARP_INTERNAL
 * The data necessary to make use of the network
 */
typedef struct network_data {
    int inuse;                      /*< Whether or not this group is in use */
    uint64_t *remote_addresses;     /*< Each PE's remote buffers */
    ucp_rkey_h *rkeys;              /*< Remote keys for RMA */
    ucp_mem_h register_buffer;      /*< Locally registered memory buffer */
} network_data_t;

/* used for buffer registration exchanges */
struct data_exchange {
    size_t pack_size;
    uint64_t remote;
    char pack[300];
};

/* used only for MPI-based endpoint creation */
struct worker_exchange {
    size_t worker_len;
    char worker[300];
};

extern network_data_t * nw_data;
extern ucp_context_h sharp_ucp_context;
extern ucp_worker_h sharp_ucp_worker; /* our local worker */
extern ucp_ep_h * sharp_endpoints; /**< An endpoint for every PE */   

#if defined(OMPI_MPI_H) || defined(MPI_INCLUDED) || defined(MPI_INT) 
/*
 * The purpose of this function is to exchange the worker address information
 * between the PEs in the job. This information will later be used to 
 * create endpoints.
 */
static inline int comms_ucp_addr_info_mpi_exchange(void *** param_worker_addrs,
                                                   void * nw_group_info)
{
    struct worker_exchange * rx; 
    struct worker_exchange * dx;
    void ** worker_addresses;
    size_t worker_len;
    void * worker_address;
    int error;
    int i;
    int ret = 0;
    MPI_Comm comm = (MPI_Comm) nw_group_info;

    /* allocate */
    worker_addresses = (void **) malloc(sizeof(void *)*comms_size);
    if (NULL == worker_addresses) {
        return SHARP_ERR_NO_MEMORY;
    }

    error = ucp_worker_get_address(sharp_ucp_worker,
                                   (ucp_address_t **) &worker_address,
                                   &worker_len);
    if(error < 0) {
        free(worker_addresses);
        return SHARP_COMM_FAILURE;
    }
    
    /* pack */
    rx = (struct worker_exchange *) 
        malloc(sizeof(struct worker_exchange)*comms_size);
    if (NULL == rx) {
        ret = SHARP_ERR_NO_MEMORY;
        goto fail_pack;
    }

    dx = (struct worker_exchange *) malloc(sizeof(struct worker_exchange));
    if (NULL == dx) {
        ret = SHARP_ERR_NO_MEMORY;
        free(rx);
        goto fail_pack;
    }

    dx->worker_len = worker_len;
    memcpy(&dx->worker, worker_address, worker_len);

    /* exchange */
    error = MPI_Allgather(dx,
                          1,
                          sharp_ucx_mpi_worker_exchange,
                          rx,
                          1,
                          sharp_ucx_mpi_worker_exchange,
                          comm);
    if (error != MPI_SUCCESS) {
        ret = SHARP_COMM_FAILURE;
        goto fail_exchange;
    }

    /* set up */
    for (i = 0; i < comms_size; i++) {
        worker_addresses[i] = malloc(rx[i].worker_len);
        if (NULL == worker_addresses[i]) {
            ret = SHARP_ERR_NO_MEMORY;
            goto fail_setup;
        }

        memcpy(worker_addresses[i], rx[i].worker, rx[i].worker_len);        
    }

    free(dx);
    free(rx);
    *param_worker_addrs = worker_addresses;
    
    return ret;

fail_setup:
    for (--i; i >= 0; i--) {
        free(worker_addresses[i]);
    }
fail_exchange:
    free(rx);
    free(dx);
fail_pack:
    free(worker_addresses);
    free(sharp_endpoints);

    return ret;
}                                               

/*
 * This function creates the ucp endpoints used for communication by SharP.
 * This leverages MPI to perform the data exchange
 */
static inline int comms_create_ucp_endpoints(void * buffer,
                                             size_t length,
                                             void * nw_group_info) 
{
    int error = 0;
    void ** worker_addresses = NULL;
    ucp_ep_params_t ep_params;
    int i;
    
    sharp_endpoints = 
        (ucp_ep_h *)malloc(comms_size * sizeof(ucp_ep_h));
    if (NULL == sharp_endpoints) {
        return SHARP_ERR_NO_MEMORY;
    }

    error = comms_ucp_addr_info_mpi_exchange(&worker_addresses,
                                             nw_group_info);
    if (error != SHARP_OK) {
        free(sharp_endpoints);
        return SHARP_EXCHANGE_FAILURE;
    }
    
    for (i = 0; i < comms_size; i++) {
        ep_params.field_mask = UCP_EP_PARAM_FIELD_REMOTE_ADDRESS;
        ep_params.address = (ucp_address_t *)worker_addresses[i];
        error = ucp_ep_create(sharp_ucp_worker,
                              &ep_params,
                              &sharp_endpoints[i]);
        if (error != UCS_OK) {
            free(sharp_endpoints);
            return SHARP_COMM_FAILURE;
        }
        free(worker_addresses[i]);
    }
    free(worker_addresses);
     
    return SHARP_OK;
}

/*
 * This function provides the functionality to allow for the interoperability
 * with applications making use of both SharP while using the UCX 
 * communication layer and MPI implementations that make use of the 
 * Open Run-Time Environment (OpenRTE). This is necessary because SharP with
 * UCX leverages librte and OpenRTE. Rather than attempting to have both 
 * SharP and MPI make use of OpenRTE at the same time, which will result in
 * a failure on the second attempt to initialize OpenRTE, we will leverage 
 * the MPI implementation to perform an exchange of UCX address related 
 * information in order to allow for allocation group creation. 
 *
 * This works because of the functionality is constrained to a header 
 * file that is included in both SharP and the user's application. As 
 * each function declared in this header is declared as static, the
 * compilation of each function is tied to its specific translation
 * unit through internal linkage. This means that a user can compile 
 * SharP with UCX support and then use SharP with UCX alongside MPI 
 * without having to recompile the SharP library. Because the user's
 * application is the one calling sharp_comms_init(), the function
 * pointer (i.e., comms_data_exchange()) can point to the correct 
 * address exchange function at runtime.
 */
static inline int comms_mpi_exchange_address(void * buffer,
                                             void *** pack_param,
                                             uint64_t * remotes,
                                             void * register_buffer,
                                             void * nw_group_info)
{
    int error = 0;
    void ** pack = NULL;
    struct data_exchange * dx;
    struct data_exchange * rx = NULL;
    ucp_mem_h * mem = (ucp_mem_h *)register_buffer;
    size_t pack_size; 
    int ret = 0, i;
    ucs_status_t status;
    MPI_Comm comm = (MPI_Comm) nw_group_info;

    pack = (void **) malloc(sizeof(void *)*comms_size);
    if (NULL == pack) {
        ret = SHARP_ERR_NO_MEMORY;
        goto fail_mpi;
    }

    status = ucp_rkey_pack(sharp_ucp_context, *mem, &pack[comms_my_pe], 
                           &pack_size);
    if (status != UCS_OK) {
        ret = error;
        goto fail_mpi;
    }

    remotes[comms_my_pe] = (uint64_t)buffer;
/*
    TODO:
1. I need to pack all of my data into a buffer, preferably a MPI_Datatype
2. I need to perform a mpi_allgather() on the data
3. I need to loop through the data and pull out the necessary parts
*/

    /* step 1: create a data type */
    rx = (struct data_exchange *)malloc(
                                    sizeof(struct data_exchange)*comms_size);
    dx = (struct data_exchange *)malloc(sizeof(struct data_exchange));
    dx->pack_size = pack_size;
    memcpy(&dx->pack, pack[comms_my_pe], pack_size);
    dx->remote = remotes[comms_my_pe];

    /* step 2: perform the allgather on the data */
    MPI_Allgather(dx, 
                  1, 
                  sharp_ucx_mpi_buffer_exchange, 
                  rx, 
                  1, 
                  sharp_ucx_mpi_buffer_exchange, 
                  comm);


    /* step 3: loop over rx and pull out the necessary parts */ 
    /* obtain the network information here... */
    for (i=0;i<comms_size;i++) {
        if (i == comms_my_pe) {
            continue;
        }

        /*FIXME: i'm ignoring the worker length and pack size */
        remotes[i] = rx[i].remote;
        pack[i] = malloc(pack_size);
        if (NULL == pack[i]) {
            ret = SHARP_ERR_NO_MEMORY;
            goto fail_purge_arrays;
        }
        memcpy(pack[i], rx[i].pack, pack_size);
    }
    
    free(rx);
    free(dx);
    *pack_param = pack; 

    return ret;

fail_purge_arrays:
    for (--i;i>=0;i--) {
        free(pack[i]);
    }
fail_mpi:
    if (rx != NULL) {
        free(rx);
    }
    if (NULL != pack) {
        free(pack);
    }
    return ret;
}
#else /* UCX/RTE only below */ 
/*
 * This function creates the ucp endpoints used for communication by SharP. 
 * Additionally, this will register an internal buffer with the NIC in order
 * to ensure later buffer registrations are successful.  
 */
static inline int comms_create_ucp_endpoints(void * buffer, 
                                             size_t length,
                                             void * nw_group_info) 
{
    int error = 0;
    void ** worker_addresses = NULL;
    void ** pack = NULL;
    int i;
    int groupid = 0;
    ucp_mem_map_params_t mem_map_params;
    ucp_ep_params_t ep_params;

    sharp_endpoints = 
        (ucp_ep_h *)malloc(comms_size * sizeof(ucp_ep_h));
    if (NULL == sharp_endpoints) {
        return SHARP_ERR_NO_MEMORY;
    }

    nw_data[groupid].rkeys = 
        (ucp_rkey_h *) malloc(sizeof(ucp_rkey_h)*comms_size);
    if (NULL == nw_data[groupid].rkeys) {
        error = SHARP_ERR_NO_MEMORY;
        goto fail_key_allocate;
    }

    nw_data[groupid].remote_addresses = 
        (uint64_t *)malloc(sizeof(uint64_t)*comms_size);
    if (NULL == nw_data[groupid].remote_addresses) {
        error = SHARP_ERR_NO_MEMORY;
        goto fail_remote_allocate;
    }

    mem_map_params.address = buffer;
    mem_map_params.length = length;
    mem_map_params.field_mask = UCP_MEM_MAP_PARAM_FIELD_ADDRESS 
                        | UCP_MEM_MAP_PARAM_FIELD_LENGTH;

    error = ucp_mem_map(sharp_ucp_context,
                        &mem_map_params,
                        &nw_data[groupid].register_buffer);
    if (UCS_OK != error) {
        error = SHARP_COMM_FAILURE;
        goto fail_mem_map;
    }

    error = comms_rte_exchange_address(buffer,
                                       &worker_addresses,
                                       &pack,
                                       nw_data[groupid].remote_addresses,
                                       &nw_data[groupid].register_buffer,
                                       nw_group_info);
    if (error != SHARP_OK) {
        goto fail_data_exchange;
    }
    
    for (i = 0; i < comms_size; i++) {
        ep_params.field_mask = UCP_EP_PARAM_FIELD_REMOTE_ADDRESS; 
        ep_params.address = (ucp_address_t *) worker_addresses[i];

        error = ucp_ep_create(sharp_ucp_worker,
                              &ep_params,
                              &sharp_endpoints[i]);
        if (error != UCS_OK) {
            error = SHARP_COMM_FAILURE;
            goto fail_purge_endpoints;
        }
    }

    for (i = 0; i < comms_size; i++) {
        ucp_ep_rkey_unpack(sharp_endpoints[i],
                           pack[i],
                           &nw_data[groupid].rkeys[i]);
        ucp_rkey_buffer_release(pack[i]);
        pack[i] = NULL;
    }

    free(worker_addresses);
    free(pack);
    nw_data[groupid].inuse = 1;
    sharp_nr_groups++;

    return SHARP_OK;
    

fail_purge_endpoints:
    for (--i; i >= 0; i--) {
        ucp_ep_destroy(sharp_endpoints[i]);
    }
    free(worker_addresses);
    free(pack);

fail_data_exchange:
    free(nw_data[groupid].register_buffer);
    nw_data[groupid].register_buffer = NULL;

fail_mem_map:    
    free(nw_data[groupid].remote_addresses);
    nw_data[groupid].remote_addresses = NULL;

fail_remote_allocate:
    free(nw_data[groupid].rkeys);
    nw_data[groupid].rkeys = NULL;

fail_key_allocate:
    free(sharp_endpoints);
    sharp_endpoints = NULL;
     
    return error;
}
#endif

static inline int sharp_comms_get_pe(void *nw_info, int *pe)
{
    *pe = (int)comms_my_pe;
    return SHARP_OK;
}

static inline int sharp_comms_get_size(void *nw_info, int *size)
{
    *size = (int)comms_size;
    return SHARP_OK;
}

static inline int sharp_comms_put_nbi(void * inbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;

    status = ucp_put_nbi(sharp_endpoints[pe], inbuf_base, size, remote_addr, nw_data[group_id].rkeys[pe]);
    if (status < UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_put(void * inbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;

    status = ucp_put(sharp_endpoints[pe], inbuf_base, size, remote_addr, nw_data[group_id].rkeys[pe]);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}


static inline int sharp_comms_get_nbi(void * outbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;

    status = ucp_get_nbi(sharp_endpoints[pe], outbuf_base, size, remote_addr, nw_data[group_id].rkeys[pe]);  
    if (status < UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;

}

static inline int sharp_comms_get(void * outbuf_base,
                            size_t size,
                            size_t remote_offset,
                            unsigned int pe,
                            unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;

    status = ucp_get(sharp_endpoints[pe], outbuf_base, size, remote_addr, nw_data[group_id].rkeys[pe]);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;

}

static inline int sharp_comms_flush_pe(int pe, unsigned int group_id)
{
    ucs_status_t status;

    status = ucp_ep_flush(sharp_endpoints[pe]);
    if (status < UCS_OK) {
        return SHARP_COMM_FAILURE;
    }
    return SHARP_OK;
}

static inline int sharp_comms_flush(unsigned int group_id)
{
    return ucp_worker_flush(sharp_ucp_worker);
}

/*
 * 
 */
static inline int sharp_comms_barrier(sharp_group_allocated_t * ag)
{
#if defined(MPI_INT)
    return MPI_Barrier(ag->nw_group->group_info);
#else
    /* 
     * FIXME: This breaks the semantics defined for the SharP barrier for 
     * MPI/SHMEM. This performs a barrier across all PEs rather than
     * only those within the allocation group. 
     */
    return __sharp_barrier();
#endif /* MPI_INT */
}

static inline int sharp_comms_lock_acquire(unsigned int pe, 
                                           unsigned int group_id)
{
    return SHARP_OK;
}

static inline int sharp_comms_lock_release(unsigned int pe, 
                                           unsigned int group_id)
{
    return SHARP_OK;
}

static inline int sharp_comms_atomic_swap64(uint64_t val,
                                            uint64_t * buf,
                                            size_t remote_offset,
                                            unsigned int pe,
                                            unsigned int group_id) 
{
    ucp_ep_h * remote_ep = &sharp_endpoints[pe];
    ucp_rkey_h * rkey = &nw_data[group_id].rkeys[pe];
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;

    status = ucp_atomic_swap64(*remote_ep,
                               val,
                               remote_addr, 
                               *rkey, 
                               buf);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }
    
    return SHARP_OK;
}    

static inline int sharp_comms_atomic_cswap64(uint64_t * buf,
                               uint64_t expected,
                               size_t remote_offset,
                               unsigned int pe,
                               unsigned int group_id)
{
    ucp_ep_h * remote_ep = &sharp_endpoints[pe];
    ucp_rkey_h * rkey = &nw_data[group_id].rkeys[pe];
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + remote_offset;
    ucs_status_t status;
    uint64_t result;

    status = ucp_atomic_cswap64(*remote_ep,
                                 expected, 
                                 *buf, 
                                 remote_addr, 
                                 *rkey, 
                                 &result);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }
    
    /* set this now */
    *buf = result;
    if (expected != result) {
        return SHARP_ERR_BAD_EXPECTED_VALUE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_atomic_add64(uint64_t local,
                                     uint64_t offset,
                                     uint64_t pe,
                                     unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + offset;
    ucs_status_t status;

    status = ucp_atomic_add64(sharp_endpoints[pe], local, remote_addr, nw_data[group_id].rkeys[pe]);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

static inline int sharp_comms_atomic_fadd64(uint64_t local,
                                     uint64_t offset,
                                     uint64_t pe,
                                     uint64_t *out,
                                     unsigned int group_id)
{
    uint64_t remote_addr = 
        nw_data[group_id].remote_addresses[pe] + offset;
    ucs_status_t status;

    status = ucp_atomic_fadd64(sharp_endpoints[pe], 
                               local, 
                               remote_addr, 
                               nw_data[group_id].rkeys[pe], 
                               out);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    return SHARP_OK;
}

/* 
 * functionality to exchange memory registration information among PEs while
 * using UCP. 
 */
static inline int comms_ucp_addr_info_ucx_exchange(void * buffer,
                                                   void *** pack_param,
                                                   uint64_t * remotes,
                                                   void * register_buffer,
                                                   void * nw_group_info)
{
    int error = 0;
    void ** pack = NULL;
    struct data_exchange dx;
    struct data_exchange * rx = NULL;
    ucp_mem_h * mem = (ucp_mem_h *)register_buffer;
    size_t pack_size; 
    int ret = 0, i;
    ucs_status_t status;
    int k = 0;
    char pack_check[300];

    pack = (void **) malloc(sizeof(void *)*comms_size);
    if (NULL == pack) {
        ret = SHARP_ERR_NO_MEMORY;
        goto fail_exchange;
    }

    status = ucp_rkey_pack(sharp_ucp_context, *mem, &pack[comms_my_pe], 
                           &pack_size);
    if (status != UCS_OK) {
        ret = error;
        goto fail_exchange;
    }

    remotes[comms_my_pe] = (uint64_t)buffer;

    /* pack and exchange information */
    dx.pack_size = pack_size;
    memcpy(&dx.pack, pack[comms_my_pe], pack_size);
    dx.remote = remotes[comms_my_pe];
    memset(internal_buffer+sizeof(int), 
           0, 
           sizeof(struct data_exchange)*comms_size);

    memcpy(internal_buffer+
           (sizeof(int)+sizeof(struct data_exchange)*comms_my_pe), 
           &dx, 
           sizeof(struct data_exchange));
    __sharp_barrier();

    /* attempt to get everyone's data */
    memset(pack_check, 0, 300);
    for (k=0;k<comms_size;k++) {
        //int comms_error;
        if (k == comms_my_pe)
            continue;
        size_t offset = (sizeof(int)+sizeof(struct data_exchange)*k); 
        rx = (struct data_exchange *)(internal_buffer+offset);
        memset(rx, 0, sizeof(struct data_exchange));
        sharp_comms_get_nbi(internal_buffer+offset, 
                            sizeof(struct data_exchange), 
                            offset,
                            k, 
                            0);
    }   
    sharp_comms_flush(0); /* flush for the nbi */
    /* loop to ensure correctness, or at least close to correctness */
    for (k=0;k<comms_size;k++) {
        int comms_error;
        if (k == comms_my_pe) 
            continue;
        size_t offset = (sizeof(int)+sizeof(struct data_exchange)*k); 
        rx = (struct data_exchange *)(internal_buffer+offset);
        for (;;) {    
            if (rx->remote != 0 
               && rx->pack_size >= pack_size-1) {
                int rc = memcmp(rx->pack, pack_check, rx->pack_size);
                if (rc != 0) {
                    break;
                } 
            }
            /* the data failed to retreive? */
            comms_error = sharp_comms_get(internal_buffer+offset, 
                                          sizeof(struct data_exchange), 
                                          offset,
                                          k, 
                                          0);
            if (comms_error < UCS_OK) {
                ret = comms_error;
                goto fail_exchange;
            }
        }
    }

    /* obtain the network information here... */
    for (i=0;i<comms_size;i++) {
        if (i == comms_my_pe) {
            continue;
        }
        rx = (struct data_exchange *)
            (internal_buffer+(sizeof(int)+sizeof(struct data_exchange)*i));

        /*FIXME: i'm ignoring the worker length and pack size */
        remotes[i] = rx->remote;

        pack[i] = malloc(pack_size);
        if (NULL == pack[i]) {
            ret = SHARP_ERR_NO_MEMORY;
            goto fail_purge_arrays;
        }
        
        memcpy(pack[i], rx->pack, pack_size);
    }

    *pack_param = pack; 
    __sharp_barrier();

    return ret;

fail_purge_arrays:
    for (--i;i>=0;i--) {
        free(pack[i]);
    }
fail_exchange:
    if (NULL != pack) {
        free(pack);
    }
    return ret;
} 

static inline int sharp_comms_init(int argc, char **argv)
{
    ucp_params_t ucp_params;
    ucp_config_t * config;
    ucs_status_t status;
    int error = 0;
    void * nw_group_info = NULL;
    ucp_worker_params_t worker_params; 

    /* setup/init the network data structure */
    sharp_nw_data_threshold = 32; 
    sharp_nr_groups = 0;
    nw_data = (network_data_t *)malloc(
                                sizeof(network_data_t)*sharp_nw_data_threshold);
    memset(nw_data, 0, sizeof(network_data_t) * sharp_nw_data_threshold);

    status = ucp_config_read(NULL, NULL, &config);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE; 
    } 

    /*
     * NOTE: UCX with UCP uses an all or nothing approach to determining if
     * a transport can be found with the features the user requests. It's 
     * important to know this because the AMOs are not always present 
     * in every NIC. For example, the mlx4_0 (Mellanox ConnectX-3) does
     * not support 64 bit swaps, but it does support everything else. 
     *
     * If you receive an error from UCX during init, then it is likely 
     * that you will need to either (1) modify UCX to support your 
     * device properly or (2) remove the AMO flag and do not use atomics. 
     */ 
    ucp_params.features = UCP_FEATURE_RMA | UCP_FEATURE_AMO64;
    ucp_params.request_size = 0;
    ucp_params.request_init = NULL;
    ucp_params.request_cleanup = NULL;
    ucp_params.mt_workers_shared = 1;
    ucp_params.field_mask = UCP_PARAM_FIELD_FEATURES
                            | UCP_PARAM_FIELD_MT_WORKERS_SHARED;

    status = ucp_init(&ucp_params, config, &sharp_ucp_context);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    ucp_config_release(config);
    worker_params.thread_mode = UCS_THREAD_MODE_SINGLE;
    worker_params.field_mask = UCP_WORKER_PARAM_FIELD_THREAD_MODE;
    status = ucp_worker_create(sharp_ucp_context, 
                               &worker_params, 
                               &sharp_ucp_worker);
    if (status != UCS_OK) {
        return SHARP_COMM_FAILURE;
    } 

    sharp_rte_interface_flag = 0;
    global_alloc_group_id_counter = 0;

/* hack to detect if we are included within an MPI, shmem, etc program */
#if defined(OMPI_MPI_H) || defined(MPI_INCLUDED) || defined(MPI_INT)
{
    /* MPI Datatype for buffer exchange */
    int buffer_nr_items = 3;
    MPI_Aint buffer_displacements[3];
    int buffer_block_lengths[3] = {1, 1, 300};
    MPI_Datatype buffer_exchange_types[3] = {MPI_UINT64_T, 
                                             MPI_UINT64_T, 
                                             MPI_BYTE};

    /* MPI Datatype for ucp worker address exchange */
    int worker_nr_items = 2;
    MPI_Aint worker_displacements[2];
    int worker_block_lengths[2] = {1, 300};
    MPI_Datatype worker_exchange_types[2] = {MPI_UINT64_T, MPI_BYTE};

    /* Check if MPI has been initialized */
    int flag = 0;
    MPI_Initialized(&flag);
    /* FIXME: The user may or may not expect us to call MPI_Init for them */
    if (flag == 0) {
        int mpi_error;
        /* FIXME: this behavior should be placed on/in the wiki/docs */
        printf("i'm initing mpi\n");
        mpi_error = MPI_Init(&argc, &argv);
        if(mpi_error != MPI_SUCCESS) {
            return SHARP_COMM_FAILURE;
        }
    }

    sharp_rte_interface_flag = RTE_INTERFACE_WITH_MPI;
    comms_data_exchange = &comms_mpi_exchange_address;

    /* since we are simply initing here, we'll just use comm world info */
    MPI_Comm_rank(MPI_COMM_WORLD, (int *)&comms_my_pe);
    MPI_Comm_size(MPI_COMM_WORLD, (int *)&comms_size);

    buffer_displacements[0] = offsetof(struct data_exchange, pack_size);
    buffer_displacements[1] = offsetof(struct data_exchange, remote);
    buffer_displacements[2] = offsetof(struct data_exchange, pack);

    worker_displacements[0] = offsetof(struct worker_exchange, worker_len);
    worker_displacements[1] = offsetof(struct worker_exchange, worker);        

    /* create an exchange data type for group creation/buffer registration */
    MPI_Type_create_struct(buffer_nr_items, 
                           buffer_block_lengths, 
                           buffer_displacements, 
                           buffer_exchange_types, 
                           &sharp_ucx_mpi_buffer_exchange);
    MPI_Type_commit(&sharp_ucx_mpi_buffer_exchange);

    /* create an exchange data type for UCP worker information exchange */
    MPI_Type_create_struct(worker_nr_items,
                           worker_block_lengths,
                           worker_displacements,
                           worker_exchange_types,
                           &sharp_ucx_mpi_worker_exchange);
    MPI_Type_commit(&sharp_ucx_mpi_worker_exchange);

    nw_group_info = MPI_COMM_WORLD;

    /* create our endpoints here */
    error = comms_create_ucp_endpoints(NULL, 
                                           0,
                                           nw_group_info);
    if (error != SHARP_OK) {
        return SHARP_COMM_FAILURE;
    } 
}
#else
    /* initialize librte */
    error = comms_rte_init(argc, argv);
    if (error != 0) {
        return SHARP_COMM_FAILURE;
    }

    /* let's do an initial exchange to create sharp related data structures */
    /*  the internal buffer should look as follows:
     *      barrier_integer <-- 4 bytes
     *      data_exchange   <-- sizeof(struct data_exchange) * comms_size
     */ 
    size_t buffer_size = sizeof(int) + 
                            sizeof(struct data_exchange)*comms_size;
    internal_buffer = malloc(buffer_size);
    if (internal_buffer == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }
    memset(internal_buffer, 
           0, 
           sizeof(struct data_exchange)*comms_size+sizeof(int));

    comms_data_exchange = &comms_ucp_addr_info_ucx_exchange;
    sharp_barrier_buffer = 0;
    
    comms_rte_barrier();

    error = comms_create_ucp_endpoints(internal_buffer,
                                           buffer_size,
                                           nw_group_info);
    if (SHARP_OK != error) {
        free(internal_buffer);
        return SHARP_COMM_FAILURE;
    }

    global_alloc_group_id_counter = 1;
#endif

    return SHARP_OK;
}

static inline int sharp_comms_finalize_group(unsigned int group_id) 
{
    int i = 0;
    
    for (i=0;i<comms_size;i++) {
        nw_data[group_id].remote_addresses[i] = 0;
        if (nw_data[group_id].rkeys[i] != NULL) {
            ucp_rkey_destroy(nw_data[group_id].rkeys[i]);
        }
    }

    free(nw_data[group_id].remote_addresses);
    ucp_mem_unmap(sharp_ucp_context,nw_data[group_id].register_buffer);
    nw_data[group_id].inuse = 0;
    return SHARP_OK;
}

static inline int sharp_comms_finalize(void) 
{
    int i = 0;

    /* 
     * have everyone sync here so that memory is still available on 
     * remote PEs 
     */ 
#if defined(OMPI_MPI_H) || defined(MPI_INCLUDED) || defined(MPI_INT) 
    MPI_Barrier(MPI_COMM_WORLD);
#else 
    comms_rte_barrier();
#endif
    /* 
     * loop over all the network groups  and free their 
     * memories
     */
    for (i=0;i<sharp_nr_groups; i++) {
        if (nw_data[i].inuse != 1) {
            continue;
        }
        sharp_comms_finalize_group(i);
    }

    /* destroy our endpoints */
    for (i=0;i<comms_size; i++) {
        ucp_ep_destroy(sharp_endpoints[i]);
    }

    free(sharp_endpoints);
    ucp_worker_destroy(sharp_ucp_worker); 
    ucp_cleanup(sharp_ucp_context); 

#if !defined(MPI_INT) && defined(WITH_RTE) /* no mpi */
    rte_finalize();
    free(internal_buffer);
#else  /* mpi */
    MPI_Type_free(&sharp_ucx_mpi_buffer_exchange);
    MPI_Type_free(&sharp_ucx_mpi_worker_exchange);
#endif
    free(nw_data);
    return SHARP_OK;
}

/*
 * This is a simple barrier which is linear in complexity and depends on a
 * pre-allocated buffer, which is in the ``internal buffer'' used if the 
 * application is using SharP for both data management and communication. 
 *
 * An alternative solution to this would be to make use of tagged 
 * messages (e.g., ucp_tag_send_nb() ucp_tag_recv_nb()), which can be used 
 * on a group to group basis and does not require a preallocated buffer.   
 *
 * Returns SHARP_COMM_FAILURE on failure, and SHARP_OK on success
 */
static inline int __sharp_barrier(void) 
{
    size_t pe = comms_my_pe;
    size_t size = comms_size;
    size_t offset = 0;
    int b;
    int target = sharp_barrier_buffer+2;
    b = target-1;
    int id = 0;
    int error;
    
    int rpartner = (pe+1) > size-1 ? 0 : pe+1;
    int lpartner = (pe-1) < 0 ? size-1 : pe-1;

    /* single PE, no barrier */
    if (size == 1)
        return SHARP_OK;
   
    if ((pe&1) == 0 && pe < (size-1)) {
        /* unlocking the right neighbor */
        int d;
        /* first test to see if they are in sync with us (i.e., we're on
         * the same barrier */ 
        for (;;) {
            error = sharp_comms_get(&d, sizeof(int), offset, rpartner, id);
            if (error < UCS_OK) {
                return SHARP_COMM_FAILURE;
            }

            if (d == target-2) { /* ok, we are. so break */
                break;
            }
        }
        /* set the right neighbors buffer */
        error = sharp_comms_put(&b, sizeof(int), offset, rpartner, id);
        if (error < UCS_OK) {
            return SHARP_COMM_FAILURE;
        }

        /* same for left neighbor if we have one */
        if (((size-1)&1) == 0) { /* this implies an odd number of pes */
            int e;
            /* are we in sync ? */
            for (;;) {
                error = sharp_comms_get(&e, sizeof(int), offset, lpartner, id);
                if (error < UCS_OK) {
                    return SHARP_COMM_FAILURE;
                }

                if (e == target-2) {
                    break;
                }
            }
            /* unlock */
            sharp_comms_put(&b, sizeof(int), offset, lpartner, id);
        } 
        /* wait for unlock from partner(s); only need 1 */  
        for (;;) {
            d = 0;
            error = sharp_comms_get(&d, sizeof(int), offset, pe, id);
            if (error < UCS_OK) {
                return SHARP_COMM_FAILURE;
            }

            /* we've been unlocked */
            if (d == target-1) {
                break;
            }
        }
    } else if ((pe&1) == 1) { /* odd nr of pes */
        int d = 0;
        /* wait for unlock from my partner(s) */
        for (;;) {
            d = 0;
            error = sharp_comms_get(&d, sizeof(int), offset, pe, id);
            if (error < UCS_OK) {
                return SHARP_COMM_FAILURE;
            }

            /* we've been unlocked */
            if (d == target-1) {
                break;
            }
        }
        /* unlock our partner */
        error = sharp_comms_put(&b, sizeof(int), offset, lpartner, id);  
        if (error < UCS_OK) {
            return SHARP_COMM_FAILURE;
        }
    } else { /* the last pe with an odd comms size (i.e., 3 PEs) */
        int d = 0;
        
        /* my right partner is PE 0 and they will unlock me */
        for (;;) {
            d = 0;
            error = sharp_comms_get(&d, sizeof(int), offset, pe, id);
            if (error < UCS_OK) {
                return SHARP_COMM_FAILURE;
            }

            if (d == target-1) {
                break;
            }
        }
    }

    /* make sure everyone is in sync */
    int i;
    for (i=0;i<size;i++) {
        for (;;) {
            int d = 0;
            error = sharp_comms_get(&d, sizeof(int), offset, i, id);
            if (error < UCS_OK) {
                return SHARP_COMM_FAILURE;
            }

            if (d >= target-1) {
                break;
            }
        }
    } 
    
    /* ok. they are. increment my local barrier buffer */
    error = sharp_comms_put(&target, sizeof(int), offset, pe, id);
    if (error < UCS_OK) {
        return SHARP_COMM_FAILURE;
    }

    sharp_barrier_buffer += 2;
    return SHARP_OK;
}

/* 
 * This will exchange networking information with all other PEs and 
 * register an allocated buffer with the local NIC. Will create endpoints 
 * if they are not already created. 
 */
static inline int sharp_comms_register_buffer(void * buffer,
                                              size_t length,
                                              sharp_group_network_t *nw_group,
                                              unsigned int groupid)
{
    int i = 0;
    int error;
    void ** pack = NULL;
    ucs_status_t status;
    ucp_mem_map_params_t mem_map_params;

    /* resize our network data groups here */
    if (groupid >= sharp_nw_data_threshold) {
        sharp_nw_data_threshold *= 2;
        nw_data = realloc(nw_data, 
                          sizeof(network_data_t) * sharp_nw_data_threshold);
        memset(&nw_data[groupid], 
               0, 
               sizeof(network_data_t) * (sharp_nw_data_threshold - groupid));
    }

    nw_data[groupid].rkeys = 
        (ucp_rkey_h *) malloc(sizeof(ucp_rkey_h)*comms_size);
    if (NULL == nw_data[groupid].rkeys) {
        error = SHARP_ERR_NO_MEMORY;
        free(nw_data[groupid].rkeys);
        goto fail;
    }

    nw_data[groupid].remote_addresses = 
        (uint64_t *)malloc(sizeof(uint64_t)*comms_size);
    if (NULL == nw_data[groupid].remote_addresses) {
        error = SHARP_ERR_NO_MEMORY;
        free(nw_data[groupid].rkeys);
        free(sharp_endpoints);
        goto fail;
    }
    
    mem_map_params.address = buffer;
    mem_map_params.length = length;
    mem_map_params.field_mask = UCP_MEM_MAP_PARAM_FIELD_ADDRESS
                                | UCP_MEM_MAP_PARAM_FIELD_LENGTH;
    status = ucp_mem_map(sharp_ucp_context, 
                        &mem_map_params, 
                        &nw_data[groupid].register_buffer);
    if (status != UCS_OK) {
        error = SHARP_COMM_FAILURE;
        free(nw_data[groupid].remote_addresses);
        free(nw_data[groupid].rkeys);
        free(sharp_endpoints);
        goto fail;
    }

    error = -1;
    
    if (sharp_rte_interface_flag == RTE_INTERFACE_WITH_MPI) {
        error = comms_data_exchange(buffer,
                               &pack,
                               nw_data[groupid].remote_addresses,
                               &nw_data[groupid].register_buffer,
                               nw_group->group_info);
    } else {
         error = comms_data_exchange(buffer, 
                               &pack, 
                               nw_data[groupid].remote_addresses,
                               &nw_data[groupid].register_buffer,
                               NULL);
    }
       
    if (error != SHARP_OK) {
        goto fail_worker_addresses;
    }

    /* register the buffers */
    for (i=0;i<comms_size;i++) {
        int rkey_error;
        rkey_error = ucp_ep_rkey_unpack(sharp_endpoints[i], 
                                        pack[i], 
                                        &nw_data[groupid].rkeys[i]);
        if (UCS_OK != rkey_error) {
            error = SHARP_COMM_FAILURE;
            goto fail_purge_arrays;
        }
        ucp_rkey_buffer_release(pack[i]); 
        pack[i]=NULL;
    }

    free(pack);
    nw_data[groupid].inuse = 1;
    sharp_nr_groups++;

    return SHARP_OK;

fail_purge_arrays:
    free(pack);
fail_worker_addresses:
    free(nw_data[groupid].register_buffer);
    nw_data[groupid].register_buffer=NULL;
    
    free(nw_data[groupid].rkeys);
    nw_data[groupid].rkeys=NULL;

    free(nw_data[groupid].remote_addresses);
    nw_data[groupid].remote_addresses=NULL;
fail:
    return error;
}

#endif
