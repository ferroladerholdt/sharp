/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#include <config.h>
#include <comms/comms-include.h>


#if COMM_UCX==1

#include <ucp/api/ucp.h>

ucp_context_h sharp_ucp_context; /* the libraries UCP application context */
ucp_worker_h sharp_ucp_worker;  /* the worker for the above application context */
ucp_ep_h * sharp_endpoints = NULL; /* endpoints across the network group */

int (*comms_data_exchange)(void * buffer,
                           void *** pack_param,
                           uint64_t * raddr,
                           void * register_buffer,
                           void * nw_group_info);

#endif /* COMM_UCX == 1 */

size_t comms_my_pe;
size_t comms_size; 


char sharp_rte_interface_flag = 0;

void * internal_buffer;
int sharp_barrier_buffer; 

/* indexable by AG group id. Should be initialized in sharp_comms_init() */
network_data_t * nw_data = NULL;
int sharp_nr_groups=0; /* the number of used nw_data slots */
int sharp_nw_data_threshold=0; /* the maximum amount of slots available at the time */

/* FIXME: the above is just a first pass... place the real data structure 
 * below and remove the above */

/* real data structure: array of pointers (ucs_array) */

