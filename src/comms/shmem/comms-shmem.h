/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#ifndef SHARP_COMMS_SHMEM_H
#define SHARP_COMMS_SHMEM_H

#include <shmem.h>
#include <ucs/debug/memtrack.h>
#include <sharp_groups/api/sharp_groups.h>
#include <sharp/sharp_errors.h>

/*
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief The network data associated with the MPI communication layer. This 
 * is used for communication within an allocation group.
 */
typedef struct network_data {
    void * buffer;
} network_data_t;

extern network_data_t * nw_data; 
extern int sharp_nr_groups;   
extern int sharp_nw_data_threshold;
extern size_t comms_my_pe;
extern size_t comms_size;

static inline int sharp_comms_get_pe(void *nw_info, int *pe) 
{
    *pe = (int) comms_my_pe;
    return SHARP_OK;
}

static inline int sharp_comms_get_size(void *nw_info, int *size)
{
    *size = (int) comms_size;
    return SHARP_OK;
}

static inline int sharp_comms_init(int argc, char ** argv) 
{
    shmem_init();

    comms_my_pe = (size_t)shmem_my_pe();
    comms_size = (size_t)shmem_n_pes();

    sharp_nw_data_threshold = 32;
    sharp_nr_groups = 0;
    nw_data = (network_data_t *)ucs_malloc(sizeof(network_data_t)*sharp_nw_data_threshold);
    memset(nw_data, 0, sizeof(network_data_t) * sharp_nw_data_threshold);
    
    return SHARP_OK;
}

static inline int sharp_comms_finalize(void) 
{
    shmem_finalize();
    return SHARP_OK;
}


static inline int sharp_comms_flush(unsigned int group_id)
{
    shmem_quiet();
    return SHARP_OK;
}

static inline int sharp_comms_flush_pe(int pe, unsigned int group_id)
{
    return sharp_comms_flush(group_id);
}

static inline int sharp_comms_barrier(sharp_group_allocated_t * ag)
{
    shmem_barrier_all();
    return SHARP_OK;
}

static inline int sharp_comms_register_buffer(void * buffer, 
                                              size_t length,
                                              sharp_group_network_t *nw_group,
                                              unsigned int group_id) 
{
    if(group_id >= sharp_nw_data_threshold) {
        sharp_nw_data_threshold *= 2;
        nw_data = realloc(nw_data, 
                          sizeof(network_data_t) * sharp_nw_data_threshold);
        memset(&nw_data[group_id], 0, sizeof(network_data_t) * (sharp_nw_data_threshold - group_id));
    }

    nw_data[group_id].buffer = buffer;
    return SHARP_OK;
}

static inline int sharp_comms_lock_acquire(unsigned int pe, 
                                           unsigned int group_id)
{
    /* FIXME: this should maybe call shmem_set_lock? */
    return SHARP_OK;
}

static inline int sharp_comms_lock_release(unsigned int pe, 
                                           unsigned int group_id)
{
    /* FIXME: this should maybe call shmem_clear_lock? */
    return SHARP_OK;
}

static inline int sharp_comms_put(void * inbuf,
                                  size_t size,
                                  size_t remote_offset,
                                  unsigned int pe,
                                  unsigned int group_id)
{
    shmem_putmem(nw_data[group_id].buffer+remote_offset, inbuf, size, pe);
    shmem_quiet();
    return SHARP_OK;
}

static inline int sharp_comms_put_nbi(void * inbuf,
                                      size_t size,
                                      size_t remote_offset,
                                      unsigned int pe,
                                      unsigned int group_id)
{
    shmem_putmem_nbi(nw_data[group_id].buffer+remote_offset, inbuf, size, pe);
    return SHARP_OK;
}

static inline int sharp_comms_get(void * outbuf,
                                  size_t size,
                                  size_t remote_offset,
                                  unsigned int pe,
                                  unsigned int group_id)
{
    shmem_getmem(outbuf, nw_data[group_id].buffer+remote_offset, size, pe);
    return SHARP_OK;
}

static inline int sharp_comms_get_nbi(void * outbuf,
                                      size_t size,
                                      size_t remote_offset,
                                      unsigned int pe,
                                      unsigned int group_id)
{
    shmem_getmem_nbi(outbuf, nw_data[group_id].buffer+remote_offset, size, pe);
    return SHARP_OK;   
}

static inline int sharp_comms_atomic_swap64(uint64_t val,
                                            uint64_t * buf,
                                            size_t remote_offset,
                                            unsigned int pe,
                                            unsigned int group_id)
{
    *buf = shmem_long_swap(nw_data[group_id].buffer+remote_offset, val, pe);
    return SHARP_OK; 
}


static inline int sharp_comms_atomic_cswap64(uint64_t * buf,
                                             uint64_t expected,
                                             size_t remote_offset,
                                             unsigned int pe,
                                             unsigned int group_id)
{
    uint64_t out;
    out = shmem_long_cswap(nw_data[group_id].buffer+remote_offset, expected, *buf, pe);
    if(out != expected) {
        return SHARP_ERR_BAD_EXPECTED_VALUE;
    } 

    return SHARP_OK;
}

static inline int sharp_comms_atomic_add64(uint64_t local,
                                           size_t offset,
                                           uint64_t pe,
                                           unsigned int group_id)
{
    shmem_long_add(nw_data[group_id].buffer+offset, local, pe);
    return SHARP_OK;
}


static inline int sharp_comms_atomic_fadd64(uint64_t local,
                                            uint64_t offset,
                                            uint64_t pe,
                                            uint64_t *out,
                                            unsigned int group_id)
{
    *out = shmem_long_fadd(nw_data[group_id].buffer+offset, local, pe);
    return SHARP_OK;

}


#endif
