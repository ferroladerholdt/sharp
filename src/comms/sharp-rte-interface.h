/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#ifndef _SHARP_RTE_INTERFACE_H
#define _SHARP_RTE_INTERFACE_H

/**
 * @ingroup SHARP_INTERNAL
 * @brief Exchange UCP addresses among all the PEs within a network group 
 * 
 * @param [in] *buffer              Local memory buffer registered with 
 *                                  the network
 * @param [out] ***worker_addresses List of UCP worker addresses for all PEs 
 *                                  within a network group. This is 
 *                                  allocated and filled for the caller.  
 * @param [out] ***pack             List of packed UCP Remote keys for all PEs
 *                                  within a network group. This is 
 *                                  allocated and filled for the caller.
 * @param [in,out] *raddr           List of remote addresses for all PEs within
 *                                  a network group. This is allocated by
 *                                  the caller.
 * @param [in] *register_buffer     The memory mapped handle to registered 
 *                                  memory in UCP
 * @returns an error code 
 */
int comms_rte_exchange_address(void * buffer,
                               void *** worker_addresses,
                               void *** pack_param,
                               uint64_t * raddr,
                               void * register_buffer,
                               void * nw_group_info);

/**
 * @ingroup SHARP_INTERNAL
 * @brief Initialize the RTE data structures
 *
 * @param [in] argc                 The number of command line arguments
 * @param [in] **argv               The command line arguments
 * @returns an error code
 */
int comms_rte_init(int argc, char ** argv);

/**
 * @ingroup SHARP_INTERNAL
 * @brief A barrier provided by RTE for initialization and finalization. 
 * Essentially, this should only be used for synchronization in those 
 * situations. 
 *
 * @returns an error code
 */
int comms_rte_barrier(void);

#endif /* _SHARP_RTE_INTERFACE_H */
