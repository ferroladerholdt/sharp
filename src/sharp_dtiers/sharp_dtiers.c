/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
*
*/

#include <hwloc.h>
#include <ucs/datastruct/list.h> /* for list related functions */
#include <khash.h> /* for the hash related functions */
#include <ucs/debug/memtrack.h> /* for ucs_malloc */

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <sharp/sharp_errors.h>

#include <stdio.h>
#include <limits.h>
#include <stdarg.h>



/*
    from hwloc/src/bitmap.c 
*/
struct hwloc_bitmap_s {
    unsigned ulongs_count;
    unsigned ulongs_allocated;
    unsigned long * ulongs;
    int infinite;
#ifdef HWLOC_DEBUG
    int magic;
#endif
};

/* from sharp_mdomains.c */
extern sharp_node_info_t sharp_node;
extern float * sharp_distance_matrix;
extern hwloc_topology_t sharp_topology;

/* initialize the hash functions/structures */
KHASH_INIT(sharp_mdomain_hash, 
           uint32_t, 
           __uint128_t, 
           1, 
           __ac_Wang_hash, 
           kh_int_hash_equal);

khash_t(sharp_mdomain_hash) * sharp_mdomain_hash = NULL;


/* need to convert the list to a hash */
static inline void convert_md_list_to_hash(void)
{
    sharp_md_list_item_t * md;
    int i = 0;

    ucs_list_for_each(md, sharp_node.md_l, next_p) {
        int md_key = 0;
        khiter_t iter;
        int put_ret = 0;
        
        md_key = sharp_md_get_device_id(md->mem_attrib);
        md_key = md_key << 8;
        md_key |= (md->mem_attrib & 0xff);

        iter = kh_put(sharp_mdomain_hash, sharp_mdomain_hash, md_key, &put_ret);
        if(put_ret != -1) {
            kh_value(sharp_mdomain_hash, iter) = md->mem_attrib;
        }
        i++;
    }
}

static inline void dtier_set_query_access(sharp_dtier_access_t access, 
                                          sharp_query_t * query) 
{
    unsigned char corrected_access = access << SHARP_CONSTRAINT_ACCESS_SHIFT;
    if(corrected_access == SHARP_ACCESS_INTRAP) {
        sharp_query_set_access(*query, NODE_RW);
    } else if(corrected_access == SHARP_ACCESS_INTERP 
           || corrected_access == SHARP_ACCESS_INTERJ) {
        sharp_query_set_access(*query, REMOTE_RW);
    } else {
        sharp_query_set_access(*query, LOCAL);
    }
}

static inline void dtier_set_query_type(sharp_hint_t hint, 
                                        sharp_constraint_t constraints,
                                        sharp_query_t * query) 
{
    /* by default the query is set to DDR */

    /* Begin CoProc */
    if(hint == SHARP_HINT_GPU) {
        sharp_query_set_type(*query, SHARP_MD_HBM);
    }
    /* End CoProc */

    /* Begin NVRAM */
    if(sharp_constraint_get_persistent(constraints)) {
        sharp_query_set_persistent(*query);
        sharp_query_set_type(*query, SHARP_MD_NVRAM);
    }
    /* End  NVRAM */
}

static inline sharp_query_t create_query(sharp_hint_t hint, 
                                         sharp_constraint_t constraints) 
{
    sharp_query_t query = 0;

    /* this also sets the persistence */
    dtier_set_query_type(hint, constraints, &query);

    if (sharp_query_get_type(query) == SHARP_MD_DDR) {
        /* FIXME: Making the assumption that DDR memory can always be 
           written to remotely unless it is tied to a device in some 
           manner, which is not provided to me by hwloc */
        sharp_query_set_access(query, REMOTE_RW); 
    } else {
        /* set access constraints for all non DDR memory */
        dtier_set_query_access(sharp_constraint_get_access(constraints), 
                               &query);
    }

    /* Set near nic based on usage hint */
    if (sharp_is_hint_latency(hint) 
        || sharp_is_hint_bandwidth(hint)) {
        sharp_query_set_nn(query);
    }

    return query;
}

/* This should be internal only. Kind of a mess if the user calls this */
int sharp_find_local_numa_device_id(void) {
    ucs_list_link_t * device_list = sharp_node.dev_l;
    sharp_dev_item_t * device_list_item;
    hwloc_bitmap_t cpuset;
    int device_id = 0;

    cpuset = hwloc_bitmap_alloc();
    hwloc_get_cpubind(sharp_topology, cpuset, HWLOC_CPUBIND_PROCESS);

    ucs_list_for_each(device_list_item, device_list, next_p) {
        unsigned int device_count_index;
        unsigned long test_cpuset;
        
        device_count_index = 
            device_list_item->device.numa_locality.locality.count_index;
        test_cpuset = 
            device_list_item->device.numa_locality.locality.cpuset;
    
        if(device_count_index > cpuset->ulongs_count) {
            continue;
        }

        if(test_cpuset & cpuset->ulongs[device_count_index]) {
            device_id = device_list_item->device.device_id;
            break;
        }
    }

    hwloc_bitmap_free(cpuset);
    return device_id;
}

/* 
    determine if we should wildcard near nic 
    i.e., is any nic near a numa?
    0 = no, 1 = yes
*/
static inline int wildcard_near_nic(void)
{
    int i = 0;
    int ddr_count = 0;
    ucs_list_link_t * md_l = sharp_node.md_l;
    sharp_md_list_item_t * md;

    ucs_list_for_each(md, md_l, next_p) {
        if(sharp_md_get_type(md->mem_attrib) != SHARP_MD_DDR) {
            continue;
        }

        ddr_count++;
        if(sharp_md_get_near_nic(md->mem_attrib) > 0) {
            i++;
        }
    }

    /* either all the nics are near some DDR, or none are */
    if((ddr_count == i) || (i==0)) {
        return 1;
    } else {
        return 0;
    }
}

static inline int query_memory_domain_no_locality(sharp_query_t query,
                                              unsigned int nr_devices,
                                          sharp_data_tier * data_tier)
{
    int nr_found_mds = 0;
    int i = 0;
    int wildcard_nic = wildcard_near_nic();

    /* we don't care about locality to the calling PE */
    for(i=0;i<nr_devices;i++) {
        khiter_t iter;
        sharp_md_t mem_attrib;
        unsigned int key = query | (i << 8);
        iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
        
        /* a mdomain was found */
        if(iter != kh_end(sharp_mdomain_hash)) {
            mem_attrib = kh_value(sharp_mdomain_hash, iter);
            data_tier->md_ids[nr_found_mds] = 
                sharp_md_get_md_id(mem_attrib);
            nr_found_mds++;
        } else {
            /* we failed to find a memory. are there 
               areas of the query that we don't care about? */
            if(wildcard_nic) {
                sharp_query_t new_query = query ^ 0x80;
                key = new_query | (i << 8);
                iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
                if(iter != kh_end(sharp_mdomain_hash)) {
                    mem_attrib = kh_value(sharp_mdomain_hash, iter);
                    data_tier->md_ids[nr_found_mds] = 
                        sharp_md_get_md_id(mem_attrib);
                    nr_found_mds++;
                }
            }
        }

        /* too many memory domains */
        if(nr_found_mds > SHARP_MDS_MAX) {
            break;
        }
    }

    return nr_found_mds;
}

static inline int query_memory_domain_explicit(sharp_query_t query,
                                           unsigned int device_id,
                                        sharp_data_tier * data_tier)
{
    int nr_found_mds = 0;
    khiter_t iter;
    sharp_md_t mem_attrib;
    unsigned int key;

    sharp_query_set_access(query, REMOTE_RW); 
    key = query | (device_id << 8);
    iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
    if (iter != kh_end(sharp_mdomain_hash)) {
        mem_attrib = kh_value(sharp_mdomain_hash, iter);
        data_tier->md_ids[0] = sharp_md_get_md_id(mem_attrib);
        nr_found_mds = 1;
    } else {
        query = query ^ 0x80;
        key = query;
        iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
        if (iter != kh_end(sharp_mdomain_hash)) {
            mem_attrib = kh_value(sharp_mdomain_hash, iter);
            data_tier->md_ids[0] = sharp_md_get_md_id(mem_attrib);
            nr_found_mds = 1;
        }
    }

    return nr_found_mds;
}

/* FIXME: is this used? */
static inline int query_memory_domain_locality(sharp_query_t query,
                                               int nr_devices,
                                               sharp_data_tier * data_tier)
{
    int nr_found_mds = 0;
    unsigned int key = 0;
    khiter_t iter;
    sharp_md_t mem_attrib;  
    /* we do care about locality */
    int my_device_id = sharp_find_local_numa_device_id();


    key = query | (my_device_id << 8);
    
    iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
    if(iter != kh_end(sharp_mdomain_hash)) {
        mem_attrib = kh_value(sharp_mdomain_hash, iter);
        data_tier->md_ids[nr_found_mds] = 
            sharp_md_get_md_id(mem_attrib);
        nr_found_mds++;
    } else {
        /* we failed to find a memory. are there 
           areas of the query that we don't care about? */
        sharp_query_t new_query = query ^ 0x80;
        key = new_query | (my_device_id << 8);
        iter = kh_get(sharp_mdomain_hash, sharp_mdomain_hash, key);
        if(iter != kh_end(sharp_mdomain_hash)) {
            mem_attrib = kh_value(sharp_mdomain_hash, iter);
            data_tier->md_ids[nr_found_mds] = 
                sharp_md_get_md_id(mem_attrib);
            nr_found_mds++;
        }
    }


    return nr_found_mds;
}

static inline int create_data_tier(sharp_hint_t * hints,
                           sharp_constraint_t * constraints,
                           sharp_data_tier * data_tier)
{
    int ret = 0;
    sharp_query_t query = 0;
    int nr_devices = 0;
    int nr_found_mds = 0;

    /* need to create a query (no locality) */
    if (sharp_constraint_get_explicit(*constraints)) {
            int device_id = sharp_constraint_get_explicit(*constraints) - 1;
            nr_found_mds = query_memory_domain_explicit(query, device_id, data_tier);
    } else {
        query = create_query(*hints, *constraints);

        /* i need the number of devices */
        nr_devices = ucs_list_length(sharp_node.dev_l);

        if (sharp_constraint_get_local(*constraints) == 0) {
            nr_found_mds = 
                query_memory_domain_no_locality(query, nr_devices, data_tier);
        } else { 
        nr_found_mds = 
            query_memory_domain_locality(query, nr_devices, data_tier);
        }
    }	

    data_tier->nr_mds = nr_found_mds;
	data_tier->constraints = *constraints;
    data_tier->hints = *hints;
    
    if(nr_found_mds == 0) {
		ret = SHARP_ERR_NO_MDS; /* FIXME: change to sharp error code */
    } else {
	    ret = nr_found_mds;
    }

	return ret;
}

/* for constraints, just OR the values together:
 *  (SHARP_ACCESS_LOCAL | SHARP_CONSTRAINT_PERSISTENT) 
 */
int sharp_create_data_tier(sharp_data_tier * dtier_list, int nr_hints, ...)
{
    va_list arg_list;
    int i = 0;
    sharp_constraint_t * constraints;
    sharp_hint_t * hints;
    int ret = 0;

    /* this should be extensible later so that we can rehash the 
       list if we change how discover_topology() works */
    if(sharp_mdomain_hash == NULL) {
        sharp_mdomain_hash = kh_init(sharp_mdomain_hash);
        kh_resize_sharp_mdomain_hash(sharp_mdomain_hash, 80); 
        convert_md_list_to_hash();
    }

    constraints = 
        (sharp_constraint_t *)ucs_malloc(sizeof(sharp_constraint_t)*nr_hints);
    hints = 
        (sharp_hint_t *)ucs_malloc(sizeof(sharp_hint_t)*nr_hints);

    va_start(arg_list, nr_hints);
    for(i=0;i<nr_hints;i++) {
        size_t size = 0;
        unsigned long lower_constraints;
        __uint128_t tmp = 0;

        hints[i] = va_arg(arg_list, int);

        lower_constraints = va_arg(arg_list, long);
        
        size = va_arg(arg_list, size_t);
        tmp = size;
        constraints[i] = ((tmp<<64) | lower_constraints);

        /*
         * the logic here:
         *
         * ret will be clobbered on each iteration and because ret can 
         * give the user an understanding of success/failure without 
         * a need to check each data tier. so, we can or. any negative 
         * value will always be present.
         */
        ret |= create_data_tier(&hints[i], 
                                &constraints[i], 
                                &dtier_list[i]);
    }
    va_end(arg_list);

    ucs_free(constraints);
    ucs_free(hints);
    return ret;
}

/* FIXME: there is no value in this function. remove me? */
int sharp_data_tier_finalize(sharp_data_tier *dtier){
    if(dtier == NULL){
        return SHARP_OK;
    }

    /* there is nothing to clean up */
    return SHARP_OK;
}
