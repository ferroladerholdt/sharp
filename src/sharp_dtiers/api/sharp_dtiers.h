/*
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#ifndef __SHARP_DTIERS_H
#define __SHARP_DTIERS_H

#include <sharp_mdomains/api/sharp_mdomains_md.h>

#include <ucs/sys/compiler_def.h>

/* this is about the size of a cache line */
#define SHARP_MDS_MAX       32

/** 
 * @ingroup SHARP_DATATYPE
 * \brief User defined constraints on the required memory attributes  
 */
typedef __uint128_t         sharp_constraint_t;

/** 
 * @ingroup SHARP_DATATYPE
 * \brief User defined hint of how the data will be used  
 */
typedef unsigned int       sharp_hint_t;

/** 
 * @ingroup SHARP_DATATYPE
 * \brief Generated query based on hints and constraints  
 */
typedef unsigned int        sharp_query_t;

/* bit layout for sharp_constraint_t 

127      63     7       6        3   2     1     0
+--------+------+-------+--------+---+-----+-----+
|  Size  | Resv | local | access | P | IPD | IJD |
+--------+------+-------+--------+---+-----+-----+

(1 bit)         IJD = Interjob decomposition
(1 bit)         IPD = Interprocess decomposition
(1 bit)         P   = Persistent memory
(3 bits)        Access constraint
(1 bit)         local to the PE
(56 bits)       Reserved for future use
(64 bits)       Size in bytes needed

*/

/* bit layout for sharp_query_t **

32            7    6   4   1   0
+-------------+----+---+---+---+
|  Device ID  | NN | A | T | P |
+-------------+----+---+---+---+

(1 bit)         P = Persistent
(3 bits)        T = Type
(3 bits)        A = Access
(1 bit)	        NN= near nic
(24 bits)       Device ID

*/

/** 
 * @ingroup SHARP_DATATYPE
 * \brief Data tier accessibility constraints. The value of these elements have
 * been pre-computed to a left bit shift of 3.
 */
typedef enum {
        SHARP_ACCESS_INTRAP = 0, /**< \brief Memory accessible between processes 
                                   located locally */
        SHARP_ACCESS_INTERP = 8, /**< \brief Memory accessible between processes 
                                   located remotely */
        SHARP_ACCESS_INTERJ = 16, /**< \brief Memory accessible between processes 
                                   on separate jobs */
        SHARP_ACCESS_LOCAL = 24,  /**< \brief Locally accessible only */
        SHARP_ACCESS_CPU = 32,    /**< \brief Accessible to the CPU only */
        SHARP_ACCESS_GPU = 40     /**< \brief Accessible to the GPU only */
} sharp_dtier_access_t;

/** 
 * @ingroup SHARP_DATATYPE
 * \brief The Data Tier. Data tiers are used to define the user's 
 * usage hints and constraints on the memory domains of the system. It will 
 * be used by grouping references to the memory domains matching those 
 * hints and constraints.
 */
typedef struct data_tier {
	int md_ids[SHARP_MDS_MAX];      /**< \brief Memory Domain List */
	int nr_mds;                     /**< \brief The number of memory domains */
	sharp_constraint_t constraints; /**< \brief Constraints */
	sharp_hint_t hints;             /**< \brief Hints */
} sharp_data_tier;

/**
 * @ingroup SHARP_DATATYPE
 * @brief Hints for data tier creation. These hints allow SharP to take 
 * advantage of how the user plans to make use of the memories available to 
 * it. Thus, memory is allocated closer to the computation.
 */

typedef enum {
    SHARP_HINT_CPU, /**< CPU Usage */ 
    SHARP_HINT_GPU, /**< GPU Usage */
    SHARP_HINT_LATENCY_OPT, /**< Latency optimal usage */
    SHARP_HINT_BANDWIDTH_OPT /**< Bandwidth optimal usage */
} sharp_dtier_hint_t;

typedef enum {
    SHARP_CONSTRAINT_NUMA_0 = (1<<8),
    SHARP_CONSTRAINT_NUMA_1 = (1<<9),
} sharp_dtier_explicit_t;
    

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Test if usage hint defines CPU usage 
 * 
 * @param [in] x                The usage hint 
 * @returns 1 if true, 0 if false
 */ 
#define sharp_is_hint_cpu(x)                    ((x) == 0x0)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Test if usage hint defines GPU usage 
 * 
 * @param [in] x                The usage hint 
 * @returns 1 if true, 0 if false
 */
#define sharp_is_hint_gpu(x)                    ((x) == 0x1)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Test if usage hint defines latency optimal usage
 * 
 * @param [in] x                The usage hint 
 * @returns 1 if true, 0 if false
 */
#define sharp_is_hint_latency(x)                ((x) == 0x2)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Test if usage hint defines bandwidth optimal usage
 * 
 * @param [in] x                The usage hint 
 * @returns 1 if true, 0 if false
 */
#define sharp_is_hint_bandwidth(x)              ((x) == 0x3)


/* Constraints */
#define SHARP_CONSTRAINT_SIZE_SHIFT         64
#define SHARP_CONSTRAINT_EXPLICIT_SHIFT     8
#define SHARP_CONSTRAINT_LOCALITY_SHIFT     7
#define SHARP_CONSTRAINT_ACCESS_SHIFT       3
#define SHARP_CONSTRAINT_PERSISTENT_SHIFT   2
#define SHARP_CONSTRAINT_INTERP_SHIFT       1
#define SHARP_CONSTRAINT_INTERJ_SHIFT       0

#define SHARP_CONSTRAINT_ACCESS_MASK        0x7
#define SHARP_CONSTRAINT_PERSISTENT_MASK    0x1
#define SHARP_CONSTRAINT_INTERP_MASK        SHARP_CONSTRAINT_PERSISTENT_MASK
#define SHARP_CONSTRAINT_INTERJ_MASK        SHARP_CONSTRAINT_INTERP_MASK
#define SHARP_CONSTRAINT_LOCALITY_MASK      SHARP_CONSTRAINT_INTERJ_MASK

/* simple macros to obtain pieces of the constraint data structure */
/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the size attribute for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the size for the constraint
 */
#define sharp_constraint_get_size(x)            ((size_t) ((x)>>SHARP_CONSTRAINT_SIZE_SHIFT))

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the access attribute for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the access attribute for the constraint
 */
#define sharp_constraint_get_access(x)          ((uint8_t)((x)>>SHARP_CONSTRAINT_ACCESS_SHIFT) & SHARP_CONSTRAINT_ACCESS_MASK)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the persistent attribute for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the persistent attribute for the constraint
 */
#define sharp_constraint_get_persistent(x)      ((uint8_t)((x)>>SHARP_CONSTRAINT_PERSISTENT_SHIFT) & SHARP_CONSTRAINT_PERSISTENT_MASK)

#define sharp_constraint_get_explicit(x) ((uint8_t)((x) >> SHARP_CONSTRAINT_EXPLICIT_SHIFT) & 3)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the interprocess decomposition attribute for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the interprocess decomposition attribute for the constraint
 */
#define sharp_constraint_get_interp_decomp(x)   ((uint8_t)((x)>>SHARP_CONSTRAINT_INTERP_SHIFT) & SHARP_CONSTRAINT_INTERP_MASK)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the interjob decomposition for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the interjob decomposition for the constraint
 */
#define sharp_constraint_get_interj_decomp(x)   ((uint8_t)((x) & SHARP_CONSTRAINT_INTERJ_MASK))

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the locality attribute for the constraint
 * 
 * @param [in] x                The constraint 
 * @returns the locality attribute for the constraint
 */
#define sharp_constraint_get_local(x)           ((uint8_t)((x)>>SHARP_CONSTRAINT_LOCALITY_SHIFT) & SHARP_CONSTRAINT_LOCALITY_MASK)

/* query related functions */

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the "near nic" bit of the data tier query
 *
 * @param [in] x                The data tier query
 * @returns the value of the "near nic" bit
 */
#define sharp_query_get_nn(x)                   sharp_md_get_nn(x)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the access attribute of the data tier query
 *
 * @param [in] x                The data tier query
 * @returns the value of the access attribute
 */
#define sharp_query_get_access(x)               sharp_md_get_access(x)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the type of memory for the data tier query
 *
 * @param [in] x                The data tier query
 * @returns the type of memory for the data tier query
 */
#define sharp_query_get_type(x)                 sharp_md_get_type(x)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Obtain the persistent bit of the data tier query
 *
 * @param [in] x                The data tier query
 * @returns the value of the persistent bit
 */
#define sharp_query_get_persistent(x)           sharp_md_get_persistent(x)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Set the "near nic" bit of the data tier query
 *
 * @param [inout] x                The "near nic" value (i.e., 0 or 1)
 * @returns nothing
 */
#define sharp_query_set_nn(x)                   sharp_md_set_near_nic(x)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Set the access attribute of the data tier query
 *
 * @param [inout] x                The query being modified
 * @param [in]    y                The access attribute 
 * @returns nothing
 */
#define sharp_query_set_access(x,y)             sharp_md_set_access(x,y)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Set the type of memory for the data tier query
 *
 * @param [inout] x                 The query being modified
 * @param [in]    y                 The type attribute 
 * @returns nothing 
 */
#define sharp_query_set_type(x,y)               sharp_md_set_type(x,y)

/**
 * @internal
 * @ingroup SHARP_INTERNAL
 * @brief Set the persistent attribute for the data tier query
 *
 * @param [inout] x                 The query being modified
 * @returns nothing 
 */
#define sharp_query_set_persistent(x)           sharp_md_set_persistent(x)

/**
 * @ingroup SHARP_DATATYPE
 * @brief Constraint defining a data structure's need to be capable of 
 * decomposing between jobs
 */
#define SHARP_CONSTRAINT_DECOMP_IJ      UCS_BIT(0)

/**
 * @ingroup SHARP_DATATYPE
 * @brief Constraint defining a data structure's need to be capable of 
 * decomposing between processes
 */
#define SHARP_CONSTRAINT_DECOMP_IP      UCS_BIT(1)

/**
 * @ingroup SHARP_DATATYPE
 * @brief Constraint defining a data structure's need to use persistent 
 * memory
 */
#define SHARP_CONSTRAINT_PERSISTENT     UCS_BIT(2)

/**
 * @ingroup SHARP_API
 * @brief A local operation to build the constraints that may be used to 
 * specify which memory domains to use in later memory allocations
 
 * @param [in]  size        The size in bytes required for this data tier
 * @param [in]  access      The accessibility constraints for the specific 
                            data structure that will be made on this data tier
 * @param [in]  persistent  Whether or not the memory needs to be persistent
 * @param [in]  interp_decomp   Whether or not the memories should allow 
                                for data structure decomposition between 
                                processes
 * @param [in]  interj_decomp   Whether or not the memories should allow 
                                for data structure decomposition between jobs
 * @param [in]  locality    Whether or not the memory needs to be local to 
                            the calling PE
 * @param [out] constraint  The created constraint 
 *
 * FIXME: possibly remove this?
 */
static inline void sharp_create_constraint(size_t size,
                                           sharp_dtier_access_t access,
                                           unsigned char persistent,
                                           unsigned char interp_decomp,
                                           unsigned char interj_decomp,
                                           unsigned char locality,
                                           sharp_constraint_t * constraint) 
{
    __uint128_t l_size = size;

    *constraint = 0;
    *constraint |= (l_size << 64);
    *constraint |= (locality << 7);
    /* the values of the enum place the values correctly for access */
    *constraint |= access; 
    *constraint |= (persistent << 2);
    *constraint |= (interp_decomp << 1);
    *constraint |= (interj_decomp);
}


/**
 * @ingroup SHARP_API
 * @brief Local operation used to create a data tier. Combines hint
 * declaration, constraint creation, and data tier creation in 
 * one call.
 *
 * @param [out] dtier_list   The data tier encapsulating the memory domain
 *                           information for later memory allocations
 * @param [in] nr_data_tiers The number of data tiers to create followed by a
 *                           hints, constraints, size tuple 
 *                           (e.g., SHARP_HINT_CPU, SHARP_ACCESS_LOCAL, 1024).
 *                           OR hints together to pass in multiple hints for a 
                             single data tier. OR constraints together to pass
                             in multiple constraints for a single data tier. 
                             The size is the constraint on the memory domain's
                             available memory in bytes.
 * @param [in] args          The hints and constraints in the format:
 *                           hint, constraints, size
 * @return Success or Failure. Positive number is success and negative is failure
 */
int sharp_create_data_tier(sharp_data_tier * dtier_list, int nr_hints, ...);



/**
 * @ingroup SHARP_API
 *
 * @brief Release any resources held by the data tier.
 *
 * This should only be called with a null pointer argument, or a dataier that was created sucesfully.
 *
 * @param [in] dtier The data tier to have it's resources released
 * @returns A sharp error code.
 */
int sharp_data_tier_finalize(sharp_data_tier *dtier);

#endif
