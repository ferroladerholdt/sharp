#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <sharp_dtiers/api/sharp_dtiers.h>
#include <allocator/sharp_allocator.h>
#include <allocator/api/sharp_allocator_api.h>
#include <sharp/sharp_errors.h>
#include <helper.h>

int sharp_init_allocator(void) 
{
    int error;
    error = sharp_create_node_info();
    return error;
}

sharp_allocator_obj_t * sharp_init_allocator_obj(
                                        sharp_allocator_info_params_t * params)
{
    sharp_data_tier dtier;
    sharp_allocator_obj_t * obj = NULL;
    int nr_found = 0;
    int i = 0;    

    /* should really only have one data tier created at a time */
    nr_found = sharp_create_data_tier(&dtier, 1, 
                                      params->allocator_hints, 
                                      params->allocator_constraints,
                                      1); /* size does not matter */
    if (nr_found <= 0) {
        /* we found 0 data tiers matching the request */
        return NULL;
    } 

    obj = (sharp_allocator_obj_t *) malloc(sizeof(sharp_allocator_obj_t));
    if (obj == NULL) {
        return NULL;
    }
    
    obj->md = (sharp_md_list_item_t **) malloc(sizeof(sharp_md_list_item_t *) * nr_found);
    if (obj->md == NULL) {
        return NULL;
    }

    for (i = 0; i < nr_found; i++) {
        obj->md[i] = get_mdomain_from_list_by_id(dtier.md_ids[i]);
    }
    obj->nr_mds = nr_found;

    return obj;
}

/* call with alignment = 0 for normal allocations */
static inline void * alloc_buffer(sharp_allocator_obj_t * a_obj, 
                                  size_t size,
                                  int alignment) 
{
    void * buffer = NULL;
    int i = 0;
    
    for (i = 0; i < a_obj->nr_mds; i++) {
        sharp_allocator_t * allocator = a_obj->md[i]->allocator;
        int error;
        if (alignment == 0) {
            error = allocator->ops->chunk_alloc(allocator,
                                                size,
                                                &buffer);
        } else {
            error = allocator->ops->chunk_memalign(allocator,
                                                   size,
                                                   &buffer,
                                                   alignment);
        }
        if (error == 0 && buffer > 0) {
            return buffer;
        }
    }
    return NULL;
}

void * sharp_allocator_alloc(sharp_allocator_obj_t * a_obj, size_t size) 
{
    return alloc_buffer(a_obj, size, 0);
}

void * sharp_allocator_alloc_memalign(sharp_allocator_obj_t * a_obj,
                                      size_t size,
                                      int alignment)
{
    return alloc_buffer(a_obj, size, alignment);
}

int sharp_allocator_free(sharp_allocator_obj_t * a_obj, void * buffer)
{
    int i = 0;
    void * addr = buffer;
    
    for (i = 0; i < a_obj->nr_mds; i++) {
        int error = 0;
        sharp_allocator_t * allocator = a_obj->md[i]->allocator;
       
        error = allocator->ops->chunk_release(allocator, &addr);
        if (error == SHARP_OK) {
            return error;
        }
    }
    return SHARP_ERR_INVALID_FREE_ATTEMPT;
}
