/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/


#include "sharp_allocator_pmem.h"
#include "sharp_allocator_utilities.h"
#include "sharp_allocator.h"
#include <ucs/datastruct/list.h>
#include <ucs/debug/memtrack.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <utils/sharp/sharp_errors.h>
#include <libpmem.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

/***********************************************************/
/*  initialize a sharp_allocator_pmem_data_t structure */
static inline void pmem_data_init(sharp_allocator_pmem_data_t *d)
{
    d->first_allocation = 1;
    d->pool = NULL;
    d->base = NULL;
    d->pool_base = NULL;
    d->size = 0;
    d->fd = -1;
    d->is_pmem = -1;
}
/***********************************************************/
/*  destroy a sharp_allocator_pmem_data_t and free it's resources */
static inline void pmem_data_finalize(sharp_allocator_pmem_data_t *d)
{
    if (d->pool) {
        vmem_delete(d->pool);
    }

    if (d->base) {
        if (munmap(d->base, d->size)) {
            perror("");
        }
    }

    if (d->fd != -1) {
        /*  ignore the return value since there is nothing we can do about the
         *  error conditions */
        close(d->fd);
    }

    /* make sure we don't have any old data that can be used */
    pmem_data_init(d);
}

/***********************************************************/
/* c != NULL*/
static inline sharp_errors_t free_pmem_chunk(sharp_allocator_pmem_data_t *d, 
                                             sharp_chunk_t *c)
{
    vmem_free(d->pool, c->buffer);
    vmem_free(d->pool, c);
    return SHARP_OK;
}

/***********************************************************/
/* this function assumes that c already has the required alignment set to 
 * a power of 2 (or 0 if no alignment is required)
 */
static inline int pmem_chunk_malloc(sharp_allocator_t * a,
                                    size_t size,
                                    sharp_chunk_t *c)
{
    sharp_allocator_pmem_data_t *d = a->data;

    if (c->alignment) {
      c->buffer = vmem_aligned_alloc(d->pool, c->alignment, size);
    } else {
      c->buffer = vmem_malloc(d->pool, size);
    }

    if (c->buffer == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    c->size = size;
    c->alignment = sharp_allocator_find_pointer_alignment(c->buffer);

    return SHARP_OK;
}



/***********************************************************/
int sharp_allocator_pmem_finalize(sharp_allocator_t *allocator)
{
    sharp_allocator_pmem_data_t *d = allocator->data;
    pmem_data_finalize(d);
    ucs_free(d);

    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_pmem_init(sharp_allocator_t *allocator,
                              sharp_dev_type_t dev_type,
                              int device_id,
                              sharp_md_type_t md_type,
                              int md_id)
{
    return SHARP_ERR_NOT_IMPLEMENTED;
}

int sharp_allocator_pmem_init2(sharp_allocator_t *allocator,
                                 char const * backing_file,
                                 size_t size)
{
    allocator->data = ucs_malloc(sizeof(sharp_allocator_pmem_data_t));
    if(!allocator->data){
        return SHARP_ERR_NO_MEMORY;
    }

    sharp_allocator_pmem_data_t *d = allocator->data;

    pmem_data_init(d);

    /* security concern: the file may already have been created with differing
     * file permissions and owner than expected. This should be checked*/
    d->base = pmem_map_file(backing_file,
                            size,
                            PMEM_FILE_CREATE,
                            S_IRUSR | S_IWUSR, /* mode flags */
                            &d->size, /* output for mapping size */
                            &d->is_pmem); /* weather or not this is pmem */

    if (d->base == NULL) {
        pmem_data_finalize(d);
        allocator->data = NULL;
        ucs_free(allocator->data);
        /* EINVAL  is returned if the file is too large or size (interpreted 
         * as an off_t) is negative. Normally it could also be the result of  
         * PMEM_FILE_TMPFILE being used without PMEM_FILE_CREATE, but we did 
         * not specify PMEM_FILE_TMPFILE */
        if (errno == EFBIG || errno == EINVAL) {
            return SHARP_ERR_BACKING_FILE_TOO_LARGE;
        } else if (errno == EPERM || errno == EROFS) {
            /* the file system can not expand files */
            return SHARP_ERR_UNSUPPORTED; 
        } else if (errno == ENOMEM) {
            return SHARP_ERR_NO_MEMORY;
        } else {
            return SHARP_ERR_FAILED_OPENING_BACKING_FILE;
        }
    }

    /* ensure that is_pmem isn't -1 since that would be a valid 'true' return
     * according to the documentation */
    if (d->is_pmem) {
        d->is_pmem = 1;
    }

    sharp_allocator_pmem_persistent_data_t * pd = d->base;
    d->can_allocate = ! pd->initialized;
    if (d->can_allocate) {
        memset(d->base + sizeof(*pd), 0, d->size - sizeof(*pd));
    }

    return SHARP_OK;
}

/***********************************************************/
/* return the next pointer after ptr that is page aligned 
 * The system page size must be a power of 2*/
static inline void * page_align(void * ptr)
{
    unsigned long page_size = sysconf(_SC_PAGE_SIZE);
    uintptr_t as_int = (uintptr_t) ptr;
    uintptr_t far_side = as_int;

    far_side += page_size;
    far_side &= ~(page_size - 1);

    if (far_side - as_int == page_size) {
        return ptr;
    } else {
        return (void*) far_side;
    }
}

/***********************************************************/
static inline int pmem_first_allocation(sharp_allocator_t *allocator,
                                        size_t size,
                                        void **buffer)
{
   sharp_allocator_pmem_data_t *d = allocator->data;
   size_t extra = d->size - 
        sizeof(sharp_allocator_pmem_persistent_data_t) - VMEM_MIN_POOL;

    /* extra overflowed */
    if (extra >= d->size) {
        return SHARP_ERR_NO_MEMORY;
    }

    if (extra <= size) {
        return SHARP_ERR_NO_MEMORY;
    }
   
    d->first_allocation = 0;
    *buffer = d->base + sizeof(sharp_allocator_pmem_persistent_data_t);

    d->pool_base = *buffer + size;
    d->pool_base = page_align(d->pool_base);
    size_t pool_size = d->size - (d->pool_base - d->base);

    if (d->can_allocate) {
        memset(*buffer, 0, size);
        d->pool = vmem_create_in_region(d->pool_base, pool_size);

        if (! d->pool) {
            d->first_allocation = 1;
            perror("Could not create a memory pool");
            return SHARP_ERR_NO_MEMORY;
        }
        ((sharp_allocator_pmem_persistent_data_t *)d->base)->initialized = 1;

        if (d->is_pmem) {
            pmem_persist(d->base, sizeof(sharp_allocator_pmem_persistent_data_t));
        } else {
            if (pmem_msync(d->base, sizeof(sharp_allocator_pmem_persistent_data_t))) {
                perror("Could not save changes");
                return SHARP_ERR_NO_MEMORY;
            }
        }
    }

    return SHARP_OK;
}

/***********************************************************/
static inline int pmem_chunk_free(sharp_allocator_t *a,
                                  sharp_chunk_t *c)
{
    sharp_allocator_pmem_data_t *d = a->data;
    vmem_free(d->pool, c->buffer);

    memset(c, -1, sizeof(*c));
    ucs_free(c);

    return SHARP_OK;

}
/***********************************************************/
int sharp_allocator_pmem_chunk_malloc(sharp_allocator_t *allocator,
                                      size_t size,
                                      void **buffer)
{
    sharp_allocator_pmem_data_t *d = allocator->data;

    /* ensure that there is enough room to still instantiate a memory pool */

    if (d->first_allocation) {
      return pmem_first_allocation(allocator, size, buffer);
    }
    sharp_chunk_t *ret = NULL;

    if (SHARP_OK == sharp_allocator_alloc_cached_chunk(allocator, size, buffer, 0)) {
        return SHARP_OK;
    }
    
    ret = ucs_malloc(sizeof(sharp_chunk_t));
    if (! ret) {
      return SHARP_ERR_NO_MEMORY;
    }
    ret->alignment = 0;

    int err;
    if (d->can_allocate) {
        err = sharp_allocator_alloc_chunk(allocator,
                                          ret,
                                          size,
                                          pmem_chunk_malloc,
                                          pmem_chunk_free);
        *buffer = ret->buffer;
        if (err) {
          return err;
        } else if (*buffer) {
          return SHARP_OK;
        } else {
          return SHARP_ERR_NO_MEMORY;
        }
    } else {
        return SHARP_ERR_UNSUPPORTED;
    }

    /* execution should never get here */
    return SHARP_ERR_NOT_IMPLEMENTED;
}

/***********************************************************/
int sharp_allocator_pmem_chunk_memalign(sharp_allocator_t *allocator,
                                        size_t size,
                                        void **buffer,
                                        int alignment)
{
    sharp_allocator_pmem_data_t *d = allocator->data;

    if (! d->can_allocate) {
        return SHARP_ERR_UNSUPPORTED;
    }
    if (d->first_allocation) {
        return SHARP_ERR_UNSUPPORTED;
    } else {
        *buffer = vmem_aligned_alloc(d->pool, alignment, size);

        if (! *buffer) {
            return SHARP_ERR_NO_MEMORY;
        } else {
            return SHARP_OK;
        }
    }

    return SHARP_ERR_NOT_IMPLEMENTED;
}

/***********************************************************/
int sharp_allocator_pmem_chunk_release(sharp_allocator_t *allocator, 
                                       void **buffer)
{
    sharp_allocator_pmem_data_t *d = allocator->data;

    /* if this wasn't the memory from the first allocation */
    if (*buffer != d->base + sizeof(sharp_allocator_pmem_persistent_data_t)) {
        return sharp_allocator_free_chunk(allocator,
                                          buffer,
                                          pmem_chunk_free);
    }
    *buffer = NULL;
    return SHARP_OK;
}
