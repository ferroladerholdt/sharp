/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#include "sharp_allocator_pmem.h"
#include "sharp_allocator_utilities.h"
#include "sharp_allocator.h"
#include <stddef.h>
#include <errno.h>
#include <string.h>

#include <stdio.h>

#include <utils/ucs/datastruct/list.h>
#include <utils/ucs/debug/memtrack.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <utils/sharp/sharp_errors.h>

/** 
 * @ingroup SHARP_INTERNAL
 * @brief This call is a nop. It is only used to satisfy sharp_allocator's ops
 * table
 * 
 * @returns SHARP_OK
 **/
int sharp_allocator_malloc_init(sharp_allocator_t *allocator,
                                sharp_dev_type_t dev_type, 
                                int device_id,
                                sharp_md_type_t md_type,
                                int md_id);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate a chunk using malloc 
 *
 * @param [in] allocator The allocator to allocate from
 * @param [in] size The size of the allocation
 * @param [out] buffer Where the allocated pointer will be stored
 * @returns SHARP_OK if there was not an error, SHARP_ERR_NO_MEMORY otherwise
 * */
int sharp_allocator_malloc_chunk_malloc(sharp_allocator_t *allocator, 
                                        size_t size,
                                        void **buffer);


/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate a chunk using malloc 
 *
 * @param [in] allocator The allocator to allocate from
 * @param [in] size The size of the allocation
 * @param [out] buffer Where the allocated pointer will be stored
 * @param alignment The required alignment in bytes of the output pointer
 * @returns SHARP_OK if there was not an error, SHARP_ERR_NO_MEMORY otherwise
 * */
int sharp_allocator_malloc_chunk_memalign(sharp_allocator_t *allocator, 
                                          size_t size,
                                          void **buffer,
                                          int alignment);

/** 
 * @ingroup SHARP_INTERNAL
 * @param [in] allocator The allocator to release to
 * @brief Return memory back to the allocator 
 * @param [in,out] buffer Where the allocated pointer will be stored.
 * */
int sharp_allocator_malloc_chunk_release(sharp_allocator_t *allocator, void **buffer);


/** 
 * @ingroup SHARP_INTERNAL
 * @brief This call ensures all allocated memory is freed
 * @param [in] allocator The allocator to finalize
 * @returns SHARP_OK
 **/
int sharp_allocator_malloc_finalize(sharp_allocator_t *allocator);


