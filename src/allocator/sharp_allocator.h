/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED.
  *
  */
#ifndef SHARP_ALLOCATOR_H_
#define SHARP_ALLOCATOR_H_

/*
   Sharp allocator acts as a meta allocator wrapping around allocators that is
   available for DDRAM, HBM, GPU memory and NVRAM. When allocator is not available for a
   device, it implements its own allocator.

   An allocator instance is attached to memory descriptor. For example, for each
   NUMA domain a memory domain is created, and thus an allocator instance is
   attached.

  */
#include <ucs/datastruct/list.h>
#include "sharp_mdomains/api/sharp_mdomains_md.h"

#include <stdint.h>
#include <numa.h>

typedef struct sharp_chunk sharp_chunk_t;
typedef struct sharp_allocator sharp_allocator_t;
typedef struct sharp_allocator_ops sharp_allocator_ops_t;
typedef struct sharp_locality sharp_locality_t;
typedef struct sharp_allocator_list_item sharp_allocator_list_item_t;


/** these enum values represent device types*/
typedef enum {
    /** this represents the default malloc allocator */
    SHARP_ALLOCATOR_MALLOC,
    /** this represents the gpu memory allocator */
    SHARP_ALLOCATOR_GMEM,
    /** this represents the persistent memory allocator */
    SHARP_ALLOCATOR_PMEM,
    /** this represents the memkind allocator */
    SHARP_ALLOCATOR_MEMKIND
} sharp_allocator_type_t;

/**
 * \brief A chunk structure used to remember past allocations
 *
 * This is used in the allocator's free and used lists
 * */
struct sharp_chunk {
    /**  the size of the allocation in bytes */
    size_t          size;
    /** the base pointer to the data. */
    void            *buffer;
    /** the maximal alignment of buffer. This is the cached result from
     * sharp_find_pointer_alignment  */
    size_t          alignment;
    /** a list link to either the free list or the used list */
    ucs_list_link_t list;
};


/**
 * \brief Contains locality information for internal use
 * */
struct sharp_locality {
    /** this contains the bitmask storing our numa nodes information */
    struct               bitmask *nodemask;
    /** this contains the maximum possible numa node number for the system*/
    int                  max_numa_nodes;
    /** this contains the sharp device type the allocator will be using */
    sharp_dev_type_t     device;
};

/**
 * @ingroup SHARP_INTERNAL
 * @brief This structure contains the functions that will be used to request
 * memory from the allocators.
 * */
struct sharp_allocator_ops {
    /** initialize an allocator. Depending on the type of the allocator another
     *  function call may be required to initialize the allocator */
    int     (*init_allocator)(sharp_allocator_t *allocator,
                              sharp_dev_type_t dev_type, int device_id,
                              sharp_md_type_t md_type, int md_id);
    /** have the allocator free its inner structures and release any held
     *  resources. After a call to this function using the memory of the
     *  allocator results in undefined behavior */
    int     (*finalize_allocator)(sharp_allocator_t *allocator);
    /** allocate some memory using the allocator. size is the size of the
     *  allocation in bytes. chunk_p will be set to a pointer to the allocated
     *  memory if the allocation is successful. */
    int     (*chunk_alloc)(sharp_allocator_t *allocator,
                           size_t size,
                           void **chunk_p);
    /** like the above function, but it will also enforce pointer alignment
     *  requirements */
    int     (*chunk_memalign)(sharp_allocator_t *allocator, size_t size_p,
                              void **chunk_p,
                              int alignment);
    /** Return memory to the allocator  */
    int    (*chunk_release)(sharp_allocator_t *allocator, void **chunk);
};

/**
 * @ingroup SHARP_INTERNAL
 * @brief This structure contains the allocators data. 
 *
 *  This structure is initialized by a call to sharp_allocator_init.
 *
 */
struct sharp_allocator {
    ucs_list_link_t         *allocated_chunk_list; /**< List of memory buffers
                                                      allocated here */
    ucs_list_link_t         *chunk_freelist;  /**< List of memory buffers
                                                 available for reuse */
    size_t freelist_size; /**< number of elements in the free list */
    void                    *data; /**< Data is specific and private to
                                      allocator */
    sharp_locality_t        *locality; /**< store the locality information */
    sharp_allocator_ops_t   *ops; /**< The function table set up by the
                                    allocator */
};

struct sharp_allocator_list_item {
    sharp_allocator_t *allocator;
    sharp_allocator_type_t type;
    ucs_list_link_t link;
};

/**
 * @ingroup SHARP_INTERNAL
 * @brief Initialize the allocator
 *
 * @param [in,out]allocator A non null pointer to to a pointer of the allocator
 *                          that is being initialized. In the event of an error
 *                          this value is not modified.
 * @param [in] type         The type of memory this allocator will manage
 * @param [in] name         Stuff
 * @param [in] device       The device that holds our locality information for
 *                          allocations.
 */
int sharp_allocator_init(sharp_allocator_t **allocator,
                         sharp_allocator_type_t type,
                         char *name, sharp_dev_t device);

/**
 * @ingroup SHARP_INTERNAL
 * @brief Initialize all of the allocators for the devices
 *
 * @param [in,out]allocator A non null pointer to the allocator that is being
 *                          initialized
 * @param [out] list        A non null pointer to a ucs_list that will 
 *                          contain all of the allocators.
 */
int sharp_allocator_init_all(ucs_list_link_t * list);

/**
 * @ingroup SHARP_INTERNAL
 * @brief Initialize all of the allocators for the devices
 *
 * @param [in,out]allocator A non null pointer to the allocator that is being
 *                          initialized
 * @param [out] list        A non null pointer to a ucs_list that will 
 *                          contain all of the allocators.
 */
int sharp_allocator_finalize_allocator_list(ucs_list_link_t *list);


/**
 * @ingroup SHARP_INTERNAL
 * @brief clean up the allocator's resources
 *
 * @param [in] allocator The allocator to be cleaned.
 *
 * */
int sharp_allocator_cleanup(sharp_allocator_t *allocator);

#endif
