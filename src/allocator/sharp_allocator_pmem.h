/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/


#ifndef SHARP_PMEM_ALLOCATOR_H_
#define SHARP_PMEM_ALLOCATOR_H_

#include "sharp_allocator.h"
#include <libpmem.h>
#include <libvmem.h>

/** 
 * @ingroup SHARP_INTERNAL
 * @brief This call is a nop. It is only used to satisfy sharp_allocator's ops
 * table
 * 
 * @returns SHARP_ERR_NOT_IMPLEMENTED
 **/
int sharp_allocator_pmem_init(sharp_allocator_t *allocator,
                              sharp_dev_type_t dev_type, 
                              int device_id,
                              sharp_md_type_t md_type,
                              int md_id);


/** 
 * @ingroup SHARP_INTERNAL
 * @brief Initialize the allocator.
 *
 * If the backing_file is not found a new one will be created. The contents of
 * the backing file are undefined if it was created by a call to this function.
 * Otherwise the contents of the first allocation will be the same every time
 * the allocator is called, assuming the size requested is the same.
 * 
 * @param [in,out] allocator     the allocator that is being initialized
 *
 * @param[in] backing_file the name of a file used to store the mapped data in a
 * non-volatile way.
 *
 * @param[in] The size the backing_file shall be set to if the backing_file must
 * be created.
 *
 * @returns When successful SHARP_OK is returned.
 * If there is a problem allocating required data structures SHARP_ERR_NO_MEMORY
 * is returned
 */
int sharp_allocator_pmem_init2(sharp_allocator_t *allocator,
                               char const * backing_file,
                               size_t size);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate non-volatile memory. 
 *
 * The first time this function is called
 * after initialization the buffer will be located at the same offset into the
 * backing non-volatile memory across
 * program runs. This allows the caller to store and recover data that can be
 * used to make use of a previous runs allocated data. If the backing_file has
 * already been used before then the allocator will fail with
 * SHARP_ERR_UNSUPPORTED and memory allocation will not be possible, but data
 * allocated and set in previous program runs can still be accessed. If the
 * backing_file has not been previously used the first allocation will be zero
 * initialized. If the
 * allocator runs out of memory SHARP_ERR_NO_MEM will be returned.
 *
 * @param [in,out]  buffer     Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned
 */
int sharp_allocator_pmem_chunk_malloc(sharp_allocator_t *allocator, 
                                      size_t size,
                                      void **buffer);


/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate memory nvram and enforce a byte alignment.
 *
 * This must not be the first allocation after the allocator has been
 * initialized. It otherwise follows the same rules as sharp_pmem_chunk_malloc
 * with the addition of user specified byte alignment for the returned pointers.
 *
 *
 * @param [in,out]  buffer     Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated. This
 * must be >= alignment or a failure return code may occur, and the memory may
 * not be allocated.
 *
 * @param [in] The byte alignment required. The value is in bytes.
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned, or
 * if this was the first allocation since the allocators initialization then
 * SHARP_ERR_UNSUPPORTED will be returned.
 */
int sharp_allocator_pmem_chunk_memalign(sharp_allocator_t *allocator, 
                                        size_t size,
                                        void **buffer,
                                        int alignment);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Release memory back to the allocator
 *
 * @param [in]  allocator   The allocator that is releasing the memory
 *
 * @param [in,out]  buffer     Where the memory allocated will be returned. This
 * must not be null. After this function is complete *buffer==NULL
 *
 * @returns When successful SHARP_OK is returned.
 * If the deallocation is not successful then SHARP_ERR_INVALID_FREE_ATTEMPT   
 * is returned.
 */
int sharp_allocator_pmem_chunk_release(sharp_allocator_t *allocator,
                                       void **buffer);



/** 
 * @ingroup SHARP_INTERNAL
 * @brief Finalize the allocator. The state of the backing files is undefined
 * after this call.
 *
 * @param [in]  allocator   The allocator that is being finalized
 *
 * @returns SHARP_OK and attempts to free all memory allocated by this
 * allocator. This includes freeing memory not explicitly freed by the user of
 * the library.
 */
int sharp_allocator_pmem_finalize(sharp_allocator_t *allocator);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief This structure is stored at the beginning of the file in nvram  and contains
 * the data required to allow the allocator to react correctly after a failure
 **/
typedef struct sharp_allocator_pmem_persistent_data {
    /** 0 if the file has not been used, 1 if the file has been used*/
    int initialized;

} sharp_allocator_pmem_persistent_data_t;

//persistent



/**
 * @ingroup SHARP_INTERNAL 
 * @brief Internal data keeping for a pmem allocator
 *
 * */
typedef struct sharp_allocator_pmem_data {
    /** has this allocator been used yet? The first allocation has special
     * rules; see sharp_pmem_malloc for details */
    int first_allocation;

    /** the pmem pool that we will be allocating out of*/
    VMEM * pool;

    /** the base pointer into the non-volatile memory region */
    void * base;

    /** the base pointer for the memory managed in the pool*/
    void * pool_base;

    /** this is the number of bytes mapped into the process so that
     * [base,base+size) are all valid to access
     */
    size_t size;

    /** weather or not this region is backed by nvram
     * -1 == uninitialized
     *  0 == not backed by nvram
     *  1 == backed by nvram
     *  */
    int is_pmem;

    /** 1 if vmem's allocator can be setup. 0 otherwise */
    int can_allocate;

    /** the backing file's file descriptor  */
    int fd;

} sharp_allocator_pmem_data_t;

#endif
