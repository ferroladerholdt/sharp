/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#include "sharp_allocator.h"
#include "sharp_allocator_utilities.h"

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <strings.h>
#include <limits.h>


/***********************************************************/
sharp_chunk_t * sharp_allocator_get_chunk(ucs_list_link_t *freelist,
                                          size_t size,
                                          size_t max_waste,
                                          size_t alignment)
{
    sharp_chunk_t *chunk_item = NULL;

    ucs_list_for_each(chunk_item, freelist, list) {
        if (chunk_item->alignment < alignment) { 
            continue;
        }
        if (chunk_item->size < size) { 
            continue;
        }
        if (max_waste < chunk_item->size - size) { 
            continue;
        }

        return chunk_item; 
    }
    return NULL;
}

/***********************************************************/
int sharp_allocator_find_pointer_alignment(void *ptr)
{
    int first;
    uintptr_t value = (uintptr_t)ptr;
    /*
        bitwise-and with INT_MAX should result in the top of value
        being set to 0. This should avoid undefined behavior when 
        trimmed would overflow without any special treatment.
    */
    int trimmed = value & (INT_MAX - 1);
    /* 2^(first bit set-1) is the largest alignment of the pointer
       30 is the largest number that doesn't result in negative output assuming 
       the int type is 32 bits
    */
    const int max_bit_set = 30; 

    first = ffs(trimmed);
    if (first == 0 || first >= max_bit_set) {
        return 1 << max_bit_set;
    } else {
        return 1 << (first-1);
    }
}


/***********************************************************/
void sharp_allocator_purge_list(sharp_allocator_t *a, 
                                ucs_list_link_t *link, 
                       sharp_errors_t (*free_chunk)(sharp_allocator_t *a, sharp_chunk_t *)) 
{
    sharp_chunk_t *chunk = NULL;
    sharp_chunk_t *next = NULL;
    ucs_list_for_each_safe(chunk, next, link, list) {
        ucs_list_del(&chunk->list);
        free_chunk(a,chunk);
    }
    a->freelist_size = 0;
}


/***********************************************************/
int sharp_allocator_free_chunk(sharp_allocator_t *allocator, 
                               void **_buffer, 
               sharp_errors_t (*chunk_free)(sharp_allocator_t *, sharp_chunk_t*))
{
    /*return the node to the free list*/

    /*find the buffer allocation in the used list*/
    
    sharp_chunk_t* current = NULL;
    int found = 0;
    void* buffer = *_buffer;
    if (allocator->freelist_size > 50) { //the constant is arbitrarily chosen
        sharp_chunk_t * last = NULL;
        last = ucs_list_tail(allocator->chunk_freelist, sharp_chunk_t, list);
        ucs_list_del(&last->list);
        allocator->freelist_size--;
        chunk_free(allocator, last);
    }

    ucs_list_for_each(current, allocator->allocated_chunk_list, list) {
        void* low = current->buffer;
        void* high = low + current->size;

        if (buffer < high && buffer >= low) {
            found = 1;
            break;
        }
    }

    if (!found) {
        return SHARP_ERR_INVALID_FREE_ATTEMPT;
    }

    ucs_list_del(&current->list);
    ucs_list_insert_after(allocator->chunk_freelist, &current->list);
    allocator->freelist_size++;
    *_buffer = NULL;
    return SHARP_OK;
}

/***********************************************************/
sharp_errors_t sharp_allocator_alloc_chunk(sharp_allocator_t *allocator,
                                           sharp_chunk_t *ret,
                                           size_t size,
                            sharp_errors_t (*chunk_alloc)(sharp_allocator_t *,
                                            size_t,
                                            sharp_chunk_t*),
                            sharp_errors_t (*chunk_free)(sharp_allocator_t *, 
                                            sharp_chunk_t *))
{
    int err;
    int first_failure = 1;
    while (1) {
        err = chunk_alloc(allocator, size, ret);
        if (err == SHARP_OK) {
            break;
        } 
        if (!first_failure) {
            return SHARP_ERR_NO_MEMORY;
        } else {
            first_failure = 0;
            sharp_allocator_purge_list(allocator,
                                       allocator->chunk_freelist, 
                                       chunk_free);
            allocator->freelist_size = 0;
        }
    }
    ucs_list_insert_after(allocator->allocated_chunk_list, &ret->list);
    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_alloc_cached_chunk(sharp_allocator_t *allocator,
                                       size_t size,
                                       void **buffer,
                                       size_t alignment)
{
    /*if the free list will satisfy the request return that*/
    sharp_chunk_t *ret;

    /* the constant is a heuristic chosen arbitrarily, this may need to be
     * modifiable, or be tested to improve the result.  */
    ret = sharp_allocator_get_chunk(allocator->chunk_freelist,
                                    size,
                                    alignment,
                                    1024);
    if (ret) {
        /*  remove it from the free list */
        ucs_list_del(&ret->list); 
        allocator->freelist_size--;
        /* add it to the used list */
        ucs_list_insert_after(allocator->allocated_chunk_list, &ret->list);
        *buffer = ret->buffer;
        return SHARP_OK;
    }
    return SHARP_ERR_NO_MEMORY;
}

/* ****************************************************** */
int list_length(ucs_list_link_t *lst)
{
    return ucs_list_length(lst);
}

