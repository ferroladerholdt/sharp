/**
 * Copyright (C) UT-Battelle, LCC. 2017. ALL RIGHTS RESERVED.
 *
 */

#ifndef _SHARP_ALLOCATOR_API
#define _SHARP_ALLOCATOR_API

/**
 * @ingroup SHARP_ALLOCATOR_DATATYPE
 * @brief Information parameters used to define implicitly and explicitly the memory
 *        types to use for future memory allocations 
 */
struct sharp_allocator_info_params {
    sharp_hint_t        allocator_hints;        /**< Allocation Hint */
    sharp_constraint_t  allocator_constraints;  /**< Allocation Constraint */
};

/**
 * @ingroup SHARP_ALLOCATOR_DATATYPE
 * @brief Memory domains associated with allocator based on hints and constraints 
 */
struct sharp_allocator_obj {
    int nr_mds;
    sharp_md_list_item_t ** md; /**< Memory domains associated with allocators */ 
};

typedef struct sharp_allocator_info_params sharp_allocator_info_params_t;
typedef struct sharp_allocator_obj sharp_allocator_obj_t;

/**
 * @ingroup SHARP_ALLOCATOR_API
 * @brief Initialize the SharP allocator such that future allocations across memory types can occur.
 * 
 * @returns On success, returns SHARP_OK. On Failure, returns an error code
 */
int sharp_init_allocator(void);

/**
 * @ingroup SHARP_ALLOCATOR_API 
 * @brief Initialize SharP allocator object with hints and constraints for future implicit and explicit memory allocations on various memory types.
 *
 * @params [in] *params         Parameters to be used to select the correct allocator
 *
 * @return On success, returns valid SharP Allocator object. On failure, returns NULL
 */
sharp_allocator_obj_t * sharp_init_allocator_obj(
                                            sharp_allocator_info_params_t * params);

/**
 * @ingroup SHARP_ALLOCATOR_API
 * @brief Allocates memory on a particular memory type
 * 
 * @param [in] *allocator       Allocator object for particular memory type
 * @param [in] size             Size of memory to be allocated
 *
 * @return On success, a valid point to buffer on allocated memory. On failure, NULL is returned.
 */
void * sharp_allocator_alloc(sharp_allocator_obj_t * allocator, size_t size); 

/**
 * @ingroup SHARP_ALLOCATOR_API
 * @brief Allocates aligned memory on a particular memory type
 *
 * @param [in] *allocator       Allocator object for particular memory type
 * @param [in] size             Size of memory to be allocated
 * @param [in] alignment        Memory alignment to be used
 *
 * @return On success, valid point to buffer allocated on memory. On failure, NULL is returned.
 */
void * sharp_allocator_alloc_memalign(sharp_allocator_obj_t * allocator, 
                                      size_t size, 
                                      int alignment);

/**
 * @ingroup SHARP_ALLOCATOR_API
 * @brief Frees previously allocated memory
 * 
 * @param [in] *allocator       Allocator object for particular memory type
 * @param [in] *buffer          Pointer to allocated buffer
 *
 * @return SHARP_OK on success; an error code on failure.
 */ 
int sharp_allocator_free(sharp_allocator_obj_t * allocator, void * buffer);

#endif
