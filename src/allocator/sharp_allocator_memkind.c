#include <ucs/datastruct/list.h>

#include "sharp_allocator_utilities.h"
#include "sharp_allocator_memkind.h"

#include <utils/sharp/sharp_errors.h>
#include <ucs/debug/memtrack.h>
#include <ucs/datastruct/list.h>

#include <string.h>
#include <stdio.h>
#include <numaif.h>
#include <stddef.h>
#include <numa.h>
#include <errno.h>
#include <unistd.h>

/***********************************************************/
/**  
 * \brief mbind on memory ranges, moving them if necessary
 *
 *  \param [in] the allocator that has the required numa information for binding
 *  \param [in] buff must be page aligned and will be bound to a particular
 *  numa node
 *  \param [in] size the length of the memory pointed to by buff
 *  */
static int sharp_internal_mbind(sharp_allocator_t *a,
                                void * buff,
                                size_t size)
{
    int mode = MPOL_BIND;
    int flags = MPOL_MF_MOVE | MPOL_MF_STRICT;
    long unsigned *nodemask = a->locality->nodemask->maskp;
    unsigned long maxnode = a->locality->nodemask->size;

    int err = mbind(buff, size, mode, nodemask, maxnode, flags);

    if (err == 0) {
        return SHARP_OK;
    }

    perror("");

    switch (errno) {
        case EINVAL:
            /* the most likely cause is that buff is not page aligned */
            return SHARP_INTERNAL_ERROR;
        case EFAULT:
            return SHARP_INVALID_MEMORY_RANGE;
        case ENOMEM:
            fprintf(stderr,"Kernel out of memory for mbind request\n");
        case EIO:
            return SHARP_ERR_NO_MEMORY;
        default:
            return SHARP_ERR_UNKNOWN_ERROR;
    }
}

/***********************************************************/
int sharp_allocator_memkind_chunk_malloc(sharp_allocator_t *a, 
                                         size_t size,
                                         void **buffer)
{
    int err;
    sharp_allocator_memkind_data_t *d = a->data;

    err = sharp_allocator_memkind_chunk_memalign(a, size, buffer, d->page_size);
    return err;
}


/***********************************************************/
/* this function assumes that c already has the required alignment set to 
 * a power of 2 (or 0 if no alignment is required)
 */
static int memkind_chunk_malloc(sharp_allocator_t * a,
                                size_t size,
                                sharp_chunk_t *c)
{
    int err;
    sharp_allocator_memkind_data_t *d = a->data;

    if (d->page_size > (int) c->alignment) {
        c->alignment = d->page_size;
    }

    err = memkind_posix_memalign(d->kind, &c->buffer, c->alignment, size);

    if (err != 0 || c->buffer == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    c->size = size;
    c->alignment = sharp_allocator_find_pointer_alignment(c->buffer);

    return SHARP_OK;
}


/***********************************************************/
static sharp_errors_t memkind_chunk_free(sharp_allocator_t * a,
                                         sharp_chunk_t *c)
{
    sharp_allocator_memkind_data_t *d = a->data;
    memkind_free(d->kind, c->buffer);
    memset(c, -1, sizeof(*c));
    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_memkind_chunk_memalign(sharp_allocator_t *allocator,
                                           size_t size,
                                           void **buffer,
                                           int alignment)
{
    int err;
    sharp_chunk_t * ret;

    err = sharp_allocator_alloc_cached_chunk(allocator, size, buffer, alignment);

    if (err == SHARP_OK) {
        return err;
    }

    ret = ucs_malloc(sizeof(sharp_chunk_t));
    if (ret == NULL) { 
        return SHARP_ERR_NO_MEMORY; 
    }
    ret->alignment = alignment;
    err = sharp_allocator_alloc_chunk(allocator,
                                      ret,
                                      size,
                                      memkind_chunk_malloc,
                                      memkind_chunk_free);

    *buffer = ret->buffer;

    if (err != 0 || *buffer == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    return sharp_internal_mbind(allocator, *buffer, size);
}

/***********************************************************/
int sharp_allocator_memkind_chunk_release(sharp_allocator_t *allocator, 
                                          void **_buffer)
{
    return sharp_allocator_free_chunk(allocator,
                                      _buffer,
                                      memkind_chunk_free);
}




/***********************************************************/
/*
 * Create an allocator based on the device and the locality information
 */
int sharp_allocator_memkind_init(sharp_allocator_t *allocator,
                                 sharp_dev_type_t dev_type,
                                 int device_id,
                                 sharp_md_type_t md_type,
                                 int md_id)
{
    sharp_allocator_memkind_data_t *d;
    int rc;
    char memkind_name[MEMKIND_NAME_LENGTH];

    allocator->data = d = (sharp_allocator_memkind_data_t*)ucs_malloc(sizeof(sharp_allocator_memkind_data_t));

    if (d == NULL) {
        fprintf(stderr, "Cannot allocate memory for allocator->data\n");
        return SHARP_ERR_NO_MEMORY;
    }

    /* Create a unique string to represent the allocator */
    snprintf(memkind_name, MEMKIND_NAME_LENGTH -1,
            "Allocator-%d%d%d%d",dev_type, device_id, md_type, md_id);

    /* Create memory kind for this device type, device id, and locality */
    switch (dev_type) {
        case SHARP_MD_HBM:
            rc = memkind_create(&SHARP_HBW_OPS, memkind_name, &d->kind);
            break;
        case SHARP_MD_DDR:
        default:
            rc = memkind_create(&SHARP_DDRAM_OPS, memkind_name, &d->kind);
            break;
    }

    if (rc) {
        fprintf(stderr, " Error creating new memkind\n");
        return SHARP_ERR_NO_MEMORY;
    }

    d->page_size = sysconf(_SC_PAGESIZE);
    if (d->page_size == -1) {
        fprintf(stderr, "_SC_PAGESIZE is not recognized on this system");
        return SHARP_ERR_UNSUPPORTED;
    }

    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_memkind_get_mbind_nodemask(struct memkind *kind,
                                               unsigned long *nodemask,
                                               unsigned long maxnode)
{
    sharp_allocator_memkind_data_t *d = (void*)
        (uintptr_t)kind - ((uintptr_t)offsetof( sharp_allocator_memkind_data_t, kind ));

    struct bitmask nodemask_bm = {maxnode, nodemask};
    numa_bitmask_clearall(&nodemask_bm);
    numa_bitmask_setbit(&nodemask_bm, d->dev_id);
    return 0;
}

/***********************************************************/
int sharp_allocator_memkind_finalize(sharp_allocator_t * allocator)
{
    sharp_allocator_memkind_data_t *d = allocator->data;

    sharp_allocator_purge_list(allocator,
                     allocator->chunk_freelist,
                     memkind_chunk_free);
    sharp_allocator_purge_list(allocator,
                     allocator->allocated_chunk_list,
                     memkind_chunk_free);

    ucs_free(d);

    /* TODO figure out how to deallocate my kind */

    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_memkind_internal_get_mbind_mode(struct memkind *kind,
                                                    int *mode)
{
    /* to silence compiler warnings */
    *mode = MPOL_BIND;
    return 0;
}


