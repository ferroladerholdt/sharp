/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/


#ifndef SHARP_ALLOCATOR_GMEM_ALLOCATOR_H_
#define SHARP_ALLOCATOR_GMEM_ALLOCATOR_H_

#include "sharp_allocator.h"
typedef struct sharp_allocator_gmem_data {
    /* Cache locality and MD device info */
    sharp_dev_type_t dev_type;
    sharp_md_type_t  md_type;
    int              dev_id;
    int              md_id;

    /** This is the id according to cuda that all allocations and releases
     * pertain to */
    int              gpu_id;
} sharp_allocator_gmem_data_t;

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Initialize the allocator.
 * 
 * @param [in,out] allocator     the allocator that is being initialized
 *
 * @param [in] md_type      unused
 *
 * @param [in] dev_type     unused
 *
 * @param [in] md_id        unused
 *
 * @param[in]  device_id    offset from the first GPU that will be used for
 * memory allocation. 
 *
 * @returns When successful SHARP_OK is returned.
 * If there is a problem allocating required data structures SHARP_ERR_NO_MEMORY
 * is returned
 */
int sharp_allocator_gmem_init(sharp_allocator_t *allocator,
                                 sharp_dev_type_t dev_type, 
                                 int device_id,
                                 sharp_md_type_t md_type,
                                 int md_id);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate memory on a GPU

 * @param [in,out]  buffer      Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned
 */
int sharp_allocator_gmem_chunk_malloc(sharp_allocator_t *allocator, 
                                      size_t size,
                                      void **buffer);


/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate memory on a GPU and enforce a byte alignment

 * @param [in,out]  buffer      Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated. This
 * must be >= alignment or a failure return code may occur, and the memory may
 * not be allocated.
 *
 * @param [in] The byte alignment required. The value is in bytes.
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned
 */
int sharp_allocator_gmem_chunk_memalign(sharp_allocator_t *allocator, 
                                        size_t size,
                                        void **buffer,
                                        int alignment);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Release memory back to the allocator
 *
 * @param [in]  allocator   The allocator that is releasing the memory
 *
 * @param [in,out]  buffer  Where the memory allocated will be returned from.
 * This must not be null. After this function is complete *buffer==NULL
 *
 * @returns When successful SHARP_OK is returned.
 * If the deallocation is not successful then SHARP_ERR_INVALID_FREE_ATTEMPT   
 * is returned.
 */
int sharp_allocator_gmem_chunk_release(sharp_allocator_t *allocator, void **buffer);



/** 
 * @ingroup SHARP_INTERNAL
 * @brief 
 *
 * @param [in]  allocator   The allocator that is being finalized
 *
 * @returns SHARP_OK and attempts to free all memory allocated by this
 * allocator. This includes freeing memory not explicitly freed by the user of
 * the library.
 */
int sharp_allocator_gmem_finalize(sharp_allocator_t *allocator);

#endif
