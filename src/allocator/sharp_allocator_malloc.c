/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#include "sharp_allocator_utilities.h"
#include "sharp_allocator.h"
#include <stddef.h>
#include <errno.h>
#include <string.h>

#include <stdio.h>

#include <utils/ucs/datastruct/list.h>
#include <utils/ucs/debug/memtrack.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <utils/sharp/sharp_errors.h>

#include "sharp_allocator_malloc.h"

/***********************************************************/
int sharp_allocator_malloc_init(sharp_allocator_t *allocator,
                              sharp_dev_type_t dev_type, 
                              int device_id,
                              sharp_md_type_t md_type,
                              int md_id){
    return SHARP_OK;
}

/***********************************************************/
static inline int malloc_chunk_malloc(sharp_allocator_t *a, 
                                    size_t size,
                                    sharp_chunk_t *c){
    /* to silence compiler warnings */

    c->buffer = malloc(size);
    if( ! c->buffer ){
        c->buffer = NULL;
        return SHARP_ERR_NO_MEMORY;
    } else {
        c->alignment = sharp_allocator_find_pointer_alignment(c->buffer);
        c->size = size;
        return SHARP_OK;
    }
}

/***********************************************************/
/* c != NULL */
static sharp_errors_t malloc_chunk_free(sharp_allocator_t *a, sharp_chunk_t *c){

    free(c->buffer);
    ucs_free( c );

    return SHARP_OK;
}



/***********************************************************/
int sharp_allocator_malloc_chunk_malloc(sharp_allocator_t *allocator, 
                            size_t size,
                            void **buffer){
    if( SHARP_OK == sharp_allocator_alloc_cached_chunk(allocator,
                                                       size,
                                                       buffer,
                                                       0)){
        return SHARP_OK;
    }

    sharp_chunk_t *ret = ucs_malloc(sizeof(*ret));
    if( ! ret ){
        return SHARP_ERR_NO_MEMORY;
    }

    int err = sharp_allocator_alloc_chunk(allocator,
                                          ret,
                                          size,
                                          malloc_chunk_malloc,
                                          malloc_chunk_free);
    if( SHARP_OK == err){
        *buffer = ret->buffer;
        return SHARP_OK;
    }

    return  err;
}



/***********************************************************/
int sharp_allocator_malloc_chunk_memalign(sharp_allocator_t *allocator,
                                          size_t size,
                                          void **buffer,
                                          int alignment){

    if( size < (size_t)alignment ){
        return SHARP_ERR_UNSUPPORTED;
    }
    /* allocate a chunk that is the size of the requested buffer and check
     * alignment*/
    int err = sharp_allocator_malloc_chunk_malloc(allocator, size, buffer);
    if( err == SHARP_OK && 
        sharp_allocator_find_pointer_alignment( buffer ) >= alignment ){
        return SHARP_OK;
    }
    err = sharp_allocator_malloc_chunk_release( allocator, buffer);
    if( err != SHARP_OK ) {
        *buffer=NULL;
        return err;
    }
    
    /* the above allocated buffer was not well aligned; allocate a new one
     * large enough to ensure that we can return a properly alignment pointer*/
    err = sharp_allocator_malloc_chunk_malloc( allocator,
                                               size+alignment,
                                               buffer);
    if( err != SHARP_OK ) {
        *buffer=NULL;
        return err;
    }

    /*mask the bits so that we have a properly alignment pointer*/
    uintptr_t raw = (uintptr_t) *buffer;
    raw += alignment;
    raw = raw & (~(alignment - 1));

    *buffer= (void*) raw;

    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_malloc_chunk_release(sharp_allocator_t *allocator,
                                         void **buffer){
    return sharp_allocator_free_chunk(allocator, buffer, malloc_chunk_free);
}


/***********************************************************/
int sharp_allocator_malloc_finalize(sharp_allocator_t *a){
    sharp_allocator_purge_list(a, a->allocated_chunk_list, malloc_chunk_free);
    sharp_allocator_purge_list(a, a->chunk_freelist, malloc_chunk_free);
    return SHARP_OK;
}
