/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#ifndef  SHARP_ALLOCATOR_UTILITIES_H
#define  SHARP_ALLOCATOR_UTILITIES_H

#include <ucs/datastruct/list.h>
#include "sharp_allocator.h"
#include <utils/sharp/sharp_errors.h>


/** 
 * @ingroup SHARP_INTERNAL
 * \brief Search the provided ucs list for a memory chunk large enough to
 * satisfy the memory request, but small enough to keep the internal
 * fragmentation below max_waste.
 * 
 * \param [in] freelist     a list of sharp_chunks that are available for
 *                          allocation to the user application.
 * \param [in] size         the required size for the allocation
 * \param [in] max_waste    The maximum amount over the requested size an
 * allocation is allowed to be. This prevents an extremely large block from being
 * used to satisfy a small request.
 * \param [in] alignment    The alignment requirement for the pointer.
 *
 * \returns When successful a pointer to a sharp_chunk is returned.
 * When unsuccessful NULL is returned. The function is still unsuccessful if the
 * request can be satisfied, but the amount of waste would be above max_waste.
 */
sharp_chunk_t * sharp_allocator_get_chunk(ucs_list_link_t *freelist,
                                          size_t size,
                                          size_t max_waste,
                                          size_t alignment);


/** 
 * @ingroup SHARP_INTERNAL
 * \brief Find the maximal memory alignment for a pointer
 *
 * The maximal alignment is the largest power of 2 alignment that this pointer
 * satisfies. 
 *
 * \param [in] ptr     the pointer to have it's maximal alignment found
 *
 * \param [in] size       the required size for the allocation
 *
 * \returns the maximal alignment of the pointer. This is currently limited to
 * 2^31 to prevent a negative value from being returned in the event that int is
 * a 32 bit signed integer.
 */
int sharp_allocator_find_pointer_alignment(void* ptr);

/**
 * @ingroup SHARP_INTERNAL
 * \brief purge a list by returning it's data back to the initial allocator 
 *
 * After this function is ran the list will be empty and all resources pointed
 * to by it should have been freed
 *
 * \param [in] link   The list the elements will be freed from
 * \param [in] free_chunk A function that will free the chunk's held data and
 * the chunk itself.
 *
 * */
void sharp_allocator_purge_list(sharp_allocator_t *a,
                                ucs_list_link_t *link,
                                sharp_errors_t (*free_chunk)(sharp_allocator_t *,
                                                            sharp_chunk_t *));



/**
 * @ingroup SHARP_INTERNAL
 * \brief remove a node from the used list and add it to the free list.
 *
 * This assumes that the free and used lists are sharp_allocator's 
 *
 * \param [in] allocator the allocator the chunks belong to
 * \param [in] points to a pointer that shall be freed
 *
 * \returns SHARP_OK iff the memory was found in the free list and the free has
 * been successful. SHARP_INVALID_FREE_ATTEMPT is returned if the memory did not
 * belong to the allocator
 * */
int sharp_allocator_free_chunk(sharp_allocator_t *allocator,
                               void **_buffer,
                               sharp_errors_t (*chunk_free)(sharp_allocator_t *,
                                                           sharp_chunk_t*));

/**
 * @ingroup SHARP_INTERNAL
 * \brief create a new chunk and if successful add it to the used list
 *
 * This function will attempt to use the provided function to create a chunk
 * with usable memory, if it fails then the free list will be purged and the
 * allocation will be re-attempted. If this also fails then the function will
 * return the error that chunk_alloc returned
 *
 * \param [in,out] allocator The allocator that is used. The free and used lists
 * may be modified by this function
 * \param [out] ret where the newly formed chunk will be stored. This will be an
 * argument to chunk_alloc. This must not be null and it will not be modified by
 * the function will the exception of any changes that chunk_alloc makes
 * \param [in] chunk_alloc the function that will be called to create a usable 
 * chunk
 *
 * \returns SHARP_OK if the function was successful, if chunk_alloc fails both
 * times it's return value will be returned
 */
sharp_errors_t sharp_allocator_alloc_chunk(sharp_allocator_t *allocator,
                                           sharp_chunk_t *ret,
                                           size_t size,
                          sharp_errors_t (*chunk_alloc)(sharp_allocator_t *,
                                                        size_t,
                                                        sharp_chunk_t*),
                          sharp_errors_t (*chunk_free)(sharp_allocator_t *,
                                                       sharp_chunk_t *));

/**
 * @ingroup SHARP_INTERNAL
 * \brief get a chunk from the free list and return it
 *
 * This function will to get a suitable chunk from the free list and set *buffer
 * accordingly. If alignment is 0 then it will not be considered for selecting
 * the chunk
 *
 * \param [in] allocator the allocator involved in the allocation. Its used and
 * free lists may be modified by this function.
 * \param [in] size how many bytes will be allocated
 * \param [out] where the memory will be stored.
 * \param [in] alignment Described above
 *
 * \returns SHARP_OK if a suitable chunk was found and SHARP_ERR_NO_MEMORY
 * otherwise
 */
int sharp_allocator_alloc_cached_chunk(sharp_allocator_t *allocator,
                                       size_t size,
                                       void **buffer,
                                       size_t alignment);



#endif   /* ----- #ifndef SHARP_ALLOCATOR_UTILITIES_H  ----- */
