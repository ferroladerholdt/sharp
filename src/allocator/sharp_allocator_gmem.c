/**
* Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
*
*/

#include <config.h>

#include "sharp_allocator_gmem.h"
#include "sharp_allocator_utilities.h"
#include "sharp_allocator.h"

#include <ucs/datastruct/list.h>
#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <utils/sharp/sharp_errors.h>

#include <stddef.h>
#include <stdio.h>

extern ucs_list_link_t *device_list;

#define PRINT_CUDA_ERROR_DETAILED(err, str) \
    print_cuda_error_detailed( err, __FILE__, __LINE__, str)

#ifdef WITH_CUDA
#include <hwloc/cuda.h>
#include <hwloc/cudart.h>


/***********************************************************/
/* print a more detailed listing of where the error occurred */
static inline cudaError_t print_cuda_error_detailed(cudaError_t err,
                                                    char * file,
                                                    int line,
                                                    char * str)
{
    if (err != cudaSuccess) {
        if (str == 0) {
            str = "";
        }
    }
    return err;
}

/***********************************************************/
/*prints the cuda error to stderr if it is not a success code*/
static inline cudaError_t print_cuda_error(cudaError_t err , char * str)
{
    if (err != cudaSuccess) {
        if (str == 0) {
            str = "";
        }
        const char * name = cudaGetErrorName(err);
        const char * string = cudaGetErrorString(err);
        fprintf(stderr,"%s: %s: %s\n", str, name, string);
    }
    return err;
}

/***********************************************************/
/* save the current cuda state
 *
 * returns 0 when successful, and -1 if an error has occurred */
static inline int change_device(int requested, int * device)
{
    if (device) {
        if (cudaSuccess != PRINT_CUDA_ERROR_DETAILED(cudaGetDevice(device),
                "The current device could not be determined")) {
            return -1;
        }
    }

    if (cudaSuccess != PRINT_CUDA_ERROR_DETAILED(cudaSetDevice(requested),
            "The device could not be set the one requested")) {
        fprintf(stderr, "Requested device number %i\n",requested);
        return -1;
    }
    return 0;
}

/***********************************************************/
/*assumes gpu is already selected*/
static inline int gmem_chunk_malloc(sharp_allocator_t *a, 
                                    size_t size,
                                    sharp_chunk_t *c)
{
    int ret = SHARP_ERR_NOT_IMPLEMENTED;
    int err = cudaMalloc(&c->buffer, size);

    switch (err) {
        case cudaErrorMemoryAllocation:
            c->buffer = NULL;
            ret = SHARP_ERR_NO_MEMORY;
            break;
        case cudaSuccess:
            c->alignment = sharp_allocator_find_pointer_alignment(c->buffer);
            c->size = size;
            ret= c->buffer ? SHARP_OK : SHARP_ERR_NO_MEMORY;
            break;
        default:
            PRINT_CUDA_ERROR_DETAILED(err,NULL);
            c->buffer = NULL;
            ret = SHARP_CUDA_ERROR;
            break;
    }
    return ret;
}

/***********************************************************/
/*assumes gpu is already selected*/
static sharp_errors_t free_gpu_chunk(sharp_allocator_t *a, sharp_chunk_t *c){

    int err = PRINT_CUDA_ERROR_DETAILED(cudaFree(c->buffer),NULL);
    free(c);

    if (err == cudaSuccess) {
        return SHARP_OK;
    } else {
        return SHARP_ERR_INVALID_FREE_ATTEMPT;
    }
}

#else
#define cudaError_t int
static inline cudaError_t print_cuda_error_detailed(cudaError_t err,
                                                    char * file,
                                                    int line,
                                                    char * str) 
{
    return SHARP_ERR_NOT_IMPLEMENTED;
}

static inline int change_device( int requested, int * device) 
{
    return SHARP_ERR_NOT_IMPLEMENTED;
}

static inline int gmem_chunk_malloc(sharp_allocator_t *a,
                                    size_t size,
                                    sharp_chunk_t *c) 
{
    return SHARP_ERR_NOT_IMPLEMENTED;
}


static sharp_errors_t free_gpu_chunk(sharp_allocator_t *a, sharp_chunk_t *c) 
{
    return SHARP_ERR_NOT_IMPLEMENTED;
}
#undef cudaError_t
#endif /* WITH_CUDA */

/***********************************************************/
static inline int gpu_by_device_id(int device_id) 
{
	sharp_dev_item_t * dev;
	
	ucs_list_for_each(dev, device_list, next_p) {
		if (dev->device.type == SHARP_DEV_CUDA) {
			break;
		}
	}

	return dev->device.device_id - device_id;
}

/***********************************************************/
int sharp_allocator_gmem_finalize(sharp_allocator_t *allocator)
{
    int device;
    sharp_allocator_gmem_data_t *data = allocator->data;

    if (change_device(data->gpu_id, &device)) {
        return SHARP_CUDA_ERROR;
    }

    /* free all lists containing CUDA memory */
    sharp_allocator_purge_list(allocator, 
                               allocator->chunk_freelist, 
                               free_gpu_chunk);
    sharp_allocator_purge_list(allocator, 
                               allocator->allocated_chunk_list, 
                               free_gpu_chunk);
    allocator->freelist_size = 0;
    free(data);

    change_device(device, NULL);
    return SHARP_OK;
}


/***********************************************************/
int sharp_allocator_gmem_init(sharp_allocator_t *allocator,
                              sharp_dev_type_t dev_type,
                              int device_id,
                              sharp_md_type_t md_type,
                              int md_id)
{
    /* initialize private data */
    allocator->data = malloc(sizeof(sharp_allocator_gmem_data_t));
    if (allocator->data == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    sharp_allocator_gmem_data_t * dat = allocator->data;

    /* decide what GPU this allocator will manage memory on */
    dat->gpu_id = gpu_by_device_id(device_id);
    dat->md_id = md_id;
    dat->dev_id = device_id;
    dat->dev_type = dev_type;
    dat->md_type = md_type;
    
    return SHARP_OK;
}



/***********************************************************/
int sharp_allocator_gmem_chunk_malloc(sharp_allocator_t *allocator,
                                      size_t size,
                                      void **buffer)
{
    int err = SHARP_OK;
    int err2;
    int device;

    err = sharp_allocator_alloc_cached_chunk(allocator, size, buffer, 0);

    if (err == SHARP_OK) {
        return SHARP_OK;
    }

    sharp_chunk_t * ret;
    sharp_allocator_gmem_data_t *data = allocator->data;

    /*else retrieve some new memory and return that*/
    ret = malloc(sizeof(*ret));
    if (ret == NULL) {
        return SHARP_ERR_NO_MEMORY;
    }

    err = change_device(data->gpu_id, &device);
    if (err != SHARP_OK) {
        err = SHARP_CUDA_ERROR;
        goto gmem_chunk_malloc_failed;
    }

    err = sharp_allocator_alloc_chunk(allocator,
                                      ret,
                                      size,
                                      gmem_chunk_malloc,
                                      free_gpu_chunk);
    /* hold off on checking err until after restoring GPU device */
    err2 = change_device(device, NULL);
    if (err2 != SHARP_OK) {
        err = SHARP_CUDA_ERROR;
#ifdef WITH_CUDA
        cudaFree(ret->buffer);
#endif
        goto gmem_chunk_malloc_failed;
    }

    if (err != SHARP_OK) {
        return err;
    }

    ret->size = size;
    *buffer = ret->buffer;
    return SHARP_OK;

gmem_chunk_malloc_failed:
    free(ret);
    PRINT_CUDA_ERROR_DETAILED(change_device(device, NULL),
                      "Error restoring the original device after an error");
    return err;
}

/***********************************************************/
int sharp_allocator_gmem_chunk_memalign(sharp_allocator_t *allocator,
                              size_t size,
                              void **buffer,
                              int alignment)
{
    int err;
    /* cudaMalloc will always return memory aligned to at least a 256 byte
     * boundary */
    if (alignment <= 256) {
        return sharp_allocator_gmem_chunk_malloc(allocator, size, buffer);
    }

    if (size < (size_t) alignment) {
        return SHARP_ERR_UNSUPPORTED;
    }
    /* allocate a chunk that is the size of the requested buffer and check
     * alignment*/
    err = sharp_allocator_gmem_chunk_malloc(allocator, size, buffer);
    if (err == SHARP_OK && 
        sharp_allocator_find_pointer_alignment(buffer) >= alignment) {
        return SHARP_OK;
    }
    err = sharp_allocator_gmem_chunk_release(allocator, buffer);
    if (err != SHARP_OK) {
        *buffer = NULL;
        return err;
    }

    /* the above allocated buffer was not well aligned; allocate a new one
     * large enough to ensure that we can return a properly alignment pointer*/
    err = sharp_allocator_gmem_chunk_malloc(allocator, 
                                            size + alignment, 
                                            buffer);
    if (err != SHARP_OK) {
        *buffer = NULL;
        return err;
    }

    /*mask the bits so that we have a properly alignment pointer*/
    uintptr_t raw = (uintptr_t) *buffer;
    raw += alignment;
    raw = raw & (~(alignment - 1));

    *buffer = (void*) raw;

    return SHARP_OK;
}

/***********************************************************/
int sharp_allocator_gmem_chunk_release(sharp_allocator_t *allocator, 
                                       void **_buffer)
{
    return sharp_allocator_free_chunk(allocator, _buffer, free_gpu_chunk);
}
