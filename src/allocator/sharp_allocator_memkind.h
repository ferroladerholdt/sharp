/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#ifndef SHARP_MEMKIND_ALLOCATOR_H_
#define SHARP_MEMKIND_ALLOCATOR_H_

#include "sharp_allocator.h"

#include <memkind.h>
#include <memkind/internal/memkind_arena.h>
#include <memkind/internal/memkind_default.h>
#include <memkind/internal/memkind_hbw.h>

typedef enum  {
    SHARP_MEMKIND_HUGETLB,
    SHARP_MEMKIND_MALLOC
} sharp_allocator_memkind_alloc_type_t;

typedef struct sharp_allocator_memkind_data {
    memkind_t        kind;
    /* Cache locality and MD device info */
    sharp_dev_type_t dev_type;
    sharp_md_type_t  md_type;
    int              dev_id;
    int              md_id;
    int              page_size;
} sharp_allocator_memkind_data_t;


/** 
 * @ingroup SHARP_INTERNAL
 * @brief Initialize the allocator.
 * 
 * @param [in,out] allocator     the allocator that is being initialized
 *
 * @param [in] md_type      yet to be documented
 *
 * @param [in] dev_type     yet to be documented
 *
 * @param [in] md_id        yet to be documented
 *
 * @param[in]  device_id    yet to be documented
 * 
 * @returns When successful SHARP_OK is returned.
 * If there is a problem allocating required data structures SHARP_ERR_NO_MEMORY
 * is returned
 */
int sharp_allocator_memkind_init(sharp_allocator_t *allocator,
                                 sharp_dev_type_t dev_type,
                                 int device_id,
                                 sharp_md_type_t md_type, 
                                 int md_id);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate memory on dram or HBW memory
 *
 * This function will attempt to mbind the memory to the allocator's numa node
 *
 * @param [in,out]  buffer  Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned
 */
int sharp_allocator_memkind_chunk_malloc(sharp_allocator_t *allocator, 
                                         size_t size,
                                         void **buffer);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Allocate memory with an alignment restriction.
 *
 * This function will attempt to mbind the memory to the allocator's numa node
 *
 * @param [in,out]  buffer  Where the memory allocated will be returned. This
 * must not be null
 *
 * @param [in] size         The number of bytes that will be allocated. 
 *
 * @param [in] The byte alignment required. The value is in bytes.
 *
 * @returns When successful SHARP_OK is returned.
 * If the allocation is not successful then SHARP_ERR_NO_MEMORY is returned
 */
int sharp_allocator_memkind_chunk_memalign(sharp_allocator_t *allocator,
                                           size_t size,
                                           void **buffer,
                                           int alignment);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief Release memory back to the allocator
 *
 * @param [in]  allocator   The allocator that is releasing the memory
 *
 * @param [in,out]  buffer   Where the memory allocated will be returned from.
 * This must not be null. After this function is complete *buffer==NULL
 *
 * @returns When successful SHARP_OK is returned.
 * If the deallocation is not successful then SHARP_ERR_INVALID_FREE_ATTEMPT   
 * is returned.
 */
int sharp_allocator_memkind_chunk_release(sharp_allocator_t *allocator,
                                          void **_buffer);

/** 
 * @ingroup SHARP_INTERNAL
 * @brief 
 *
 * @param [in]  allocator   The allocator that is being finalized
 *
 * @returns SHARP_OK and attempts to free all memory allocated by this
 * allocator. This includes freeing memory not explicitly freed by the user of
 * the library.
 */
int sharp_allocator_memkind_finalize(sharp_allocator_t * allocator);


/**
 * @ingroup SHARP_INTERNAL
 * @brief sets nodemask to the correct nodemask for this numa and returns 0
 * */
int sharp_allocator_memkind_get_mbind_nodemask(struct memkind *kind,
                                               unsigned long *nodemask,
                                               unsigned long maxnode);

/**
 * @ingroup SHARP_INTERNAL
 * @brief sets mode to MPOL_BIND and returns 0
 * */
int sharp_allocator_memkind_internal_get_mbind_mode(struct memkind *kind,
                                                    int *mode);

static const struct memkind_ops SHARP_DDRAM_OPS = {
    .create = memkind_arena_create,
    .destroy = memkind_arena_destroy,
    .malloc = memkind_default_malloc,
    .calloc = memkind_default_calloc,
    .posix_memalign = memkind_default_posix_memalign,
    .realloc = memkind_default_realloc,
    .free = memkind_default_free,
    .check_available = memkind_hbw_check_available,
    .mbind = memkind_default_mbind,
    .get_mmap_flags = memkind_default_get_mmap_flags,
    .get_mbind_mode = sharp_allocator_memkind_internal_get_mbind_mode,
    .get_mbind_nodemask = sharp_allocator_memkind_get_mbind_nodemask,
    .get_arena = memkind_thread_get_arena,
    .get_size = memkind_default_get_size
    };

static const struct memkind_ops SHARP_HBW_OPS = {
    .create = memkind_arena_create,
    .destroy = memkind_arena_destroy,
    .malloc = memkind_default_malloc,
    .calloc = memkind_arena_calloc,
    .posix_memalign = memkind_arena_posix_memalign,
    .realloc = memkind_arena_realloc,
    .free = memkind_default_free,
    .mbind = memkind_default_mbind,
    .get_mmap_flags = memkind_default_get_mmap_flags,
    .get_mbind_mode = memkind_default_get_mbind_mode,
    .get_mbind_nodemask = sharp_allocator_memkind_get_mbind_nodemask,
    .get_arena = memkind_bijective_get_arena,
    .init_once = memkind_hbw_init_once,
    .get_size = memkind_default_get_size
};



#endif
