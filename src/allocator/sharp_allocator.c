/**
  * Copyright (C) UT-Battelle, LLC. 2016. ALL RIGHTS RESERVED. 
  *
  */
#include "sharp_allocator.h"

#include <sharp_mdomains/api/sharp_mdomains_md.h>
#include <ucs/datastruct/list.h>
#include <ucs/debug/memtrack.h>
#include <utils/sharp/sharp_errors.h>

#include "sharp_allocator_gmem.h"
#include "sharp_allocator_pmem.h"
#include "sharp_allocator_memkind.h" 
#include "sharp_allocator_malloc.h"

extern sharp_node_info_t sharp_node;

/* get the md id */
static inline int get_md_id(sharp_dev_t *device)
{
    return device->md_id;
}

/* get the device id */
static inline int get_dev_id(sharp_dev_t *device)
{
    return device->device_id;
}

/* get the device type */
static inline sharp_dev_type_t get_dev_type(sharp_dev_t *device)
{ 
    return device->type;
}

/* get the memory domain type */
static inline sharp_md_type_t get_md_type(sharp_dev_t *device)
{
    sharp_md_list_item_t * mdomain;

    ucs_list_for_each(mdomain, sharp_node.md_l, next_p) {
        if(sharp_md_get_md_id(mdomain->mem_attrib) == device->md_id) {
            return sharp_md_get_type(mdomain->mem_attrib);
        }
    }

    /* WARNING: execution should never get here */
    return SHARP_ERR_LAST; /* FIXME: a better error value */
}

int sharp_allocator_finalize(sharp_allocator_t *a) 
{
    if (a->locality->nodemask) {
        numa_free_nodemask(a->locality->nodemask);
    }

    return SHARP_OK;
}

int sharp_allocator_init(sharp_allocator_t **allocator,
                         sharp_allocator_type_t allocator_type,
                         char *name, sharp_dev_t device)                            
{
    sharp_allocator_ops_t * ops;
    sharp_allocator_t *allocator_l = *allocator = 
        (sharp_allocator_t *)ucs_malloc(sizeof(sharp_allocator_t));

    if (NULL == allocator_l) {
       return SHARP_ERR_NO_MEMORY; 
    }

    allocator_l->locality = 
        (sharp_locality_t*)ucs_malloc(sizeof(sharp_locality_t));
    if (NULL == allocator_l->locality) {
        return SHARP_ERR_NO_MEMORY;
    }

    allocator_l->locality->device = get_dev_type(&device);

    /* set up the numa bitmask  */
    allocator_l->locality->max_numa_nodes = numa_max_possible_node();
    allocator_l->locality->nodemask = numa_allocate_nodemask();

    copy_bitmask_to_bitmask(numa_no_nodes_ptr, 
                            allocator_l->locality->nodemask);
    numa_bitmask_setbit(allocator_l->locality->nodemask, 
                        get_dev_id(&device));


    /* separate chunk/free list for each allocator */
    allocator_l->allocated_chunk_list = 
        (ucs_list_link_t *)ucs_malloc(sizeof(ucs_list_link_t));
    allocator_l->chunk_freelist = 
        (ucs_list_link_t *)ucs_malloc(sizeof(ucs_list_link_t));

    /* init empty lists */    
    ucs_list_head_init(allocator_l->allocated_chunk_list);
    ucs_list_head_init(allocator_l->chunk_freelist);
    allocator_l->freelist_size = 0;

    allocator_l->ops = ops = 
        (sharp_allocator_ops_t *)ucs_malloc(sizeof(sharp_allocator_ops_t));
    if (NULL == ops) {
        return SHARP_ERR_NO_MEMORY;
    }

    switch (allocator_type) {
        case SHARP_ALLOCATOR_MEMKIND:
            ops->init_allocator = sharp_allocator_memkind_init;
            ops->finalize_allocator = sharp_allocator_memkind_finalize;
            ops->chunk_alloc = sharp_allocator_memkind_chunk_malloc;
            ops->chunk_release = sharp_allocator_memkind_chunk_release;
            break;
        case SHARP_ALLOCATOR_GMEM:
            ops->init_allocator = sharp_allocator_gmem_init;
            ops->finalize_allocator = sharp_allocator_gmem_finalize;
            ops->chunk_alloc = sharp_allocator_gmem_chunk_malloc;
            ops->chunk_release = sharp_allocator_gmem_chunk_release;
            ops->chunk_memalign = sharp_allocator_gmem_chunk_memalign;
            break;
        case SHARP_ALLOCATOR_PMEM:
            ops->init_allocator = sharp_allocator_pmem_init;
            ops->finalize_allocator = sharp_allocator_pmem_finalize;
            ops->chunk_alloc = sharp_allocator_pmem_chunk_malloc;
            ops->chunk_release = sharp_allocator_pmem_chunk_release;
            ops->chunk_memalign = sharp_allocator_pmem_chunk_memalign;
            break;
        default:
            ops->init_allocator = sharp_allocator_malloc_init;
            ops->finalize_allocator = sharp_allocator_malloc_finalize;
            ops->chunk_alloc = sharp_allocator_malloc_chunk_malloc;
            ops->chunk_release = sharp_allocator_malloc_chunk_release;
            ops->chunk_memalign = sharp_allocator_malloc_chunk_memalign;
            break;
    }

    return ops->init_allocator(allocator_l, 
                              get_dev_type(&device), 
                              get_dev_id(&device),
                              get_md_type(&device), 
                              get_md_id(&device));
}

int sharp_allocator_init_all(ucs_list_link_t * list)
{
    int err;

    sharp_dev_item_t *current;
    ucs_list_for_each(current, sharp_node.dev_l, next_p) {
        sharp_allocator_list_item_t * adding =  NULL;
        adding = (sharp_allocator_list_item_t*)ucs_malloc(sizeof(*adding));

        if (adding == 0) {
            sharp_allocator_finalize_allocator_list(list);
            return SHARP_ERR_NO_MEMORY;
        }
        adding->allocator = NULL;

        switch(current->device.type) {
            case SHARP_DEV_CUDA:
                err = sharp_allocator_init(&adding->allocator,
                                           SHARP_ALLOCATOR_GMEM,
                                           NULL,
                                           current->device);
                adding->type = SHARP_ALLOCATOR_GMEM;
                break;
            case SHARP_DEV_CPU:
            case SHARP_DEV_PHI:
                err = sharp_allocator_init(&adding->allocator,
                                           SHARP_ALLOCATOR_MEMKIND,
                                           NULL,
                                           current->device);
                adding->type = SHARP_ALLOCATOR_MEMKIND;
                break;
            case SHARP_DEV_PMEM:
                err = sharp_allocator_init(&adding->allocator,
                                           SHARP_ALLOCATOR_PMEM,
                                           NULL,
                                           current->device);
                err =
                    sharp_allocator_pmem_init2(adding->allocator,
                                               "/tmp/bob", /*TODO:replace*/
                                               1024ul*1024*1024*4); /* 4GB */
                adding->type = SHARP_ALLOCATOR_MEMKIND;
                break;
            default:
                err = sharp_allocator_init(&adding->allocator,
                                           SHARP_ALLOCATOR_MALLOC,
                                           NULL,
                                           current->device);
                adding->type = SHARP_ALLOCATOR_MALLOC;
                break;
        }
        if (err != SHARP_OK) {
            ucs_free(adding);
            sharp_allocator_finalize_allocator_list(list);
            return err;
        }
        ucs_list_add_tail(list, &adding->link);
    }
    return SHARP_OK;
}

int sharp_allocator_finalize_allocator_list(ucs_list_link_t *list)
{
    int err;
    sharp_allocator_list_item_t *current = NULL, *temp = NULL;

    ucs_list_for_each_safe(current, temp, list, link) {
        err = sharp_allocator_finalize(current->allocator);
        ucs_free(current);
        if (err) {
            return err;
        }
    }
    return SHARP_OK;
}
