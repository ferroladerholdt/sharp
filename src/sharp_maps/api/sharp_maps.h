/**
 * Copyright (C) 2016 UT-Battelle, LLC. ALL RIGHTS RESERVED.
 */

#ifndef __SHARP_MAPS_H
#define __SHARP_MAPS_H

#include <utils/sharp/sharp_errors.h>

/**
 * @ingroup SHARP_API
 * @brief The callback to used if making use of a uniform mapping for a SharP
 * object
 */
#define SHARP_MAP_NULL  NULL

/** 
 * @ingroup SHARP_API
 * @brief Callback used for custom mappings. The custom mapping operates by 
 * taking in an index and returning the PE number for which the index is 
 * hosted, a low index for that PE, and a high index for that PE. The user 
 * needs to provide this functionality.
 *
 * @param [in] index                The index of the sharp object
 * @param [out] *low                The lowest index of the object on the PE
 * @param [out] *high               The highest index of the object on the PE
 * @param [out] *index_offset       The offset in indices from lowest index on
 *                                  the PE
 * @returns The PE number of the PE hosting the questioned index 
 */ 
typedef int (*sharp_map_callback_t)(size_t index, size_t * low, size_t * high, size_t *index_offset); 

/**
 * @ingroup SHARP_API
 * @brief The types of maps: uniform and custom. A uniform mapping spans a 
 * data structure across all of the PEs within an allocation group. A custom
 * mapping is defined by the user and can allow for a custom spanning of the
 * data structure across an allocation group
 */ 
typedef enum {
    SHARP_UNIFORM_MAP, /**< The uniform mapping */
    SHARP_CUSTOM_MAP   /**< The custom mapping */
} sharp_map_type_t; 

/**
 * @ingroup SHARP_INTERNAL
 * @brief Internal mapping information for an object. This information is per 
 * PE and kept in a hash tieing object ids to PE allocations.
 */
typedef struct map_item { /* key = PE number */
    size_t low;            /**< The low index of the object for the local PE */
    size_t high;           /**< The high index of the object for the local PE */
} sharp_map_item_t;

/**
 * @ingroup SHARP_INTERNAL
 * @brief The primary internal mapping for an object that is allocated on an 
 * allocation group. This mapping is keyed based on object id, which has a 
 * value of another hash of map_items
 */
typedef struct map_table { /* key = obj id */
    void * map_row;     /**< A map to a hash of map_items */
} sharp_map_table_t;

#endif 
